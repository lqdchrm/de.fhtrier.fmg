package de.fhtrier.fmg.logic.impl.nodes;

import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class OrTest {

	private Or		or;
	private Literal	_true;
	private Literal	_false;
	private Literal	_true2;
	private Literal	_false2;

	@Before
	public void setUp() {
		or = new Or();
		_true = new Literal("True", true);
		_false = new Literal("False", false);
		_true2 = new Literal("True2", true);
		_false2 = new Literal("False2", false);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testEvalEmpty() {
		assertThat(or.eval()).isFalse();
	}

	@Test
	public void testEvalFalseFalse() {
		or.add(_false);
		or.add(_false2);

		assertThat(or.eval()).isFalse();
	}

	@Test
	public void testEvalFalseFalseTrue() {
		or.add(_false);
		or.add(_false2);
		or.add(_true);

		assertThat(or.eval()).isTrue();
	}

	@Test
	public void testEvalFalseTrue() {
		or.add(_false);
		or.add(_true);

		assertThat(or.eval()).isTrue();
	}

	@Test
	public void testEvalSingleFalse() {
		or.add(_false);

		assertThat(or.eval()).isFalse();
	}

	@Test
	public void testEvalSingleTrue() {
		or.add(_true);

		assertThat(or.eval()).isTrue();
	}

	@Test
	public void testEvalTrueFalse() {
		or.add(_true);
		or.add(_false);

		assertThat(or.eval()).isTrue();
	}

	@Test
	public void testEvalTrueTrue() {
		or.add(_true);
		or.add(_true2);

		assertThat(or.eval()).isTrue();
	}
}
