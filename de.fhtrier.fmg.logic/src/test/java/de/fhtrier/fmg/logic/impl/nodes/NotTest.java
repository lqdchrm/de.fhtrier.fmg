package de.fhtrier.fmg.logic.impl.nodes;

import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class NotTest {

	private Not		_not;
	private Literal	_true;
	private Literal	_false;

	@Before
	public void setUp() {
		_true = new Literal("True", true);
		_false = new Literal("False", false);
	}

	@Test
	public void testEvalFalse() {
		_not = new Not(_false);

		assertThat(_not.eval()).isTrue();
	}

	@Test
	public void testEvalTrue() {
		_not = new Not(_true);

		assertThat(_not.eval()).isFalse();
	}

	@Test
	public void testNotChild() {
		_not = new Not(_true);

		assertThat(_not).isNotNull();
		assertThat(_not.child)
				.isNotNull()
				.isEqualTo(_true);
	}

	@Test(expected = NullPointerException.class)
	public void testNotNull() {
		new Not(null);
	}

	@Test
	public void testToString() {
		_not = new Not(_true);

		assertThat(_not.toString())
				.isEqualTo("not(" + _true.toString() + ")");
	}
}
