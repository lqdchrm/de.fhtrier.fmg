package de.fhtrier.fmg.logic.impl.nodes;

import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class AndTest {

	private And		and;
	private Literal	_true;
	private Literal	_false;
	private Literal	_true2;
	private Literal	_false2;

	@Before
	public void setUp() {
		and = new And();
		_true = new Literal("True", true);
		_false = new Literal("False", false);
		_true2 = new Literal("True2", true);
		_false2 = new Literal("False2", false);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testEvalEmpty() {
		and.eval();
	}

	@Test
	public void testEvalFalseFalse() {
		and.add(_false);
		and.add(_false2);

		assertThat(and.eval()).isFalse();
	}

	@Test
	public void testEvalFalseTrue() {
		and.add(_false);
		and.add(_true);

		assertThat(and.eval()).isFalse();
	}

	@Test
	public void testEvalFalseTrueTrue() {
		and.add(_false);
		and.add(_true);
		and.add(_true2);

		assertThat(and.eval()).isFalse();
	}

	@Test
	public void testEvalSingleFalse() {
		and.add(_false);

		assertThat(and.eval()).isFalse();
	}

	@Test
	public void testEvalSingleTrue() {
		and.add(_true);

		assertThat(and.eval()).isTrue();
	}

	@Test
	public void testEvalTrueFalse() {
		and.add(_true);
		and.add(_false);

		assertThat(and.eval()).isFalse();
	}

	@Test
	public void testEvalTrueTrue() {
		and.add(_true);
		and.add(_true2);

		assertThat(and.eval()).isTrue();
	}
}
