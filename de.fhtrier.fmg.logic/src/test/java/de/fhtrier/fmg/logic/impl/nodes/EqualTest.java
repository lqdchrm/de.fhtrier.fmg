package de.fhtrier.fmg.logic.impl.nodes;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class EqualTest {

	private Equal	equal;
	private Literal	_true;
	private Literal	_false;
	private Literal	_true2;
	private Literal	_false2;

	@Before
	public void setUp() {
		equal = new Equal();
		_true = new Literal("True", true);
		_false = new Literal("False", false);
		_true2 = new Literal("True2", true);
		_false2 = new Literal("False2", false);
	}

	@Test
	public void testEvalEmpty() {
		try {
			equal.eval();
			Assert.fail();
		} catch (UnsupportedOperationException expected)
		{

		}
	}

	@Test
	public void testEvalFalseFalse() {
		equal.add(_false);
		equal.add(_false2);

		assertThat(equal.eval()).isTrue();
	}

	@Test
	public void testEvalFalseFalseTrue() {
		equal.add(_false);
		equal.add(_false2);
		equal.add(_true);

		assertThat(equal.eval()).isFalse();
	}

	@Test
	public void testEvalFalseTrue() {
		equal.add(_false);
		equal.add(_true);

		assertThat(equal.eval()).isFalse();
	}

	@Test
	public void testEvalFalseTrueTrue() {
		equal.add(_false);
		equal.add(_true);
		equal.add(_true2);

		assertThat(equal.eval()).isFalse();
	}

	@Test
	public void testEvalSingleFalse() {
		try {
			equal.add(_false);
			equal.eval();
			Assert.fail();
		} catch (UnsupportedOperationException expected)
		{

		}
	}

	@Test
	public void testEvalSingleTrue() {
		try
		{
			equal.add(_true);
			equal.eval();
			Assert.fail();
		} catch (UnsupportedOperationException expected)
		{

		}
	}

	@Test
	public void testEvalTrueFalse() {
		equal.add(_true);
		equal.add(_false);

		assertThat(equal.eval()).isFalse();
	}

	@Test
	public void testEvalTrueTrue() {
		equal.add(_true);
		equal.add(_true2);

		assertThat(equal.eval()).isTrue();
	}
}
