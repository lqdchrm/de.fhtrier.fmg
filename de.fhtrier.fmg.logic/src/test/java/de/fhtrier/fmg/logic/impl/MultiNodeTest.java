package de.fhtrier.fmg.logic.impl;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import de.fhtrier.fmg.logic.INode;
import de.fhtrier.fmg.logic.impl.nodes.Literal;

import static org.fest.assertions.Assertions.assertThat;

public class MultiNodeTest {

	private MultiNode	node;
	private INode		childA;
	private INode		childB;

	@Before
	public void setUp() {
		node = new MultiNode();
		childA = new Literal("childA");
		childB = new Literal("childB");
	}

	@Test
	public void testAddExistingChild() {
		node.add(childA);
		node.add(childA);

		assertThat(node.children)
				.hasSize(1)
				.contains(childA);
	}

	@Test
	public void testAddNonExistingChild() {
		node.add(childA);

		assertThat(node.children)
				.hasSize(1)
				.contains(childA);
	}

	@Test(expected = NullPointerException.class)
	public void testAddNull() {
		node.add(null);
	}

	@Test
	public void testContainsExistingChild() {
		node.add(childA);
		node.add(childB);

		assertThat(node.contains(childA));
		assertThat(node.contains(childB));
	}

	@Test
	public void testContainsNull() {
		node.add(childA);
		node.add(childB);

		assertThat(node.contains(null)).isFalse();

	}

	@Test(expected = UnsupportedOperationException.class)
	public void testEval() {
		node.eval();
	}

	@Test
	public void testMultiNodeArray() {
		MultiNode _node = new MultiNode(new INode[] { childA, childB });

		assertThat(_node.children)
				.hasSize(2)
				.contains(childA)
				.contains(childB);
	}

	@Test(expected = NullPointerException.class)
	public void testMultiNodeNull() {
		new MultiNode(null);
	}

	@Test
	public void testRemoveExistingChild() {
		node.add(childA);
		node.remove(childA);

		assertThat(node.children)
				.hasSize(0);
	}

	@Test
	public void testRemoveNonExistingChild() {
		node.add(childA);
		node.remove(childB);

		assertThat(node.children)
				.hasSize(1)
				.contains(childA);
	}

	@Test
	public void testRemoveNull() {
		node.add(childA);
		node.add(childB);
		node.remove(null);

		assertThat(node.children)
				.hasSize(2)
				.contains(childA)
				.contains(childB);
	}

	@Test
	public void testSize() {
		node.add(childA);

		assertThat(node.size())
				.isEqualTo(1);

		node.add(childB);

		assertThat(node.size())
				.isEqualTo(2);
	}

	@Test
	public void testSizeOfEmpty() {
		assertThat(node.size())
				.isEqualTo(0);
	}

	@Test
	public void testToStringEmpty() {
		assertThat(node.toString())
				.isEqualTo("()");
	}

	@Test
	public void testToStringMulti() {
		node.add(childA);
		node.add(childB);

		ArrayList<String> results = new ArrayList<>();
		results.add("(" + childA.toString() + ", " + childB.toString() + ")");
		results.add("(" + childB.toString() + ", " + childA.toString() + ")");

		assertThat(results)
				.contains(node.toString());
	}

	@Test
	public void testToStringSingle() {
		node.add(childA);
		assertThat(node.toString())
				.isEqualTo("(" + childA.toString() + ")");
	}
}
