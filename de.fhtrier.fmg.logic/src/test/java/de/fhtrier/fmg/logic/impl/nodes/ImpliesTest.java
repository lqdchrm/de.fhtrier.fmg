package de.fhtrier.fmg.logic.impl.nodes;

import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class ImpliesTest {

	private Implies	_implies;
	private Literal	_true;
	private Literal	_false;

	@Before
	public void setUp() {
		_true = new Literal("True", true);
		_false = new Literal("False", false);
	}

	@Test
	public void testEvalFalseFalse() {
		_implies = new Implies(_false, _false);

		assertThat(_implies.eval()).isTrue();
	}

	@Test
	public void testEvalFalseTrue() {
		_implies = new Implies(_false, _true);

		assertThat(_implies.eval()).isTrue();
	}

	@Test
	public void testEvalTrueFalse() {
		_implies = new Implies(_true, _false);

		assertThat(_implies.eval()).isFalse();
	}

	@Test
	public void testEvalTrueTrue() {
		_implies = new Implies(_true, _true);

		assertThat(_implies.eval()).isTrue();
	}

	@Test
	public void testImpliesNodeNode() {
		Implies node = new Implies(_true, _false);

		assertThat(node).isNotNull();
	}

	@Test(expected = NullPointerException.class)
	public void testImpliesNodeNull() {
		new Implies(_true, null);
	}

	@Test(expected = NullPointerException.class)
	public void testImpliesNullNode() {
		new Implies(null, _true);
	}

	@Test(expected = NullPointerException.class)
	public void testImpliesNullNull() {
		new Implies(null, null);
	}
}
