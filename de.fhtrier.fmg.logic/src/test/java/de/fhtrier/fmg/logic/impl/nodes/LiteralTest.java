package de.fhtrier.fmg.logic.impl.nodes;

import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class LiteralTest {

	private Literal	_unspecified;
	private Literal	_true;
	private Literal	_false;

	@Before
	public void setUp() {
		_unspecified = new Literal("Unspecified");
		_true = new Literal("True", true);
		_false = new Literal("False", false);
	}

	@Test
	public void testEval() {
		assertThat(_true.eval()).isTrue();
		assertThat(_false.eval()).isFalse();
	}

	@Test
	public void testLiteralName() {
		assertThat(_unspecified).isNotNull();
		assertThat(_unspecified.name).isEqualTo("Unspecified");
		assertThat(_unspecified.value).isFalse();
	}

	@Test
	public void testLiteralNameFalse() {
		assertThat(_false).isNotNull();
		assertThat(_false.name).isEqualTo("False");
		assertThat(_false.value).isFalse();
	}

	@Test
	public void testLiteralNameTrue() {
		assertThat(_true).isNotNull();
		assertThat(_true.name).isEqualTo("True");
		assertThat(_true.value).isTrue();
	}

	@Test(expected = NullPointerException.class)
	public void testLiteralNull() {
		new Literal(null);
	}

	@Test(expected = NullPointerException.class)
	public void testLiteralNullTrue() {
		new Literal(null, true);
	}

	@Test
	public void testName() {
		_unspecified.setName("Test");

		assertThat(_unspecified.getName())
				.isEqualTo("Test");
	}

	@Test(expected = NullPointerException.class)
	public void testSetNameNull() {
		_unspecified.setName(null);
	}

	@Test
	public void testToString() {
		assertThat(_unspecified.toString())
				.isEqualTo("Unspecified");
	}

	@Test
	public void testValueFalse() {
		_unspecified.setValue(false);

		assertThat(_unspecified.getValue()).isFalse();
	}

	@Test
	public void testValueTrue() {
		_unspecified.setValue(true);

		assertThat(_unspecified.getValue()).isTrue();
	}
}
