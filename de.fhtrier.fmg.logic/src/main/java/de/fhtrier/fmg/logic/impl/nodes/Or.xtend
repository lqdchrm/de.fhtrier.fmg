package de.fhtrier.fmg.logic.impl.nodes

import de.fhtrier.fmg.logic.INode
import de.fhtrier.fmg.logic.impl.MultiNode

class Or extends MultiNode {
	
	new() {
		super()
	}	

	new(INode[] children) {
		super(children)
	}
	
	override eval() {
		if (size == 0)
			return super.eval();
			
		children.fold(false, [result, node | result || node.eval])
	}
	
	override toString() {
		"or" + super.toString()
	}
}