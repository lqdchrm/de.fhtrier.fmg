package de.fhtrier.fmg.logic.impl.nodes

import de.fhtrier.fmg.logic.impl.MultiNode
import de.fhtrier.fmg.logic.INode

class And extends MultiNode {
	
	new() {
		super()
	}	
	
	new(INode[] children) {
		super(children)
	}
	
	override eval() {
		if (size == 0)
			super.eval();
			
		children.fold(true, [result, node | result && node.eval])
	}
	
	override toString() {
		"and" + super.toString()
	}
}