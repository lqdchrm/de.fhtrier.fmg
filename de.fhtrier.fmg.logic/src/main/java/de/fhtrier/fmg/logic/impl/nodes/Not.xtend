package de.fhtrier.fmg.logic.impl.nodes

import de.fhtrier.fmg.logic.INode

public class Not implements INode {
	
	protected INode child
	
	new(INode child) {
		if (child == null)
			throw new NullPointerException()
		
		this.child = child
	}

	override eval() {
		!child.eval
	}
	
	override toString() {
		"not(" + child.toString + ")"
	}
}