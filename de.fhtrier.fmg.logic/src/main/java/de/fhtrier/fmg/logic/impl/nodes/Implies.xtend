package de.fhtrier.fmg.logic.impl.nodes

import de.fhtrier.fmg.logic.INode

class Implies implements INode {

	protected INode left
	protected INode right
	
	new(INode left, INode right) {
		
		if (left == null || right == null)
			throw new NullPointerException()
			
		this.left = left
		this.right = right
	}
	
	override eval() {
		(!left.eval || right.eval)
	}
	
	override toString() { 
		"implies(" + left.toString + ", " + right.toString + ")"
	}
}