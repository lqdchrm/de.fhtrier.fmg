package de.fhtrier.fmg.logic.impl.nodes

import de.fhtrier.fmg.logic.impl.MultiNode
import de.fhtrier.fmg.logic.INode

class Equal extends MultiNode {
	
	new() {
		super()
	}	
	
	new(INode[] children) {
		super(children)
	}
	
	override eval() {
		if (size < 2)
			super.eval()
		
		val headValue = children.head.eval
			
		return children.fold(true, [result, node | result && (node.eval == headValue) ] )
	}

	override toString() {
		"equiv" + super.toString()
	}
}