package de.fhtrier.fmg.logic;

public interface INode {
	public abstract boolean eval();
}
