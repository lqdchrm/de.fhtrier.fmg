package de.fhtrier.fmg.logic.impl.nodes

import de.fhtrier.fmg.logic.INode

class Literal implements INode {
	
	protected boolean value
	protected String name
		
	new(String name) {
		if (name == null)
			throw new NullPointerException()
		
		this.name = name
		this.value = false
	}
	
	new(String name, boolean value) {
		if (name == null)
			throw new NullPointerException()
			
		this.name = name
		this.value = value
	}
	
	def setName(String name) {
		if (name == null)
			throw new NullPointerException()
			
		this.name = name
	}
	
	def getName() {
		name
	}
	
	def setValue(boolean value) {
		this.value = value
	}
	
	def getValue() {
		value
	}
	
	override eval() {
		value
	}
	
	override toString() {
		name
	}
}