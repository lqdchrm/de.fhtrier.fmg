package de.fhtrier.fmg.logic.impl

import java.util.HashSet
import de.fhtrier.fmg.logic.INode

class MultiNode implements INode {
	
	protected HashSet<INode> children
		
	new() {
		this.children = newHashSet
	}
	
	new(INode[] children) {
		this.children = newHashSet(children)
	}
			
	def add(INode child) {
		if (child == null)
			throw new NullPointerException();
		children.add(child)
	}
	
	def remove(INode child) {
		children.remove(child)
	}
	
	def contains(INode child) {
		children.contains(child)
	}
	
	def size() {
		children.size
	}
		
	override toString() {
		'(' + children.map[toString].join(", ") + ')'
	}	

	override eval() {
		throw new UnsupportedOperationException()
	}
}