/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.runtime;

import javax.swing.ListModel;
import javax.swing.event.ListSelectionListener;

public interface IFeatureModelMgr extends ListModel<IManagedFeatureModel>,
		ListSelectionListener {

	void addElement(IManagedFeatureModel m);

	boolean removeElement(Object m);

	IManagedFeatureModel getSelected();

	IManagedFeatureModel getById(int id);

	void addFeatureModelMgrListener(IFeatureModelMgrListener l);

	void removeFeatureModelMgrListener(IFeatureModelMgrListener l);
}
