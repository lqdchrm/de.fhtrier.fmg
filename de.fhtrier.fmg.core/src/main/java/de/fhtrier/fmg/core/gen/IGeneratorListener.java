/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.gen;

import java.util.List;

import de.fhtrier.fmg.core.fm.IFeature;

public interface IGeneratorListener {
	void featureAdded(IGenerator sender, IFeature feature);

	void updateAlternative(IFeature parent, List<IFeature> children);

	void updateMandatory(IFeature parent, IFeature child);

	void updateOptional(IFeature parent, IFeature child);

	void updateOr(IFeature parent, List<IFeature> children);

	void updateRoot(IFeature root);
}
