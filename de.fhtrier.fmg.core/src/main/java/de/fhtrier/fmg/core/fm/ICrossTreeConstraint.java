/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.fm;

public interface ICrossTreeConstraint {

	int getId();

	IFeature getDestination();

	IFeature getOrigin();

	CrossTreeConstraintType getType();
}
