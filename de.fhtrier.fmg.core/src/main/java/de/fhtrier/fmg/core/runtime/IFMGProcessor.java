/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.runtime;

import de.fhtrier.fmg.core.gen.IGenerator;
import de.fhtrier.fmg.core.gen.IParameterSet;
import de.fhtrier.fmg.core.stats.IStatistics;

public interface IFMGProcessor {

	IStatistics getStats();

	void run(IGenerator generator, IParameterSet params);

	void run();
}
