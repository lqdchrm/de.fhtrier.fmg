/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.runtime;

import java.beans.ExceptionListener;

public interface IExceptionThrower {
	void addListener(ExceptionListener l);

	void exceptionThrown(Exception e);

	void removeListener(ExceptionListener l);
}
