/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.runtime;

public interface IModelList {

	IFeatureModelMgr getModelMgr();

	IManagedFeatureModel getSelectedValue();
}
