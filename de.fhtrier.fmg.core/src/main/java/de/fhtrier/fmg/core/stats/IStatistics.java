/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.stats;

public interface IStatistics {

	int getNumNodes();

	int getDepth();

	int getNumRequires();

	int getNumExcludes();

	long getStartTime();

	long getEndTime();
}
