/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.gen;

import de.fhtrier.fmg.core.fm.IFeatureModel;
import de.fhtrier.fmg.core.stats.IStatistics;

public interface IGenerator {

	String getName();

	void addListener(IGeneratorListener l);

	void removeListener(IGeneratorListener l);

	IParameterSet createParameterSet(String name, int seed);

	IFeatureModel generate(IParameterSet params);

	IStatistics getStats();
}
