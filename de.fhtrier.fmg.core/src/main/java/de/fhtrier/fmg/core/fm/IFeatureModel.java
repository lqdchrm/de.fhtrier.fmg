/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.fm;

public interface IFeatureModel {

	int getId();

	String getName();

	String getFeaturePrefix();

	IFeature getRoot();

	IFeature getFeature(int id);

	Iterable<? extends IFeature> getFeatures();

	Iterable<? extends ICrossTreeConstraint> getCrossTreeConstraints();
}
