/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.runtime;

import com.mxgraph.view.mxGraph;
import de.fhtrier.fmg.core.fm.IFeatureModel;
import de.fhtrier.fmg.core.gen.IGenerator;
import de.fhtrier.fmg.core.gen.IParameterSet;

public interface IManagedFeatureModel {

	int getId();

	IFeatureModel getModel();

	IGenerator getGenerator();

	void setGenerator(String name);

	void setGenerator(IGenerator generator);

	IParameterSet getParameters();

	void setParameters(IParameterSet parameters);

	mxGraph getGraph();

	void regenerate();

	void add(IManagedFeatureModelChangedListener l);

	void remove(IManagedFeatureModelChangedListener l);
}
