/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.runtime;

import de.fhtrier.fmg.utils.factory.ITypeDescriptor;

public interface IFrameworkDescriptor extends ITypeDescriptor<IFramework> {

	IFramework getSingleton();

}
