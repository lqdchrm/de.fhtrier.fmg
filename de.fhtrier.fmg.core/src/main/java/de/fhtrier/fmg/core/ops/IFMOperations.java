/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.ops;

import de.fhtrier.fmg.core.fm.IFeature;
import de.fhtrier.fmg.core.fm.IRelation;
import de.fhtrier.fmg.core.fm.RelationType;

public interface IFMOperations {

	IFeature feature(int id);

	IRelation relation(IFeature source, RelationType type,
			Iterable<? extends IFeature> dests);
}
