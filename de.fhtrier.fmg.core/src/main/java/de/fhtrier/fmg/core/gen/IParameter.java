/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.gen;

public interface IParameter<T> {

	public String getDescription();

	public String getName();

	public Class<T> getType();

	public T getValue();

	public void setValue(T value);
}
