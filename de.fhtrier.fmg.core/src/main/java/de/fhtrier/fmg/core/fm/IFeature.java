/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.fm;

/**
 * represents a single feature
 * 
 * @author Robert Rößger
 */
public interface IFeature {

	/**
	 * @return the identifier
	 */
	public int getId();

	/**
	 * @return the label for this feature
	 */
	public String getName();

	/**
	 * link to parent.
	 * 
	 * please be aware, that relations are directed from root to leaves, i.e. the returned relation's origin points to the parent and it's destination is this
	 * feature
	 * 
	 * @return relation to this feature's parent feature
	 */
	public IRelation getParent();

	/**
	 * @return collection of relations to children
	 */
	public Iterable<? extends IRelation> getChildren();

	/**
	 * @return custom user data
	 */
	public Object getUserData();

	/**
	 * @param data
	 *            custom user data, e.g. links to graphical nodes
	 */
	public void setUserData(Object data);
}
