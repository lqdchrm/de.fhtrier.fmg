/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.fm;

public interface IRelation {

	IFeature getOrigin();

	Iterable<? extends IFeature> getDestinations();

	RelationType getType();

	int getNumDestinations();
}
