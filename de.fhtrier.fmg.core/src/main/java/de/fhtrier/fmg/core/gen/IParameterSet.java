/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.gen;

public interface IParameterSet {

	public void addListener(IParameterSetChangedListener l);

	public String getDescription(String key);

	public float getFloat(String key);

	public int getInt(String key);

	public Iterable<? extends IParameterSetChangedListener> getListeners();

	public long getLong(String key);

	public String getName();

	public Iterable<? extends String> getParams();

	public long getSeed();

	public String getString(String key);

	public String getAsString(String key);

	public String getType(String key);

	public void notifyChanged();

	public void removeListener(IParameterSetChangedListener l);

	public void setName(String name);

	public void setSeed(long seed);

	public void setValue(String key, float value);

	public void setValue(String key, int value);

	public void setValue(String key, long value);

	public void setValue(String key, String value);
}
