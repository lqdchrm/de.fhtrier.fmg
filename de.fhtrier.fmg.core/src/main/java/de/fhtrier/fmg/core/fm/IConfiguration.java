/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.fm;

public interface IConfiguration {

	void addFeature(int id);

	Iterable<? extends IFeature> getFeatures();

	IFeatureModel getModel();

	boolean hasFeature(int id);

	void removeFeature(int id);
}
