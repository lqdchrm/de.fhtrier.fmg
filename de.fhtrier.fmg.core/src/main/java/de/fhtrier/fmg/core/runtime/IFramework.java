/*******************************************************************************
 * // TODO Copyright notice
 ******************************************************************************/
package de.fhtrier.fmg.core.runtime;

import de.fhtrier.fmg.core.gen.IGenerator;
import de.fhtrier.fmg.core.gen.IGeneratorDescriptor;

public interface IFramework {

	IGenerator createGenerator(String name);

	Iterable<? extends String> getGenerators();

	void registerGenerator(IGeneratorDescriptor desc);
}
