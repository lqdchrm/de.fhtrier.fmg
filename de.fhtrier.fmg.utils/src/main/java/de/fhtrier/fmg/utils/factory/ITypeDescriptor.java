package de.fhtrier.fmg.utils.factory;

import java.lang.reflect.InvocationTargetException;

/**
 * Interface, which describes a factory managed type.
 * 
 * @author Robert Rößger
 * @param <ReturnType>
 *            the managed type
 */
public interface ITypeDescriptor<ReturnType> {

	/**
	 * the name, which is used to register the managed type at the factory
	 * 
	 * @return the managed type's name
	 */
	String getName();

	/**
	 * method for creating new instances of the described type
	 * 
	 * @param args
	 *            arguments to the managed type's constructor
	 * @return an instance of the managed type
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	ReturnType createNew(Object... args) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException;
}
