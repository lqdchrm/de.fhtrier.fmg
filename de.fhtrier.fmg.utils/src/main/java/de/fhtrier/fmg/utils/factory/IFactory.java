package de.fhtrier.fmg.utils.factory;

/**
 * a generic factory interface.
 * 
 * This factory manages types which implement a common base type and provides runtime creation of instances of these types. New types can be registered and
 * unregistered via a type descriptor.
 * 
 * The main purpose is to have a factory's functionality with types unknown at design time.
 * 
 * @author Robert Rößger
 * @param <ManagedType>
 *            the base type which is managed by this factory
 * @param <TypeDescriptor>
 *            the type descriptor for the managed type
 */
public interface IFactory<ManagedType, TypeDescriptor extends ITypeDescriptor<ManagedType>> {

	public void addListener(IFactoryListener<ManagedType, TypeDescriptor> listener);

	public void removeListener(IFactoryListener<ManagedType, TypeDescriptor> listener);

	public void registerType(TypeDescriptor typeDescriptor);

	public void unregisterType(String descriptorName);

	public boolean containsType(String descriptorName);

	public TypeDescriptor getDescriptor(String descriptorName);

	public Iterable<String> getRegisteredTypes();

	/**
	 * creates a new instance of the concrete managed type
	 * 
	 * @param descName
	 *            the descriptor name
	 * @param args
	 *            dynamic list of arguments to the managed types constructor
	 * @return an instance of the respective type
	 */
	public ManagedType createInstanceOfType(String descName, Object... args);
}
