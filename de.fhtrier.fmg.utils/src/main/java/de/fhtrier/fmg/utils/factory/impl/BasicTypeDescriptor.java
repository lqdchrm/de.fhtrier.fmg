package de.fhtrier.fmg.utils.factory.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import de.fhtrier.fmg.utils.factory.ITypeDescriptor;

/**
 * basic generic implementation of a type descriptor.
 * 
 * 
 * @author Robert Rößger
 * @param <ReturnType>
 *            the managed type
 */
public class BasicTypeDescriptor<ReturnType> implements
		ITypeDescriptor<ReturnType> {

	private String						name;
	private Class<? extends ReturnType>	returnType;
	private Class<?>[]					paramTypes;

	/**
	 * @param name
	 *            the name this managed type is referred to
	 * @param returnType
	 *            the managed type, which can be constructed
	 * @param paramTypes
	 *            parameter types for the managed type's constructor parameters
	 */
	@SafeVarargs
	public BasicTypeDescriptor(String name,
			Class<? extends ReturnType> returnType,
			Class<?>... paramTypes) {
		this.name = name;
		this.returnType = returnType;
		this.paramTypes = paramTypes;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ReturnType createNew(Object... args) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		Constructor<? extends ReturnType> cons;

		cons = returnType.getConstructor(paramTypes);
		if (cons != null)
			return cons.newInstance(args);

		return null;
	}
}
