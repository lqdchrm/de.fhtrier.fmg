package de.fhtrier.fmg.utils.factory.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.fhtrier.fmg.utils.factory.IFactory;
import de.fhtrier.fmg.utils.factory.IFactoryListener;
import de.fhtrier.fmg.utils.factory.ITypeDescriptor;

/**
 * a generic factory impl.
 * 
 * @see IFactory
 * 
 * @author Robert Rößger
 * @param <ManagedType>
 *            the base type which is managed by this factory
 * @param <TypeDescriptor>
 *            the type descriptor for the managed type
 */
public class Factory<ManagedType, TypeDescriptor extends ITypeDescriptor<ManagedType>> implements IFactory<ManagedType, TypeDescriptor> {

	private HashMap<String, TypeDescriptor>						_registeredTypes;
	private List<IFactoryListener<ManagedType, TypeDescriptor>>	_listeners;

	public Factory() {
		this._registeredTypes = new HashMap<>();
		this._listeners = new ArrayList<>();
	}

	@Override
	public boolean containsType(String descName) {
		return _registeredTypes.containsKey(descName);
	}

	@Override
	public TypeDescriptor getDescriptor(String descName) {
		return _registeredTypes.get(descName);
	}

	@Override
	public Iterable<String> getRegisteredTypes() {
		return _registeredTypes.keySet();
	}

	@Override
	public ManagedType createInstanceOfType(String descName, Object... args) {
		try {
			return _registeredTypes.get(descName).createNew(args);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			// TODO proper exception handling
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void registerType(TypeDescriptor desc) {
		_registeredTypes.put(desc.getName(), desc);
		for (IFactoryListener<ManagedType, TypeDescriptor> l : _listeners) {
			l.factoryChanged(this);
		}
	}

	@Override
	public void unregisterType(String descName) {
		_registeredTypes.remove(descName);
		for (IFactoryListener<ManagedType, TypeDescriptor> l : _listeners) {
			l.factoryChanged(this);
		}
	}

	@Override
	public void addListener(IFactoryListener<ManagedType, TypeDescriptor> listener) {
		if (!_listeners.contains(listener)) {
			_listeners.add(listener);
		}
	}

	@Override
	public void removeListener(IFactoryListener<ManagedType, TypeDescriptor> listener) {
		if (_listeners.contains(listener)) {
			_listeners.remove(listener);
		}
	}
}
