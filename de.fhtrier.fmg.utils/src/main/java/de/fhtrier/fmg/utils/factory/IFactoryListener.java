package de.fhtrier.fmg.utils.factory;

public interface IFactoryListener<ManagedType, TypeDescriptor extends ITypeDescriptor<ManagedType>> {
	public void factoryChanged(IFactory<ManagedType, TypeDescriptor> factory);
}
