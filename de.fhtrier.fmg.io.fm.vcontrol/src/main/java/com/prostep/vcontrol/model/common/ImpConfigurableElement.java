/**
 */
package com.prostep.vcontrol.model.common;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Imp Configurable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getImpConfiguredElements <em>Imp Configured Elements</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getRightElements <em>Right Elements</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getLeftElements <em>Left Elements</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getExtensions <em>Extensions</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getImpConfiguredElements1 <em>Imp Configured Elements1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#isOptional <em>Optional</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getRightElements1 <em>Right Elements1</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableElement()
 * @model abstract="true"
 *        extendedMetaData="name='ImpConfigurableElement' kind='elementOnly'"
 * @generated
 */
public interface ImpConfigurableElement extends ImpItem {
	/**
	 * Returns the value of the '<em><b>Group1</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group1</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group1</em>' attribute list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableElement_Group1()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:13'"
	 * @generated
	 */
	FeatureMap getGroup1();

	/**
	 * Returns the value of the '<em><b>Imp Configured Elements</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpConfiguredElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Configured Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Configured Elements</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableElement_ImpConfiguredElements()
	 * @model type="com.prostep.vcontrol.model.common.ImpConfiguredElement" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='impConfiguredElements' group='#group:13'"
	 * @generated
	 */
	EList getImpConfiguredElements();

	/**
	 * Returns the value of the '<em><b>Right Elements</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpLinkingRelation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Elements</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableElement_RightElements()
	 * @model type="com.prostep.vcontrol.model.common.ImpLinkingRelation" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='rightElements' group='#group:13'"
	 * @generated
	 */
	EList getRightElements();

	/**
	 * Returns the value of the '<em><b>Left Elements</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpLinkingRelation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Elements</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableElement_LeftElements()
	 * @model type="com.prostep.vcontrol.model.common.ImpLinkingRelation" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='leftElements' group='#group:13'"
	 * @generated
	 */
	EList getLeftElements();

	/**
	 * Returns the value of the '<em><b>Extensions</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extensions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extensions</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableElement_Extensions()
	 * @model type="com.prostep.vcontrol.model.common.ImpConfigurableElementExtension" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='extensions' group='#group:13'"
	 * @generated
	 */
	EList getExtensions();

	/**
	 * Returns the value of the '<em><b>Imp Configured Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Configured Elements1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Configured Elements1</em>' attribute.
	 * @see #setImpConfiguredElements1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableElement_ImpConfiguredElements1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='impConfiguredElements'"
	 * @generated
	 */
	String getImpConfiguredElements1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getImpConfiguredElements1 <em>Imp Configured Elements1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Configured Elements1</em>' attribute.
	 * @see #getImpConfiguredElements1()
	 * @generated
	 */
	void setImpConfiguredElements1(String value);

	/**
	 * Returns the value of the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optional</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optional</em>' attribute.
	 * @see #isSetOptional()
	 * @see #unsetOptional()
	 * @see #setOptional(boolean)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableElement_Optional()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean" required="true"
	 *        extendedMetaData="kind='attribute' name='optional'"
	 * @generated
	 */
	boolean isOptional();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#isOptional <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optional</em>' attribute.
	 * @see #isSetOptional()
	 * @see #unsetOptional()
	 * @see #isOptional()
	 * @generated
	 */
	void setOptional(boolean value);

	/**
	 * Unsets the value of the '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#isOptional <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOptional()
	 * @see #isOptional()
	 * @see #setOptional(boolean)
	 * @generated
	 */
	void unsetOptional();

	/**
	 * Returns whether the value of the '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#isOptional <em>Optional</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Optional</em>' attribute is set.
	 * @see #unsetOptional()
	 * @see #isOptional()
	 * @see #setOptional(boolean)
	 * @generated
	 */
	boolean isSetOptional();

	/**
	 * Returns the value of the '<em><b>Right Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Elements1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Elements1</em>' attribute.
	 * @see #setRightElements1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableElement_RightElements1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='rightElements'"
	 * @generated
	 */
	String getRightElements1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getRightElements1 <em>Right Elements1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Elements1</em>' attribute.
	 * @see #getRightElements1()
	 * @generated
	 */
	void setRightElements1(String value);

} // ImpConfigurableElement
