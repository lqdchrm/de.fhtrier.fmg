/**
 */
package com.prostep.vcontrol.model.feature.impl;

import com.prostep.vcontrol.model.feature.DocumentRoot;
import com.prostep.vcontrol.model.feature.Feature;
import com.prostep.vcontrol.model.feature.FeatureGroup;
import com.prostep.vcontrol.model.feature.FeatureModel;
import com.prostep.vcontrol.model.feature.FeatureNode;
import com.prostep.vcontrol.model.feature.FeatureOccurrence;
import com.prostep.vcontrol.model.feature.FeaturePackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.DocumentRootImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.DocumentRootImpl#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.DocumentRootImpl#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.DocumentRootImpl#getFeature <em>Feature</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.DocumentRootImpl#getFeatureGroup <em>Feature Group</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.DocumentRootImpl#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.DocumentRootImpl#getFeatureNode <em>Feature Node</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.DocumentRootImpl#getFeatureOccurrence <em>Feature Occurrence</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DocumentRootImpl extends EObjectImpl implements DocumentRoot {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * The cached value of the '{@link #getXMLNSPrefixMap() <em>XMLNS Prefix Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXMLNSPrefixMap()
	 * @generated
	 * @ordered
	 */
	protected EMap xMLNSPrefixMap;

	/**
	 * The cached value of the '{@link #getXSISchemaLocation() <em>XSI Schema Location</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXSISchemaLocation()
	 * @generated
	 * @ordered
	 */
	protected EMap xSISchemaLocation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DocumentRootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return FeaturePackage.Literals.DOCUMENT_ROOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, FeaturePackage.DOCUMENT_ROOT__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap getXMLNSPrefixMap() {
		if (xMLNSPrefixMap == null) {
			xMLNSPrefixMap = new EcoreEMap(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, FeaturePackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		}
		return xMLNSPrefixMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap getXSISchemaLocation() {
		if (xSISchemaLocation == null) {
			xSISchemaLocation = new EcoreEMap(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, FeaturePackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		}
		return xSISchemaLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature getFeature() {
		return (Feature)getMixed().get(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeature(Feature newFeature, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE, newFeature, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeature(Feature newFeature) {
		((FeatureMap.Internal)getMixed()).set(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE, newFeature);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureGroup getFeatureGroup() {
		return (FeatureGroup)getMixed().get(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_GROUP, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureGroup(FeatureGroup newFeatureGroup, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_GROUP, newFeatureGroup, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureGroup(FeatureGroup newFeatureGroup) {
		((FeatureMap.Internal)getMixed()).set(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_GROUP, newFeatureGroup);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModel getFeatureModel() {
		return (FeatureModel)getMixed().get(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_MODEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureModel(FeatureModel newFeatureModel, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_MODEL, newFeatureModel, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureModel(FeatureModel newFeatureModel) {
		((FeatureMap.Internal)getMixed()).set(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_MODEL, newFeatureModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureNode getFeatureNode() {
		return (FeatureNode)getMixed().get(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_NODE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureNode(FeatureNode newFeatureNode, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_NODE, newFeatureNode, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureNode(FeatureNode newFeatureNode) {
		((FeatureMap.Internal)getMixed()).set(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_NODE, newFeatureNode);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureOccurrence getFeatureOccurrence() {
		return (FeatureOccurrence)getMixed().get(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_OCCURRENCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureOccurrence(FeatureOccurrence newFeatureOccurrence, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_OCCURRENCE, newFeatureOccurrence, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureOccurrence(FeatureOccurrence newFeatureOccurrence) {
		((FeatureMap.Internal)getMixed()).set(FeaturePackage.Literals.DOCUMENT_ROOT__FEATURE_OCCURRENCE, newFeatureOccurrence);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FeaturePackage.DOCUMENT_ROOT__MIXED:
				return ((InternalEList)getMixed()).basicRemove(otherEnd, msgs);
			case FeaturePackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return ((InternalEList)getXMLNSPrefixMap()).basicRemove(otherEnd, msgs);
			case FeaturePackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return ((InternalEList)getXSISchemaLocation()).basicRemove(otherEnd, msgs);
			case FeaturePackage.DOCUMENT_ROOT__FEATURE:
				return basicSetFeature(null, msgs);
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_GROUP:
				return basicSetFeatureGroup(null, msgs);
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_MODEL:
				return basicSetFeatureModel(null, msgs);
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_NODE:
				return basicSetFeatureNode(null, msgs);
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_OCCURRENCE:
				return basicSetFeatureOccurrence(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FeaturePackage.DOCUMENT_ROOT__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case FeaturePackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				if (coreType) return getXMLNSPrefixMap();
				else return getXMLNSPrefixMap().map();
			case FeaturePackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				if (coreType) return getXSISchemaLocation();
				else return getXSISchemaLocation().map();
			case FeaturePackage.DOCUMENT_ROOT__FEATURE:
				return getFeature();
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_GROUP:
				return getFeatureGroup();
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_MODEL:
				return getFeatureModel();
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_NODE:
				return getFeatureNode();
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_OCCURRENCE:
				return getFeatureOccurrence();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FeaturePackage.DOCUMENT_ROOT__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case FeaturePackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				((EStructuralFeature.Setting)getXMLNSPrefixMap()).set(newValue);
				return;
			case FeaturePackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				((EStructuralFeature.Setting)getXSISchemaLocation()).set(newValue);
				return;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE:
				setFeature((Feature)newValue);
				return;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_GROUP:
				setFeatureGroup((FeatureGroup)newValue);
				return;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_MODEL:
				setFeatureModel((FeatureModel)newValue);
				return;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_NODE:
				setFeatureNode((FeatureNode)newValue);
				return;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_OCCURRENCE:
				setFeatureOccurrence((FeatureOccurrence)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case FeaturePackage.DOCUMENT_ROOT__MIXED:
				getMixed().clear();
				return;
			case FeaturePackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				getXMLNSPrefixMap().clear();
				return;
			case FeaturePackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				getXSISchemaLocation().clear();
				return;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE:
				setFeature((Feature)null);
				return;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_GROUP:
				setFeatureGroup((FeatureGroup)null);
				return;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_MODEL:
				setFeatureModel((FeatureModel)null);
				return;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_NODE:
				setFeatureNode((FeatureNode)null);
				return;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_OCCURRENCE:
				setFeatureOccurrence((FeatureOccurrence)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FeaturePackage.DOCUMENT_ROOT__MIXED:
				return mixed != null && !mixed.isEmpty();
			case FeaturePackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return xMLNSPrefixMap != null && !xMLNSPrefixMap.isEmpty();
			case FeaturePackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return xSISchemaLocation != null && !xSISchemaLocation.isEmpty();
			case FeaturePackage.DOCUMENT_ROOT__FEATURE:
				return getFeature() != null;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_GROUP:
				return getFeatureGroup() != null;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_MODEL:
				return getFeatureModel() != null;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_NODE:
				return getFeatureNode() != null;
			case FeaturePackage.DOCUMENT_ROOT__FEATURE_OCCURRENCE:
				return getFeatureOccurrence() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //DocumentRootImpl
