/**
 */
package com.prostep.vcontrol.model.feature;

import com.prostep.vcontrol.model.common.ImpNode;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureNode#getGroup2 <em>Group2</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureNode#getParent <em>Parent</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureNode#getChildren <em>Children</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureNode#getModel <em>Model</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureNode#getOccurrences <em>Occurrences</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureNode#getModel1 <em>Model1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureNode#getOccurrences1 <em>Occurrences1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureNode#getParent1 <em>Parent1</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureNode()
 * @model abstract="true"
 *        extendedMetaData="name='FeatureNode' kind='elementOnly'"
 * @generated
 */
public interface FeatureNode extends ImpNode {
	/**
	 * Returns the value of the '<em><b>Group2</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group2</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group2</em>' attribute list.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureNode_Group2()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:21'"
	 * @generated
	 */
	FeatureMap getGroup2();

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.feature.FeatureNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureNode_Parent()
	 * @model type="com.prostep.vcontrol.model.feature.FeatureNode" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='parent' group='#group:21'"
	 * @generated
	 */
	EList getParent();

	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.feature.FeatureNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureNode_Children()
	 * @model type="com.prostep.vcontrol.model.feature.FeatureNode" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='children' group='#group:21'"
	 * @generated
	 */
	EList getChildren();

	/**
	 * Returns the value of the '<em><b>Model</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.feature.FeatureModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureNode_Model()
	 * @model type="com.prostep.vcontrol.model.feature.FeatureModel" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model' group='#group:21'"
	 * @generated
	 */
	EList getModel();

	/**
	 * Returns the value of the '<em><b>Occurrences</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.feature.FeatureOccurrence}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Occurrences</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Occurrences</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureNode_Occurrences()
	 * @model type="com.prostep.vcontrol.model.feature.FeatureOccurrence" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='occurrences' group='#group:21'"
	 * @generated
	 */
	EList getOccurrences();

	/**
	 * Returns the value of the '<em><b>Model1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model1</em>' attribute.
	 * @see #setModel1(String)
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureNode_Model1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='model'"
	 * @generated
	 */
	String getModel1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.feature.FeatureNode#getModel1 <em>Model1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model1</em>' attribute.
	 * @see #getModel1()
	 * @generated
	 */
	void setModel1(String value);

	/**
	 * Returns the value of the '<em><b>Occurrences1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Occurrences1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Occurrences1</em>' attribute.
	 * @see #setOccurrences1(String)
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureNode_Occurrences1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='occurrences'"
	 * @generated
	 */
	String getOccurrences1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.feature.FeatureNode#getOccurrences1 <em>Occurrences1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Occurrences1</em>' attribute.
	 * @see #getOccurrences1()
	 * @generated
	 */
	void setOccurrences1(String value);

	/**
	 * Returns the value of the '<em><b>Parent1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent1</em>' attribute.
	 * @see #setParent1(String)
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureNode_Parent1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='parent'"
	 * @generated
	 */
	String getParent1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.feature.FeatureNode#getParent1 <em>Parent1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent1</em>' attribute.
	 * @see #getParent1()
	 * @generated
	 */
	void setParent1(String value);

} // FeatureNode
