/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.CommonPackage;
import com.prostep.vcontrol.model.common.Constraint;
import com.prostep.vcontrol.model.common.DescribedItem;
import com.prostep.vcontrol.model.common.DocumentRoot;
import com.prostep.vcontrol.model.common.Identifiable;
import com.prostep.vcontrol.model.common.ImpConfigurableElement;
import com.prostep.vcontrol.model.common.ImpConfigurableElementExtension;
import com.prostep.vcontrol.model.common.ImpConfigurableGroup;
import com.prostep.vcontrol.model.common.ImpConfiguration;
import com.prostep.vcontrol.model.common.ImpConfiguredElement;
import com.prostep.vcontrol.model.common.ImpConnector;
import com.prostep.vcontrol.model.common.ImpExtensibleElement;
import com.prostep.vcontrol.model.common.ImpItem;
import com.prostep.vcontrol.model.common.ImpLinkingRelation;
import com.prostep.vcontrol.model.common.ImpModel;
import com.prostep.vcontrol.model.common.ImpNode;
import com.prostep.vcontrol.model.common.ImpPortNode;
import com.prostep.vcontrol.model.common.ImpProject;
import com.prostep.vcontrol.model.common.ImpTerm;
import com.prostep.vcontrol.model.common.NamedItem;
import com.prostep.vcontrol.model.common.Property;
import com.prostep.vcontrol.model.common.PropertySet;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getDescribedItem <em>Described Item</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getIdentifiable <em>Identifiable</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpConfigurableElement <em>Imp Configurable Element</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpConfigurableElementExtension <em>Imp Configurable Element Extension</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpConfigurableGroup <em>Imp Configurable Group</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpConfiguration <em>Imp Configuration</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpConfiguredElement <em>Imp Configured Element</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpConnector <em>Imp Connector</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpExtensibleElement <em>Imp Extensible Element</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpItem <em>Imp Item</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpLinkingRelation <em>Imp Linking Relation</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpModel <em>Imp Model</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpNode <em>Imp Node</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpPortNode <em>Imp Port Node</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpProject <em>Imp Project</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getImpTerm <em>Imp Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getNamedItem <em>Named Item</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl#getPropertySet <em>Property Set</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DocumentRootImpl extends EObjectImpl implements DocumentRoot {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * The cached value of the '{@link #getXMLNSPrefixMap() <em>XMLNS Prefix Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXMLNSPrefixMap()
	 * @generated
	 * @ordered
	 */
	protected EMap xMLNSPrefixMap;

	/**
	 * The cached value of the '{@link #getXSISchemaLocation() <em>XSI Schema Location</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXSISchemaLocation()
	 * @generated
	 * @ordered
	 */
	protected EMap xSISchemaLocation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DocumentRootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CommonPackage.Literals.DOCUMENT_ROOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, CommonPackage.DOCUMENT_ROOT__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap getXMLNSPrefixMap() {
		if (xMLNSPrefixMap == null) {
			xMLNSPrefixMap = new EcoreEMap(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, CommonPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		}
		return xMLNSPrefixMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap getXSISchemaLocation() {
		if (xSISchemaLocation == null) {
			xSISchemaLocation = new EcoreEMap(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, CommonPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		}
		return xSISchemaLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint getConstraint() {
		return (Constraint)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__CONSTRAINT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstraint(Constraint newConstraint, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__CONSTRAINT, newConstraint, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraint(Constraint newConstraint) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__CONSTRAINT, newConstraint);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DescribedItem getDescribedItem() {
		return (DescribedItem)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__DESCRIBED_ITEM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescribedItem(DescribedItem newDescribedItem, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__DESCRIBED_ITEM, newDescribedItem, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescribedItem(DescribedItem newDescribedItem) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__DESCRIBED_ITEM, newDescribedItem);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Identifiable getIdentifiable() {
		return (Identifiable)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IDENTIFIABLE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdentifiable(Identifiable newIdentifiable, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IDENTIFIABLE, newIdentifiable, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentifiable(Identifiable newIdentifiable) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IDENTIFIABLE, newIdentifiable);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpConfigurableElement getImpConfigurableElement() {
		return (ImpConfigurableElement)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpConfigurableElement(ImpConfigurableElement newImpConfigurableElement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT, newImpConfigurableElement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpConfigurableElement(ImpConfigurableElement newImpConfigurableElement) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT, newImpConfigurableElement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpConfigurableElementExtension getImpConfigurableElementExtension() {
		return (ImpConfigurableElementExtension)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT_EXTENSION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpConfigurableElementExtension(ImpConfigurableElementExtension newImpConfigurableElementExtension, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT_EXTENSION, newImpConfigurableElementExtension, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpConfigurableElementExtension(ImpConfigurableElementExtension newImpConfigurableElementExtension) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT_EXTENSION, newImpConfigurableElementExtension);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpConfigurableGroup getImpConfigurableGroup() {
		return (ImpConfigurableGroup)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURABLE_GROUP, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpConfigurableGroup(ImpConfigurableGroup newImpConfigurableGroup, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURABLE_GROUP, newImpConfigurableGroup, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpConfigurableGroup(ImpConfigurableGroup newImpConfigurableGroup) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURABLE_GROUP, newImpConfigurableGroup);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpConfiguration getImpConfiguration() {
		return (ImpConfiguration)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpConfiguration(ImpConfiguration newImpConfiguration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURATION, newImpConfiguration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpConfiguration(ImpConfiguration newImpConfiguration) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURATION, newImpConfiguration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpConfiguredElement getImpConfiguredElement() {
		return (ImpConfiguredElement)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURED_ELEMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpConfiguredElement(ImpConfiguredElement newImpConfiguredElement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURED_ELEMENT, newImpConfiguredElement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpConfiguredElement(ImpConfiguredElement newImpConfiguredElement) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONFIGURED_ELEMENT, newImpConfiguredElement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpConnector getImpConnector() {
		return (ImpConnector)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONNECTOR, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpConnector(ImpConnector newImpConnector, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONNECTOR, newImpConnector, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpConnector(ImpConnector newImpConnector) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_CONNECTOR, newImpConnector);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpExtensibleElement getImpExtensibleElement() {
		return (ImpExtensibleElement)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_EXTENSIBLE_ELEMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpExtensibleElement(ImpExtensibleElement newImpExtensibleElement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_EXTENSIBLE_ELEMENT, newImpExtensibleElement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpExtensibleElement(ImpExtensibleElement newImpExtensibleElement) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_EXTENSIBLE_ELEMENT, newImpExtensibleElement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpItem getImpItem() {
		return (ImpItem)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_ITEM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpItem(ImpItem newImpItem, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_ITEM, newImpItem, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpItem(ImpItem newImpItem) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_ITEM, newImpItem);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpLinkingRelation getImpLinkingRelation() {
		return (ImpLinkingRelation)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_LINKING_RELATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpLinkingRelation(ImpLinkingRelation newImpLinkingRelation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_LINKING_RELATION, newImpLinkingRelation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpLinkingRelation(ImpLinkingRelation newImpLinkingRelation) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_LINKING_RELATION, newImpLinkingRelation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpModel getImpModel() {
		return (ImpModel)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_MODEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpModel(ImpModel newImpModel, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_MODEL, newImpModel, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpModel(ImpModel newImpModel) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_MODEL, newImpModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpNode getImpNode() {
		return (ImpNode)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_NODE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpNode(ImpNode newImpNode, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_NODE, newImpNode, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpNode(ImpNode newImpNode) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_NODE, newImpNode);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpPortNode getImpPortNode() {
		return (ImpPortNode)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_PORT_NODE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpPortNode(ImpPortNode newImpPortNode, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_PORT_NODE, newImpPortNode, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpPortNode(ImpPortNode newImpPortNode) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_PORT_NODE, newImpPortNode);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpProject getImpProject() {
		return (ImpProject)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_PROJECT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpProject(ImpProject newImpProject, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_PROJECT, newImpProject, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpProject(ImpProject newImpProject) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_PROJECT, newImpProject);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpTerm getImpTerm() {
		return (ImpTerm)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__IMP_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpTerm(ImpTerm newImpTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__IMP_TERM, newImpTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpTerm(ImpTerm newImpTerm) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__IMP_TERM, newImpTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedItem getNamedItem() {
		return (NamedItem)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__NAMED_ITEM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamedItem(NamedItem newNamedItem, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__NAMED_ITEM, newNamedItem, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamedItem(NamedItem newNamedItem) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__NAMED_ITEM, newNamedItem);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getProperty() {
		return (Property)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__PROPERTY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProperty(Property newProperty, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__PROPERTY, newProperty, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProperty(Property newProperty) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__PROPERTY, newProperty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertySet getPropertySet() {
		return (PropertySet)getMixed().get(CommonPackage.Literals.DOCUMENT_ROOT__PROPERTY_SET, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPropertySet(PropertySet newPropertySet, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(CommonPackage.Literals.DOCUMENT_ROOT__PROPERTY_SET, newPropertySet, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertySet(PropertySet newPropertySet) {
		((FeatureMap.Internal)getMixed()).set(CommonPackage.Literals.DOCUMENT_ROOT__PROPERTY_SET, newPropertySet);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CommonPackage.DOCUMENT_ROOT__MIXED:
				return ((InternalEList)getMixed()).basicRemove(otherEnd, msgs);
			case CommonPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return ((InternalEList)getXMLNSPrefixMap()).basicRemove(otherEnd, msgs);
			case CommonPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return ((InternalEList)getXSISchemaLocation()).basicRemove(otherEnd, msgs);
			case CommonPackage.DOCUMENT_ROOT__CONSTRAINT:
				return basicSetConstraint(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__DESCRIBED_ITEM:
				return basicSetDescribedItem(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IDENTIFIABLE:
				return basicSetIdentifiable(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT:
				return basicSetImpConfigurableElement(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT_EXTENSION:
				return basicSetImpConfigurableElementExtension(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_GROUP:
				return basicSetImpConfigurableGroup(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURATION:
				return basicSetImpConfiguration(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURED_ELEMENT:
				return basicSetImpConfiguredElement(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_CONNECTOR:
				return basicSetImpConnector(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_EXTENSIBLE_ELEMENT:
				return basicSetImpExtensibleElement(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_ITEM:
				return basicSetImpItem(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_LINKING_RELATION:
				return basicSetImpLinkingRelation(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_MODEL:
				return basicSetImpModel(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_NODE:
				return basicSetImpNode(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_PORT_NODE:
				return basicSetImpPortNode(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_PROJECT:
				return basicSetImpProject(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__IMP_TERM:
				return basicSetImpTerm(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__NAMED_ITEM:
				return basicSetNamedItem(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__PROPERTY:
				return basicSetProperty(null, msgs);
			case CommonPackage.DOCUMENT_ROOT__PROPERTY_SET:
				return basicSetPropertySet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CommonPackage.DOCUMENT_ROOT__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case CommonPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				if (coreType) return getXMLNSPrefixMap();
				else return getXMLNSPrefixMap().map();
			case CommonPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				if (coreType) return getXSISchemaLocation();
				else return getXSISchemaLocation().map();
			case CommonPackage.DOCUMENT_ROOT__CONSTRAINT:
				return getConstraint();
			case CommonPackage.DOCUMENT_ROOT__DESCRIBED_ITEM:
				return getDescribedItem();
			case CommonPackage.DOCUMENT_ROOT__IDENTIFIABLE:
				return getIdentifiable();
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT:
				return getImpConfigurableElement();
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT_EXTENSION:
				return getImpConfigurableElementExtension();
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_GROUP:
				return getImpConfigurableGroup();
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURATION:
				return getImpConfiguration();
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURED_ELEMENT:
				return getImpConfiguredElement();
			case CommonPackage.DOCUMENT_ROOT__IMP_CONNECTOR:
				return getImpConnector();
			case CommonPackage.DOCUMENT_ROOT__IMP_EXTENSIBLE_ELEMENT:
				return getImpExtensibleElement();
			case CommonPackage.DOCUMENT_ROOT__IMP_ITEM:
				return getImpItem();
			case CommonPackage.DOCUMENT_ROOT__IMP_LINKING_RELATION:
				return getImpLinkingRelation();
			case CommonPackage.DOCUMENT_ROOT__IMP_MODEL:
				return getImpModel();
			case CommonPackage.DOCUMENT_ROOT__IMP_NODE:
				return getImpNode();
			case CommonPackage.DOCUMENT_ROOT__IMP_PORT_NODE:
				return getImpPortNode();
			case CommonPackage.DOCUMENT_ROOT__IMP_PROJECT:
				return getImpProject();
			case CommonPackage.DOCUMENT_ROOT__IMP_TERM:
				return getImpTerm();
			case CommonPackage.DOCUMENT_ROOT__NAMED_ITEM:
				return getNamedItem();
			case CommonPackage.DOCUMENT_ROOT__PROPERTY:
				return getProperty();
			case CommonPackage.DOCUMENT_ROOT__PROPERTY_SET:
				return getPropertySet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CommonPackage.DOCUMENT_ROOT__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				((EStructuralFeature.Setting)getXMLNSPrefixMap()).set(newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				((EStructuralFeature.Setting)getXSISchemaLocation()).set(newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__CONSTRAINT:
				setConstraint((Constraint)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__DESCRIBED_ITEM:
				setDescribedItem((DescribedItem)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IDENTIFIABLE:
				setIdentifiable((Identifiable)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT:
				setImpConfigurableElement((ImpConfigurableElement)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT_EXTENSION:
				setImpConfigurableElementExtension((ImpConfigurableElementExtension)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_GROUP:
				setImpConfigurableGroup((ImpConfigurableGroup)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURATION:
				setImpConfiguration((ImpConfiguration)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURED_ELEMENT:
				setImpConfiguredElement((ImpConfiguredElement)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONNECTOR:
				setImpConnector((ImpConnector)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_EXTENSIBLE_ELEMENT:
				setImpExtensibleElement((ImpExtensibleElement)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_ITEM:
				setImpItem((ImpItem)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_LINKING_RELATION:
				setImpLinkingRelation((ImpLinkingRelation)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_MODEL:
				setImpModel((ImpModel)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_NODE:
				setImpNode((ImpNode)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_PORT_NODE:
				setImpPortNode((ImpPortNode)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_PROJECT:
				setImpProject((ImpProject)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_TERM:
				setImpTerm((ImpTerm)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__NAMED_ITEM:
				setNamedItem((NamedItem)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__PROPERTY:
				setProperty((Property)newValue);
				return;
			case CommonPackage.DOCUMENT_ROOT__PROPERTY_SET:
				setPropertySet((PropertySet)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CommonPackage.DOCUMENT_ROOT__MIXED:
				getMixed().clear();
				return;
			case CommonPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				getXMLNSPrefixMap().clear();
				return;
			case CommonPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				getXSISchemaLocation().clear();
				return;
			case CommonPackage.DOCUMENT_ROOT__CONSTRAINT:
				setConstraint((Constraint)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__DESCRIBED_ITEM:
				setDescribedItem((DescribedItem)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IDENTIFIABLE:
				setIdentifiable((Identifiable)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT:
				setImpConfigurableElement((ImpConfigurableElement)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT_EXTENSION:
				setImpConfigurableElementExtension((ImpConfigurableElementExtension)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_GROUP:
				setImpConfigurableGroup((ImpConfigurableGroup)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURATION:
				setImpConfiguration((ImpConfiguration)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURED_ELEMENT:
				setImpConfiguredElement((ImpConfiguredElement)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONNECTOR:
				setImpConnector((ImpConnector)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_EXTENSIBLE_ELEMENT:
				setImpExtensibleElement((ImpExtensibleElement)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_ITEM:
				setImpItem((ImpItem)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_LINKING_RELATION:
				setImpLinkingRelation((ImpLinkingRelation)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_MODEL:
				setImpModel((ImpModel)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_NODE:
				setImpNode((ImpNode)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_PORT_NODE:
				setImpPortNode((ImpPortNode)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_PROJECT:
				setImpProject((ImpProject)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__IMP_TERM:
				setImpTerm((ImpTerm)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__NAMED_ITEM:
				setNamedItem((NamedItem)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__PROPERTY:
				setProperty((Property)null);
				return;
			case CommonPackage.DOCUMENT_ROOT__PROPERTY_SET:
				setPropertySet((PropertySet)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CommonPackage.DOCUMENT_ROOT__MIXED:
				return mixed != null && !mixed.isEmpty();
			case CommonPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return xMLNSPrefixMap != null && !xMLNSPrefixMap.isEmpty();
			case CommonPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return xSISchemaLocation != null && !xSISchemaLocation.isEmpty();
			case CommonPackage.DOCUMENT_ROOT__CONSTRAINT:
				return getConstraint() != null;
			case CommonPackage.DOCUMENT_ROOT__DESCRIBED_ITEM:
				return getDescribedItem() != null;
			case CommonPackage.DOCUMENT_ROOT__IDENTIFIABLE:
				return getIdentifiable() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT:
				return getImpConfigurableElement() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT_EXTENSION:
				return getImpConfigurableElementExtension() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURABLE_GROUP:
				return getImpConfigurableGroup() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURATION:
				return getImpConfiguration() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONFIGURED_ELEMENT:
				return getImpConfiguredElement() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_CONNECTOR:
				return getImpConnector() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_EXTENSIBLE_ELEMENT:
				return getImpExtensibleElement() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_ITEM:
				return getImpItem() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_LINKING_RELATION:
				return getImpLinkingRelation() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_MODEL:
				return getImpModel() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_NODE:
				return getImpNode() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_PORT_NODE:
				return getImpPortNode() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_PROJECT:
				return getImpProject() != null;
			case CommonPackage.DOCUMENT_ROOT__IMP_TERM:
				return getImpTerm() != null;
			case CommonPackage.DOCUMENT_ROOT__NAMED_ITEM:
				return getNamedItem() != null;
			case CommonPackage.DOCUMENT_ROOT__PROPERTY:
				return getProperty() != null;
			case CommonPackage.DOCUMENT_ROOT__PROPERTY_SET:
				return getPropertySet() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //DocumentRootImpl
