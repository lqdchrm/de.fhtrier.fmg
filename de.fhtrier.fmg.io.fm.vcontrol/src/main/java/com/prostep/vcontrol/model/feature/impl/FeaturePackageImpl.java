/**
 */
package com.prostep.vcontrol.model.feature.impl;

import com.prostep.vcontrol.model.common.CommonPackage;

import com.prostep.vcontrol.model.common.impl.CommonPackageImpl;

import com.prostep.vcontrol.model.feature.DocumentRoot;
import com.prostep.vcontrol.model.feature.Feature;
import com.prostep.vcontrol.model.feature.FeatureFactory;
import com.prostep.vcontrol.model.feature.FeatureGroup;
import com.prostep.vcontrol.model.feature.FeatureGroupType;
import com.prostep.vcontrol.model.feature.FeatureModel;
import com.prostep.vcontrol.model.feature.FeatureNode;
import com.prostep.vcontrol.model.feature.FeatureOccurrence;
import com.prostep.vcontrol.model.feature.FeaturePackage;

import com.prostep.vcontrol.model.terms.TermsPackage;

import com.prostep.vcontrol.model.terms.impl.TermsPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import xmi.XmiPackage;

import xmi.impl.XmiPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FeaturePackageImpl extends EPackageImpl implements FeaturePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureOccurrenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum featureGroupTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType featureGroupTypeObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FeaturePackageImpl() {
		super(eNS_URI, FeatureFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link FeaturePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FeaturePackage init() {
		if (isInited) return (FeaturePackage)EPackage.Registry.INSTANCE.getEPackage(FeaturePackage.eNS_URI);

		// Obtain or create and register package
		FeaturePackageImpl theFeaturePackage = (FeaturePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof FeaturePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new FeaturePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CommonPackageImpl theCommonPackage = (CommonPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) instanceof CommonPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) : CommonPackage.eINSTANCE);
		TermsPackageImpl theTermsPackage = (TermsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TermsPackage.eNS_URI) instanceof TermsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TermsPackage.eNS_URI) : TermsPackage.eINSTANCE);
		XmiPackageImpl theXmiPackage = (XmiPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(XmiPackage.eNS_URI) instanceof XmiPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(XmiPackage.eNS_URI) : XmiPackage.eINSTANCE);

		// Create package meta-data objects
		theFeaturePackage.createPackageContents();
		theCommonPackage.createPackageContents();
		theTermsPackage.createPackageContents();
		theXmiPackage.createPackageContents();

		// Initialize created meta-data
		theFeaturePackage.initializePackageContents();
		theCommonPackage.initializePackageContents();
		theTermsPackage.initializePackageContents();
		theXmiPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFeaturePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FeaturePackage.eNS_URI, theFeaturePackage);
		return theFeaturePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Mixed() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Feature() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_FeatureGroup() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_FeatureModel() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_FeatureNode() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_FeatureOccurrence() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeature() {
		return featureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureGroup() {
		return featureGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureGroup_Max() {
		return (EAttribute)featureGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureGroup_Min() {
		return (EAttribute)featureGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureModel() {
		return featureModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureModel_Group2() {
		return (EAttribute)featureModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureModel_RootFeature() {
		return (EReference)featureModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureNode() {
		return featureNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureNode_Group2() {
		return (EAttribute)featureNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureNode_Parent() {
		return (EReference)featureNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureNode_Children() {
		return (EReference)featureNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureNode_Model() {
		return (EReference)featureNodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureNode_Occurrences() {
		return (EReference)featureNodeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureNode_Model1() {
		return (EAttribute)featureNodeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureNode_Occurrences1() {
		return (EAttribute)featureNodeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureNode_Parent1() {
		return (EAttribute)featureNodeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureOccurrence() {
		return featureOccurrenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureOccurrence_Group2() {
		return (EAttribute)featureOccurrenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureOccurrence_FeatureNode() {
		return (EReference)featureOccurrenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureOccurrence_FeatureNode1() {
		return (EAttribute)featureOccurrenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFeatureGroupType() {
		return featureGroupTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getFeatureGroupTypeObject() {
		return featureGroupTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureFactory getFeatureFactory() {
		return (FeatureFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__FEATURE);
		createEReference(documentRootEClass, DOCUMENT_ROOT__FEATURE_GROUP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__FEATURE_MODEL);
		createEReference(documentRootEClass, DOCUMENT_ROOT__FEATURE_NODE);
		createEReference(documentRootEClass, DOCUMENT_ROOT__FEATURE_OCCURRENCE);

		featureEClass = createEClass(FEATURE);

		featureGroupEClass = createEClass(FEATURE_GROUP);
		createEAttribute(featureGroupEClass, FEATURE_GROUP__MAX);
		createEAttribute(featureGroupEClass, FEATURE_GROUP__MIN);

		featureModelEClass = createEClass(FEATURE_MODEL);
		createEAttribute(featureModelEClass, FEATURE_MODEL__GROUP2);
		createEReference(featureModelEClass, FEATURE_MODEL__ROOT_FEATURE);

		featureNodeEClass = createEClass(FEATURE_NODE);
		createEAttribute(featureNodeEClass, FEATURE_NODE__GROUP2);
		createEReference(featureNodeEClass, FEATURE_NODE__PARENT);
		createEReference(featureNodeEClass, FEATURE_NODE__CHILDREN);
		createEReference(featureNodeEClass, FEATURE_NODE__MODEL);
		createEReference(featureNodeEClass, FEATURE_NODE__OCCURRENCES);
		createEAttribute(featureNodeEClass, FEATURE_NODE__MODEL1);
		createEAttribute(featureNodeEClass, FEATURE_NODE__OCCURRENCES1);
		createEAttribute(featureNodeEClass, FEATURE_NODE__PARENT1);

		featureOccurrenceEClass = createEClass(FEATURE_OCCURRENCE);
		createEAttribute(featureOccurrenceEClass, FEATURE_OCCURRENCE__GROUP2);
		createEReference(featureOccurrenceEClass, FEATURE_OCCURRENCE__FEATURE_NODE);
		createEAttribute(featureOccurrenceEClass, FEATURE_OCCURRENCE__FEATURE_NODE1);

		// Create enums
		featureGroupTypeEEnum = createEEnum(FEATURE_GROUP_TYPE);

		// Create data types
		featureGroupTypeObjectEDataType = createEDataType(FEATURE_GROUP_TYPE_OBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);
		CommonPackage theCommonPackage = (CommonPackage)EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI);
		TermsPackage theTermsPackage = (TermsPackage)EPackage.Registry.INSTANCE.getEPackage(TermsPackage.eNS_URI);

		// Add supertypes to classes
		featureEClass.getESuperTypes().add(this.getFeatureNode());
		featureGroupEClass.getESuperTypes().add(this.getFeatureNode());
		featureModelEClass.getESuperTypes().add(theCommonPackage.getImpModel());
		featureNodeEClass.getESuperTypes().add(theCommonPackage.getImpNode());
		featureOccurrenceEClass.getESuperTypes().add(theTermsPackage.getAtomicTerm());

		// Initialize classes and features; add operations and parameters
		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Feature(), this.getFeature(), null, "feature", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_FeatureGroup(), this.getFeatureGroup(), null, "featureGroup", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_FeatureModel(), this.getFeatureModel(), null, "featureModel", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_FeatureNode(), this.getFeatureNode(), null, "featureNode", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_FeatureOccurrence(), this.getFeatureOccurrence(), null, "featureOccurrence", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(featureEClass, Feature.class, "Feature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(featureGroupEClass, FeatureGroup.class, "FeatureGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFeatureGroup_Max(), theXMLTypePackage.getInt(), "max", null, 1, 1, FeatureGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureGroup_Min(), theXMLTypePackage.getInt(), "min", null, 1, 1, FeatureGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureModelEClass, FeatureModel.class, "FeatureModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFeatureModel_Group2(), ecorePackage.getEFeatureMapEntry(), "group2", null, 0, -1, FeatureModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureModel_RootFeature(), this.getFeatureNode(), null, "rootFeature", null, 0, -1, FeatureModel.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(featureNodeEClass, FeatureNode.class, "FeatureNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFeatureNode_Group2(), ecorePackage.getEFeatureMapEntry(), "group2", null, 0, -1, FeatureNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureNode_Parent(), this.getFeatureNode(), null, "parent", null, 0, -1, FeatureNode.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureNode_Children(), this.getFeatureNode(), null, "children", null, 0, -1, FeatureNode.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureNode_Model(), this.getFeatureModel(), null, "model", null, 0, -1, FeatureNode.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureNode_Occurrences(), this.getFeatureOccurrence(), null, "occurrences", null, 0, -1, FeatureNode.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureNode_Model1(), theXMLTypePackage.getString(), "model1", null, 0, 1, FeatureNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureNode_Occurrences1(), theXMLTypePackage.getString(), "occurrences1", null, 0, 1, FeatureNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureNode_Parent1(), theXMLTypePackage.getString(), "parent1", null, 0, 1, FeatureNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureOccurrenceEClass, FeatureOccurrence.class, "FeatureOccurrence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFeatureOccurrence_Group2(), ecorePackage.getEFeatureMapEntry(), "group2", null, 0, -1, FeatureOccurrence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureOccurrence_FeatureNode(), this.getFeatureNode(), null, "featureNode", null, 0, -1, FeatureOccurrence.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureOccurrence_FeatureNode1(), theXMLTypePackage.getString(), "featureNode1", null, 0, 1, FeatureOccurrence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(featureGroupTypeEEnum, FeatureGroupType.class, "FeatureGroupType");
		addEEnumLiteral(featureGroupTypeEEnum, FeatureGroupType.CARDINALITYGROUP_LITERAL);
		addEEnumLiteral(featureGroupTypeEEnum, FeatureGroupType.ORGROUP_LITERAL);
		addEEnumLiteral(featureGroupTypeEEnum, FeatureGroupType.XORGROUP_LITERAL);

		// Initialize data types
		initEDataType(featureGroupTypeObjectEDataType, FeatureGroupType.class, "FeatureGroupTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";		
		addAnnotation
		  (documentRootEClass, 
		   source, 
		   new String[] {
			 "name", "",
			 "kind", "mixed"
		   });		
		addAnnotation
		  (getDocumentRoot_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });		
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xmlns:prefix"
		   });		
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xsi:schemaLocation"
		   });		
		addAnnotation
		  (getDocumentRoot_Feature(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Feature",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_FeatureGroup(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "FeatureGroup",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_FeatureModel(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "FeatureModel",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_FeatureNode(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "FeatureNode",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_FeatureOccurrence(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "FeatureOccurrence",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (featureEClass, 
		   source, 
		   new String[] {
			 "name", "Feature",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (featureGroupEClass, 
		   source, 
		   new String[] {
			 "name", "FeatureGroup",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getFeatureGroup_Max(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "max"
		   });		
		addAnnotation
		  (getFeatureGroup_Min(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "min"
		   });		
		addAnnotation
		  (featureGroupTypeEEnum, 
		   source, 
		   new String[] {
			 "name", "FeatureGroupType"
		   });		
		addAnnotation
		  (featureGroupTypeObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "FeatureGroupType:Object",
			 "baseType", "FeatureGroupType"
		   });		
		addAnnotation
		  (featureModelEClass, 
		   source, 
		   new String[] {
			 "name", "FeatureModel",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getFeatureModel_Group2(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:19"
		   });		
		addAnnotation
		  (getFeatureModel_RootFeature(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "rootFeature",
			 "group", "#group:19"
		   });		
		addAnnotation
		  (featureNodeEClass, 
		   source, 
		   new String[] {
			 "name", "FeatureNode",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getFeatureNode_Group2(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:21"
		   });		
		addAnnotation
		  (getFeatureNode_Parent(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "parent",
			 "group", "#group:21"
		   });		
		addAnnotation
		  (getFeatureNode_Children(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "children",
			 "group", "#group:21"
		   });		
		addAnnotation
		  (getFeatureNode_Model(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "model",
			 "group", "#group:21"
		   });		
		addAnnotation
		  (getFeatureNode_Occurrences(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "occurrences",
			 "group", "#group:21"
		   });		
		addAnnotation
		  (getFeatureNode_Model1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "model"
		   });		
		addAnnotation
		  (getFeatureNode_Occurrences1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "occurrences"
		   });		
		addAnnotation
		  (getFeatureNode_Parent1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "parent"
		   });		
		addAnnotation
		  (featureOccurrenceEClass, 
		   source, 
		   new String[] {
			 "name", "FeatureOccurrence",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getFeatureOccurrence_Group2(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:12"
		   });		
		addAnnotation
		  (getFeatureOccurrence_FeatureNode(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "featureNode",
			 "group", "#group:12"
		   });		
		addAnnotation
		  (getFeatureOccurrence_FeatureNode1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "featureNode"
		   });
	}

} //FeaturePackageImpl
