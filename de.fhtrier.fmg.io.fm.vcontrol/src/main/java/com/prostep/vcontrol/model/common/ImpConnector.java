/**
 */
package com.prostep.vcontrol.model.common;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Imp Connector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConnector#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConnector#getSourcePort <em>Source Port</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConnector#getTargetPort <em>Target Port</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConnector#getSourcePort1 <em>Source Port1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConnector#getTargetPort1 <em>Target Port1</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConnector()
 * @model abstract="true"
 *        extendedMetaData="name='ImpConnector' kind='elementOnly'"
 * @generated
 */
public interface ImpConnector extends ImpItem {
	/**
	 * Returns the value of the '<em><b>Group1</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group1</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group1</em>' attribute list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConnector_Group1()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:13'"
	 * @generated
	 */
	FeatureMap getGroup1();

	/**
	 * Returns the value of the '<em><b>Source Port</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpPortNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Port</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Port</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConnector_SourcePort()
	 * @model type="com.prostep.vcontrol.model.common.ImpPortNode" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='sourcePort' group='#group:13'"
	 * @generated
	 */
	EList getSourcePort();

	/**
	 * Returns the value of the '<em><b>Target Port</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpPortNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Port</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Port</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConnector_TargetPort()
	 * @model type="com.prostep.vcontrol.model.common.ImpPortNode" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='targetPort' group='#group:13'"
	 * @generated
	 */
	EList getTargetPort();

	/**
	 * Returns the value of the '<em><b>Source Port1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Port1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Port1</em>' attribute.
	 * @see #setSourcePort1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConnector_SourcePort1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='sourcePort'"
	 * @generated
	 */
	String getSourcePort1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpConnector#getSourcePort1 <em>Source Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Port1</em>' attribute.
	 * @see #getSourcePort1()
	 * @generated
	 */
	void setSourcePort1(String value);

	/**
	 * Returns the value of the '<em><b>Target Port1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Port1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Port1</em>' attribute.
	 * @see #setTargetPort1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConnector_TargetPort1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='targetPort'"
	 * @generated
	 */
	String getTargetPort1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpConnector#getTargetPort1 <em>Target Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Port1</em>' attribute.
	 * @see #getTargetPort1()
	 * @generated
	 */
	void setTargetPort1(String value);

} // ImpConnector
