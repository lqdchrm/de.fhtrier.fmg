/**
 */
package com.prostep.vcontrol.model.terms;

import com.prostep.vcontrol.model.common.ImpTerm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Atomic Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.terms.AtomicTerm#getElement <em>Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getAtomicTerm()
 * @model extendedMetaData="name='AtomicTerm' kind='elementOnly'"
 * @generated
 */
public interface AtomicTerm extends ImpTerm {
	/**
	 * Returns the value of the '<em><b>Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element</em>' attribute.
	 * @see #setElement(String)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getAtomicTerm_Element()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='element'"
	 * @generated
	 */
	String getElement();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.AtomicTerm#getElement <em>Element</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element</em>' attribute.
	 * @see #getElement()
	 * @generated
	 */
	void setElement(String value);

} // AtomicTerm
