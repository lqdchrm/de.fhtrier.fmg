/**
 */
package com.prostep.vcontrol.model.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Imp Configurable Group Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableGroupType()
 * @model extendedMetaData="name='ImpConfigurableGroupType'"
 * @generated
 */
public final class ImpConfigurableGroupType extends AbstractEnumerator {
	/**
	 * The '<em><b>NONE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NONE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NONE_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NONE = 0;

	/**
	 * The '<em><b>AND</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AND</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AND_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AND = 1;

	/**
	 * The '<em><b>OR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>OR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OR_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OR = 2;

	/**
	 * The '<em><b>XOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>XOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #XOR_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int XOR = 3;

	/**
	 * The '<em><b>NONE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NONE
	 * @generated
	 * @ordered
	 */
	public static final ImpConfigurableGroupType NONE_LITERAL = new ImpConfigurableGroupType(NONE, "NONE", "NONE");

	/**
	 * The '<em><b>AND</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AND
	 * @generated
	 * @ordered
	 */
	public static final ImpConfigurableGroupType AND_LITERAL = new ImpConfigurableGroupType(AND, "AND", "AND");

	/**
	 * The '<em><b>OR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OR
	 * @generated
	 * @ordered
	 */
	public static final ImpConfigurableGroupType OR_LITERAL = new ImpConfigurableGroupType(OR, "OR", "OR");

	/**
	 * The '<em><b>XOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #XOR
	 * @generated
	 * @ordered
	 */
	public static final ImpConfigurableGroupType XOR_LITERAL = new ImpConfigurableGroupType(XOR, "XOR", "XOR");

	/**
	 * An array of all the '<em><b>Imp Configurable Group Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ImpConfigurableGroupType[] VALUES_ARRAY =
		new ImpConfigurableGroupType[] {
			NONE_LITERAL,
			AND_LITERAL,
			OR_LITERAL,
			XOR_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Imp Configurable Group Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Imp Configurable Group Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ImpConfigurableGroupType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ImpConfigurableGroupType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Imp Configurable Group Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ImpConfigurableGroupType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ImpConfigurableGroupType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Imp Configurable Group Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ImpConfigurableGroupType get(int value) {
		switch (value) {
			case NONE: return NONE_LITERAL;
			case AND: return AND_LITERAL;
			case OR: return OR_LITERAL;
			case XOR: return XOR_LITERAL;
		}
		return null;
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ImpConfigurableGroupType(int value, String name, String literal) {
		super(value, name, literal);
	}

} //ImpConfigurableGroupType
