/**
 */
package com.prostep.vcontrol.model.feature.impl;

import com.prostep.vcontrol.model.feature.Feature;
import com.prostep.vcontrol.model.feature.FeaturePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class FeatureImpl extends FeatureNodeImpl implements Feature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return FeaturePackage.Literals.FEATURE;
	}

} //FeatureImpl
