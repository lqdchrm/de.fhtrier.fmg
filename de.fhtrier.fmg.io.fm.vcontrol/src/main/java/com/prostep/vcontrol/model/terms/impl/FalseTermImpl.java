/**
 */
package com.prostep.vcontrol.model.terms.impl;

import com.prostep.vcontrol.model.common.impl.ImpTermImpl;

import com.prostep.vcontrol.model.terms.FalseTerm;
import com.prostep.vcontrol.model.terms.TermsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>False Term</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class FalseTermImpl extends ImpTermImpl implements FalseTerm {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FalseTermImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return TermsPackage.Literals.FALSE_TERM;
	}

} //FalseTermImpl
