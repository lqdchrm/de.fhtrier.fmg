/**
 */
package com.prostep.vcontrol.model.common;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Imp Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpProject#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpProject#getModels <em>Models</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpProject#getImpConfigurations <em>Imp Configurations</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpProject#getImpExtensions <em>Imp Extensions</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpProject()
 * @model extendedMetaData="name='ImpProject' kind='elementOnly'"
 * @generated
 */
public interface ImpProject extends ImpItem {
	/**
	 * Returns the value of the '<em><b>Group1</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group1</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group1</em>' attribute list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpProject_Group1()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:13'"
	 * @generated
	 */
	FeatureMap getGroup1();

	/**
	 * Returns the value of the '<em><b>Models</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Models</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpProject_Models()
	 * @model type="com.prostep.vcontrol.model.common.ImpModel" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='models' group='#group:13'"
	 * @generated
	 */
	EList getModels();

	/**
	 * Returns the value of the '<em><b>Imp Configurations</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpConfiguration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Configurations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Configurations</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpProject_ImpConfigurations()
	 * @model type="com.prostep.vcontrol.model.common.ImpConfiguration" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='impConfigurations' group='#group:13'"
	 * @generated
	 */
	EList getImpConfigurations();

	/**
	 * Returns the value of the '<em><b>Imp Extensions</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpExtensibleElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Extensions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Extensions</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpProject_ImpExtensions()
	 * @model type="com.prostep.vcontrol.model.common.ImpExtensibleElement" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='impExtensions' group='#group:13'"
	 * @generated
	 */
	EList getImpExtensions();

} // ImpProject
