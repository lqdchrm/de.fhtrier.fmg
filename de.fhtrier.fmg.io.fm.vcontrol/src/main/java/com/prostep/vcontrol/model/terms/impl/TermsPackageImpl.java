/**
 */
package com.prostep.vcontrol.model.terms.impl;

import com.prostep.vcontrol.model.common.CommonPackage;

import com.prostep.vcontrol.model.common.impl.CommonPackageImpl;

import com.prostep.vcontrol.model.feature.FeaturePackage;

import com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl;

import com.prostep.vcontrol.model.terms.AbstractMathTerm;
import com.prostep.vcontrol.model.terms.AndTerm;
import com.prostep.vcontrol.model.terms.AtomicTerm;
import com.prostep.vcontrol.model.terms.BinaryTerm;
import com.prostep.vcontrol.model.terms.DocumentRoot;
import com.prostep.vcontrol.model.terms.EquivalentTerm;
import com.prostep.vcontrol.model.terms.ErrorStatus;
import com.prostep.vcontrol.model.terms.ErrorStatusTerm;
import com.prostep.vcontrol.model.terms.ExcludesTerm;
import com.prostep.vcontrol.model.terms.FalseTerm;
import com.prostep.vcontrol.model.terms.ImpliesTerm;
import com.prostep.vcontrol.model.terms.NaryTerm;
import com.prostep.vcontrol.model.terms.NotTerm;
import com.prostep.vcontrol.model.terms.OrTerm;
import com.prostep.vcontrol.model.terms.TermsFactory;
import com.prostep.vcontrol.model.terms.TermsPackage;
import com.prostep.vcontrol.model.terms.TrueTerm;
import com.prostep.vcontrol.model.terms.UnaryTerm;
import com.prostep.vcontrol.model.terms.XorTerm;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import xmi.XmiPackage;

import xmi.impl.XmiPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TermsPackageImpl extends EPackageImpl implements TermsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractMathTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass andTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass atomicTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equivalentTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass errorStatusTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass excludesTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass falseTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impliesTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass naryTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass trueTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unaryTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xorTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum errorStatusEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType errorStatusObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TermsPackageImpl() {
		super(eNS_URI, TermsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TermsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TermsPackage init() {
		if (isInited) return (TermsPackage)EPackage.Registry.INSTANCE.getEPackage(TermsPackage.eNS_URI);

		// Obtain or create and register package
		TermsPackageImpl theTermsPackage = (TermsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TermsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TermsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CommonPackageImpl theCommonPackage = (CommonPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) instanceof CommonPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI) : CommonPackage.eINSTANCE);
		FeaturePackageImpl theFeaturePackage = (FeaturePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FeaturePackage.eNS_URI) instanceof FeaturePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FeaturePackage.eNS_URI) : FeaturePackage.eINSTANCE);
		XmiPackageImpl theXmiPackage = (XmiPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(XmiPackage.eNS_URI) instanceof XmiPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(XmiPackage.eNS_URI) : XmiPackage.eINSTANCE);

		// Create package meta-data objects
		theTermsPackage.createPackageContents();
		theCommonPackage.createPackageContents();
		theFeaturePackage.createPackageContents();
		theXmiPackage.createPackageContents();

		// Initialize created meta-data
		theTermsPackage.initializePackageContents();
		theCommonPackage.initializePackageContents();
		theFeaturePackage.initializePackageContents();
		theXmiPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTermsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TermsPackage.eNS_URI, theTermsPackage);
		return theTermsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractMathTerm() {
		return abstractMathTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAndTerm() {
		return andTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAtomicTerm() {
		return atomicTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAtomicTerm_Element() {
		return (EAttribute)atomicTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBinaryTerm() {
		return binaryTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryTerm_Group1() {
		return (EAttribute)binaryTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinaryTerm_LeftOperand() {
		return (EReference)binaryTermEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinaryTerm_RightOperand() {
		return (EReference)binaryTermEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Mixed() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AbstractMathTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AndTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_AtomicTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_BinaryTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_EquivalentTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ErrorStatusTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ExcludesTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_FalseTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpliesTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_NaryTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_NotTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_OrTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_TrueTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_UnaryTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XorTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEquivalentTerm() {
		return equivalentTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getErrorStatusTerm() {
		return errorStatusTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getErrorStatusTerm_ErrorStatus() {
		return (EAttribute)errorStatusTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getErrorStatusTerm_Message() {
		return (EAttribute)errorStatusTermEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExcludesTerm() {
		return excludesTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFalseTerm() {
		return falseTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpliesTerm() {
		return impliesTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNaryTerm() {
		return naryTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNaryTerm_Group1() {
		return (EAttribute)naryTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNaryTerm_Operands() {
		return (EReference)naryTermEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNotTerm() {
		return notTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrTerm() {
		return orTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTrueTerm() {
		return trueTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnaryTerm() {
		return unaryTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnaryTerm_Group1() {
		return (EAttribute)unaryTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnaryTerm_Operand() {
		return (EReference)unaryTermEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXorTerm() {
		return xorTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getErrorStatus() {
		return errorStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getErrorStatusObject() {
		return errorStatusObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TermsFactory getTermsFactory() {
		return (TermsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		abstractMathTermEClass = createEClass(ABSTRACT_MATH_TERM);

		andTermEClass = createEClass(AND_TERM);

		atomicTermEClass = createEClass(ATOMIC_TERM);
		createEAttribute(atomicTermEClass, ATOMIC_TERM__ELEMENT);

		binaryTermEClass = createEClass(BINARY_TERM);
		createEAttribute(binaryTermEClass, BINARY_TERM__GROUP1);
		createEReference(binaryTermEClass, BINARY_TERM__LEFT_OPERAND);
		createEReference(binaryTermEClass, BINARY_TERM__RIGHT_OPERAND);

		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__ABSTRACT_MATH_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__AND_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__ATOMIC_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__BINARY_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__EQUIVALENT_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__ERROR_STATUS_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__EXCLUDES_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__FALSE_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMPLIES_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__NARY_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__NOT_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__OR_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__TRUE_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__UNARY_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XOR_TERM);

		equivalentTermEClass = createEClass(EQUIVALENT_TERM);

		errorStatusTermEClass = createEClass(ERROR_STATUS_TERM);
		createEAttribute(errorStatusTermEClass, ERROR_STATUS_TERM__ERROR_STATUS);
		createEAttribute(errorStatusTermEClass, ERROR_STATUS_TERM__MESSAGE);

		excludesTermEClass = createEClass(EXCLUDES_TERM);

		falseTermEClass = createEClass(FALSE_TERM);

		impliesTermEClass = createEClass(IMPLIES_TERM);

		naryTermEClass = createEClass(NARY_TERM);
		createEAttribute(naryTermEClass, NARY_TERM__GROUP1);
		createEReference(naryTermEClass, NARY_TERM__OPERANDS);

		notTermEClass = createEClass(NOT_TERM);

		orTermEClass = createEClass(OR_TERM);

		trueTermEClass = createEClass(TRUE_TERM);

		unaryTermEClass = createEClass(UNARY_TERM);
		createEAttribute(unaryTermEClass, UNARY_TERM__GROUP1);
		createEReference(unaryTermEClass, UNARY_TERM__OPERAND);

		xorTermEClass = createEClass(XOR_TERM);

		// Create enums
		errorStatusEEnum = createEEnum(ERROR_STATUS);

		// Create data types
		errorStatusObjectEDataType = createEDataType(ERROR_STATUS_OBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CommonPackage theCommonPackage = (CommonPackage)EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI);
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Add supertypes to classes
		abstractMathTermEClass.getESuperTypes().add(this.getAtomicTerm());
		andTermEClass.getESuperTypes().add(this.getNaryTerm());
		atomicTermEClass.getESuperTypes().add(theCommonPackage.getImpTerm());
		binaryTermEClass.getESuperTypes().add(theCommonPackage.getImpTerm());
		equivalentTermEClass.getESuperTypes().add(this.getBinaryTerm());
		errorStatusTermEClass.getESuperTypes().add(theCommonPackage.getImpTerm());
		excludesTermEClass.getESuperTypes().add(this.getBinaryTerm());
		falseTermEClass.getESuperTypes().add(theCommonPackage.getImpTerm());
		impliesTermEClass.getESuperTypes().add(this.getBinaryTerm());
		naryTermEClass.getESuperTypes().add(theCommonPackage.getImpTerm());
		notTermEClass.getESuperTypes().add(this.getUnaryTerm());
		orTermEClass.getESuperTypes().add(this.getNaryTerm());
		trueTermEClass.getESuperTypes().add(theCommonPackage.getImpTerm());
		unaryTermEClass.getESuperTypes().add(theCommonPackage.getImpTerm());
		xorTermEClass.getESuperTypes().add(this.getNaryTerm());

		// Initialize classes and features; add operations and parameters
		initEClass(abstractMathTermEClass, AbstractMathTerm.class, "AbstractMathTerm", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(andTermEClass, AndTerm.class, "AndTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(atomicTermEClass, AtomicTerm.class, "AtomicTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAtomicTerm_Element(), theXMLTypePackage.getString(), "element", null, 0, 1, AtomicTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(binaryTermEClass, BinaryTerm.class, "BinaryTerm", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBinaryTerm_Group1(), ecorePackage.getEFeatureMapEntry(), "group1", null, 0, -1, BinaryTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryTerm_LeftOperand(), theCommonPackage.getImpTerm(), null, "leftOperand", null, 0, -1, BinaryTerm.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryTerm_RightOperand(), theCommonPackage.getImpTerm(), null, "rightOperand", null, 0, -1, BinaryTerm.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_AbstractMathTerm(), this.getAbstractMathTerm(), null, "abstractMathTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_AndTerm(), this.getAndTerm(), null, "andTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_AtomicTerm(), this.getAtomicTerm(), null, "atomicTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_BinaryTerm(), this.getBinaryTerm(), null, "binaryTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_EquivalentTerm(), this.getEquivalentTerm(), null, "equivalentTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ErrorStatusTerm(), this.getErrorStatusTerm(), null, "errorStatusTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ExcludesTerm(), this.getExcludesTerm(), null, "excludesTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_FalseTerm(), this.getFalseTerm(), null, "falseTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpliesTerm(), this.getImpliesTerm(), null, "impliesTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_NaryTerm(), this.getNaryTerm(), null, "naryTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_NotTerm(), this.getNotTerm(), null, "notTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_OrTerm(), this.getOrTerm(), null, "orTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_TrueTerm(), this.getTrueTerm(), null, "trueTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_UnaryTerm(), this.getUnaryTerm(), null, "unaryTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XorTerm(), this.getXorTerm(), null, "xorTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(equivalentTermEClass, EquivalentTerm.class, "EquivalentTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(errorStatusTermEClass, ErrorStatusTerm.class, "ErrorStatusTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getErrorStatusTerm_ErrorStatus(), this.getErrorStatus(), "errorStatus", null, 1, 1, ErrorStatusTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getErrorStatusTerm_Message(), theXMLTypePackage.getString(), "message", null, 1, 1, ErrorStatusTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(excludesTermEClass, ExcludesTerm.class, "ExcludesTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(falseTermEClass, FalseTerm.class, "FalseTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(impliesTermEClass, ImpliesTerm.class, "ImpliesTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(naryTermEClass, NaryTerm.class, "NaryTerm", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNaryTerm_Group1(), ecorePackage.getEFeatureMapEntry(), "group1", null, 0, -1, NaryTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNaryTerm_Operands(), theCommonPackage.getImpTerm(), null, "operands", null, 0, -1, NaryTerm.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(notTermEClass, NotTerm.class, "NotTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(orTermEClass, OrTerm.class, "OrTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(trueTermEClass, TrueTerm.class, "TrueTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(unaryTermEClass, UnaryTerm.class, "UnaryTerm", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUnaryTerm_Group1(), ecorePackage.getEFeatureMapEntry(), "group1", null, 0, -1, UnaryTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnaryTerm_Operand(), theCommonPackage.getImpTerm(), null, "operand", null, 0, -1, UnaryTerm.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(xorTermEClass, XorTerm.class, "XorTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(errorStatusEEnum, ErrorStatus.class, "ErrorStatus");
		addEEnumLiteral(errorStatusEEnum, ErrorStatus.OK_LITERAL);
		addEEnumLiteral(errorStatusEEnum, ErrorStatus.ERROR_LITERAL);

		// Initialize data types
		initEDataType(errorStatusObjectEDataType, ErrorStatus.class, "ErrorStatusObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";		
		addAnnotation
		  (abstractMathTermEClass, 
		   source, 
		   new String[] {
			 "name", "AbstractMathTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (andTermEClass, 
		   source, 
		   new String[] {
			 "name", "AndTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (atomicTermEClass, 
		   source, 
		   new String[] {
			 "name", "AtomicTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getAtomicTerm_Element(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "element"
		   });		
		addAnnotation
		  (binaryTermEClass, 
		   source, 
		   new String[] {
			 "name", "BinaryTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getBinaryTerm_Group1(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:9"
		   });		
		addAnnotation
		  (getBinaryTerm_LeftOperand(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "leftOperand",
			 "group", "#group:9"
		   });		
		addAnnotation
		  (getBinaryTerm_RightOperand(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "rightOperand",
			 "group", "#group:9"
		   });		
		addAnnotation
		  (documentRootEClass, 
		   source, 
		   new String[] {
			 "name", "",
			 "kind", "mixed"
		   });		
		addAnnotation
		  (getDocumentRoot_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });		
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xmlns:prefix"
		   });		
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xsi:schemaLocation"
		   });		
		addAnnotation
		  (getDocumentRoot_AbstractMathTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "AbstractMathTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_AndTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "AndTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_AtomicTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "AtomicTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_BinaryTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "BinaryTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_EquivalentTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "EquivalentTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ErrorStatusTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ErrorStatusTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ExcludesTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ExcludesTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_FalseTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "FalseTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpliesTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpliesTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_NaryTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "NaryTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_NotTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "NotTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_OrTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "OrTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_TrueTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "TrueTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_UnaryTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "UnaryTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_XorTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "XorTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (equivalentTermEClass, 
		   source, 
		   new String[] {
			 "name", "EquivalentTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (errorStatusEEnum, 
		   source, 
		   new String[] {
			 "name", "ErrorStatus"
		   });		
		addAnnotation
		  (errorStatusObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "ErrorStatus:Object",
			 "baseType", "ErrorStatus"
		   });		
		addAnnotation
		  (errorStatusTermEClass, 
		   source, 
		   new String[] {
			 "name", "ErrorStatusTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getErrorStatusTerm_ErrorStatus(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "errorStatus"
		   });		
		addAnnotation
		  (getErrorStatusTerm_Message(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "message"
		   });		
		addAnnotation
		  (excludesTermEClass, 
		   source, 
		   new String[] {
			 "name", "ExcludesTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (falseTermEClass, 
		   source, 
		   new String[] {
			 "name", "FalseTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (impliesTermEClass, 
		   source, 
		   new String[] {
			 "name", "ImpliesTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (naryTermEClass, 
		   source, 
		   new String[] {
			 "name", "NaryTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getNaryTerm_Group1(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:9"
		   });		
		addAnnotation
		  (getNaryTerm_Operands(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "operands",
			 "group", "#group:9"
		   });		
		addAnnotation
		  (notTermEClass, 
		   source, 
		   new String[] {
			 "name", "NotTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (orTermEClass, 
		   source, 
		   new String[] {
			 "name", "OrTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (trueTermEClass, 
		   source, 
		   new String[] {
			 "name", "TrueTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (unaryTermEClass, 
		   source, 
		   new String[] {
			 "name", "UnaryTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getUnaryTerm_Group1(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:9"
		   });		
		addAnnotation
		  (getUnaryTerm_Operand(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "operand",
			 "group", "#group:9"
		   });		
		addAnnotation
		  (xorTermEClass, 
		   source, 
		   new String[] {
			 "name", "XorTerm",
			 "kind", "elementOnly"
		   });
	}

} //TermsPackageImpl
