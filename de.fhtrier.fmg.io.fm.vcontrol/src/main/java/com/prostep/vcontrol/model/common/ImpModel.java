/**
 */
package com.prostep.vcontrol.model.common;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Imp Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpModel#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpModel#getProject <em>Project</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpModel#getImpConfigurations <em>Imp Configurations</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpModel#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpModel#getImpConfigurations1 <em>Imp Configurations1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpModel#getProject1 <em>Project1</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpModel()
 * @model abstract="true"
 *        extendedMetaData="name='ImpModel' kind='elementOnly'"
 * @generated
 */
public interface ImpModel extends ImpItem {
	/**
	 * Returns the value of the '<em><b>Group1</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group1</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group1</em>' attribute list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpModel_Group1()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:13'"
	 * @generated
	 */
	FeatureMap getGroup1();

	/**
	 * Returns the value of the '<em><b>Project</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpProject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpModel_Project()
	 * @model type="com.prostep.vcontrol.model.common.ImpProject" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='project' group='#group:13'"
	 * @generated
	 */
	EList getProject();

	/**
	 * Returns the value of the '<em><b>Imp Configurations</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpConfiguration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Configurations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Configurations</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpModel_ImpConfigurations()
	 * @model type="com.prostep.vcontrol.model.common.ImpConfiguration" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='impConfigurations' group='#group:13'"
	 * @generated
	 */
	EList getImpConfigurations();

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.Constraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpModel_Constraints()
	 * @model type="com.prostep.vcontrol.model.common.Constraint" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='constraints' group='#group:13'"
	 * @generated
	 */
	EList getConstraints();

	/**
	 * Returns the value of the '<em><b>Imp Configurations1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Configurations1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Configurations1</em>' attribute.
	 * @see #setImpConfigurations1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpModel_ImpConfigurations1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='impConfigurations'"
	 * @generated
	 */
	String getImpConfigurations1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpModel#getImpConfigurations1 <em>Imp Configurations1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Configurations1</em>' attribute.
	 * @see #getImpConfigurations1()
	 * @generated
	 */
	void setImpConfigurations1(String value);

	/**
	 * Returns the value of the '<em><b>Project1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project1</em>' attribute.
	 * @see #setProject1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpModel_Project1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='project'"
	 * @generated
	 */
	String getProject1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpModel#getProject1 <em>Project1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project1</em>' attribute.
	 * @see #getProject1()
	 * @generated
	 */
	void setProject1(String value);

} // ImpModel
