/**
 */
package com.prostep.vcontrol.model.feature;

import com.prostep.vcontrol.model.common.ImpModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureModel#getGroup2 <em>Group2</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureModel#getRootFeature <em>Root Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureModel()
 * @model extendedMetaData="name='FeatureModel' kind='elementOnly'"
 * @generated
 */
public interface FeatureModel extends ImpModel {
	/**
	 * Returns the value of the '<em><b>Group2</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group2</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group2</em>' attribute list.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureModel_Group2()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:19'"
	 * @generated
	 */
	FeatureMap getGroup2();

	/**
	 * Returns the value of the '<em><b>Root Feature</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.feature.FeatureNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Feature</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Feature</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureModel_RootFeature()
	 * @model type="com.prostep.vcontrol.model.feature.FeatureNode" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='rootFeature' group='#group:19'"
	 * @generated
	 */
	EList getRootFeature();

} // FeatureModel
