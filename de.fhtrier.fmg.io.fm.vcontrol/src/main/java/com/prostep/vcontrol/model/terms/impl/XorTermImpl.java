/**
 */
package com.prostep.vcontrol.model.terms.impl;

import com.prostep.vcontrol.model.terms.TermsPackage;
import com.prostep.vcontrol.model.terms.XorTerm;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Xor Term</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class XorTermImpl extends NaryTermImpl implements XorTerm {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XorTermImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return TermsPackage.Literals.XOR_TERM;
	}

} //XorTermImpl
