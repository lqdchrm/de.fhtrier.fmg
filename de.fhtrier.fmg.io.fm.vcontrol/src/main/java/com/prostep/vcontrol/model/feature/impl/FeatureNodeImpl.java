/**
 */
package com.prostep.vcontrol.model.feature.impl;

import com.prostep.vcontrol.model.common.impl.ImpNodeImpl;

import com.prostep.vcontrol.model.feature.FeatureNode;
import com.prostep.vcontrol.model.feature.FeaturePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl#getGroup2 <em>Group2</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl#getChildren <em>Children</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl#getModel <em>Model</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl#getOccurrences <em>Occurrences</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl#getModel1 <em>Model1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl#getOccurrences1 <em>Occurrences1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl#getParent1 <em>Parent1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class FeatureNodeImpl extends ImpNodeImpl implements FeatureNode {
	/**
	 * The cached value of the '{@link #getGroup2() <em>Group2</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup2()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group2;

	/**
	 * The default value of the '{@link #getModel1() <em>Model1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel1()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModel1() <em>Model1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel1()
	 * @generated
	 * @ordered
	 */
	protected String model1 = MODEL1_EDEFAULT;

	/**
	 * The default value of the '{@link #getOccurrences1() <em>Occurrences1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOccurrences1()
	 * @generated
	 * @ordered
	 */
	protected static final String OCCURRENCES1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOccurrences1() <em>Occurrences1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOccurrences1()
	 * @generated
	 * @ordered
	 */
	protected String occurrences1 = OCCURRENCES1_EDEFAULT;

	/**
	 * The default value of the '{@link #getParent1() <em>Parent1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent1()
	 * @generated
	 * @ordered
	 */
	protected static final String PARENT1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getParent1() <em>Parent1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent1()
	 * @generated
	 * @ordered
	 */
	protected String parent1 = PARENT1_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return FeaturePackage.Literals.FEATURE_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup2() {
		if (group2 == null) {
			group2 = new BasicFeatureMap(this, FeaturePackage.FEATURE_NODE__GROUP2);
		}
		return group2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getParent() {
		return getGroup2().list(FeaturePackage.Literals.FEATURE_NODE__PARENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getChildren() {
		return getGroup2().list(FeaturePackage.Literals.FEATURE_NODE__CHILDREN);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getModel() {
		return getGroup2().list(FeaturePackage.Literals.FEATURE_NODE__MODEL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getOccurrences() {
		return getGroup2().list(FeaturePackage.Literals.FEATURE_NODE__OCCURRENCES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModel1() {
		return model1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModel1(String newModel1) {
		String oldModel1 = model1;
		model1 = newModel1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeaturePackage.FEATURE_NODE__MODEL1, oldModel1, model1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOccurrences1() {
		return occurrences1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOccurrences1(String newOccurrences1) {
		String oldOccurrences1 = occurrences1;
		occurrences1 = newOccurrences1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeaturePackage.FEATURE_NODE__OCCURRENCES1, oldOccurrences1, occurrences1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getParent1() {
		return parent1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent1(String newParent1) {
		String oldParent1 = parent1;
		parent1 = newParent1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeaturePackage.FEATURE_NODE__PARENT1, oldParent1, parent1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FeaturePackage.FEATURE_NODE__GROUP2:
				return ((InternalEList)getGroup2()).basicRemove(otherEnd, msgs);
			case FeaturePackage.FEATURE_NODE__PARENT:
				return ((InternalEList)getParent()).basicRemove(otherEnd, msgs);
			case FeaturePackage.FEATURE_NODE__CHILDREN:
				return ((InternalEList)getChildren()).basicRemove(otherEnd, msgs);
			case FeaturePackage.FEATURE_NODE__MODEL:
				return ((InternalEList)getModel()).basicRemove(otherEnd, msgs);
			case FeaturePackage.FEATURE_NODE__OCCURRENCES:
				return ((InternalEList)getOccurrences()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FeaturePackage.FEATURE_NODE__GROUP2:
				if (coreType) return getGroup2();
				return ((FeatureMap.Internal)getGroup2()).getWrapper();
			case FeaturePackage.FEATURE_NODE__PARENT:
				return getParent();
			case FeaturePackage.FEATURE_NODE__CHILDREN:
				return getChildren();
			case FeaturePackage.FEATURE_NODE__MODEL:
				return getModel();
			case FeaturePackage.FEATURE_NODE__OCCURRENCES:
				return getOccurrences();
			case FeaturePackage.FEATURE_NODE__MODEL1:
				return getModel1();
			case FeaturePackage.FEATURE_NODE__OCCURRENCES1:
				return getOccurrences1();
			case FeaturePackage.FEATURE_NODE__PARENT1:
				return getParent1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FeaturePackage.FEATURE_NODE__GROUP2:
				((FeatureMap.Internal)getGroup2()).set(newValue);
				return;
			case FeaturePackage.FEATURE_NODE__PARENT:
				getParent().clear();
				getParent().addAll((Collection)newValue);
				return;
			case FeaturePackage.FEATURE_NODE__CHILDREN:
				getChildren().clear();
				getChildren().addAll((Collection)newValue);
				return;
			case FeaturePackage.FEATURE_NODE__MODEL:
				getModel().clear();
				getModel().addAll((Collection)newValue);
				return;
			case FeaturePackage.FEATURE_NODE__OCCURRENCES:
				getOccurrences().clear();
				getOccurrences().addAll((Collection)newValue);
				return;
			case FeaturePackage.FEATURE_NODE__MODEL1:
				setModel1((String)newValue);
				return;
			case FeaturePackage.FEATURE_NODE__OCCURRENCES1:
				setOccurrences1((String)newValue);
				return;
			case FeaturePackage.FEATURE_NODE__PARENT1:
				setParent1((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case FeaturePackage.FEATURE_NODE__GROUP2:
				getGroup2().clear();
				return;
			case FeaturePackage.FEATURE_NODE__PARENT:
				getParent().clear();
				return;
			case FeaturePackage.FEATURE_NODE__CHILDREN:
				getChildren().clear();
				return;
			case FeaturePackage.FEATURE_NODE__MODEL:
				getModel().clear();
				return;
			case FeaturePackage.FEATURE_NODE__OCCURRENCES:
				getOccurrences().clear();
				return;
			case FeaturePackage.FEATURE_NODE__MODEL1:
				setModel1(MODEL1_EDEFAULT);
				return;
			case FeaturePackage.FEATURE_NODE__OCCURRENCES1:
				setOccurrences1(OCCURRENCES1_EDEFAULT);
				return;
			case FeaturePackage.FEATURE_NODE__PARENT1:
				setParent1(PARENT1_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FeaturePackage.FEATURE_NODE__GROUP2:
				return group2 != null && !group2.isEmpty();
			case FeaturePackage.FEATURE_NODE__PARENT:
				return !getParent().isEmpty();
			case FeaturePackage.FEATURE_NODE__CHILDREN:
				return !getChildren().isEmpty();
			case FeaturePackage.FEATURE_NODE__MODEL:
				return !getModel().isEmpty();
			case FeaturePackage.FEATURE_NODE__OCCURRENCES:
				return !getOccurrences().isEmpty();
			case FeaturePackage.FEATURE_NODE__MODEL1:
				return MODEL1_EDEFAULT == null ? model1 != null : !MODEL1_EDEFAULT.equals(model1);
			case FeaturePackage.FEATURE_NODE__OCCURRENCES1:
				return OCCURRENCES1_EDEFAULT == null ? occurrences1 != null : !OCCURRENCES1_EDEFAULT.equals(occurrences1);
			case FeaturePackage.FEATURE_NODE__PARENT1:
				return PARENT1_EDEFAULT == null ? parent1 != null : !PARENT1_EDEFAULT.equals(parent1);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group2: ");
		result.append(group2);
		result.append(", model1: ");
		result.append(model1);
		result.append(", occurrences1: ");
		result.append(occurrences1);
		result.append(", parent1: ");
		result.append(parent1);
		result.append(')');
		return result.toString();
	}

} //FeatureNodeImpl
