/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.CommonPackage;
import com.prostep.vcontrol.model.common.ImpConfigurableElement;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Imp Configurable Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl#getImpConfiguredElements <em>Imp Configured Elements</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl#getRightElements <em>Right Elements</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl#getLeftElements <em>Left Elements</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl#getExtensions <em>Extensions</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl#getImpConfiguredElements1 <em>Imp Configured Elements1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl#isOptional <em>Optional</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl#getRightElements1 <em>Right Elements1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ImpConfigurableElementImpl extends ImpItemImpl implements ImpConfigurableElement {
	/**
	 * The cached value of the '{@link #getGroup1() <em>Group1</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup1()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group1;

	/**
	 * The default value of the '{@link #getImpConfiguredElements1() <em>Imp Configured Elements1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpConfiguredElements1()
	 * @generated
	 * @ordered
	 */
	protected static final String IMP_CONFIGURED_ELEMENTS1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImpConfiguredElements1() <em>Imp Configured Elements1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpConfiguredElements1()
	 * @generated
	 * @ordered
	 */
	protected String impConfiguredElements1 = IMP_CONFIGURED_ELEMENTS1_EDEFAULT;

	/**
	 * The default value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected static final boolean OPTIONAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOptional() <em>Optional</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOptional()
	 * @generated
	 * @ordered
	 */
	protected boolean optional = OPTIONAL_EDEFAULT;

	/**
	 * This is true if the Optional attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean optionalESet;

	/**
	 * The default value of the '{@link #getRightElements1() <em>Right Elements1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightElements1()
	 * @generated
	 * @ordered
	 */
	protected static final String RIGHT_ELEMENTS1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRightElements1() <em>Right Elements1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightElements1()
	 * @generated
	 * @ordered
	 */
	protected String rightElements1 = RIGHT_ELEMENTS1_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImpConfigurableElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CommonPackage.Literals.IMP_CONFIGURABLE_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup1() {
		if (group1 == null) {
			group1 = new BasicFeatureMap(this, CommonPackage.IMP_CONFIGURABLE_ELEMENT__GROUP1);
		}
		return group1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getImpConfiguredElements() {
		return getGroup1().list(CommonPackage.Literals.IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRightElements() {
		return getGroup1().list(CommonPackage.Literals.IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getLeftElements() {
		return getGroup1().list(CommonPackage.Literals.IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getExtensions() {
		return getGroup1().list(CommonPackage.Literals.IMP_CONFIGURABLE_ELEMENT__EXTENSIONS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImpConfiguredElements1() {
		return impConfiguredElements1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpConfiguredElements1(String newImpConfiguredElements1) {
		String oldImpConfiguredElements1 = impConfiguredElements1;
		impConfiguredElements1 = newImpConfiguredElements1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS1, oldImpConfiguredElements1, impConfiguredElements1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOptional() {
		return optional;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptional(boolean newOptional) {
		boolean oldOptional = optional;
		optional = newOptional;
		boolean oldOptionalESet = optionalESet;
		optionalESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURABLE_ELEMENT__OPTIONAL, oldOptional, optional, !oldOptionalESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOptional() {
		boolean oldOptional = optional;
		boolean oldOptionalESet = optionalESet;
		optional = OPTIONAL_EDEFAULT;
		optionalESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.IMP_CONFIGURABLE_ELEMENT__OPTIONAL, oldOptional, OPTIONAL_EDEFAULT, oldOptionalESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOptional() {
		return optionalESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRightElements1() {
		return rightElements1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightElements1(String newRightElements1) {
		String oldRightElements1 = rightElements1;
		rightElements1 = newRightElements1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS1, oldRightElements1, rightElements1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__GROUP1:
				return ((InternalEList)getGroup1()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS:
				return ((InternalEList)getImpConfiguredElements()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS:
				return ((InternalEList)getRightElements()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS:
				return ((InternalEList)getLeftElements()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__EXTENSIONS:
				return ((InternalEList)getExtensions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__GROUP1:
				if (coreType) return getGroup1();
				return ((FeatureMap.Internal)getGroup1()).getWrapper();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS:
				return getImpConfiguredElements();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS:
				return getRightElements();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS:
				return getLeftElements();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__EXTENSIONS:
				return getExtensions();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS1:
				return getImpConfiguredElements1();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__OPTIONAL:
				return isOptional() ? Boolean.TRUE : Boolean.FALSE;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS1:
				return getRightElements1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__GROUP1:
				((FeatureMap.Internal)getGroup1()).set(newValue);
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS:
				getImpConfiguredElements().clear();
				getImpConfiguredElements().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS:
				getRightElements().clear();
				getRightElements().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS:
				getLeftElements().clear();
				getLeftElements().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__EXTENSIONS:
				getExtensions().clear();
				getExtensions().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS1:
				setImpConfiguredElements1((String)newValue);
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__OPTIONAL:
				setOptional(((Boolean)newValue).booleanValue());
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS1:
				setRightElements1((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__GROUP1:
				getGroup1().clear();
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS:
				getImpConfiguredElements().clear();
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS:
				getRightElements().clear();
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS:
				getLeftElements().clear();
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__EXTENSIONS:
				getExtensions().clear();
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS1:
				setImpConfiguredElements1(IMP_CONFIGURED_ELEMENTS1_EDEFAULT);
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__OPTIONAL:
				unsetOptional();
				return;
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS1:
				setRightElements1(RIGHT_ELEMENTS1_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__GROUP1:
				return group1 != null && !group1.isEmpty();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS:
				return !getImpConfiguredElements().isEmpty();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS:
				return !getRightElements().isEmpty();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS:
				return !getLeftElements().isEmpty();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__EXTENSIONS:
				return !getExtensions().isEmpty();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS1:
				return IMP_CONFIGURED_ELEMENTS1_EDEFAULT == null ? impConfiguredElements1 != null : !IMP_CONFIGURED_ELEMENTS1_EDEFAULT.equals(impConfiguredElements1);
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__OPTIONAL:
				return isSetOptional();
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS1:
				return RIGHT_ELEMENTS1_EDEFAULT == null ? rightElements1 != null : !RIGHT_ELEMENTS1_EDEFAULT.equals(rightElements1);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group1: ");
		result.append(group1);
		result.append(", impConfiguredElements1: ");
		result.append(impConfiguredElements1);
		result.append(", optional: ");
		if (optionalESet) result.append(optional); else result.append("<unset>");
		result.append(", rightElements1: ");
		result.append(rightElements1);
		result.append(')');
		return result.toString();
	}

} //ImpConfigurableElementImpl
