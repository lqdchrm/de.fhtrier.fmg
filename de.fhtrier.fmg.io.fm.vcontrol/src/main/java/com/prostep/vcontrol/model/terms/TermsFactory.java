/**
 */
package com.prostep.vcontrol.model.terms;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.terms.TermsPackage
 * @generated
 */
public interface TermsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TermsFactory eINSTANCE = com.prostep.vcontrol.model.terms.impl.TermsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>And Term</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>And Term</em>'.
	 * @generated
	 */
	AndTerm createAndTerm();

	/**
	 * Returns a new object of class '<em>Atomic Term</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Atomic Term</em>'.
	 * @generated
	 */
	AtomicTerm createAtomicTerm();

	/**
	 * Returns a new object of class '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document Root</em>'.
	 * @generated
	 */
	DocumentRoot createDocumentRoot();

	/**
	 * Returns a new object of class '<em>Equivalent Term</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equivalent Term</em>'.
	 * @generated
	 */
	EquivalentTerm createEquivalentTerm();

	/**
	 * Returns a new object of class '<em>Error Status Term</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Error Status Term</em>'.
	 * @generated
	 */
	ErrorStatusTerm createErrorStatusTerm();

	/**
	 * Returns a new object of class '<em>Excludes Term</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Excludes Term</em>'.
	 * @generated
	 */
	ExcludesTerm createExcludesTerm();

	/**
	 * Returns a new object of class '<em>False Term</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>False Term</em>'.
	 * @generated
	 */
	FalseTerm createFalseTerm();

	/**
	 * Returns a new object of class '<em>Implies Term</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Implies Term</em>'.
	 * @generated
	 */
	ImpliesTerm createImpliesTerm();

	/**
	 * Returns a new object of class '<em>Not Term</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not Term</em>'.
	 * @generated
	 */
	NotTerm createNotTerm();

	/**
	 * Returns a new object of class '<em>Or Term</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Or Term</em>'.
	 * @generated
	 */
	OrTerm createOrTerm();

	/**
	 * Returns a new object of class '<em>True Term</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>True Term</em>'.
	 * @generated
	 */
	TrueTerm createTrueTerm();

	/**
	 * Returns a new object of class '<em>Xor Term</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Xor Term</em>'.
	 * @generated
	 */
	XorTerm createXorTerm();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TermsPackage getTermsPackage();

} //TermsFactory
