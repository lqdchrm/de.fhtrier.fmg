/**
 */
package com.prostep.vcontrol.model.terms;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not Term</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getNotTerm()
 * @model extendedMetaData="name='NotTerm' kind='elementOnly'"
 * @generated
 */
public interface NotTerm extends UnaryTerm {
} // NotTerm
