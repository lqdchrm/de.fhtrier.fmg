/**
 */
package com.prostep.vcontrol.model.feature.util;

import com.prostep.vcontrol.model.common.ImpConfigurableElement;
import com.prostep.vcontrol.model.common.ImpItem;
import com.prostep.vcontrol.model.common.ImpModel;
import com.prostep.vcontrol.model.common.ImpNode;
import com.prostep.vcontrol.model.common.ImpTerm;
import com.prostep.vcontrol.model.common.PropertySet;

import com.prostep.vcontrol.model.feature.*;

import com.prostep.vcontrol.model.terms.AtomicTerm;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.feature.FeaturePackage
 * @generated
 */
public class FeatureAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FeaturePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = FeaturePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureSwitch modelSwitch =
		new FeatureSwitch() {
			public Object caseDocumentRoot(DocumentRoot object) {
				return createDocumentRootAdapter();
			}
			public Object caseFeature(Feature object) {
				return createFeatureAdapter();
			}
			public Object caseFeatureGroup(FeatureGroup object) {
				return createFeatureGroupAdapter();
			}
			public Object caseFeatureModel(FeatureModel object) {
				return createFeatureModelAdapter();
			}
			public Object caseFeatureNode(FeatureNode object) {
				return createFeatureNodeAdapter();
			}
			public Object caseFeatureOccurrence(FeatureOccurrence object) {
				return createFeatureOccurrenceAdapter();
			}
			public Object casePropertySet(PropertySet object) {
				return createPropertySetAdapter();
			}
			public Object caseImpItem(ImpItem object) {
				return createImpItemAdapter();
			}
			public Object caseImpConfigurableElement(ImpConfigurableElement object) {
				return createImpConfigurableElementAdapter();
			}
			public Object caseImpNode(ImpNode object) {
				return createImpNodeAdapter();
			}
			public Object caseImpModel(ImpModel object) {
				return createImpModelAdapter();
			}
			public Object caseImpTerm(ImpTerm object) {
				return createImpTermAdapter();
			}
			public Object caseAtomicTerm(AtomicTerm object) {
				return createAtomicTermAdapter();
			}
			public Object defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter)modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.feature.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.feature.DocumentRoot
	 * @generated
	 */
	public Adapter createDocumentRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.feature.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.feature.Feature
	 * @generated
	 */
	public Adapter createFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.feature.FeatureGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.feature.FeatureGroup
	 * @generated
	 */
	public Adapter createFeatureGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.feature.FeatureModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.feature.FeatureModel
	 * @generated
	 */
	public Adapter createFeatureModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.feature.FeatureNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.feature.FeatureNode
	 * @generated
	 */
	public Adapter createFeatureNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.feature.FeatureOccurrence <em>Occurrence</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.feature.FeatureOccurrence
	 * @generated
	 */
	public Adapter createFeatureOccurrenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.PropertySet <em>Property Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.PropertySet
	 * @generated
	 */
	public Adapter createPropertySetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpItem <em>Imp Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpItem
	 * @generated
	 */
	public Adapter createImpItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement <em>Imp Configurable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElement
	 * @generated
	 */
	public Adapter createImpConfigurableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpNode <em>Imp Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpNode
	 * @generated
	 */
	public Adapter createImpNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpModel <em>Imp Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpModel
	 * @generated
	 */
	public Adapter createImpModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpTerm <em>Imp Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpTerm
	 * @generated
	 */
	public Adapter createImpTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.AtomicTerm <em>Atomic Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.AtomicTerm
	 * @generated
	 */
	public Adapter createAtomicTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //FeatureAdapterFactory
