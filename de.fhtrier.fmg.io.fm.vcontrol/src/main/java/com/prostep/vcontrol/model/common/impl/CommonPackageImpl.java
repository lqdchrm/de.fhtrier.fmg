/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.CommonFactory;
import com.prostep.vcontrol.model.common.CommonPackage;
import com.prostep.vcontrol.model.common.Constraint;
import com.prostep.vcontrol.model.common.DescribedItem;
import com.prostep.vcontrol.model.common.DocumentRoot;
import com.prostep.vcontrol.model.common.Identifiable;
import com.prostep.vcontrol.model.common.ImpConfigurableElement;
import com.prostep.vcontrol.model.common.ImpConfigurableElementExtension;
import com.prostep.vcontrol.model.common.ImpConfigurableGroup;
import com.prostep.vcontrol.model.common.ImpConfigurableGroupType;
import com.prostep.vcontrol.model.common.ImpConfiguration;
import com.prostep.vcontrol.model.common.ImpConfiguredElement;
import com.prostep.vcontrol.model.common.ImpConnector;
import com.prostep.vcontrol.model.common.ImpExtensibleElement;
import com.prostep.vcontrol.model.common.ImpItem;
import com.prostep.vcontrol.model.common.ImpLinkingRelation;
import com.prostep.vcontrol.model.common.ImpModel;
import com.prostep.vcontrol.model.common.ImpNode;
import com.prostep.vcontrol.model.common.ImpPortNode;
import com.prostep.vcontrol.model.common.ImpProject;
import com.prostep.vcontrol.model.common.ImpTerm;
import com.prostep.vcontrol.model.common.LinkingType;
import com.prostep.vcontrol.model.common.NamedItem;
import com.prostep.vcontrol.model.common.PortDirection;
import com.prostep.vcontrol.model.common.Property;
import com.prostep.vcontrol.model.common.PropertySet;
import com.prostep.vcontrol.model.common.SelectionKind;

import com.prostep.vcontrol.model.feature.FeaturePackage;

import com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl;

import com.prostep.vcontrol.model.terms.TermsPackage;

import com.prostep.vcontrol.model.terms.impl.TermsPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import xmi.XmiPackage;

import xmi.impl.XmiPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CommonPackageImpl extends EPackageImpl implements CommonPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass describedItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identifiableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impConfigurableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impConfigurableElementExtensionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impConfigurableGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impConfiguredElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impConnectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impExtensibleElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impLinkingRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impPortNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impProjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass impTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertySetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum impConfigurableGroupTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum linkingTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum portDirectionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum selectionKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType impConfigurableGroupTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType linkingTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType portDirectionObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType selectionKindObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.prostep.vcontrol.model.common.CommonPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CommonPackageImpl() {
		super(eNS_URI, CommonFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CommonPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CommonPackage init() {
		if (isInited) return (CommonPackage)EPackage.Registry.INSTANCE.getEPackage(CommonPackage.eNS_URI);

		// Obtain or create and register package
		CommonPackageImpl theCommonPackage = (CommonPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CommonPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CommonPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		FeaturePackageImpl theFeaturePackage = (FeaturePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FeaturePackage.eNS_URI) instanceof FeaturePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FeaturePackage.eNS_URI) : FeaturePackage.eINSTANCE);
		TermsPackageImpl theTermsPackage = (TermsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TermsPackage.eNS_URI) instanceof TermsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TermsPackage.eNS_URI) : TermsPackage.eINSTANCE);
		XmiPackageImpl theXmiPackage = (XmiPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(XmiPackage.eNS_URI) instanceof XmiPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(XmiPackage.eNS_URI) : XmiPackage.eINSTANCE);

		// Create package meta-data objects
		theCommonPackage.createPackageContents();
		theFeaturePackage.createPackageContents();
		theTermsPackage.createPackageContents();
		theXmiPackage.createPackageContents();

		// Initialize created meta-data
		theCommonPackage.initializePackageContents();
		theFeaturePackage.initializePackageContents();
		theTermsPackage.initializePackageContents();
		theXmiPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCommonPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CommonPackage.eNS_URI, theCommonPackage);
		return theCommonPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstraint() {
		return constraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConstraint_Group1() {
		return (EAttribute)constraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstraint_RootTerm() {
		return (EReference)constraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstraint_Model() {
		return (EReference)constraintEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConstraint_Ignore() {
		return (EAttribute)constraintEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConstraint_Model1() {
		return (EAttribute)constraintEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDescribedItem() {
		return describedItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDescribedItem_Group() {
		return (EAttribute)describedItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDescribedItem_Extension() {
		return (EReference)describedItemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDescribedItem_Description() {
		return (EAttribute)describedItemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDescribedItem_Href() {
		return (EAttribute)describedItemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDescribedItem_Id() {
		return (EAttribute)describedItemEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDescribedItem_Idref() {
		return (EAttribute)describedItemEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDescribedItem_Label() {
		return (EAttribute)describedItemEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDescribedItem_Type() {
		return (EAttribute)describedItemEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDescribedItem_Uuid() {
		return (EAttribute)describedItemEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDescribedItem_Version() {
		return (EAttribute)describedItemEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIdentifiable() {
		return identifiableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifiable_Group() {
		return (EAttribute)identifiableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIdentifiable_Extension() {
		return (EReference)identifiableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifiable_Href() {
		return (EAttribute)identifiableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifiable_Id() {
		return (EAttribute)identifiableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifiable_Id1() {
		return (EAttribute)identifiableEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifiable_Idref() {
		return (EAttribute)identifiableEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifiable_Label() {
		return (EAttribute)identifiableEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifiable_Type() {
		return (EAttribute)identifiableEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifiable_Uuid() {
		return (EAttribute)identifiableEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifiable_Version() {
		return (EAttribute)identifiableEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpConfigurableElement() {
		return impConfigurableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElement_Group1() {
		return (EAttribute)impConfigurableElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfigurableElement_ImpConfiguredElements() {
		return (EReference)impConfigurableElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfigurableElement_RightElements() {
		return (EReference)impConfigurableElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfigurableElement_LeftElements() {
		return (EReference)impConfigurableElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfigurableElement_Extensions() {
		return (EReference)impConfigurableElementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElement_ImpConfiguredElements1() {
		return (EAttribute)impConfigurableElementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElement_Optional() {
		return (EAttribute)impConfigurableElementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElement_RightElements1() {
		return (EAttribute)impConfigurableElementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpConfigurableElementExtension() {
		return impConfigurableElementExtensionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElementExtension_Group() {
		return (EAttribute)impConfigurableElementExtensionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfigurableElementExtension_Extension() {
		return (EReference)impConfigurableElementExtensionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElementExtension_Href() {
		return (EAttribute)impConfigurableElementExtensionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElementExtension_Id() {
		return (EAttribute)impConfigurableElementExtensionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElementExtension_Idref() {
		return (EAttribute)impConfigurableElementExtensionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElementExtension_Label() {
		return (EAttribute)impConfigurableElementExtensionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElementExtension_Type() {
		return (EAttribute)impConfigurableElementExtensionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElementExtension_Uuid() {
		return (EAttribute)impConfigurableElementExtensionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableElementExtension_Version() {
		return (EAttribute)impConfigurableElementExtensionEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpConfigurableGroup() {
		return impConfigurableGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfigurableGroup_GroupType() {
		return (EAttribute)impConfigurableGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpConfiguration() {
		return impConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguration_Group1() {
		return (EAttribute)impConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfiguration_ImpConfiguredElements() {
		return (EReference)impConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfiguration_Models() {
		return (EReference)impConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfiguration_Project() {
		return (EReference)impConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguration_Models1() {
		return (EAttribute)impConfigurationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguration_Project1() {
		return (EAttribute)impConfigurationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpConfiguredElement() {
		return impConfiguredElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguredElement_Group() {
		return (EAttribute)impConfiguredElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfiguredElement_ImpConfigurableElement() {
		return (EReference)impConfiguredElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfiguredElement_ImpConfiguration() {
		return (EReference)impConfiguredElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfiguredElement_ConfigurationForHook() {
		return (EReference)impConfiguredElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConfiguredElement_Extension() {
		return (EReference)impConfiguredElementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguredElement_Href() {
		return (EAttribute)impConfiguredElementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguredElement_Id() {
		return (EAttribute)impConfiguredElementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguredElement_Idref() {
		return (EAttribute)impConfiguredElementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguredElement_ImpConfigurableElement1() {
		return (EAttribute)impConfiguredElementEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguredElement_ImpConfiguration1() {
		return (EAttribute)impConfiguredElementEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguredElement_Label() {
		return (EAttribute)impConfiguredElementEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguredElement_SelectionInformation() {
		return (EAttribute)impConfiguredElementEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguredElement_Type() {
		return (EAttribute)impConfiguredElementEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguredElement_Uuid() {
		return (EAttribute)impConfiguredElementEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConfiguredElement_Version() {
		return (EAttribute)impConfiguredElementEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpConnector() {
		return impConnectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConnector_Group1() {
		return (EAttribute)impConnectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConnector_SourcePort() {
		return (EReference)impConnectorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpConnector_TargetPort() {
		return (EReference)impConnectorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConnector_SourcePort1() {
		return (EAttribute)impConnectorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpConnector_TargetPort1() {
		return (EAttribute)impConnectorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpExtensibleElement() {
		return impExtensibleElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpExtensibleElement_Group() {
		return (EAttribute)impExtensibleElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpExtensibleElement_Extension() {
		return (EReference)impExtensibleElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpExtensibleElement_Href() {
		return (EAttribute)impExtensibleElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpExtensibleElement_Id() {
		return (EAttribute)impExtensibleElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpExtensibleElement_Idref() {
		return (EAttribute)impExtensibleElementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpExtensibleElement_Label() {
		return (EAttribute)impExtensibleElementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpExtensibleElement_Type() {
		return (EAttribute)impExtensibleElementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpExtensibleElement_Uuid() {
		return (EAttribute)impExtensibleElementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpExtensibleElement_Version() {
		return (EAttribute)impExtensibleElementEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpItem() {
		return impItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpItem_Description() {
		return (EAttribute)impItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpItem_Id1() {
		return (EAttribute)impItemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpItem_Name() {
		return (EAttribute)impItemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpLinkingRelation() {
		return impLinkingRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpLinkingRelation_Group() {
		return (EAttribute)impLinkingRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpLinkingRelation_RightElement() {
		return (EReference)impLinkingRelationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpLinkingRelation_LeftElement() {
		return (EReference)impLinkingRelationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpLinkingRelation_Extension() {
		return (EReference)impLinkingRelationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpLinkingRelation_Href() {
		return (EAttribute)impLinkingRelationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpLinkingRelation_Id() {
		return (EAttribute)impLinkingRelationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpLinkingRelation_Idref() {
		return (EAttribute)impLinkingRelationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpLinkingRelation_Label() {
		return (EAttribute)impLinkingRelationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpLinkingRelation_LeftElement1() {
		return (EAttribute)impLinkingRelationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpLinkingRelation_LinkingType() {
		return (EAttribute)impLinkingRelationEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpLinkingRelation_RightElement1() {
		return (EAttribute)impLinkingRelationEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpLinkingRelation_Type() {
		return (EAttribute)impLinkingRelationEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpLinkingRelation_Uuid() {
		return (EAttribute)impLinkingRelationEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpLinkingRelation_Version() {
		return (EAttribute)impLinkingRelationEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpModel() {
		return impModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpModel_Group1() {
		return (EAttribute)impModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpModel_Project() {
		return (EReference)impModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpModel_ImpConfigurations() {
		return (EReference)impModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpModel_Constraints() {
		return (EReference)impModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpModel_ImpConfigurations1() {
		return (EAttribute)impModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpModel_Project1() {
		return (EAttribute)impModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpNode() {
		return impNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpPortNode() {
		return impPortNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpPortNode_Group2() {
		return (EAttribute)impPortNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpPortNode_ConnectorsForSourcePort() {
		return (EReference)impPortNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpPortNode_ConnectorsForTargetPort() {
		return (EReference)impPortNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpPortNode_ConnectorsForSourcePort1() {
		return (EAttribute)impPortNodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpPortNode_ConnectorsForTargetPort1() {
		return (EAttribute)impPortNodeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpProject() {
		return impProjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpProject_Group1() {
		return (EAttribute)impProjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpProject_Models() {
		return (EReference)impProjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpProject_ImpConfigurations() {
		return (EReference)impProjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpProject_ImpExtensions() {
		return (EReference)impProjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImpTerm() {
		return impTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpTerm_Group() {
		return (EAttribute)impTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImpTerm_Extension() {
		return (EReference)impTermEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpTerm_Href() {
		return (EAttribute)impTermEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpTerm_Id() {
		return (EAttribute)impTermEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpTerm_Idref() {
		return (EAttribute)impTermEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpTerm_Label() {
		return (EAttribute)impTermEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpTerm_Type() {
		return (EAttribute)impTermEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpTerm_Uuid() {
		return (EAttribute)impTermEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImpTerm_Version() {
		return (EAttribute)impTermEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedItem() {
		return namedItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedItem_Group() {
		return (EAttribute)namedItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNamedItem_Extension() {
		return (EReference)namedItemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedItem_Href() {
		return (EAttribute)namedItemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedItem_Id() {
		return (EAttribute)namedItemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedItem_Idref() {
		return (EAttribute)namedItemEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedItem_Label() {
		return (EAttribute)namedItemEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedItem_Name() {
		return (EAttribute)namedItemEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedItem_Type() {
		return (EAttribute)namedItemEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedItem_Uuid() {
		return (EAttribute)namedItemEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedItem_Version() {
		return (EAttribute)namedItemEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProperty() {
		return propertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProperty_Group() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProperty_Extension() {
		return (EReference)propertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProperty_Href() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProperty_Id() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProperty_Idref() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProperty_Key() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProperty_Label() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProperty_Type() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProperty_Uuid() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProperty_Value() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProperty_Version() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertySet() {
		return propertySetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertySet_Group() {
		return (EAttribute)propertySetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertySet_Properties() {
		return (EReference)propertySetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertySet_Extension() {
		return (EReference)propertySetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertySet_Href() {
		return (EAttribute)propertySetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertySet_Id() {
		return (EAttribute)propertySetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertySet_Idref() {
		return (EAttribute)propertySetEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertySet_Label() {
		return (EAttribute)propertySetEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertySet_Type() {
		return (EAttribute)propertySetEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertySet_Uuid() {
		return (EAttribute)propertySetEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPropertySet_Version() {
		return (EAttribute)propertySetEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocumentRoot_Mixed() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Constraint() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_DescribedItem() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Identifiable() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpConfigurableElement() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpConfigurableElementExtension() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpConfigurableGroup() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpConfiguration() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpConfiguredElement() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpConnector() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpExtensibleElement() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpItem() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpLinkingRelation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpModel() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpNode() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpPortNode() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpProject() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_ImpTerm() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_NamedItem() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_Property() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocumentRoot_PropertySet() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getImpConfigurableGroupType() {
		return impConfigurableGroupTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLinkingType() {
		return linkingTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPortDirection() {
		return portDirectionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSelectionKind() {
		return selectionKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getImpConfigurableGroupTypeObject() {
		return impConfigurableGroupTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getLinkingTypeObject() {
		return linkingTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPortDirectionObject() {
		return portDirectionObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSelectionKindObject() {
		return selectionKindObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommonFactory getCommonFactory() {
		return (CommonFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		constraintEClass = createEClass(CONSTRAINT);
		createEAttribute(constraintEClass, CONSTRAINT__GROUP1);
		createEReference(constraintEClass, CONSTRAINT__ROOT_TERM);
		createEReference(constraintEClass, CONSTRAINT__MODEL);
		createEAttribute(constraintEClass, CONSTRAINT__IGNORE);
		createEAttribute(constraintEClass, CONSTRAINT__MODEL1);

		describedItemEClass = createEClass(DESCRIBED_ITEM);
		createEAttribute(describedItemEClass, DESCRIBED_ITEM__GROUP);
		createEReference(describedItemEClass, DESCRIBED_ITEM__EXTENSION);
		createEAttribute(describedItemEClass, DESCRIBED_ITEM__DESCRIPTION);
		createEAttribute(describedItemEClass, DESCRIBED_ITEM__HREF);
		createEAttribute(describedItemEClass, DESCRIBED_ITEM__ID);
		createEAttribute(describedItemEClass, DESCRIBED_ITEM__IDREF);
		createEAttribute(describedItemEClass, DESCRIBED_ITEM__LABEL);
		createEAttribute(describedItemEClass, DESCRIBED_ITEM__TYPE);
		createEAttribute(describedItemEClass, DESCRIBED_ITEM__UUID);
		createEAttribute(describedItemEClass, DESCRIBED_ITEM__VERSION);

		identifiableEClass = createEClass(IDENTIFIABLE);
		createEAttribute(identifiableEClass, IDENTIFIABLE__GROUP);
		createEReference(identifiableEClass, IDENTIFIABLE__EXTENSION);
		createEAttribute(identifiableEClass, IDENTIFIABLE__HREF);
		createEAttribute(identifiableEClass, IDENTIFIABLE__ID);
		createEAttribute(identifiableEClass, IDENTIFIABLE__ID1);
		createEAttribute(identifiableEClass, IDENTIFIABLE__IDREF);
		createEAttribute(identifiableEClass, IDENTIFIABLE__LABEL);
		createEAttribute(identifiableEClass, IDENTIFIABLE__TYPE);
		createEAttribute(identifiableEClass, IDENTIFIABLE__UUID);
		createEAttribute(identifiableEClass, IDENTIFIABLE__VERSION);

		impConfigurableElementEClass = createEClass(IMP_CONFIGURABLE_ELEMENT);
		createEAttribute(impConfigurableElementEClass, IMP_CONFIGURABLE_ELEMENT__GROUP1);
		createEReference(impConfigurableElementEClass, IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS);
		createEReference(impConfigurableElementEClass, IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS);
		createEReference(impConfigurableElementEClass, IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS);
		createEReference(impConfigurableElementEClass, IMP_CONFIGURABLE_ELEMENT__EXTENSIONS);
		createEAttribute(impConfigurableElementEClass, IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS1);
		createEAttribute(impConfigurableElementEClass, IMP_CONFIGURABLE_ELEMENT__OPTIONAL);
		createEAttribute(impConfigurableElementEClass, IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS1);

		impConfigurableElementExtensionEClass = createEClass(IMP_CONFIGURABLE_ELEMENT_EXTENSION);
		createEAttribute(impConfigurableElementExtensionEClass, IMP_CONFIGURABLE_ELEMENT_EXTENSION__GROUP);
		createEReference(impConfigurableElementExtensionEClass, IMP_CONFIGURABLE_ELEMENT_EXTENSION__EXTENSION);
		createEAttribute(impConfigurableElementExtensionEClass, IMP_CONFIGURABLE_ELEMENT_EXTENSION__HREF);
		createEAttribute(impConfigurableElementExtensionEClass, IMP_CONFIGURABLE_ELEMENT_EXTENSION__ID);
		createEAttribute(impConfigurableElementExtensionEClass, IMP_CONFIGURABLE_ELEMENT_EXTENSION__IDREF);
		createEAttribute(impConfigurableElementExtensionEClass, IMP_CONFIGURABLE_ELEMENT_EXTENSION__LABEL);
		createEAttribute(impConfigurableElementExtensionEClass, IMP_CONFIGURABLE_ELEMENT_EXTENSION__TYPE);
		createEAttribute(impConfigurableElementExtensionEClass, IMP_CONFIGURABLE_ELEMENT_EXTENSION__UUID);
		createEAttribute(impConfigurableElementExtensionEClass, IMP_CONFIGURABLE_ELEMENT_EXTENSION__VERSION);

		impConfigurableGroupEClass = createEClass(IMP_CONFIGURABLE_GROUP);
		createEAttribute(impConfigurableGroupEClass, IMP_CONFIGURABLE_GROUP__GROUP_TYPE);

		impConfigurationEClass = createEClass(IMP_CONFIGURATION);
		createEAttribute(impConfigurationEClass, IMP_CONFIGURATION__GROUP1);
		createEReference(impConfigurationEClass, IMP_CONFIGURATION__IMP_CONFIGURED_ELEMENTS);
		createEReference(impConfigurationEClass, IMP_CONFIGURATION__MODELS);
		createEReference(impConfigurationEClass, IMP_CONFIGURATION__PROJECT);
		createEAttribute(impConfigurationEClass, IMP_CONFIGURATION__MODELS1);
		createEAttribute(impConfigurationEClass, IMP_CONFIGURATION__PROJECT1);

		impConfiguredElementEClass = createEClass(IMP_CONFIGURED_ELEMENT);
		createEAttribute(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__GROUP);
		createEReference(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT);
		createEReference(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION);
		createEReference(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__CONFIGURATION_FOR_HOOK);
		createEReference(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__EXTENSION);
		createEAttribute(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__HREF);
		createEAttribute(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__ID);
		createEAttribute(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__IDREF);
		createEAttribute(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT1);
		createEAttribute(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION1);
		createEAttribute(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__LABEL);
		createEAttribute(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__SELECTION_INFORMATION);
		createEAttribute(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__TYPE);
		createEAttribute(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__UUID);
		createEAttribute(impConfiguredElementEClass, IMP_CONFIGURED_ELEMENT__VERSION);

		impConnectorEClass = createEClass(IMP_CONNECTOR);
		createEAttribute(impConnectorEClass, IMP_CONNECTOR__GROUP1);
		createEReference(impConnectorEClass, IMP_CONNECTOR__SOURCE_PORT);
		createEReference(impConnectorEClass, IMP_CONNECTOR__TARGET_PORT);
		createEAttribute(impConnectorEClass, IMP_CONNECTOR__SOURCE_PORT1);
		createEAttribute(impConnectorEClass, IMP_CONNECTOR__TARGET_PORT1);

		impExtensibleElementEClass = createEClass(IMP_EXTENSIBLE_ELEMENT);
		createEAttribute(impExtensibleElementEClass, IMP_EXTENSIBLE_ELEMENT__GROUP);
		createEReference(impExtensibleElementEClass, IMP_EXTENSIBLE_ELEMENT__EXTENSION);
		createEAttribute(impExtensibleElementEClass, IMP_EXTENSIBLE_ELEMENT__HREF);
		createEAttribute(impExtensibleElementEClass, IMP_EXTENSIBLE_ELEMENT__ID);
		createEAttribute(impExtensibleElementEClass, IMP_EXTENSIBLE_ELEMENT__IDREF);
		createEAttribute(impExtensibleElementEClass, IMP_EXTENSIBLE_ELEMENT__LABEL);
		createEAttribute(impExtensibleElementEClass, IMP_EXTENSIBLE_ELEMENT__TYPE);
		createEAttribute(impExtensibleElementEClass, IMP_EXTENSIBLE_ELEMENT__UUID);
		createEAttribute(impExtensibleElementEClass, IMP_EXTENSIBLE_ELEMENT__VERSION);

		impItemEClass = createEClass(IMP_ITEM);
		createEAttribute(impItemEClass, IMP_ITEM__DESCRIPTION);
		createEAttribute(impItemEClass, IMP_ITEM__ID1);
		createEAttribute(impItemEClass, IMP_ITEM__NAME);

		impLinkingRelationEClass = createEClass(IMP_LINKING_RELATION);
		createEAttribute(impLinkingRelationEClass, IMP_LINKING_RELATION__GROUP);
		createEReference(impLinkingRelationEClass, IMP_LINKING_RELATION__RIGHT_ELEMENT);
		createEReference(impLinkingRelationEClass, IMP_LINKING_RELATION__LEFT_ELEMENT);
		createEReference(impLinkingRelationEClass, IMP_LINKING_RELATION__EXTENSION);
		createEAttribute(impLinkingRelationEClass, IMP_LINKING_RELATION__HREF);
		createEAttribute(impLinkingRelationEClass, IMP_LINKING_RELATION__ID);
		createEAttribute(impLinkingRelationEClass, IMP_LINKING_RELATION__IDREF);
		createEAttribute(impLinkingRelationEClass, IMP_LINKING_RELATION__LABEL);
		createEAttribute(impLinkingRelationEClass, IMP_LINKING_RELATION__LEFT_ELEMENT1);
		createEAttribute(impLinkingRelationEClass, IMP_LINKING_RELATION__LINKING_TYPE);
		createEAttribute(impLinkingRelationEClass, IMP_LINKING_RELATION__RIGHT_ELEMENT1);
		createEAttribute(impLinkingRelationEClass, IMP_LINKING_RELATION__TYPE);
		createEAttribute(impLinkingRelationEClass, IMP_LINKING_RELATION__UUID);
		createEAttribute(impLinkingRelationEClass, IMP_LINKING_RELATION__VERSION);

		impModelEClass = createEClass(IMP_MODEL);
		createEAttribute(impModelEClass, IMP_MODEL__GROUP1);
		createEReference(impModelEClass, IMP_MODEL__PROJECT);
		createEReference(impModelEClass, IMP_MODEL__IMP_CONFIGURATIONS);
		createEReference(impModelEClass, IMP_MODEL__CONSTRAINTS);
		createEAttribute(impModelEClass, IMP_MODEL__IMP_CONFIGURATIONS1);
		createEAttribute(impModelEClass, IMP_MODEL__PROJECT1);

		impNodeEClass = createEClass(IMP_NODE);

		impPortNodeEClass = createEClass(IMP_PORT_NODE);
		createEAttribute(impPortNodeEClass, IMP_PORT_NODE__GROUP2);
		createEReference(impPortNodeEClass, IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT);
		createEReference(impPortNodeEClass, IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT);
		createEAttribute(impPortNodeEClass, IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT1);
		createEAttribute(impPortNodeEClass, IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT1);

		impProjectEClass = createEClass(IMP_PROJECT);
		createEAttribute(impProjectEClass, IMP_PROJECT__GROUP1);
		createEReference(impProjectEClass, IMP_PROJECT__MODELS);
		createEReference(impProjectEClass, IMP_PROJECT__IMP_CONFIGURATIONS);
		createEReference(impProjectEClass, IMP_PROJECT__IMP_EXTENSIONS);

		impTermEClass = createEClass(IMP_TERM);
		createEAttribute(impTermEClass, IMP_TERM__GROUP);
		createEReference(impTermEClass, IMP_TERM__EXTENSION);
		createEAttribute(impTermEClass, IMP_TERM__HREF);
		createEAttribute(impTermEClass, IMP_TERM__ID);
		createEAttribute(impTermEClass, IMP_TERM__IDREF);
		createEAttribute(impTermEClass, IMP_TERM__LABEL);
		createEAttribute(impTermEClass, IMP_TERM__TYPE);
		createEAttribute(impTermEClass, IMP_TERM__UUID);
		createEAttribute(impTermEClass, IMP_TERM__VERSION);

		namedItemEClass = createEClass(NAMED_ITEM);
		createEAttribute(namedItemEClass, NAMED_ITEM__GROUP);
		createEReference(namedItemEClass, NAMED_ITEM__EXTENSION);
		createEAttribute(namedItemEClass, NAMED_ITEM__HREF);
		createEAttribute(namedItemEClass, NAMED_ITEM__ID);
		createEAttribute(namedItemEClass, NAMED_ITEM__IDREF);
		createEAttribute(namedItemEClass, NAMED_ITEM__LABEL);
		createEAttribute(namedItemEClass, NAMED_ITEM__NAME);
		createEAttribute(namedItemEClass, NAMED_ITEM__TYPE);
		createEAttribute(namedItemEClass, NAMED_ITEM__UUID);
		createEAttribute(namedItemEClass, NAMED_ITEM__VERSION);

		propertyEClass = createEClass(PROPERTY);
		createEAttribute(propertyEClass, PROPERTY__GROUP);
		createEReference(propertyEClass, PROPERTY__EXTENSION);
		createEAttribute(propertyEClass, PROPERTY__HREF);
		createEAttribute(propertyEClass, PROPERTY__ID);
		createEAttribute(propertyEClass, PROPERTY__IDREF);
		createEAttribute(propertyEClass, PROPERTY__KEY);
		createEAttribute(propertyEClass, PROPERTY__LABEL);
		createEAttribute(propertyEClass, PROPERTY__TYPE);
		createEAttribute(propertyEClass, PROPERTY__UUID);
		createEAttribute(propertyEClass, PROPERTY__VALUE);
		createEAttribute(propertyEClass, PROPERTY__VERSION);

		propertySetEClass = createEClass(PROPERTY_SET);
		createEAttribute(propertySetEClass, PROPERTY_SET__GROUP);
		createEReference(propertySetEClass, PROPERTY_SET__PROPERTIES);
		createEReference(propertySetEClass, PROPERTY_SET__EXTENSION);
		createEAttribute(propertySetEClass, PROPERTY_SET__HREF);
		createEAttribute(propertySetEClass, PROPERTY_SET__ID);
		createEAttribute(propertySetEClass, PROPERTY_SET__IDREF);
		createEAttribute(propertySetEClass, PROPERTY_SET__LABEL);
		createEAttribute(propertySetEClass, PROPERTY_SET__TYPE);
		createEAttribute(propertySetEClass, PROPERTY_SET__UUID);
		createEAttribute(propertySetEClass, PROPERTY_SET__VERSION);

		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__CONSTRAINT);
		createEReference(documentRootEClass, DOCUMENT_ROOT__DESCRIBED_ITEM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IDENTIFIABLE);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT_EXTENSION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_CONFIGURABLE_GROUP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_CONFIGURATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_CONFIGURED_ELEMENT);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_CONNECTOR);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_EXTENSIBLE_ELEMENT);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_ITEM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_LINKING_RELATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_MODEL);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_NODE);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_PORT_NODE);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_PROJECT);
		createEReference(documentRootEClass, DOCUMENT_ROOT__IMP_TERM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__NAMED_ITEM);
		createEReference(documentRootEClass, DOCUMENT_ROOT__PROPERTY);
		createEReference(documentRootEClass, DOCUMENT_ROOT__PROPERTY_SET);

		// Create enums
		impConfigurableGroupTypeEEnum = createEEnum(IMP_CONFIGURABLE_GROUP_TYPE);
		linkingTypeEEnum = createEEnum(LINKING_TYPE);
		portDirectionEEnum = createEEnum(PORT_DIRECTION);
		selectionKindEEnum = createEEnum(SELECTION_KIND);

		// Create data types
		impConfigurableGroupTypeObjectEDataType = createEDataType(IMP_CONFIGURABLE_GROUP_TYPE_OBJECT);
		linkingTypeObjectEDataType = createEDataType(LINKING_TYPE_OBJECT);
		portDirectionObjectEDataType = createEDataType(PORT_DIRECTION_OBJECT);
		selectionKindObjectEDataType = createEDataType(SELECTION_KIND_OBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);
		XmiPackage theXmiPackage = (XmiPackage)EPackage.Registry.INSTANCE.getEPackage(XmiPackage.eNS_URI);

		// Add supertypes to classes
		constraintEClass.getESuperTypes().add(this.getImpItem());
		impConfigurableElementEClass.getESuperTypes().add(this.getImpItem());
		impConfigurableGroupEClass.getESuperTypes().add(this.getImpConfigurableElement());
		impConfigurationEClass.getESuperTypes().add(this.getImpItem());
		impConnectorEClass.getESuperTypes().add(this.getImpItem());
		impItemEClass.getESuperTypes().add(this.getPropertySet());
		impModelEClass.getESuperTypes().add(this.getImpItem());
		impNodeEClass.getESuperTypes().add(this.getImpConfigurableElement());
		impPortNodeEClass.getESuperTypes().add(this.getImpConfigurableElement());
		impProjectEClass.getESuperTypes().add(this.getImpItem());

		// Initialize classes and features; add operations and parameters
		initEClass(constraintEClass, Constraint.class, "Constraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConstraint_Group1(), ecorePackage.getEFeatureMapEntry(), "group1", null, 0, -1, Constraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConstraint_RootTerm(), this.getImpTerm(), null, "rootTerm", null, 0, -1, Constraint.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getConstraint_Model(), this.getImpModel(), null, "model", null, 0, -1, Constraint.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getConstraint_Ignore(), theXMLTypePackage.getBoolean(), "ignore", null, 1, 1, Constraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConstraint_Model1(), theXMLTypePackage.getString(), "model1", null, 0, 1, Constraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(describedItemEClass, DescribedItem.class, "DescribedItem", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDescribedItem_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, DescribedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDescribedItem_Extension(), theXmiPackage.getExtension(), null, "extension", null, 0, -1, DescribedItem.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDescribedItem_Description(), theXMLTypePackage.getString(), "description", null, 0, 1, DescribedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDescribedItem_Href(), theXMLTypePackage.getString(), "href", null, 0, 1, DescribedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDescribedItem_Id(), theXMLTypePackage.getID(), "id", null, 0, 1, DescribedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDescribedItem_Idref(), theXMLTypePackage.getIDREF(), "idref", null, 0, 1, DescribedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDescribedItem_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, DescribedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDescribedItem_Type(), theXMLTypePackage.getQName(), "type", null, 0, 1, DescribedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDescribedItem_Uuid(), theXMLTypePackage.getString(), "uuid", null, 0, 1, DescribedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDescribedItem_Version(), theXMLTypePackage.getString(), "version", "2.0", 0, 1, DescribedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(identifiableEClass, Identifiable.class, "Identifiable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIdentifiable_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIdentifiable_Extension(), theXmiPackage.getExtension(), null, "extension", null, 0, -1, Identifiable.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdentifiable_Href(), theXMLTypePackage.getString(), "href", null, 0, 1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdentifiable_Id(), theXMLTypePackage.getID(), "id", null, 0, 1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdentifiable_Id1(), theXMLTypePackage.getString(), "id1", null, 1, 1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdentifiable_Idref(), theXMLTypePackage.getIDREF(), "idref", null, 0, 1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdentifiable_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdentifiable_Type(), theXMLTypePackage.getQName(), "type", null, 0, 1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdentifiable_Uuid(), theXMLTypePackage.getString(), "uuid", null, 0, 1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIdentifiable_Version(), theXMLTypePackage.getString(), "version", "2.0", 0, 1, Identifiable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impConfigurableElementEClass, ImpConfigurableElement.class, "ImpConfigurableElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpConfigurableElement_Group1(), ecorePackage.getEFeatureMapEntry(), "group1", null, 0, -1, ImpConfigurableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfigurableElement_ImpConfiguredElements(), this.getImpConfiguredElement(), null, "impConfiguredElements", null, 0, -1, ImpConfigurableElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfigurableElement_RightElements(), this.getImpLinkingRelation(), null, "rightElements", null, 0, -1, ImpConfigurableElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfigurableElement_LeftElements(), this.getImpLinkingRelation(), null, "leftElements", null, 0, -1, ImpConfigurableElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfigurableElement_Extensions(), this.getImpConfigurableElementExtension(), null, "extensions", null, 0, -1, ImpConfigurableElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfigurableElement_ImpConfiguredElements1(), theXMLTypePackage.getString(), "impConfiguredElements1", null, 0, 1, ImpConfigurableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfigurableElement_Optional(), theXMLTypePackage.getBoolean(), "optional", null, 1, 1, ImpConfigurableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfigurableElement_RightElements1(), theXMLTypePackage.getString(), "rightElements1", null, 0, 1, ImpConfigurableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impConfigurableElementExtensionEClass, ImpConfigurableElementExtension.class, "ImpConfigurableElementExtension", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpConfigurableElementExtension_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ImpConfigurableElementExtension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfigurableElementExtension_Extension(), theXmiPackage.getExtension(), null, "extension", null, 0, -1, ImpConfigurableElementExtension.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfigurableElementExtension_Href(), theXMLTypePackage.getString(), "href", null, 0, 1, ImpConfigurableElementExtension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfigurableElementExtension_Id(), theXMLTypePackage.getID(), "id", null, 0, 1, ImpConfigurableElementExtension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfigurableElementExtension_Idref(), theXMLTypePackage.getIDREF(), "idref", null, 0, 1, ImpConfigurableElementExtension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfigurableElementExtension_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, ImpConfigurableElementExtension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfigurableElementExtension_Type(), theXMLTypePackage.getQName(), "type", null, 0, 1, ImpConfigurableElementExtension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfigurableElementExtension_Uuid(), theXMLTypePackage.getString(), "uuid", null, 0, 1, ImpConfigurableElementExtension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfigurableElementExtension_Version(), theXMLTypePackage.getString(), "version", "2.0", 0, 1, ImpConfigurableElementExtension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impConfigurableGroupEClass, ImpConfigurableGroup.class, "ImpConfigurableGroup", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpConfigurableGroup_GroupType(), this.getImpConfigurableGroupType(), "groupType", null, 1, 1, ImpConfigurableGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impConfigurationEClass, ImpConfiguration.class, "ImpConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpConfiguration_Group1(), ecorePackage.getEFeatureMapEntry(), "group1", null, 0, -1, ImpConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfiguration_ImpConfiguredElements(), this.getImpConfiguredElement(), null, "impConfiguredElements", null, 0, -1, ImpConfiguration.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfiguration_Models(), this.getImpModel(), null, "models", null, 0, -1, ImpConfiguration.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfiguration_Project(), this.getImpProject(), null, "project", null, 0, -1, ImpConfiguration.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguration_Models1(), theXMLTypePackage.getString(), "models1", null, 0, 1, ImpConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguration_Project1(), theXMLTypePackage.getString(), "project1", null, 0, 1, ImpConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impConfiguredElementEClass, ImpConfiguredElement.class, "ImpConfiguredElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpConfiguredElement_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ImpConfiguredElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfiguredElement_ImpConfigurableElement(), this.getImpConfigurableElement(), null, "impConfigurableElement", null, 0, -1, ImpConfiguredElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfiguredElement_ImpConfiguration(), this.getImpConfiguration(), null, "impConfiguration", null, 0, -1, ImpConfiguredElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfiguredElement_ConfigurationForHook(), this.getImpConfiguration(), null, "configurationForHook", null, 0, -1, ImpConfiguredElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpConfiguredElement_Extension(), theXmiPackage.getExtension(), null, "extension", null, 0, -1, ImpConfiguredElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguredElement_Href(), theXMLTypePackage.getString(), "href", null, 0, 1, ImpConfiguredElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguredElement_Id(), theXMLTypePackage.getID(), "id", null, 0, 1, ImpConfiguredElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguredElement_Idref(), theXMLTypePackage.getIDREF(), "idref", null, 0, 1, ImpConfiguredElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguredElement_ImpConfigurableElement1(), theXMLTypePackage.getString(), "impConfigurableElement1", null, 0, 1, ImpConfiguredElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguredElement_ImpConfiguration1(), theXMLTypePackage.getString(), "impConfiguration1", null, 0, 1, ImpConfiguredElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguredElement_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, ImpConfiguredElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguredElement_SelectionInformation(), this.getSelectionKind(), "selectionInformation", null, 1, 1, ImpConfiguredElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguredElement_Type(), theXMLTypePackage.getQName(), "type", null, 0, 1, ImpConfiguredElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguredElement_Uuid(), theXMLTypePackage.getString(), "uuid", null, 0, 1, ImpConfiguredElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConfiguredElement_Version(), theXMLTypePackage.getString(), "version", "2.0", 0, 1, ImpConfiguredElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impConnectorEClass, ImpConnector.class, "ImpConnector", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpConnector_Group1(), ecorePackage.getEFeatureMapEntry(), "group1", null, 0, -1, ImpConnector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImpConnector_SourcePort(), this.getImpPortNode(), null, "sourcePort", null, 0, -1, ImpConnector.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpConnector_TargetPort(), this.getImpPortNode(), null, "targetPort", null, 0, -1, ImpConnector.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConnector_SourcePort1(), theXMLTypePackage.getString(), "sourcePort1", null, 0, 1, ImpConnector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpConnector_TargetPort1(), theXMLTypePackage.getString(), "targetPort1", null, 0, 1, ImpConnector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impExtensibleElementEClass, ImpExtensibleElement.class, "ImpExtensibleElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpExtensibleElement_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ImpExtensibleElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImpExtensibleElement_Extension(), theXmiPackage.getExtension(), null, "extension", null, 0, -1, ImpExtensibleElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpExtensibleElement_Href(), theXMLTypePackage.getString(), "href", null, 0, 1, ImpExtensibleElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpExtensibleElement_Id(), theXMLTypePackage.getID(), "id", null, 0, 1, ImpExtensibleElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpExtensibleElement_Idref(), theXMLTypePackage.getIDREF(), "idref", null, 0, 1, ImpExtensibleElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpExtensibleElement_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, ImpExtensibleElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpExtensibleElement_Type(), theXMLTypePackage.getQName(), "type", null, 0, 1, ImpExtensibleElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpExtensibleElement_Uuid(), theXMLTypePackage.getString(), "uuid", null, 0, 1, ImpExtensibleElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpExtensibleElement_Version(), theXMLTypePackage.getString(), "version", "2.0", 0, 1, ImpExtensibleElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impItemEClass, ImpItem.class, "ImpItem", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpItem_Description(), theXMLTypePackage.getString(), "description", null, 0, 1, ImpItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpItem_Id1(), theXMLTypePackage.getString(), "id1", null, 1, 1, ImpItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpItem_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, ImpItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impLinkingRelationEClass, ImpLinkingRelation.class, "ImpLinkingRelation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpLinkingRelation_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ImpLinkingRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImpLinkingRelation_RightElement(), this.getImpConfigurableElement(), null, "rightElement", null, 0, -1, ImpLinkingRelation.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpLinkingRelation_LeftElement(), this.getImpConfigurableElement(), null, "leftElement", null, 0, -1, ImpLinkingRelation.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpLinkingRelation_Extension(), theXmiPackage.getExtension(), null, "extension", null, 0, -1, ImpLinkingRelation.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpLinkingRelation_Href(), theXMLTypePackage.getString(), "href", null, 0, 1, ImpLinkingRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpLinkingRelation_Id(), theXMLTypePackage.getID(), "id", null, 0, 1, ImpLinkingRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpLinkingRelation_Idref(), theXMLTypePackage.getIDREF(), "idref", null, 0, 1, ImpLinkingRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpLinkingRelation_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, ImpLinkingRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpLinkingRelation_LeftElement1(), theXMLTypePackage.getString(), "leftElement1", null, 0, 1, ImpLinkingRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpLinkingRelation_LinkingType(), this.getLinkingType(), "linkingType", null, 1, 1, ImpLinkingRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpLinkingRelation_RightElement1(), theXMLTypePackage.getString(), "rightElement1", null, 0, 1, ImpLinkingRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpLinkingRelation_Type(), theXMLTypePackage.getQName(), "type", null, 0, 1, ImpLinkingRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpLinkingRelation_Uuid(), theXMLTypePackage.getString(), "uuid", null, 0, 1, ImpLinkingRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpLinkingRelation_Version(), theXMLTypePackage.getString(), "version", "2.0", 0, 1, ImpLinkingRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impModelEClass, ImpModel.class, "ImpModel", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpModel_Group1(), ecorePackage.getEFeatureMapEntry(), "group1", null, 0, -1, ImpModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImpModel_Project(), this.getImpProject(), null, "project", null, 0, -1, ImpModel.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpModel_ImpConfigurations(), this.getImpConfiguration(), null, "impConfigurations", null, 0, -1, ImpModel.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpModel_Constraints(), this.getConstraint(), null, "constraints", null, 0, -1, ImpModel.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpModel_ImpConfigurations1(), theXMLTypePackage.getString(), "impConfigurations1", null, 0, 1, ImpModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpModel_Project1(), theXMLTypePackage.getString(), "project1", null, 0, 1, ImpModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impNodeEClass, ImpNode.class, "ImpNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(impPortNodeEClass, ImpPortNode.class, "ImpPortNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpPortNode_Group2(), ecorePackage.getEFeatureMapEntry(), "group2", null, 0, -1, ImpPortNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImpPortNode_ConnectorsForSourcePort(), this.getImpConnector(), null, "connectorsForSourcePort", null, 0, -1, ImpPortNode.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpPortNode_ConnectorsForTargetPort(), this.getImpConnector(), null, "connectorsForTargetPort", null, 0, -1, ImpPortNode.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpPortNode_ConnectorsForSourcePort1(), theXMLTypePackage.getString(), "connectorsForSourcePort1", null, 0, 1, ImpPortNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpPortNode_ConnectorsForTargetPort1(), theXMLTypePackage.getString(), "connectorsForTargetPort1", null, 0, 1, ImpPortNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(impProjectEClass, ImpProject.class, "ImpProject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpProject_Group1(), ecorePackage.getEFeatureMapEntry(), "group1", null, 0, -1, ImpProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImpProject_Models(), this.getImpModel(), null, "models", null, 0, -1, ImpProject.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpProject_ImpConfigurations(), this.getImpConfiguration(), null, "impConfigurations", null, 0, -1, ImpProject.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getImpProject_ImpExtensions(), this.getImpExtensibleElement(), null, "impExtensions", null, 0, -1, ImpProject.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(impTermEClass, ImpTerm.class, "ImpTerm", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImpTerm_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ImpTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getImpTerm_Extension(), theXmiPackage.getExtension(), null, "extension", null, 0, -1, ImpTerm.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpTerm_Href(), theXMLTypePackage.getString(), "href", null, 0, 1, ImpTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpTerm_Id(), theXMLTypePackage.getID(), "id", null, 0, 1, ImpTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpTerm_Idref(), theXMLTypePackage.getIDREF(), "idref", null, 0, 1, ImpTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpTerm_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, ImpTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpTerm_Type(), theXMLTypePackage.getQName(), "type", null, 0, 1, ImpTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpTerm_Uuid(), theXMLTypePackage.getString(), "uuid", null, 0, 1, ImpTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImpTerm_Version(), theXMLTypePackage.getString(), "version", "2.0", 0, 1, ImpTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedItemEClass, NamedItem.class, "NamedItem", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedItem_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, NamedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNamedItem_Extension(), theXmiPackage.getExtension(), null, "extension", null, 0, -1, NamedItem.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedItem_Href(), theXMLTypePackage.getString(), "href", null, 0, 1, NamedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedItem_Id(), theXMLTypePackage.getID(), "id", null, 0, 1, NamedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedItem_Idref(), theXMLTypePackage.getIDREF(), "idref", null, 0, 1, NamedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedItem_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, NamedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedItem_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, NamedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedItem_Type(), theXMLTypePackage.getQName(), "type", null, 0, 1, NamedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedItem_Uuid(), theXMLTypePackage.getString(), "uuid", null, 0, 1, NamedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNamedItem_Version(), theXMLTypePackage.getString(), "version", "2.0", 0, 1, NamedItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyEClass, Property.class, "Property", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProperty_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProperty_Extension(), theXmiPackage.getExtension(), null, "extension", null, 0, -1, Property.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperty_Href(), theXMLTypePackage.getString(), "href", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperty_Id(), theXMLTypePackage.getID(), "id", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperty_Idref(), theXMLTypePackage.getIDREF(), "idref", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperty_Key(), theXMLTypePackage.getString(), "key", null, 1, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperty_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperty_Type(), theXMLTypePackage.getQName(), "type", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperty_Uuid(), theXMLTypePackage.getString(), "uuid", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperty_Value(), theXMLTypePackage.getString(), "value", null, 1, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProperty_Version(), theXMLTypePackage.getString(), "version", "2.0", 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertySetEClass, PropertySet.class, "PropertySet", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPropertySet_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, PropertySet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertySet_Properties(), this.getProperty(), null, "properties", null, 0, -1, PropertySet.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getPropertySet_Extension(), theXmiPackage.getExtension(), null, "extension", null, 0, -1, PropertySet.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertySet_Href(), theXMLTypePackage.getString(), "href", null, 0, 1, PropertySet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertySet_Id(), theXMLTypePackage.getID(), "id", null, 0, 1, PropertySet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertySet_Idref(), theXMLTypePackage.getIDREF(), "idref", null, 0, 1, PropertySet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertySet_Label(), theXMLTypePackage.getString(), "label", null, 0, 1, PropertySet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertySet_Type(), theXMLTypePackage.getQName(), "type", null, 0, 1, PropertySet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertySet_Uuid(), theXMLTypePackage.getString(), "uuid", null, 0, 1, PropertySet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPropertySet_Version(), theXMLTypePackage.getString(), "version", "2.0", 0, 1, PropertySet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Constraint(), this.getConstraint(), null, "constraint", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_DescribedItem(), this.getDescribedItem(), null, "describedItem", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Identifiable(), this.getIdentifiable(), null, "identifiable", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpConfigurableElement(), this.getImpConfigurableElement(), null, "impConfigurableElement", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpConfigurableElementExtension(), this.getImpConfigurableElementExtension(), null, "impConfigurableElementExtension", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpConfigurableGroup(), this.getImpConfigurableGroup(), null, "impConfigurableGroup", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpConfiguration(), this.getImpConfiguration(), null, "impConfiguration", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpConfiguredElement(), this.getImpConfiguredElement(), null, "impConfiguredElement", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpConnector(), this.getImpConnector(), null, "impConnector", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpExtensibleElement(), this.getImpExtensibleElement(), null, "impExtensibleElement", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpItem(), this.getImpItem(), null, "impItem", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpLinkingRelation(), this.getImpLinkingRelation(), null, "impLinkingRelation", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpModel(), this.getImpModel(), null, "impModel", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpNode(), this.getImpNode(), null, "impNode", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpPortNode(), this.getImpPortNode(), null, "impPortNode", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpProject(), this.getImpProject(), null, "impProject", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_ImpTerm(), this.getImpTerm(), null, "impTerm", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_NamedItem(), this.getNamedItem(), null, "namedItem", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_Property(), this.getProperty(), null, "property", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_PropertySet(), this.getPropertySet(), null, "propertySet", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(impConfigurableGroupTypeEEnum, ImpConfigurableGroupType.class, "ImpConfigurableGroupType");
		addEEnumLiteral(impConfigurableGroupTypeEEnum, ImpConfigurableGroupType.NONE_LITERAL);
		addEEnumLiteral(impConfigurableGroupTypeEEnum, ImpConfigurableGroupType.AND_LITERAL);
		addEEnumLiteral(impConfigurableGroupTypeEEnum, ImpConfigurableGroupType.OR_LITERAL);
		addEEnumLiteral(impConfigurableGroupTypeEEnum, ImpConfigurableGroupType.XOR_LITERAL);

		initEEnum(linkingTypeEEnum, LinkingType.class, "LinkingType");
		addEEnumLiteral(linkingTypeEEnum, LinkingType.TRACEABILITY_LITERAL);
		addEEnumLiteral(linkingTypeEEnum, LinkingType.IMPLIES_LITERAL);
		addEEnumLiteral(linkingTypeEEnum, LinkingType.EQUIVALENT_LITERAL);

		initEEnum(portDirectionEEnum, PortDirection.class, "PortDirection");
		addEEnumLiteral(portDirectionEEnum, PortDirection.PROVIDE_LITERAL);
		addEEnumLiteral(portDirectionEEnum, PortDirection.REQUIRE_LITERAL);

		initEEnum(selectionKindEEnum, SelectionKind.class, "SelectionKind");
		addEEnumLiteral(selectionKindEEnum, SelectionKind.NOTYETDECIDED_LITERAL);
		addEEnumLiteral(selectionKindEEnum, SelectionKind.SELECTEDEXPLICIT_LITERAL);
		addEEnumLiteral(selectionKindEEnum, SelectionKind.SELECTEDDERIVED_LITERAL);
		addEEnumLiteral(selectionKindEEnum, SelectionKind.DESELECTEDEXPLICIT_LITERAL);
		addEEnumLiteral(selectionKindEEnum, SelectionKind.DESELECTEDDERIVED_LITERAL);
		addEEnumLiteral(selectionKindEEnum, SelectionKind.MANDATORYELEMENT_LITERAL);

		// Initialize data types
		initEDataType(impConfigurableGroupTypeObjectEDataType, ImpConfigurableGroupType.class, "ImpConfigurableGroupTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(linkingTypeObjectEDataType, LinkingType.class, "LinkingTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(portDirectionObjectEDataType, PortDirection.class, "PortDirectionObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(selectionKindObjectEDataType, SelectionKind.class, "SelectionKindObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";		
		addAnnotation
		  (constraintEClass, 
		   source, 
		   new String[] {
			 "name", "Constraint",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getConstraint_Group1(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:13"
		   });		
		addAnnotation
		  (getConstraint_RootTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "rootTerm",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getConstraint_Model(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "model",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getConstraint_Ignore(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "ignore"
		   });		
		addAnnotation
		  (getConstraint_Model1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "model"
		   });		
		addAnnotation
		  (describedItemEClass, 
		   source, 
		   new String[] {
			 "name", "DescribedItem",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getDescribedItem_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getDescribedItem_Extension(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Extension",
			 "namespace", "http://www.omg.org/XMI",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getDescribedItem_Description(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "description"
		   });		
		addAnnotation
		  (getDescribedItem_Href(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "href"
		   });		
		addAnnotation
		  (getDescribedItem_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getDescribedItem_Idref(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "idref",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getDescribedItem_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getDescribedItem_Type(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "type",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getDescribedItem_Uuid(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "uuid",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getDescribedItem_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (identifiableEClass, 
		   source, 
		   new String[] {
			 "name", "Identifiable",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getIdentifiable_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getIdentifiable_Extension(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Extension",
			 "namespace", "http://www.omg.org/XMI",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getIdentifiable_Href(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "href"
		   });		
		addAnnotation
		  (getIdentifiable_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getIdentifiable_Id1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id"
		   });		
		addAnnotation
		  (getIdentifiable_Idref(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "idref",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getIdentifiable_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getIdentifiable_Type(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "type",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getIdentifiable_Uuid(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "uuid",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getIdentifiable_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (impConfigurableElementEClass, 
		   source, 
		   new String[] {
			 "name", "ImpConfigurableElement",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpConfigurableElement_Group1(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:13"
		   });		
		addAnnotation
		  (getImpConfigurableElement_ImpConfiguredElements(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "impConfiguredElements",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpConfigurableElement_RightElements(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "rightElements",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpConfigurableElement_LeftElements(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "leftElements",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpConfigurableElement_Extensions(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "extensions",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpConfigurableElement_ImpConfiguredElements1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "impConfiguredElements"
		   });		
		addAnnotation
		  (getImpConfigurableElement_Optional(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "optional"
		   });		
		addAnnotation
		  (getImpConfigurableElement_RightElements1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "rightElements"
		   });		
		addAnnotation
		  (impConfigurableElementExtensionEClass, 
		   source, 
		   new String[] {
			 "name", "ImpConfigurableElementExtension",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpConfigurableElementExtension_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getImpConfigurableElementExtension_Extension(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Extension",
			 "namespace", "http://www.omg.org/XMI",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getImpConfigurableElementExtension_Href(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "href"
		   });		
		addAnnotation
		  (getImpConfigurableElementExtension_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpConfigurableElementExtension_Idref(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "idref",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpConfigurableElementExtension_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpConfigurableElementExtension_Type(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "type",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpConfigurableElementExtension_Uuid(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "uuid",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpConfigurableElementExtension_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (impConfigurableGroupEClass, 
		   source, 
		   new String[] {
			 "name", "ImpConfigurableGroup",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpConfigurableGroup_GroupType(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "groupType"
		   });		
		addAnnotation
		  (impConfigurableGroupTypeEEnum, 
		   source, 
		   new String[] {
			 "name", "ImpConfigurableGroupType"
		   });		
		addAnnotation
		  (impConfigurableGroupTypeObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "ImpConfigurableGroupType:Object",
			 "baseType", "ImpConfigurableGroupType"
		   });		
		addAnnotation
		  (impConfigurationEClass, 
		   source, 
		   new String[] {
			 "name", "ImpConfiguration",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpConfiguration_Group1(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:13"
		   });		
		addAnnotation
		  (getImpConfiguration_ImpConfiguredElements(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "impConfiguredElements",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpConfiguration_Models(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "models",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpConfiguration_Project(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "project",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpConfiguration_Models1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "models"
		   });		
		addAnnotation
		  (getImpConfiguration_Project1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "project"
		   });		
		addAnnotation
		  (impConfiguredElementEClass, 
		   source, 
		   new String[] {
			 "name", "ImpConfiguredElement",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpConfiguredElement_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getImpConfiguredElement_ImpConfigurableElement(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "impConfigurableElement",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getImpConfiguredElement_ImpConfiguration(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "impConfiguration",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getImpConfiguredElement_ConfigurationForHook(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "configurationForHook",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getImpConfiguredElement_Extension(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Extension",
			 "namespace", "http://www.omg.org/XMI",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getImpConfiguredElement_Href(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "href"
		   });		
		addAnnotation
		  (getImpConfiguredElement_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpConfiguredElement_Idref(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "idref",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpConfiguredElement_ImpConfigurableElement1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "impConfigurableElement"
		   });		
		addAnnotation
		  (getImpConfiguredElement_ImpConfiguration1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "impConfiguration"
		   });		
		addAnnotation
		  (getImpConfiguredElement_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpConfiguredElement_SelectionInformation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "selectionInformation"
		   });		
		addAnnotation
		  (getImpConfiguredElement_Type(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "type",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpConfiguredElement_Uuid(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "uuid",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpConfiguredElement_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (impConnectorEClass, 
		   source, 
		   new String[] {
			 "name", "ImpConnector",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpConnector_Group1(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:13"
		   });		
		addAnnotation
		  (getImpConnector_SourcePort(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "sourcePort",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpConnector_TargetPort(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "targetPort",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpConnector_SourcePort1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "sourcePort"
		   });		
		addAnnotation
		  (getImpConnector_TargetPort1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "targetPort"
		   });		
		addAnnotation
		  (impExtensibleElementEClass, 
		   source, 
		   new String[] {
			 "name", "ImpExtensibleElement",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpExtensibleElement_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getImpExtensibleElement_Extension(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Extension",
			 "namespace", "http://www.omg.org/XMI",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getImpExtensibleElement_Href(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "href"
		   });		
		addAnnotation
		  (getImpExtensibleElement_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpExtensibleElement_Idref(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "idref",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpExtensibleElement_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpExtensibleElement_Type(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "type",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpExtensibleElement_Uuid(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "uuid",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpExtensibleElement_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (impItemEClass, 
		   source, 
		   new String[] {
			 "name", "ImpItem",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpItem_Description(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "description"
		   });		
		addAnnotation
		  (getImpItem_Id1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id"
		   });		
		addAnnotation
		  (getImpItem_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name"
		   });		
		addAnnotation
		  (impLinkingRelationEClass, 
		   source, 
		   new String[] {
			 "name", "ImpLinkingRelation",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpLinkingRelation_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getImpLinkingRelation_RightElement(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "rightElement",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getImpLinkingRelation_LeftElement(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "leftElement",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getImpLinkingRelation_Extension(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Extension",
			 "namespace", "http://www.omg.org/XMI",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getImpLinkingRelation_Href(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "href"
		   });		
		addAnnotation
		  (getImpLinkingRelation_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpLinkingRelation_Idref(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "idref",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpLinkingRelation_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpLinkingRelation_LeftElement1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "leftElement"
		   });		
		addAnnotation
		  (getImpLinkingRelation_LinkingType(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "linkingType"
		   });		
		addAnnotation
		  (getImpLinkingRelation_RightElement1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "rightElement"
		   });		
		addAnnotation
		  (getImpLinkingRelation_Type(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "type",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpLinkingRelation_Uuid(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "uuid",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpLinkingRelation_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (impModelEClass, 
		   source, 
		   new String[] {
			 "name", "ImpModel",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpModel_Group1(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:13"
		   });		
		addAnnotation
		  (getImpModel_Project(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "project",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpModel_ImpConfigurations(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "impConfigurations",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpModel_Constraints(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "constraints",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpModel_ImpConfigurations1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "impConfigurations"
		   });		
		addAnnotation
		  (getImpModel_Project1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "project"
		   });		
		addAnnotation
		  (impNodeEClass, 
		   source, 
		   new String[] {
			 "name", "ImpNode",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (impPortNodeEClass, 
		   source, 
		   new String[] {
			 "name", "ImpPortNode",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpPortNode_Group2(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:21"
		   });		
		addAnnotation
		  (getImpPortNode_ConnectorsForSourcePort(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "connectorsForSourcePort",
			 "group", "#group:21"
		   });		
		addAnnotation
		  (getImpPortNode_ConnectorsForTargetPort(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "connectorsForTargetPort",
			 "group", "#group:21"
		   });		
		addAnnotation
		  (getImpPortNode_ConnectorsForSourcePort1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "connectorsForSourcePort"
		   });		
		addAnnotation
		  (getImpPortNode_ConnectorsForTargetPort1(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "connectorsForTargetPort"
		   });		
		addAnnotation
		  (impProjectEClass, 
		   source, 
		   new String[] {
			 "name", "ImpProject",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpProject_Group1(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:13"
		   });		
		addAnnotation
		  (getImpProject_Models(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "models",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpProject_ImpConfigurations(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "impConfigurations",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (getImpProject_ImpExtensions(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "impExtensions",
			 "group", "#group:13"
		   });		
		addAnnotation
		  (impTermEClass, 
		   source, 
		   new String[] {
			 "name", "ImpTerm",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getImpTerm_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getImpTerm_Extension(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Extension",
			 "namespace", "http://www.omg.org/XMI",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getImpTerm_Href(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "href"
		   });		
		addAnnotation
		  (getImpTerm_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpTerm_Idref(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "idref",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpTerm_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpTerm_Type(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "type",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpTerm_Uuid(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "uuid",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getImpTerm_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (linkingTypeEEnum, 
		   source, 
		   new String[] {
			 "name", "LinkingType"
		   });		
		addAnnotation
		  (linkingTypeObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "LinkingType:Object",
			 "baseType", "LinkingType"
		   });		
		addAnnotation
		  (namedItemEClass, 
		   source, 
		   new String[] {
			 "name", "NamedItem",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getNamedItem_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getNamedItem_Extension(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Extension",
			 "namespace", "http://www.omg.org/XMI",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getNamedItem_Href(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "href"
		   });		
		addAnnotation
		  (getNamedItem_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getNamedItem_Idref(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "idref",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getNamedItem_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getNamedItem_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "name"
		   });		
		addAnnotation
		  (getNamedItem_Type(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "type",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getNamedItem_Uuid(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "uuid",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getNamedItem_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (portDirectionEEnum, 
		   source, 
		   new String[] {
			 "name", "PortDirection"
		   });		
		addAnnotation
		  (portDirectionObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "PortDirection:Object",
			 "baseType", "PortDirection"
		   });		
		addAnnotation
		  (propertyEClass, 
		   source, 
		   new String[] {
			 "name", "Property",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getProperty_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getProperty_Extension(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Extension",
			 "namespace", "http://www.omg.org/XMI",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getProperty_Href(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "href"
		   });		
		addAnnotation
		  (getProperty_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getProperty_Idref(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "idref",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getProperty_Key(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "key"
		   });		
		addAnnotation
		  (getProperty_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getProperty_Type(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "type",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getProperty_Uuid(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "uuid",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getProperty_Value(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "value"
		   });		
		addAnnotation
		  (getProperty_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (propertySetEClass, 
		   source, 
		   new String[] {
			 "name", "PropertySet",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getPropertySet_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getPropertySet_Properties(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "properties",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getPropertySet_Extension(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Extension",
			 "namespace", "http://www.omg.org/XMI",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getPropertySet_Href(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "href"
		   });		
		addAnnotation
		  (getPropertySet_Id(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "id",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getPropertySet_Idref(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "idref",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getPropertySet_Label(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "label",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getPropertySet_Type(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "type",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getPropertySet_Uuid(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "uuid",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (getPropertySet_Version(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "version",
			 "namespace", "http://www.omg.org/XMI"
		   });		
		addAnnotation
		  (selectionKindEEnum, 
		   source, 
		   new String[] {
			 "name", "SelectionKind"
		   });		
		addAnnotation
		  (selectionKindObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "SelectionKind:Object",
			 "baseType", "SelectionKind"
		   });		
		addAnnotation
		  (documentRootEClass, 
		   source, 
		   new String[] {
			 "name", "",
			 "kind", "mixed"
		   });		
		addAnnotation
		  (getDocumentRoot_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });		
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xmlns:prefix"
		   });		
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xsi:schemaLocation"
		   });		
		addAnnotation
		  (getDocumentRoot_Constraint(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Constraint",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_DescribedItem(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "DescribedItem",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_Identifiable(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Identifiable",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpConfigurableElement(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpConfigurableElement",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpConfigurableElementExtension(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpConfigurableElementExtension",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpConfigurableGroup(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpConfigurableGroup",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpConfiguration(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpConfiguration",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpConfiguredElement(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpConfiguredElement",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpConnector(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpConnector",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpExtensibleElement(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpExtensibleElement",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpItem(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpItem",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpLinkingRelation(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpLinkingRelation",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpModel(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpModel",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpNode(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpNode",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpPortNode(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpPortNode",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpProject(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpProject",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_ImpTerm(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ImpTerm",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_NamedItem(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "NamedItem",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_Property(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Property",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getDocumentRoot_PropertySet(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "PropertySet",
			 "namespace", "##targetNamespace"
		   });
	}

} //CommonPackageImpl
