/**
 */
package com.prostep.vcontrol.model.terms;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Excludes Term</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getExcludesTerm()
 * @model extendedMetaData="name='ExcludesTerm' kind='elementOnly'"
 * @generated
 */
public interface ExcludesTerm extends BinaryTerm {
} // ExcludesTerm
