/**
 */
package com.prostep.vcontrol.model.common;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getMixed <em>Mixed</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getDescribedItem <em>Described Item</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getIdentifiable <em>Identifiable</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableElement <em>Imp Configurable Element</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableElementExtension <em>Imp Configurable Element Extension</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableGroup <em>Imp Configurable Group</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfiguration <em>Imp Configuration</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfiguredElement <em>Imp Configured Element</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConnector <em>Imp Connector</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpExtensibleElement <em>Imp Extensible Element</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpItem <em>Imp Item</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpLinkingRelation <em>Imp Linking Relation</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpModel <em>Imp Model</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpNode <em>Imp Node</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpPortNode <em>Imp Port Node</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpProject <em>Imp Project</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpTerm <em>Imp Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getNamedItem <em>Named Item</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getProperty <em>Property</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.DocumentRoot#getPropertySet <em>Property Set</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot()
 * @model extendedMetaData="name='' kind='mixed'"
 * @generated
 */
public interface DocumentRoot extends EObject {
	/**
	 * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mixed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mixed</em>' attribute list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_Mixed()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' name=':mixed'"
	 * @generated
	 */
	FeatureMap getMixed();

	/**
	 * Returns the value of the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XMLNS Prefix Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XMLNS Prefix Map</em>' map.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_XMLNSPrefixMap()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry" keyType="java.lang.String" valueType="java.lang.String" transient="true"
	 *        extendedMetaData="kind='attribute' name='xmlns:prefix'"
	 * @generated
	 */
	EMap getXMLNSPrefixMap();

	/**
	 * Returns the value of the '<em><b>XSI Schema Location</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XSI Schema Location</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XSI Schema Location</em>' map.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_XSISchemaLocation()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry" keyType="java.lang.String" valueType="java.lang.String" transient="true"
	 *        extendedMetaData="kind='attribute' name='xsi:schemaLocation'"
	 * @generated
	 */
	EMap getXSISchemaLocation();

	/**
	 * Returns the value of the '<em><b>Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint</em>' containment reference.
	 * @see #setConstraint(Constraint)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_Constraint()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Constraint' namespace='##targetNamespace'"
	 * @generated
	 */
	Constraint getConstraint();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getConstraint <em>Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraint</em>' containment reference.
	 * @see #getConstraint()
	 * @generated
	 */
	void setConstraint(Constraint value);

	/**
	 * Returns the value of the '<em><b>Described Item</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Described Item</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Described Item</em>' containment reference.
	 * @see #setDescribedItem(DescribedItem)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_DescribedItem()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DescribedItem' namespace='##targetNamespace'"
	 * @generated
	 */
	DescribedItem getDescribedItem();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getDescribedItem <em>Described Item</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Described Item</em>' containment reference.
	 * @see #getDescribedItem()
	 * @generated
	 */
	void setDescribedItem(DescribedItem value);

	/**
	 * Returns the value of the '<em><b>Identifiable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifiable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifiable</em>' containment reference.
	 * @see #setIdentifiable(Identifiable)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_Identifiable()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Identifiable' namespace='##targetNamespace'"
	 * @generated
	 */
	Identifiable getIdentifiable();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getIdentifiable <em>Identifiable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identifiable</em>' containment reference.
	 * @see #getIdentifiable()
	 * @generated
	 */
	void setIdentifiable(Identifiable value);

	/**
	 * Returns the value of the '<em><b>Imp Configurable Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Configurable Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Configurable Element</em>' containment reference.
	 * @see #setImpConfigurableElement(ImpConfigurableElement)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpConfigurableElement()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpConfigurableElement' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpConfigurableElement getImpConfigurableElement();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableElement <em>Imp Configurable Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Configurable Element</em>' containment reference.
	 * @see #getImpConfigurableElement()
	 * @generated
	 */
	void setImpConfigurableElement(ImpConfigurableElement value);

	/**
	 * Returns the value of the '<em><b>Imp Configurable Element Extension</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Configurable Element Extension</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Configurable Element Extension</em>' containment reference.
	 * @see #setImpConfigurableElementExtension(ImpConfigurableElementExtension)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpConfigurableElementExtension()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpConfigurableElementExtension' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpConfigurableElementExtension getImpConfigurableElementExtension();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableElementExtension <em>Imp Configurable Element Extension</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Configurable Element Extension</em>' containment reference.
	 * @see #getImpConfigurableElementExtension()
	 * @generated
	 */
	void setImpConfigurableElementExtension(ImpConfigurableElementExtension value);

	/**
	 * Returns the value of the '<em><b>Imp Configurable Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Configurable Group</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Configurable Group</em>' containment reference.
	 * @see #setImpConfigurableGroup(ImpConfigurableGroup)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpConfigurableGroup()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpConfigurableGroup' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpConfigurableGroup getImpConfigurableGroup();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableGroup <em>Imp Configurable Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Configurable Group</em>' containment reference.
	 * @see #getImpConfigurableGroup()
	 * @generated
	 */
	void setImpConfigurableGroup(ImpConfigurableGroup value);

	/**
	 * Returns the value of the '<em><b>Imp Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Configuration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Configuration</em>' containment reference.
	 * @see #setImpConfiguration(ImpConfiguration)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpConfiguration()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpConfiguration' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpConfiguration getImpConfiguration();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfiguration <em>Imp Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Configuration</em>' containment reference.
	 * @see #getImpConfiguration()
	 * @generated
	 */
	void setImpConfiguration(ImpConfiguration value);

	/**
	 * Returns the value of the '<em><b>Imp Configured Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Configured Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Configured Element</em>' containment reference.
	 * @see #setImpConfiguredElement(ImpConfiguredElement)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpConfiguredElement()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpConfiguredElement' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpConfiguredElement getImpConfiguredElement();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfiguredElement <em>Imp Configured Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Configured Element</em>' containment reference.
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	void setImpConfiguredElement(ImpConfiguredElement value);

	/**
	 * Returns the value of the '<em><b>Imp Connector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Connector</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Connector</em>' containment reference.
	 * @see #setImpConnector(ImpConnector)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpConnector()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpConnector' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpConnector getImpConnector();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConnector <em>Imp Connector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Connector</em>' containment reference.
	 * @see #getImpConnector()
	 * @generated
	 */
	void setImpConnector(ImpConnector value);

	/**
	 * Returns the value of the '<em><b>Imp Extensible Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Extensible Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Extensible Element</em>' containment reference.
	 * @see #setImpExtensibleElement(ImpExtensibleElement)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpExtensibleElement()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpExtensibleElement' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpExtensibleElement getImpExtensibleElement();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpExtensibleElement <em>Imp Extensible Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Extensible Element</em>' containment reference.
	 * @see #getImpExtensibleElement()
	 * @generated
	 */
	void setImpExtensibleElement(ImpExtensibleElement value);

	/**
	 * Returns the value of the '<em><b>Imp Item</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Item</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Item</em>' containment reference.
	 * @see #setImpItem(ImpItem)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpItem()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpItem' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpItem getImpItem();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpItem <em>Imp Item</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Item</em>' containment reference.
	 * @see #getImpItem()
	 * @generated
	 */
	void setImpItem(ImpItem value);

	/**
	 * Returns the value of the '<em><b>Imp Linking Relation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Linking Relation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Linking Relation</em>' containment reference.
	 * @see #setImpLinkingRelation(ImpLinkingRelation)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpLinkingRelation()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpLinkingRelation' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpLinkingRelation getImpLinkingRelation();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpLinkingRelation <em>Imp Linking Relation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Linking Relation</em>' containment reference.
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	void setImpLinkingRelation(ImpLinkingRelation value);

	/**
	 * Returns the value of the '<em><b>Imp Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Model</em>' containment reference.
	 * @see #setImpModel(ImpModel)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpModel()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpModel' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpModel getImpModel();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpModel <em>Imp Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Model</em>' containment reference.
	 * @see #getImpModel()
	 * @generated
	 */
	void setImpModel(ImpModel value);

	/**
	 * Returns the value of the '<em><b>Imp Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Node</em>' containment reference.
	 * @see #setImpNode(ImpNode)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpNode()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpNode' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpNode getImpNode();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpNode <em>Imp Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Node</em>' containment reference.
	 * @see #getImpNode()
	 * @generated
	 */
	void setImpNode(ImpNode value);

	/**
	 * Returns the value of the '<em><b>Imp Port Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Port Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Port Node</em>' containment reference.
	 * @see #setImpPortNode(ImpPortNode)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpPortNode()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpPortNode' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpPortNode getImpPortNode();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpPortNode <em>Imp Port Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Port Node</em>' containment reference.
	 * @see #getImpPortNode()
	 * @generated
	 */
	void setImpPortNode(ImpPortNode value);

	/**
	 * Returns the value of the '<em><b>Imp Project</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Project</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Project</em>' containment reference.
	 * @see #setImpProject(ImpProject)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpProject()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpProject' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpProject getImpProject();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpProject <em>Imp Project</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Project</em>' containment reference.
	 * @see #getImpProject()
	 * @generated
	 */
	void setImpProject(ImpProject value);

	/**
	 * Returns the value of the '<em><b>Imp Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Term</em>' containment reference.
	 * @see #setImpTerm(ImpTerm)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_ImpTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpTerm getImpTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpTerm <em>Imp Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp Term</em>' containment reference.
	 * @see #getImpTerm()
	 * @generated
	 */
	void setImpTerm(ImpTerm value);

	/**
	 * Returns the value of the '<em><b>Named Item</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Item</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Item</em>' containment reference.
	 * @see #setNamedItem(NamedItem)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_NamedItem()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='NamedItem' namespace='##targetNamespace'"
	 * @generated
	 */
	NamedItem getNamedItem();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getNamedItem <em>Named Item</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Named Item</em>' containment reference.
	 * @see #getNamedItem()
	 * @generated
	 */
	void setNamedItem(NamedItem value);

	/**
	 * Returns the value of the '<em><b>Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' containment reference.
	 * @see #setProperty(Property)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_Property()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Property' namespace='##targetNamespace'"
	 * @generated
	 */
	Property getProperty();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getProperty <em>Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' containment reference.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(Property value);

	/**
	 * Returns the value of the '<em><b>Property Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Set</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Set</em>' containment reference.
	 * @see #setPropertySet(PropertySet)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getDocumentRoot_PropertySet()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='PropertySet' namespace='##targetNamespace'"
	 * @generated
	 */
	PropertySet getPropertySet();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.DocumentRoot#getPropertySet <em>Property Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Set</em>' containment reference.
	 * @see #getPropertySet()
	 * @generated
	 */
	void setPropertySet(PropertySet value);

} // DocumentRoot
