/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.CommonPackage;
import com.prostep.vcontrol.model.common.Constraint;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ConstraintImpl#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ConstraintImpl#getRootTerm <em>Root Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ConstraintImpl#getModel <em>Model</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ConstraintImpl#isIgnore <em>Ignore</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ConstraintImpl#getModel1 <em>Model1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConstraintImpl extends ImpItemImpl implements Constraint {
	/**
	 * The cached value of the '{@link #getGroup1() <em>Group1</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup1()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group1;

	/**
	 * The default value of the '{@link #isIgnore() <em>Ignore</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIgnore()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IGNORE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIgnore() <em>Ignore</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIgnore()
	 * @generated
	 * @ordered
	 */
	protected boolean ignore = IGNORE_EDEFAULT;

	/**
	 * This is true if the Ignore attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean ignoreESet;

	/**
	 * The default value of the '{@link #getModel1() <em>Model1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel1()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModel1() <em>Model1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel1()
	 * @generated
	 * @ordered
	 */
	protected String model1 = MODEL1_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CommonPackage.Literals.CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup1() {
		if (group1 == null) {
			group1 = new BasicFeatureMap(this, CommonPackage.CONSTRAINT__GROUP1);
		}
		return group1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRootTerm() {
		return getGroup1().list(CommonPackage.Literals.CONSTRAINT__ROOT_TERM);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getModel() {
		return getGroup1().list(CommonPackage.Literals.CONSTRAINT__MODEL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIgnore() {
		return ignore;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIgnore(boolean newIgnore) {
		boolean oldIgnore = ignore;
		ignore = newIgnore;
		boolean oldIgnoreESet = ignoreESet;
		ignoreESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.CONSTRAINT__IGNORE, oldIgnore, ignore, !oldIgnoreESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIgnore() {
		boolean oldIgnore = ignore;
		boolean oldIgnoreESet = ignoreESet;
		ignore = IGNORE_EDEFAULT;
		ignoreESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.CONSTRAINT__IGNORE, oldIgnore, IGNORE_EDEFAULT, oldIgnoreESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIgnore() {
		return ignoreESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModel1() {
		return model1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModel1(String newModel1) {
		String oldModel1 = model1;
		model1 = newModel1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.CONSTRAINT__MODEL1, oldModel1, model1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CommonPackage.CONSTRAINT__GROUP1:
				return ((InternalEList)getGroup1()).basicRemove(otherEnd, msgs);
			case CommonPackage.CONSTRAINT__ROOT_TERM:
				return ((InternalEList)getRootTerm()).basicRemove(otherEnd, msgs);
			case CommonPackage.CONSTRAINT__MODEL:
				return ((InternalEList)getModel()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CommonPackage.CONSTRAINT__GROUP1:
				if (coreType) return getGroup1();
				return ((FeatureMap.Internal)getGroup1()).getWrapper();
			case CommonPackage.CONSTRAINT__ROOT_TERM:
				return getRootTerm();
			case CommonPackage.CONSTRAINT__MODEL:
				return getModel();
			case CommonPackage.CONSTRAINT__IGNORE:
				return isIgnore() ? Boolean.TRUE : Boolean.FALSE;
			case CommonPackage.CONSTRAINT__MODEL1:
				return getModel1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CommonPackage.CONSTRAINT__GROUP1:
				((FeatureMap.Internal)getGroup1()).set(newValue);
				return;
			case CommonPackage.CONSTRAINT__ROOT_TERM:
				getRootTerm().clear();
				getRootTerm().addAll((Collection)newValue);
				return;
			case CommonPackage.CONSTRAINT__MODEL:
				getModel().clear();
				getModel().addAll((Collection)newValue);
				return;
			case CommonPackage.CONSTRAINT__IGNORE:
				setIgnore(((Boolean)newValue).booleanValue());
				return;
			case CommonPackage.CONSTRAINT__MODEL1:
				setModel1((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CommonPackage.CONSTRAINT__GROUP1:
				getGroup1().clear();
				return;
			case CommonPackage.CONSTRAINT__ROOT_TERM:
				getRootTerm().clear();
				return;
			case CommonPackage.CONSTRAINT__MODEL:
				getModel().clear();
				return;
			case CommonPackage.CONSTRAINT__IGNORE:
				unsetIgnore();
				return;
			case CommonPackage.CONSTRAINT__MODEL1:
				setModel1(MODEL1_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CommonPackage.CONSTRAINT__GROUP1:
				return group1 != null && !group1.isEmpty();
			case CommonPackage.CONSTRAINT__ROOT_TERM:
				return !getRootTerm().isEmpty();
			case CommonPackage.CONSTRAINT__MODEL:
				return !getModel().isEmpty();
			case CommonPackage.CONSTRAINT__IGNORE:
				return isSetIgnore();
			case CommonPackage.CONSTRAINT__MODEL1:
				return MODEL1_EDEFAULT == null ? model1 != null : !MODEL1_EDEFAULT.equals(model1);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group1: ");
		result.append(group1);
		result.append(", ignore: ");
		if (ignoreESet) result.append(ignore); else result.append("<unset>");
		result.append(", model1: ");
		result.append(model1);
		result.append(')');
		return result.toString();
	}

} //ConstraintImpl
