/**
 */
package com.prostep.vcontrol.model.terms.impl;

import com.prostep.vcontrol.model.terms.AbstractMathTerm;
import com.prostep.vcontrol.model.terms.AndTerm;
import com.prostep.vcontrol.model.terms.AtomicTerm;
import com.prostep.vcontrol.model.terms.BinaryTerm;
import com.prostep.vcontrol.model.terms.DocumentRoot;
import com.prostep.vcontrol.model.terms.EquivalentTerm;
import com.prostep.vcontrol.model.terms.ErrorStatusTerm;
import com.prostep.vcontrol.model.terms.ExcludesTerm;
import com.prostep.vcontrol.model.terms.FalseTerm;
import com.prostep.vcontrol.model.terms.ImpliesTerm;
import com.prostep.vcontrol.model.terms.NaryTerm;
import com.prostep.vcontrol.model.terms.NotTerm;
import com.prostep.vcontrol.model.terms.OrTerm;
import com.prostep.vcontrol.model.terms.TermsPackage;
import com.prostep.vcontrol.model.terms.TrueTerm;
import com.prostep.vcontrol.model.terms.UnaryTerm;
import com.prostep.vcontrol.model.terms.XorTerm;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getAbstractMathTerm <em>Abstract Math Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getAndTerm <em>And Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getAtomicTerm <em>Atomic Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getBinaryTerm <em>Binary Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getEquivalentTerm <em>Equivalent Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getErrorStatusTerm <em>Error Status Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getExcludesTerm <em>Excludes Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getFalseTerm <em>False Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getImpliesTerm <em>Implies Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getNaryTerm <em>Nary Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getNotTerm <em>Not Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getOrTerm <em>Or Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getTrueTerm <em>True Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getUnaryTerm <em>Unary Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl#getXorTerm <em>Xor Term</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DocumentRootImpl extends EObjectImpl implements DocumentRoot {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * The cached value of the '{@link #getXMLNSPrefixMap() <em>XMLNS Prefix Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXMLNSPrefixMap()
	 * @generated
	 * @ordered
	 */
	protected EMap xMLNSPrefixMap;

	/**
	 * The cached value of the '{@link #getXSISchemaLocation() <em>XSI Schema Location</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXSISchemaLocation()
	 * @generated
	 * @ordered
	 */
	protected EMap xSISchemaLocation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DocumentRootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return TermsPackage.Literals.DOCUMENT_ROOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, TermsPackage.DOCUMENT_ROOT__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap getXMLNSPrefixMap() {
		if (xMLNSPrefixMap == null) {
			xMLNSPrefixMap = new EcoreEMap(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, TermsPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		}
		return xMLNSPrefixMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap getXSISchemaLocation() {
		if (xSISchemaLocation == null) {
			xSISchemaLocation = new EcoreEMap(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, TermsPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		}
		return xSISchemaLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractMathTerm getAbstractMathTerm() {
		return (AbstractMathTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__ABSTRACT_MATH_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstractMathTerm(AbstractMathTerm newAbstractMathTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__ABSTRACT_MATH_TERM, newAbstractMathTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractMathTerm(AbstractMathTerm newAbstractMathTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__ABSTRACT_MATH_TERM, newAbstractMathTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AndTerm getAndTerm() {
		return (AndTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__AND_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAndTerm(AndTerm newAndTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__AND_TERM, newAndTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAndTerm(AndTerm newAndTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__AND_TERM, newAndTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicTerm getAtomicTerm() {
		return (AtomicTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__ATOMIC_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtomicTerm(AtomicTerm newAtomicTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__ATOMIC_TERM, newAtomicTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicTerm(AtomicTerm newAtomicTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__ATOMIC_TERM, newAtomicTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryTerm getBinaryTerm() {
		return (BinaryTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__BINARY_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBinaryTerm(BinaryTerm newBinaryTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__BINARY_TERM, newBinaryTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBinaryTerm(BinaryTerm newBinaryTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__BINARY_TERM, newBinaryTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquivalentTerm getEquivalentTerm() {
		return (EquivalentTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__EQUIVALENT_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEquivalentTerm(EquivalentTerm newEquivalentTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__EQUIVALENT_TERM, newEquivalentTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEquivalentTerm(EquivalentTerm newEquivalentTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__EQUIVALENT_TERM, newEquivalentTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ErrorStatusTerm getErrorStatusTerm() {
		return (ErrorStatusTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__ERROR_STATUS_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetErrorStatusTerm(ErrorStatusTerm newErrorStatusTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__ERROR_STATUS_TERM, newErrorStatusTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setErrorStatusTerm(ErrorStatusTerm newErrorStatusTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__ERROR_STATUS_TERM, newErrorStatusTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExcludesTerm getExcludesTerm() {
		return (ExcludesTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__EXCLUDES_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExcludesTerm(ExcludesTerm newExcludesTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__EXCLUDES_TERM, newExcludesTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExcludesTerm(ExcludesTerm newExcludesTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__EXCLUDES_TERM, newExcludesTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FalseTerm getFalseTerm() {
		return (FalseTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__FALSE_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFalseTerm(FalseTerm newFalseTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__FALSE_TERM, newFalseTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFalseTerm(FalseTerm newFalseTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__FALSE_TERM, newFalseTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpliesTerm getImpliesTerm() {
		return (ImpliesTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__IMPLIES_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpliesTerm(ImpliesTerm newImpliesTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__IMPLIES_TERM, newImpliesTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpliesTerm(ImpliesTerm newImpliesTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__IMPLIES_TERM, newImpliesTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NaryTerm getNaryTerm() {
		return (NaryTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__NARY_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNaryTerm(NaryTerm newNaryTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__NARY_TERM, newNaryTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNaryTerm(NaryTerm newNaryTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__NARY_TERM, newNaryTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotTerm getNotTerm() {
		return (NotTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__NOT_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotTerm(NotTerm newNotTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__NOT_TERM, newNotTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotTerm(NotTerm newNotTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__NOT_TERM, newNotTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrTerm getOrTerm() {
		return (OrTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__OR_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrTerm(OrTerm newOrTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__OR_TERM, newOrTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrTerm(OrTerm newOrTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__OR_TERM, newOrTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrueTerm getTrueTerm() {
		return (TrueTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__TRUE_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTrueTerm(TrueTerm newTrueTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__TRUE_TERM, newTrueTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTrueTerm(TrueTerm newTrueTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__TRUE_TERM, newTrueTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryTerm getUnaryTerm() {
		return (UnaryTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__UNARY_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnaryTerm(UnaryTerm newUnaryTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__UNARY_TERM, newUnaryTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnaryTerm(UnaryTerm newUnaryTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__UNARY_TERM, newUnaryTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XorTerm getXorTerm() {
		return (XorTerm)getMixed().get(TermsPackage.Literals.DOCUMENT_ROOT__XOR_TERM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetXorTerm(XorTerm newXorTerm, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(TermsPackage.Literals.DOCUMENT_ROOT__XOR_TERM, newXorTerm, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setXorTerm(XorTerm newXorTerm) {
		((FeatureMap.Internal)getMixed()).set(TermsPackage.Literals.DOCUMENT_ROOT__XOR_TERM, newXorTerm);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TermsPackage.DOCUMENT_ROOT__MIXED:
				return ((InternalEList)getMixed()).basicRemove(otherEnd, msgs);
			case TermsPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return ((InternalEList)getXMLNSPrefixMap()).basicRemove(otherEnd, msgs);
			case TermsPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return ((InternalEList)getXSISchemaLocation()).basicRemove(otherEnd, msgs);
			case TermsPackage.DOCUMENT_ROOT__ABSTRACT_MATH_TERM:
				return basicSetAbstractMathTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__AND_TERM:
				return basicSetAndTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__ATOMIC_TERM:
				return basicSetAtomicTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__BINARY_TERM:
				return basicSetBinaryTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__EQUIVALENT_TERM:
				return basicSetEquivalentTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__ERROR_STATUS_TERM:
				return basicSetErrorStatusTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__EXCLUDES_TERM:
				return basicSetExcludesTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__FALSE_TERM:
				return basicSetFalseTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__IMPLIES_TERM:
				return basicSetImpliesTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__NARY_TERM:
				return basicSetNaryTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__NOT_TERM:
				return basicSetNotTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__OR_TERM:
				return basicSetOrTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__TRUE_TERM:
				return basicSetTrueTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__UNARY_TERM:
				return basicSetUnaryTerm(null, msgs);
			case TermsPackage.DOCUMENT_ROOT__XOR_TERM:
				return basicSetXorTerm(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TermsPackage.DOCUMENT_ROOT__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case TermsPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				if (coreType) return getXMLNSPrefixMap();
				else return getXMLNSPrefixMap().map();
			case TermsPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				if (coreType) return getXSISchemaLocation();
				else return getXSISchemaLocation().map();
			case TermsPackage.DOCUMENT_ROOT__ABSTRACT_MATH_TERM:
				return getAbstractMathTerm();
			case TermsPackage.DOCUMENT_ROOT__AND_TERM:
				return getAndTerm();
			case TermsPackage.DOCUMENT_ROOT__ATOMIC_TERM:
				return getAtomicTerm();
			case TermsPackage.DOCUMENT_ROOT__BINARY_TERM:
				return getBinaryTerm();
			case TermsPackage.DOCUMENT_ROOT__EQUIVALENT_TERM:
				return getEquivalentTerm();
			case TermsPackage.DOCUMENT_ROOT__ERROR_STATUS_TERM:
				return getErrorStatusTerm();
			case TermsPackage.DOCUMENT_ROOT__EXCLUDES_TERM:
				return getExcludesTerm();
			case TermsPackage.DOCUMENT_ROOT__FALSE_TERM:
				return getFalseTerm();
			case TermsPackage.DOCUMENT_ROOT__IMPLIES_TERM:
				return getImpliesTerm();
			case TermsPackage.DOCUMENT_ROOT__NARY_TERM:
				return getNaryTerm();
			case TermsPackage.DOCUMENT_ROOT__NOT_TERM:
				return getNotTerm();
			case TermsPackage.DOCUMENT_ROOT__OR_TERM:
				return getOrTerm();
			case TermsPackage.DOCUMENT_ROOT__TRUE_TERM:
				return getTrueTerm();
			case TermsPackage.DOCUMENT_ROOT__UNARY_TERM:
				return getUnaryTerm();
			case TermsPackage.DOCUMENT_ROOT__XOR_TERM:
				return getXorTerm();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TermsPackage.DOCUMENT_ROOT__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				((EStructuralFeature.Setting)getXMLNSPrefixMap()).set(newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				((EStructuralFeature.Setting)getXSISchemaLocation()).set(newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__ABSTRACT_MATH_TERM:
				setAbstractMathTerm((AbstractMathTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__AND_TERM:
				setAndTerm((AndTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__ATOMIC_TERM:
				setAtomicTerm((AtomicTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__BINARY_TERM:
				setBinaryTerm((BinaryTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__EQUIVALENT_TERM:
				setEquivalentTerm((EquivalentTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__ERROR_STATUS_TERM:
				setErrorStatusTerm((ErrorStatusTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__EXCLUDES_TERM:
				setExcludesTerm((ExcludesTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__FALSE_TERM:
				setFalseTerm((FalseTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__IMPLIES_TERM:
				setImpliesTerm((ImpliesTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__NARY_TERM:
				setNaryTerm((NaryTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__NOT_TERM:
				setNotTerm((NotTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__OR_TERM:
				setOrTerm((OrTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__TRUE_TERM:
				setTrueTerm((TrueTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__UNARY_TERM:
				setUnaryTerm((UnaryTerm)newValue);
				return;
			case TermsPackage.DOCUMENT_ROOT__XOR_TERM:
				setXorTerm((XorTerm)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case TermsPackage.DOCUMENT_ROOT__MIXED:
				getMixed().clear();
				return;
			case TermsPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				getXMLNSPrefixMap().clear();
				return;
			case TermsPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				getXSISchemaLocation().clear();
				return;
			case TermsPackage.DOCUMENT_ROOT__ABSTRACT_MATH_TERM:
				setAbstractMathTerm((AbstractMathTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__AND_TERM:
				setAndTerm((AndTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__ATOMIC_TERM:
				setAtomicTerm((AtomicTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__BINARY_TERM:
				setBinaryTerm((BinaryTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__EQUIVALENT_TERM:
				setEquivalentTerm((EquivalentTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__ERROR_STATUS_TERM:
				setErrorStatusTerm((ErrorStatusTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__EXCLUDES_TERM:
				setExcludesTerm((ExcludesTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__FALSE_TERM:
				setFalseTerm((FalseTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__IMPLIES_TERM:
				setImpliesTerm((ImpliesTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__NARY_TERM:
				setNaryTerm((NaryTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__NOT_TERM:
				setNotTerm((NotTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__OR_TERM:
				setOrTerm((OrTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__TRUE_TERM:
				setTrueTerm((TrueTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__UNARY_TERM:
				setUnaryTerm((UnaryTerm)null);
				return;
			case TermsPackage.DOCUMENT_ROOT__XOR_TERM:
				setXorTerm((XorTerm)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TermsPackage.DOCUMENT_ROOT__MIXED:
				return mixed != null && !mixed.isEmpty();
			case TermsPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return xMLNSPrefixMap != null && !xMLNSPrefixMap.isEmpty();
			case TermsPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return xSISchemaLocation != null && !xSISchemaLocation.isEmpty();
			case TermsPackage.DOCUMENT_ROOT__ABSTRACT_MATH_TERM:
				return getAbstractMathTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__AND_TERM:
				return getAndTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__ATOMIC_TERM:
				return getAtomicTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__BINARY_TERM:
				return getBinaryTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__EQUIVALENT_TERM:
				return getEquivalentTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__ERROR_STATUS_TERM:
				return getErrorStatusTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__EXCLUDES_TERM:
				return getExcludesTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__FALSE_TERM:
				return getFalseTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__IMPLIES_TERM:
				return getImpliesTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__NARY_TERM:
				return getNaryTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__NOT_TERM:
				return getNotTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__OR_TERM:
				return getOrTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__TRUE_TERM:
				return getTrueTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__UNARY_TERM:
				return getUnaryTerm() != null;
			case TermsPackage.DOCUMENT_ROOT__XOR_TERM:
				return getXorTerm() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //DocumentRootImpl
