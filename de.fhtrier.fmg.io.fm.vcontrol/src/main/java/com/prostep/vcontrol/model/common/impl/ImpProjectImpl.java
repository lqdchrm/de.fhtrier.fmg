/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.CommonPackage;
import com.prostep.vcontrol.model.common.ImpProject;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Imp Project</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpProjectImpl#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpProjectImpl#getModels <em>Models</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpProjectImpl#getImpConfigurations <em>Imp Configurations</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpProjectImpl#getImpExtensions <em>Imp Extensions</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ImpProjectImpl extends ImpItemImpl implements ImpProject {
	/**
	 * The cached value of the '{@link #getGroup1() <em>Group1</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup1()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImpProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CommonPackage.Literals.IMP_PROJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup1() {
		if (group1 == null) {
			group1 = new BasicFeatureMap(this, CommonPackage.IMP_PROJECT__GROUP1);
		}
		return group1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getModels() {
		return getGroup1().list(CommonPackage.Literals.IMP_PROJECT__MODELS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getImpConfigurations() {
		return getGroup1().list(CommonPackage.Literals.IMP_PROJECT__IMP_CONFIGURATIONS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getImpExtensions() {
		return getGroup1().list(CommonPackage.Literals.IMP_PROJECT__IMP_EXTENSIONS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CommonPackage.IMP_PROJECT__GROUP1:
				return ((InternalEList)getGroup1()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_PROJECT__MODELS:
				return ((InternalEList)getModels()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_PROJECT__IMP_CONFIGURATIONS:
				return ((InternalEList)getImpConfigurations()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_PROJECT__IMP_EXTENSIONS:
				return ((InternalEList)getImpExtensions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CommonPackage.IMP_PROJECT__GROUP1:
				if (coreType) return getGroup1();
				return ((FeatureMap.Internal)getGroup1()).getWrapper();
			case CommonPackage.IMP_PROJECT__MODELS:
				return getModels();
			case CommonPackage.IMP_PROJECT__IMP_CONFIGURATIONS:
				return getImpConfigurations();
			case CommonPackage.IMP_PROJECT__IMP_EXTENSIONS:
				return getImpExtensions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CommonPackage.IMP_PROJECT__GROUP1:
				((FeatureMap.Internal)getGroup1()).set(newValue);
				return;
			case CommonPackage.IMP_PROJECT__MODELS:
				getModels().clear();
				getModels().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_PROJECT__IMP_CONFIGURATIONS:
				getImpConfigurations().clear();
				getImpConfigurations().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_PROJECT__IMP_EXTENSIONS:
				getImpExtensions().clear();
				getImpExtensions().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_PROJECT__GROUP1:
				getGroup1().clear();
				return;
			case CommonPackage.IMP_PROJECT__MODELS:
				getModels().clear();
				return;
			case CommonPackage.IMP_PROJECT__IMP_CONFIGURATIONS:
				getImpConfigurations().clear();
				return;
			case CommonPackage.IMP_PROJECT__IMP_EXTENSIONS:
				getImpExtensions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_PROJECT__GROUP1:
				return group1 != null && !group1.isEmpty();
			case CommonPackage.IMP_PROJECT__MODELS:
				return !getModels().isEmpty();
			case CommonPackage.IMP_PROJECT__IMP_CONFIGURATIONS:
				return !getImpConfigurations().isEmpty();
			case CommonPackage.IMP_PROJECT__IMP_EXTENSIONS:
				return !getImpExtensions().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group1: ");
		result.append(group1);
		result.append(')');
		return result.toString();
	}

} //ImpProjectImpl
