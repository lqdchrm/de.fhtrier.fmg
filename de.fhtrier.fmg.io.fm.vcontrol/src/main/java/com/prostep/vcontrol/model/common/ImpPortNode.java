/**
 */
package com.prostep.vcontrol.model.common;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Imp Port Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpPortNode#getGroup2 <em>Group2</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForSourcePort <em>Connectors For Source Port</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForTargetPort <em>Connectors For Target Port</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForSourcePort1 <em>Connectors For Source Port1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForTargetPort1 <em>Connectors For Target Port1</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpPortNode()
 * @model abstract="true"
 *        extendedMetaData="name='ImpPortNode' kind='elementOnly'"
 * @generated
 */
public interface ImpPortNode extends ImpConfigurableElement {
	/**
	 * Returns the value of the '<em><b>Group2</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group2</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group2</em>' attribute list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpPortNode_Group2()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:21'"
	 * @generated
	 */
	FeatureMap getGroup2();

	/**
	 * Returns the value of the '<em><b>Connectors For Source Port</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpConnector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connectors For Source Port</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connectors For Source Port</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpPortNode_ConnectorsForSourcePort()
	 * @model type="com.prostep.vcontrol.model.common.ImpConnector" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='connectorsForSourcePort' group='#group:21'"
	 * @generated
	 */
	EList getConnectorsForSourcePort();

	/**
	 * Returns the value of the '<em><b>Connectors For Target Port</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpConnector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connectors For Target Port</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connectors For Target Port</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpPortNode_ConnectorsForTargetPort()
	 * @model type="com.prostep.vcontrol.model.common.ImpConnector" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='connectorsForTargetPort' group='#group:21'"
	 * @generated
	 */
	EList getConnectorsForTargetPort();

	/**
	 * Returns the value of the '<em><b>Connectors For Source Port1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connectors For Source Port1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connectors For Source Port1</em>' attribute.
	 * @see #setConnectorsForSourcePort1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpPortNode_ConnectorsForSourcePort1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='connectorsForSourcePort'"
	 * @generated
	 */
	String getConnectorsForSourcePort1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForSourcePort1 <em>Connectors For Source Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connectors For Source Port1</em>' attribute.
	 * @see #getConnectorsForSourcePort1()
	 * @generated
	 */
	void setConnectorsForSourcePort1(String value);

	/**
	 * Returns the value of the '<em><b>Connectors For Target Port1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connectors For Target Port1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connectors For Target Port1</em>' attribute.
	 * @see #setConnectorsForTargetPort1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpPortNode_ConnectorsForTargetPort1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='connectorsForTargetPort'"
	 * @generated
	 */
	String getConnectorsForTargetPort1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForTargetPort1 <em>Connectors For Target Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connectors For Target Port1</em>' attribute.
	 * @see #getConnectorsForTargetPort1()
	 * @generated
	 */
	void setConnectorsForTargetPort1(String value);

} // ImpPortNode
