/**
 */
package com.prostep.vcontrol.model.common;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Imp Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfiguration#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfiguration#getImpConfiguredElements <em>Imp Configured Elements</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfiguration#getModels <em>Models</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfiguration#getProject <em>Project</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfiguration#getModels1 <em>Models1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfiguration#getProject1 <em>Project1</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfiguration()
 * @model extendedMetaData="name='ImpConfiguration' kind='elementOnly'"
 * @generated
 */
public interface ImpConfiguration extends ImpItem {
	/**
	 * Returns the value of the '<em><b>Group1</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group1</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group1</em>' attribute list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfiguration_Group1()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:13'"
	 * @generated
	 */
	FeatureMap getGroup1();

	/**
	 * Returns the value of the '<em><b>Imp Configured Elements</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpConfiguredElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imp Configured Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp Configured Elements</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfiguration_ImpConfiguredElements()
	 * @model type="com.prostep.vcontrol.model.common.ImpConfiguredElement" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='impConfiguredElements' group='#group:13'"
	 * @generated
	 */
	EList getImpConfiguredElements();

	/**
	 * Returns the value of the '<em><b>Models</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Models</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfiguration_Models()
	 * @model type="com.prostep.vcontrol.model.common.ImpModel" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='models' group='#group:13'"
	 * @generated
	 */
	EList getModels();

	/**
	 * Returns the value of the '<em><b>Project</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpProject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfiguration_Project()
	 * @model type="com.prostep.vcontrol.model.common.ImpProject" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='project' group='#group:13'"
	 * @generated
	 */
	EList getProject();

	/**
	 * Returns the value of the '<em><b>Models1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Models1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Models1</em>' attribute.
	 * @see #setModels1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfiguration_Models1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='models'"
	 * @generated
	 */
	String getModels1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpConfiguration#getModels1 <em>Models1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Models1</em>' attribute.
	 * @see #getModels1()
	 * @generated
	 */
	void setModels1(String value);

	/**
	 * Returns the value of the '<em><b>Project1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project1</em>' attribute.
	 * @see #setProject1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfiguration_Project1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='project'"
	 * @generated
	 */
	String getProject1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpConfiguration#getProject1 <em>Project1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project1</em>' attribute.
	 * @see #getProject1()
	 * @generated
	 */
	void setProject1(String value);

} // ImpConfiguration
