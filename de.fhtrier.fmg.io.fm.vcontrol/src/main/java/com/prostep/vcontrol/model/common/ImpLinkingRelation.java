/**
 */
package com.prostep.vcontrol.model.common;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Imp Linking Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getGroup <em>Group</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getRightElement <em>Right Element</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLeftElement <em>Left Element</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getExtension <em>Extension</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getHref <em>Href</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getId <em>Id</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getIdref <em>Idref</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLabel <em>Label</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLeftElement1 <em>Left Element1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLinkingType <em>Linking Type</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getRightElement1 <em>Right Element1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getType <em>Type</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getUuid <em>Uuid</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getVersion <em>Version</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation()
 * @model extendedMetaData="name='ImpLinkingRelation' kind='elementOnly'"
 * @generated
 */
public interface ImpLinkingRelation extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Right Element</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpConfigurableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Element</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_RightElement()
	 * @model type="com.prostep.vcontrol.model.common.ImpConfigurableElement" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='rightElement' group='#group:0'"
	 * @generated
	 */
	EList getRightElement();

	/**
	 * Returns the value of the '<em><b>Left Element</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpConfigurableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Element</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_LeftElement()
	 * @model type="com.prostep.vcontrol.model.common.ImpConfigurableElement" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='leftElement' group='#group:0'"
	 * @generated
	 */
	EList getLeftElement();

	/**
	 * Returns the value of the '<em><b>Extension</b></em>' containment reference list.
	 * The list contents are of type {@link xmi.Extension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extension</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_Extension()
	 * @model type="xmi.Extension" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Extension' namespace='http://www.omg.org/XMI' group='#group:0'"
	 * @generated
	 */
	EList getExtension();

	/**
	 * Returns the value of the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Href</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Href</em>' attribute.
	 * @see #setHref(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_Href()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='href'"
	 * @generated
	 */
	String getHref();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getHref <em>Href</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Href</em>' attribute.
	 * @see #getHref()
	 * @generated
	 */
	void setHref(String value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_Id()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID"
	 *        extendedMetaData="kind='attribute' name='id' namespace='http://www.omg.org/XMI'"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Idref</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Idref</em>' attribute.
	 * @see #setIdref(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_Idref()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.IDREF"
	 *        extendedMetaData="kind='attribute' name='idref' namespace='http://www.omg.org/XMI'"
	 * @generated
	 */
	String getIdref();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getIdref <em>Idref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Idref</em>' attribute.
	 * @see #getIdref()
	 * @generated
	 */
	void setIdref(String value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_Label()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='label' namespace='http://www.omg.org/XMI'"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Left Element1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Element1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Element1</em>' attribute.
	 * @see #setLeftElement1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_LeftElement1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='leftElement'"
	 * @generated
	 */
	String getLeftElement1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLeftElement1 <em>Left Element1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Element1</em>' attribute.
	 * @see #getLeftElement1()
	 * @generated
	 */
	void setLeftElement1(String value);

	/**
	 * Returns the value of the '<em><b>Linking Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.prostep.vcontrol.model.common.LinkingType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linking Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linking Type</em>' attribute.
	 * @see com.prostep.vcontrol.model.common.LinkingType
	 * @see #isSetLinkingType()
	 * @see #unsetLinkingType()
	 * @see #setLinkingType(LinkingType)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_LinkingType()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='linkingType'"
	 * @generated
	 */
	LinkingType getLinkingType();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLinkingType <em>Linking Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Linking Type</em>' attribute.
	 * @see com.prostep.vcontrol.model.common.LinkingType
	 * @see #isSetLinkingType()
	 * @see #unsetLinkingType()
	 * @see #getLinkingType()
	 * @generated
	 */
	void setLinkingType(LinkingType value);

	/**
	 * Unsets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLinkingType <em>Linking Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLinkingType()
	 * @see #getLinkingType()
	 * @see #setLinkingType(LinkingType)
	 * @generated
	 */
	void unsetLinkingType();

	/**
	 * Returns whether the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLinkingType <em>Linking Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Linking Type</em>' attribute is set.
	 * @see #unsetLinkingType()
	 * @see #getLinkingType()
	 * @see #setLinkingType(LinkingType)
	 * @generated
	 */
	boolean isSetLinkingType();

	/**
	 * Returns the value of the '<em><b>Right Element1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Element1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Element1</em>' attribute.
	 * @see #setRightElement1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_RightElement1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='rightElement'"
	 * @generated
	 */
	String getRightElement1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getRightElement1 <em>Right Element1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Element1</em>' attribute.
	 * @see #getRightElement1()
	 * @generated
	 */
	void setRightElement1(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(Object)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.QName"
	 *        extendedMetaData="kind='attribute' name='type' namespace='http://www.omg.org/XMI'"
	 * @generated
	 */
	Object getType();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(Object value);

	/**
	 * Returns the value of the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uuid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uuid</em>' attribute.
	 * @see #setUuid(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_Uuid()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='uuid' namespace='http://www.omg.org/XMI'"
	 * @generated
	 */
	String getUuid();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getUuid <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uuid</em>' attribute.
	 * @see #getUuid()
	 * @generated
	 */
	void setUuid(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * The default value is <code>"2.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #isSetVersion()
	 * @see #unsetVersion()
	 * @see #setVersion(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpLinkingRelation_Version()
	 * @model default="2.0" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='version' namespace='http://www.omg.org/XMI'"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #isSetVersion()
	 * @see #unsetVersion()
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Unsets the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVersion()
	 * @see #getVersion()
	 * @see #setVersion(String)
	 * @generated
	 */
	void unsetVersion();

	/**
	 * Returns whether the value of the '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getVersion <em>Version</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Version</em>' attribute is set.
	 * @see #unsetVersion()
	 * @see #getVersion()
	 * @see #setVersion(String)
	 * @generated
	 */
	boolean isSetVersion();

} // ImpLinkingRelation
