/**
 */
package com.prostep.vcontrol.model.feature.impl;

import com.prostep.vcontrol.model.feature.FeatureOccurrence;
import com.prostep.vcontrol.model.feature.FeaturePackage;

import com.prostep.vcontrol.model.terms.impl.AtomicTermImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Occurrence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.FeatureOccurrenceImpl#getGroup2 <em>Group2</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.FeatureOccurrenceImpl#getFeatureNode <em>Feature Node</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.impl.FeatureOccurrenceImpl#getFeatureNode1 <em>Feature Node1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FeatureOccurrenceImpl extends AtomicTermImpl implements FeatureOccurrence {
	/**
	 * The cached value of the '{@link #getGroup2() <em>Group2</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup2()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group2;

	/**
	 * The default value of the '{@link #getFeatureNode1() <em>Feature Node1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureNode1()
	 * @generated
	 * @ordered
	 */
	protected static final String FEATURE_NODE1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFeatureNode1() <em>Feature Node1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureNode1()
	 * @generated
	 * @ordered
	 */
	protected String featureNode1 = FEATURE_NODE1_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureOccurrenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return FeaturePackage.Literals.FEATURE_OCCURRENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup2() {
		if (group2 == null) {
			group2 = new BasicFeatureMap(this, FeaturePackage.FEATURE_OCCURRENCE__GROUP2);
		}
		return group2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getFeatureNode() {
		return getGroup2().list(FeaturePackage.Literals.FEATURE_OCCURRENCE__FEATURE_NODE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFeatureNode1() {
		return featureNode1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureNode1(String newFeatureNode1) {
		String oldFeatureNode1 = featureNode1;
		featureNode1 = newFeatureNode1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeaturePackage.FEATURE_OCCURRENCE__FEATURE_NODE1, oldFeatureNode1, featureNode1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FeaturePackage.FEATURE_OCCURRENCE__GROUP2:
				return ((InternalEList)getGroup2()).basicRemove(otherEnd, msgs);
			case FeaturePackage.FEATURE_OCCURRENCE__FEATURE_NODE:
				return ((InternalEList)getFeatureNode()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FeaturePackage.FEATURE_OCCURRENCE__GROUP2:
				if (coreType) return getGroup2();
				return ((FeatureMap.Internal)getGroup2()).getWrapper();
			case FeaturePackage.FEATURE_OCCURRENCE__FEATURE_NODE:
				return getFeatureNode();
			case FeaturePackage.FEATURE_OCCURRENCE__FEATURE_NODE1:
				return getFeatureNode1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FeaturePackage.FEATURE_OCCURRENCE__GROUP2:
				((FeatureMap.Internal)getGroup2()).set(newValue);
				return;
			case FeaturePackage.FEATURE_OCCURRENCE__FEATURE_NODE:
				getFeatureNode().clear();
				getFeatureNode().addAll((Collection)newValue);
				return;
			case FeaturePackage.FEATURE_OCCURRENCE__FEATURE_NODE1:
				setFeatureNode1((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case FeaturePackage.FEATURE_OCCURRENCE__GROUP2:
				getGroup2().clear();
				return;
			case FeaturePackage.FEATURE_OCCURRENCE__FEATURE_NODE:
				getFeatureNode().clear();
				return;
			case FeaturePackage.FEATURE_OCCURRENCE__FEATURE_NODE1:
				setFeatureNode1(FEATURE_NODE1_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FeaturePackage.FEATURE_OCCURRENCE__GROUP2:
				return group2 != null && !group2.isEmpty();
			case FeaturePackage.FEATURE_OCCURRENCE__FEATURE_NODE:
				return !getFeatureNode().isEmpty();
			case FeaturePackage.FEATURE_OCCURRENCE__FEATURE_NODE1:
				return FEATURE_NODE1_EDEFAULT == null ? featureNode1 != null : !FEATURE_NODE1_EDEFAULT.equals(featureNode1);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group2: ");
		result.append(group2);
		result.append(", featureNode1: ");
		result.append(featureNode1);
		result.append(')');
		return result.toString();
	}

} //FeatureOccurrenceImpl
