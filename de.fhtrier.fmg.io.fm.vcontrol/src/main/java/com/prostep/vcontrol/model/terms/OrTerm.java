/**
 */
package com.prostep.vcontrol.model.terms;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Or Term</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getOrTerm()
 * @model extendedMetaData="name='OrTerm' kind='elementOnly'"
 * @generated
 */
public interface OrTerm extends NaryTerm {
} // OrTerm
