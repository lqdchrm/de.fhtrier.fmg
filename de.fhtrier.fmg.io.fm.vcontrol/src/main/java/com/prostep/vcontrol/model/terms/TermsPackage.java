/**
 */
package com.prostep.vcontrol.model.terms;

import com.prostep.vcontrol.model.common.CommonPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.terms.TermsFactory
 * @model kind="package"
 * @generated
 */
public interface TermsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "terms";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.prostep.com/vcontrol/model/terms.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "com.prostep.vcontrol.model.terms";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TermsPackage eINSTANCE = com.prostep.vcontrol.model.terms.impl.TermsPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.AtomicTermImpl <em>Atomic Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.AtomicTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getAtomicTerm()
	 * @generated
	 */
	int ATOMIC_TERM = 2;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_TERM__GROUP = CommonPackage.IMP_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_TERM__EXTENSION = CommonPackage.IMP_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_TERM__HREF = CommonPackage.IMP_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_TERM__ID = CommonPackage.IMP_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_TERM__IDREF = CommonPackage.IMP_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_TERM__LABEL = CommonPackage.IMP_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_TERM__TYPE = CommonPackage.IMP_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_TERM__UUID = CommonPackage.IMP_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_TERM__VERSION = CommonPackage.IMP_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_TERM__ELEMENT = CommonPackage.IMP_TERM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Atomic Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_TERM_FEATURE_COUNT = CommonPackage.IMP_TERM_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.AbstractMathTermImpl <em>Abstract Math Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.AbstractMathTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getAbstractMathTerm()
	 * @generated
	 */
	int ABSTRACT_MATH_TERM = 0;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MATH_TERM__GROUP = ATOMIC_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MATH_TERM__EXTENSION = ATOMIC_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MATH_TERM__HREF = ATOMIC_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MATH_TERM__ID = ATOMIC_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MATH_TERM__IDREF = ATOMIC_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MATH_TERM__LABEL = ATOMIC_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MATH_TERM__TYPE = ATOMIC_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MATH_TERM__UUID = ATOMIC_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MATH_TERM__VERSION = ATOMIC_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MATH_TERM__ELEMENT = ATOMIC_TERM__ELEMENT;

	/**
	 * The number of structural features of the '<em>Abstract Math Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MATH_TERM_FEATURE_COUNT = ATOMIC_TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.NaryTermImpl <em>Nary Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.NaryTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getNaryTerm()
	 * @generated
	 */
	int NARY_TERM = 10;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM__GROUP = CommonPackage.IMP_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM__EXTENSION = CommonPackage.IMP_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM__HREF = CommonPackage.IMP_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM__ID = CommonPackage.IMP_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM__IDREF = CommonPackage.IMP_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM__LABEL = CommonPackage.IMP_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM__TYPE = CommonPackage.IMP_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM__UUID = CommonPackage.IMP_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM__VERSION = CommonPackage.IMP_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM__GROUP1 = CommonPackage.IMP_TERM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM__OPERANDS = CommonPackage.IMP_TERM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Nary Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NARY_TERM_FEATURE_COUNT = CommonPackage.IMP_TERM_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.AndTermImpl <em>And Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.AndTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getAndTerm()
	 * @generated
	 */
	int AND_TERM = 1;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM__GROUP = NARY_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM__EXTENSION = NARY_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM__HREF = NARY_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM__ID = NARY_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM__IDREF = NARY_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM__LABEL = NARY_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM__TYPE = NARY_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM__UUID = NARY_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM__VERSION = NARY_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM__GROUP1 = NARY_TERM__GROUP1;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM__OPERANDS = NARY_TERM__OPERANDS;

	/**
	 * The number of structural features of the '<em>And Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_TERM_FEATURE_COUNT = NARY_TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.BinaryTermImpl <em>Binary Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.BinaryTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getBinaryTerm()
	 * @generated
	 */
	int BINARY_TERM = 3;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__GROUP = CommonPackage.IMP_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__EXTENSION = CommonPackage.IMP_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__HREF = CommonPackage.IMP_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__ID = CommonPackage.IMP_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__IDREF = CommonPackage.IMP_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__LABEL = CommonPackage.IMP_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__TYPE = CommonPackage.IMP_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__UUID = CommonPackage.IMP_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__VERSION = CommonPackage.IMP_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__GROUP1 = CommonPackage.IMP_TERM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Left Operand</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__LEFT_OPERAND = CommonPackage.IMP_TERM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right Operand</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM__RIGHT_OPERAND = CommonPackage.IMP_TERM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Binary Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_TERM_FEATURE_COUNT = CommonPackage.IMP_TERM_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.DocumentRootImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 4;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Abstract Math Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ABSTRACT_MATH_TERM = 3;

	/**
	 * The feature id for the '<em><b>And Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__AND_TERM = 4;

	/**
	 * The feature id for the '<em><b>Atomic Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ATOMIC_TERM = 5;

	/**
	 * The feature id for the '<em><b>Binary Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__BINARY_TERM = 6;

	/**
	 * The feature id for the '<em><b>Equivalent Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__EQUIVALENT_TERM = 7;

	/**
	 * The feature id for the '<em><b>Error Status Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ERROR_STATUS_TERM = 8;

	/**
	 * The feature id for the '<em><b>Excludes Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__EXCLUDES_TERM = 9;

	/**
	 * The feature id for the '<em><b>False Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__FALSE_TERM = 10;

	/**
	 * The feature id for the '<em><b>Implies Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMPLIES_TERM = 11;

	/**
	 * The feature id for the '<em><b>Nary Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__NARY_TERM = 12;

	/**
	 * The feature id for the '<em><b>Not Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__NOT_TERM = 13;

	/**
	 * The feature id for the '<em><b>Or Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__OR_TERM = 14;

	/**
	 * The feature id for the '<em><b>True Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__TRUE_TERM = 15;

	/**
	 * The feature id for the '<em><b>Unary Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__UNARY_TERM = 16;

	/**
	 * The feature id for the '<em><b>Xor Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XOR_TERM = 17;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 18;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.EquivalentTermImpl <em>Equivalent Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.EquivalentTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getEquivalentTerm()
	 * @generated
	 */
	int EQUIVALENT_TERM = 5;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__GROUP = BINARY_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__EXTENSION = BINARY_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__HREF = BINARY_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__ID = BINARY_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__IDREF = BINARY_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__LABEL = BINARY_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__TYPE = BINARY_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__UUID = BINARY_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__VERSION = BINARY_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__GROUP1 = BINARY_TERM__GROUP1;

	/**
	 * The feature id for the '<em><b>Left Operand</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__LEFT_OPERAND = BINARY_TERM__LEFT_OPERAND;

	/**
	 * The feature id for the '<em><b>Right Operand</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM__RIGHT_OPERAND = BINARY_TERM__RIGHT_OPERAND;

	/**
	 * The number of structural features of the '<em>Equivalent Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUIVALENT_TERM_FEATURE_COUNT = BINARY_TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.ErrorStatusTermImpl <em>Error Status Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.ErrorStatusTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getErrorStatusTerm()
	 * @generated
	 */
	int ERROR_STATUS_TERM = 6;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM__GROUP = CommonPackage.IMP_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM__EXTENSION = CommonPackage.IMP_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM__HREF = CommonPackage.IMP_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM__ID = CommonPackage.IMP_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM__IDREF = CommonPackage.IMP_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM__LABEL = CommonPackage.IMP_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM__TYPE = CommonPackage.IMP_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM__UUID = CommonPackage.IMP_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM__VERSION = CommonPackage.IMP_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Error Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM__ERROR_STATUS = CommonPackage.IMP_TERM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM__MESSAGE = CommonPackage.IMP_TERM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Error Status Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ERROR_STATUS_TERM_FEATURE_COUNT = CommonPackage.IMP_TERM_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.ExcludesTermImpl <em>Excludes Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.ExcludesTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getExcludesTerm()
	 * @generated
	 */
	int EXCLUDES_TERM = 7;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__GROUP = BINARY_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__EXTENSION = BINARY_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__HREF = BINARY_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__ID = BINARY_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__IDREF = BINARY_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__LABEL = BINARY_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__TYPE = BINARY_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__UUID = BINARY_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__VERSION = BINARY_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__GROUP1 = BINARY_TERM__GROUP1;

	/**
	 * The feature id for the '<em><b>Left Operand</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__LEFT_OPERAND = BINARY_TERM__LEFT_OPERAND;

	/**
	 * The feature id for the '<em><b>Right Operand</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM__RIGHT_OPERAND = BINARY_TERM__RIGHT_OPERAND;

	/**
	 * The number of structural features of the '<em>Excludes Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDES_TERM_FEATURE_COUNT = BINARY_TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.FalseTermImpl <em>False Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.FalseTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getFalseTerm()
	 * @generated
	 */
	int FALSE_TERM = 8;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_TERM__GROUP = CommonPackage.IMP_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_TERM__EXTENSION = CommonPackage.IMP_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_TERM__HREF = CommonPackage.IMP_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_TERM__ID = CommonPackage.IMP_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_TERM__IDREF = CommonPackage.IMP_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_TERM__LABEL = CommonPackage.IMP_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_TERM__TYPE = CommonPackage.IMP_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_TERM__UUID = CommonPackage.IMP_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_TERM__VERSION = CommonPackage.IMP_TERM__VERSION;

	/**
	 * The number of structural features of the '<em>False Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_TERM_FEATURE_COUNT = CommonPackage.IMP_TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.ImpliesTermImpl <em>Implies Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.ImpliesTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getImpliesTerm()
	 * @generated
	 */
	int IMPLIES_TERM = 9;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__GROUP = BINARY_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__EXTENSION = BINARY_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__HREF = BINARY_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__ID = BINARY_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__IDREF = BINARY_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__LABEL = BINARY_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__TYPE = BINARY_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__UUID = BINARY_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__VERSION = BINARY_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__GROUP1 = BINARY_TERM__GROUP1;

	/**
	 * The feature id for the '<em><b>Left Operand</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__LEFT_OPERAND = BINARY_TERM__LEFT_OPERAND;

	/**
	 * The feature id for the '<em><b>Right Operand</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM__RIGHT_OPERAND = BINARY_TERM__RIGHT_OPERAND;

	/**
	 * The number of structural features of the '<em>Implies Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLIES_TERM_FEATURE_COUNT = BINARY_TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.UnaryTermImpl <em>Unary Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.UnaryTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getUnaryTerm()
	 * @generated
	 */
	int UNARY_TERM = 14;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM__GROUP = CommonPackage.IMP_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM__EXTENSION = CommonPackage.IMP_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM__HREF = CommonPackage.IMP_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM__ID = CommonPackage.IMP_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM__IDREF = CommonPackage.IMP_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM__LABEL = CommonPackage.IMP_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM__TYPE = CommonPackage.IMP_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM__UUID = CommonPackage.IMP_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM__VERSION = CommonPackage.IMP_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM__GROUP1 = CommonPackage.IMP_TERM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM__OPERAND = CommonPackage.IMP_TERM_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Unary Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_TERM_FEATURE_COUNT = CommonPackage.IMP_TERM_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.NotTermImpl <em>Not Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.NotTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getNotTerm()
	 * @generated
	 */
	int NOT_TERM = 11;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM__GROUP = UNARY_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM__EXTENSION = UNARY_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM__HREF = UNARY_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM__ID = UNARY_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM__IDREF = UNARY_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM__LABEL = UNARY_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM__TYPE = UNARY_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM__UUID = UNARY_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM__VERSION = UNARY_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM__GROUP1 = UNARY_TERM__GROUP1;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM__OPERAND = UNARY_TERM__OPERAND;

	/**
	 * The number of structural features of the '<em>Not Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_TERM_FEATURE_COUNT = UNARY_TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.OrTermImpl <em>Or Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.OrTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getOrTerm()
	 * @generated
	 */
	int OR_TERM = 12;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM__GROUP = NARY_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM__EXTENSION = NARY_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM__HREF = NARY_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM__ID = NARY_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM__IDREF = NARY_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM__LABEL = NARY_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM__TYPE = NARY_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM__UUID = NARY_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM__VERSION = NARY_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM__GROUP1 = NARY_TERM__GROUP1;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM__OPERANDS = NARY_TERM__OPERANDS;

	/**
	 * The number of structural features of the '<em>Or Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_TERM_FEATURE_COUNT = NARY_TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.TrueTermImpl <em>True Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.TrueTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getTrueTerm()
	 * @generated
	 */
	int TRUE_TERM = 13;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_TERM__GROUP = CommonPackage.IMP_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_TERM__EXTENSION = CommonPackage.IMP_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_TERM__HREF = CommonPackage.IMP_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_TERM__ID = CommonPackage.IMP_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_TERM__IDREF = CommonPackage.IMP_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_TERM__LABEL = CommonPackage.IMP_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_TERM__TYPE = CommonPackage.IMP_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_TERM__UUID = CommonPackage.IMP_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_TERM__VERSION = CommonPackage.IMP_TERM__VERSION;

	/**
	 * The number of structural features of the '<em>True Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_TERM_FEATURE_COUNT = CommonPackage.IMP_TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.impl.XorTermImpl <em>Xor Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.impl.XorTermImpl
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getXorTerm()
	 * @generated
	 */
	int XOR_TERM = 15;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM__GROUP = NARY_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM__EXTENSION = NARY_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM__HREF = NARY_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM__ID = NARY_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM__IDREF = NARY_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM__LABEL = NARY_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM__TYPE = NARY_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM__UUID = NARY_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM__VERSION = NARY_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM__GROUP1 = NARY_TERM__GROUP1;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM__OPERANDS = NARY_TERM__OPERANDS;

	/**
	 * The number of structural features of the '<em>Xor Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_TERM_FEATURE_COUNT = NARY_TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.terms.ErrorStatus <em>Error Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.ErrorStatus
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getErrorStatus()
	 * @generated
	 */
	int ERROR_STATUS = 16;

	/**
	 * The meta object id for the '<em>Error Status Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.terms.ErrorStatus
	 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getErrorStatusObject()
	 * @generated
	 */
	int ERROR_STATUS_OBJECT = 17;


	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.AbstractMathTerm <em>Abstract Math Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Math Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.AbstractMathTerm
	 * @generated
	 */
	EClass getAbstractMathTerm();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.AndTerm <em>And Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.AndTerm
	 * @generated
	 */
	EClass getAndTerm();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.AtomicTerm <em>Atomic Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atomic Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.AtomicTerm
	 * @generated
	 */
	EClass getAtomicTerm();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.terms.AtomicTerm#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element</em>'.
	 * @see com.prostep.vcontrol.model.terms.AtomicTerm#getElement()
	 * @see #getAtomicTerm()
	 * @generated
	 */
	EAttribute getAtomicTerm_Element();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.BinaryTerm <em>Binary Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.BinaryTerm
	 * @generated
	 */
	EClass getBinaryTerm();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.terms.BinaryTerm#getGroup1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group1</em>'.
	 * @see com.prostep.vcontrol.model.terms.BinaryTerm#getGroup1()
	 * @see #getBinaryTerm()
	 * @generated
	 */
	EAttribute getBinaryTerm_Group1();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.terms.BinaryTerm#getLeftOperand <em>Left Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Left Operand</em>'.
	 * @see com.prostep.vcontrol.model.terms.BinaryTerm#getLeftOperand()
	 * @see #getBinaryTerm()
	 * @generated
	 */
	EReference getBinaryTerm_LeftOperand();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.terms.BinaryTerm#getRightOperand <em>Right Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Right Operand</em>'.
	 * @see com.prostep.vcontrol.model.terms.BinaryTerm#getRightOperand()
	 * @see #getBinaryTerm()
	 * @generated
	 */
	EReference getBinaryTerm_RightOperand();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getAbstractMathTerm <em>Abstract Math Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Abstract Math Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getAbstractMathTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_AbstractMathTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getAndTerm <em>And Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>And Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getAndTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_AndTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getAtomicTerm <em>Atomic Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Atomic Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getAtomicTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_AtomicTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getBinaryTerm <em>Binary Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Binary Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getBinaryTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_BinaryTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getEquivalentTerm <em>Equivalent Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Equivalent Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getEquivalentTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_EquivalentTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getErrorStatusTerm <em>Error Status Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Error Status Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getErrorStatusTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ErrorStatusTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getExcludesTerm <em>Excludes Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Excludes Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getExcludesTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ExcludesTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getFalseTerm <em>False Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>False Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getFalseTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_FalseTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getImpliesTerm <em>Implies Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Implies Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getImpliesTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpliesTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getNaryTerm <em>Nary Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Nary Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getNaryTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_NaryTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getNotTerm <em>Not Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Not Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getNotTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_NotTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getOrTerm <em>Or Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Or Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getOrTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_OrTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getTrueTerm <em>True Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>True Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getTrueTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_TrueTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getUnaryTerm <em>Unary Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Unary Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getUnaryTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_UnaryTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getXorTerm <em>Xor Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Xor Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot#getXorTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XorTerm();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.EquivalentTerm <em>Equivalent Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equivalent Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.EquivalentTerm
	 * @generated
	 */
	EClass getEquivalentTerm();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.ErrorStatusTerm <em>Error Status Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Error Status Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.ErrorStatusTerm
	 * @generated
	 */
	EClass getErrorStatusTerm();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.terms.ErrorStatusTerm#getErrorStatus <em>Error Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Error Status</em>'.
	 * @see com.prostep.vcontrol.model.terms.ErrorStatusTerm#getErrorStatus()
	 * @see #getErrorStatusTerm()
	 * @generated
	 */
	EAttribute getErrorStatusTerm_ErrorStatus();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.terms.ErrorStatusTerm#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message</em>'.
	 * @see com.prostep.vcontrol.model.terms.ErrorStatusTerm#getMessage()
	 * @see #getErrorStatusTerm()
	 * @generated
	 */
	EAttribute getErrorStatusTerm_Message();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.ExcludesTerm <em>Excludes Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Excludes Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.ExcludesTerm
	 * @generated
	 */
	EClass getExcludesTerm();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.FalseTerm <em>False Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>False Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.FalseTerm
	 * @generated
	 */
	EClass getFalseTerm();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.ImpliesTerm <em>Implies Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Implies Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.ImpliesTerm
	 * @generated
	 */
	EClass getImpliesTerm();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.NaryTerm <em>Nary Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nary Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.NaryTerm
	 * @generated
	 */
	EClass getNaryTerm();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.terms.NaryTerm#getGroup1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group1</em>'.
	 * @see com.prostep.vcontrol.model.terms.NaryTerm#getGroup1()
	 * @see #getNaryTerm()
	 * @generated
	 */
	EAttribute getNaryTerm_Group1();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.terms.NaryTerm#getOperands <em>Operands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operands</em>'.
	 * @see com.prostep.vcontrol.model.terms.NaryTerm#getOperands()
	 * @see #getNaryTerm()
	 * @generated
	 */
	EReference getNaryTerm_Operands();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.NotTerm <em>Not Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Not Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.NotTerm
	 * @generated
	 */
	EClass getNotTerm();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.OrTerm <em>Or Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.OrTerm
	 * @generated
	 */
	EClass getOrTerm();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.TrueTerm <em>True Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>True Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.TrueTerm
	 * @generated
	 */
	EClass getTrueTerm();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.UnaryTerm <em>Unary Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.UnaryTerm
	 * @generated
	 */
	EClass getUnaryTerm();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.terms.UnaryTerm#getGroup1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group1</em>'.
	 * @see com.prostep.vcontrol.model.terms.UnaryTerm#getGroup1()
	 * @see #getUnaryTerm()
	 * @generated
	 */
	EAttribute getUnaryTerm_Group1();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.terms.UnaryTerm#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operand</em>'.
	 * @see com.prostep.vcontrol.model.terms.UnaryTerm#getOperand()
	 * @see #getUnaryTerm()
	 * @generated
	 */
	EReference getUnaryTerm_Operand();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.terms.XorTerm <em>Xor Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Xor Term</em>'.
	 * @see com.prostep.vcontrol.model.terms.XorTerm
	 * @generated
	 */
	EClass getXorTerm();

	/**
	 * Returns the meta object for enum '{@link com.prostep.vcontrol.model.terms.ErrorStatus <em>Error Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Error Status</em>'.
	 * @see com.prostep.vcontrol.model.terms.ErrorStatus
	 * @generated
	 */
	EEnum getErrorStatus();

	/**
	 * Returns the meta object for data type '{@link com.prostep.vcontrol.model.terms.ErrorStatus <em>Error Status Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Error Status Object</em>'.
	 * @see com.prostep.vcontrol.model.terms.ErrorStatus
	 * @model instanceClass="com.prostep.vcontrol.model.terms.ErrorStatus"
	 *        extendedMetaData="name='ErrorStatus:Object' baseType='ErrorStatus'"
	 * @generated
	 */
	EDataType getErrorStatusObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TermsFactory getTermsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.AbstractMathTermImpl <em>Abstract Math Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.AbstractMathTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getAbstractMathTerm()
		 * @generated
		 */
		EClass ABSTRACT_MATH_TERM = eINSTANCE.getAbstractMathTerm();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.AndTermImpl <em>And Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.AndTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getAndTerm()
		 * @generated
		 */
		EClass AND_TERM = eINSTANCE.getAndTerm();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.AtomicTermImpl <em>Atomic Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.AtomicTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getAtomicTerm()
		 * @generated
		 */
		EClass ATOMIC_TERM = eINSTANCE.getAtomicTerm();

		/**
		 * The meta object literal for the '<em><b>Element</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATOMIC_TERM__ELEMENT = eINSTANCE.getAtomicTerm_Element();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.BinaryTermImpl <em>Binary Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.BinaryTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getBinaryTerm()
		 * @generated
		 */
		EClass BINARY_TERM = eINSTANCE.getBinaryTerm();

		/**
		 * The meta object literal for the '<em><b>Group1</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_TERM__GROUP1 = eINSTANCE.getBinaryTerm_Group1();

		/**
		 * The meta object literal for the '<em><b>Left Operand</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_TERM__LEFT_OPERAND = eINSTANCE.getBinaryTerm_LeftOperand();

		/**
		 * The meta object literal for the '<em><b>Right Operand</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_TERM__RIGHT_OPERAND = eINSTANCE.getBinaryTerm_RightOperand();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.DocumentRootImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Abstract Math Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__ABSTRACT_MATH_TERM = eINSTANCE.getDocumentRoot_AbstractMathTerm();

		/**
		 * The meta object literal for the '<em><b>And Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__AND_TERM = eINSTANCE.getDocumentRoot_AndTerm();

		/**
		 * The meta object literal for the '<em><b>Atomic Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__ATOMIC_TERM = eINSTANCE.getDocumentRoot_AtomicTerm();

		/**
		 * The meta object literal for the '<em><b>Binary Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__BINARY_TERM = eINSTANCE.getDocumentRoot_BinaryTerm();

		/**
		 * The meta object literal for the '<em><b>Equivalent Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__EQUIVALENT_TERM = eINSTANCE.getDocumentRoot_EquivalentTerm();

		/**
		 * The meta object literal for the '<em><b>Error Status Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__ERROR_STATUS_TERM = eINSTANCE.getDocumentRoot_ErrorStatusTerm();

		/**
		 * The meta object literal for the '<em><b>Excludes Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__EXCLUDES_TERM = eINSTANCE.getDocumentRoot_ExcludesTerm();

		/**
		 * The meta object literal for the '<em><b>False Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__FALSE_TERM = eINSTANCE.getDocumentRoot_FalseTerm();

		/**
		 * The meta object literal for the '<em><b>Implies Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMPLIES_TERM = eINSTANCE.getDocumentRoot_ImpliesTerm();

		/**
		 * The meta object literal for the '<em><b>Nary Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__NARY_TERM = eINSTANCE.getDocumentRoot_NaryTerm();

		/**
		 * The meta object literal for the '<em><b>Not Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__NOT_TERM = eINSTANCE.getDocumentRoot_NotTerm();

		/**
		 * The meta object literal for the '<em><b>Or Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__OR_TERM = eINSTANCE.getDocumentRoot_OrTerm();

		/**
		 * The meta object literal for the '<em><b>True Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__TRUE_TERM = eINSTANCE.getDocumentRoot_TrueTerm();

		/**
		 * The meta object literal for the '<em><b>Unary Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__UNARY_TERM = eINSTANCE.getDocumentRoot_UnaryTerm();

		/**
		 * The meta object literal for the '<em><b>Xor Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XOR_TERM = eINSTANCE.getDocumentRoot_XorTerm();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.EquivalentTermImpl <em>Equivalent Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.EquivalentTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getEquivalentTerm()
		 * @generated
		 */
		EClass EQUIVALENT_TERM = eINSTANCE.getEquivalentTerm();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.ErrorStatusTermImpl <em>Error Status Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.ErrorStatusTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getErrorStatusTerm()
		 * @generated
		 */
		EClass ERROR_STATUS_TERM = eINSTANCE.getErrorStatusTerm();

		/**
		 * The meta object literal for the '<em><b>Error Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ERROR_STATUS_TERM__ERROR_STATUS = eINSTANCE.getErrorStatusTerm_ErrorStatus();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ERROR_STATUS_TERM__MESSAGE = eINSTANCE.getErrorStatusTerm_Message();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.ExcludesTermImpl <em>Excludes Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.ExcludesTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getExcludesTerm()
		 * @generated
		 */
		EClass EXCLUDES_TERM = eINSTANCE.getExcludesTerm();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.FalseTermImpl <em>False Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.FalseTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getFalseTerm()
		 * @generated
		 */
		EClass FALSE_TERM = eINSTANCE.getFalseTerm();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.ImpliesTermImpl <em>Implies Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.ImpliesTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getImpliesTerm()
		 * @generated
		 */
		EClass IMPLIES_TERM = eINSTANCE.getImpliesTerm();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.NaryTermImpl <em>Nary Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.NaryTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getNaryTerm()
		 * @generated
		 */
		EClass NARY_TERM = eINSTANCE.getNaryTerm();

		/**
		 * The meta object literal for the '<em><b>Group1</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NARY_TERM__GROUP1 = eINSTANCE.getNaryTerm_Group1();

		/**
		 * The meta object literal for the '<em><b>Operands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NARY_TERM__OPERANDS = eINSTANCE.getNaryTerm_Operands();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.NotTermImpl <em>Not Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.NotTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getNotTerm()
		 * @generated
		 */
		EClass NOT_TERM = eINSTANCE.getNotTerm();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.OrTermImpl <em>Or Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.OrTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getOrTerm()
		 * @generated
		 */
		EClass OR_TERM = eINSTANCE.getOrTerm();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.TrueTermImpl <em>True Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.TrueTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getTrueTerm()
		 * @generated
		 */
		EClass TRUE_TERM = eINSTANCE.getTrueTerm();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.UnaryTermImpl <em>Unary Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.UnaryTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getUnaryTerm()
		 * @generated
		 */
		EClass UNARY_TERM = eINSTANCE.getUnaryTerm();

		/**
		 * The meta object literal for the '<em><b>Group1</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_TERM__GROUP1 = eINSTANCE.getUnaryTerm_Group1();

		/**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_TERM__OPERAND = eINSTANCE.getUnaryTerm_Operand();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.impl.XorTermImpl <em>Xor Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.impl.XorTermImpl
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getXorTerm()
		 * @generated
		 */
		EClass XOR_TERM = eINSTANCE.getXorTerm();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.terms.ErrorStatus <em>Error Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.ErrorStatus
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getErrorStatus()
		 * @generated
		 */
		EEnum ERROR_STATUS = eINSTANCE.getErrorStatus();

		/**
		 * The meta object literal for the '<em>Error Status Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.terms.ErrorStatus
		 * @see com.prostep.vcontrol.model.terms.impl.TermsPackageImpl#getErrorStatusObject()
		 * @generated
		 */
		EDataType ERROR_STATUS_OBJECT = eINSTANCE.getErrorStatusObject();

	}

} //TermsPackage
