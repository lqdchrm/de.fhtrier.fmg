/**
 */
package com.prostep.vcontrol.model.terms;

import com.prostep.vcontrol.model.common.ImpTerm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>True Term</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getTrueTerm()
 * @model extendedMetaData="name='TrueTerm' kind='elementOnly'"
 * @generated
 */
public interface TrueTerm extends ImpTerm {
} // TrueTerm
