/**
 */
package com.prostep.vcontrol.model.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Port Direction</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.common.CommonPackage#getPortDirection()
 * @model extendedMetaData="name='PortDirection'"
 * @generated
 */
public final class PortDirection extends AbstractEnumerator {
	/**
	 * The '<em><b>PROVIDE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PROVIDE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PROVIDE_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PROVIDE = 0;

	/**
	 * The '<em><b>REQUIRE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>REQUIRE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REQUIRE_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REQUIRE = 1;

	/**
	 * The '<em><b>PROVIDE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROVIDE
	 * @generated
	 * @ordered
	 */
	public static final PortDirection PROVIDE_LITERAL = new PortDirection(PROVIDE, "PROVIDE", "PROVIDE");

	/**
	 * The '<em><b>REQUIRE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUIRE
	 * @generated
	 * @ordered
	 */
	public static final PortDirection REQUIRE_LITERAL = new PortDirection(REQUIRE, "REQUIRE", "REQUIRE");

	/**
	 * An array of all the '<em><b>Port Direction</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final PortDirection[] VALUES_ARRAY =
		new PortDirection[] {
			PROVIDE_LITERAL,
			REQUIRE_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Port Direction</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Port Direction</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PortDirection get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			PortDirection result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Port Direction</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PortDirection getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			PortDirection result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Port Direction</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PortDirection get(int value) {
		switch (value) {
			case PROVIDE: return PROVIDE_LITERAL;
			case REQUIRE: return REQUIRE_LITERAL;
		}
		return null;
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private PortDirection(int value, String name, String literal) {
		super(value, name, literal);
	}

} //PortDirection
