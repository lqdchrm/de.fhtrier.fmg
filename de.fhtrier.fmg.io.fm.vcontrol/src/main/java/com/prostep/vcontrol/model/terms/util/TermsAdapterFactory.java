/**
 */
package com.prostep.vcontrol.model.terms.util;

import com.prostep.vcontrol.model.common.ImpTerm;

import com.prostep.vcontrol.model.terms.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.terms.TermsPackage
 * @generated
 */
public class TermsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TermsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TermsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TermsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TermsSwitch modelSwitch =
		new TermsSwitch() {
			public Object caseAbstractMathTerm(AbstractMathTerm object) {
				return createAbstractMathTermAdapter();
			}
			public Object caseAndTerm(AndTerm object) {
				return createAndTermAdapter();
			}
			public Object caseAtomicTerm(AtomicTerm object) {
				return createAtomicTermAdapter();
			}
			public Object caseBinaryTerm(BinaryTerm object) {
				return createBinaryTermAdapter();
			}
			public Object caseDocumentRoot(DocumentRoot object) {
				return createDocumentRootAdapter();
			}
			public Object caseEquivalentTerm(EquivalentTerm object) {
				return createEquivalentTermAdapter();
			}
			public Object caseErrorStatusTerm(ErrorStatusTerm object) {
				return createErrorStatusTermAdapter();
			}
			public Object caseExcludesTerm(ExcludesTerm object) {
				return createExcludesTermAdapter();
			}
			public Object caseFalseTerm(FalseTerm object) {
				return createFalseTermAdapter();
			}
			public Object caseImpliesTerm(ImpliesTerm object) {
				return createImpliesTermAdapter();
			}
			public Object caseNaryTerm(NaryTerm object) {
				return createNaryTermAdapter();
			}
			public Object caseNotTerm(NotTerm object) {
				return createNotTermAdapter();
			}
			public Object caseOrTerm(OrTerm object) {
				return createOrTermAdapter();
			}
			public Object caseTrueTerm(TrueTerm object) {
				return createTrueTermAdapter();
			}
			public Object caseUnaryTerm(UnaryTerm object) {
				return createUnaryTermAdapter();
			}
			public Object caseXorTerm(XorTerm object) {
				return createXorTermAdapter();
			}
			public Object caseImpTerm(ImpTerm object) {
				return createImpTermAdapter();
			}
			public Object defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter)modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.AbstractMathTerm <em>Abstract Math Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.AbstractMathTerm
	 * @generated
	 */
	public Adapter createAbstractMathTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.AndTerm <em>And Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.AndTerm
	 * @generated
	 */
	public Adapter createAndTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.AtomicTerm <em>Atomic Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.AtomicTerm
	 * @generated
	 */
	public Adapter createAtomicTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.BinaryTerm <em>Binary Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.BinaryTerm
	 * @generated
	 */
	public Adapter createBinaryTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.DocumentRoot
	 * @generated
	 */
	public Adapter createDocumentRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.EquivalentTerm <em>Equivalent Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.EquivalentTerm
	 * @generated
	 */
	public Adapter createEquivalentTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.ErrorStatusTerm <em>Error Status Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.ErrorStatusTerm
	 * @generated
	 */
	public Adapter createErrorStatusTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.ExcludesTerm <em>Excludes Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.ExcludesTerm
	 * @generated
	 */
	public Adapter createExcludesTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.FalseTerm <em>False Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.FalseTerm
	 * @generated
	 */
	public Adapter createFalseTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.ImpliesTerm <em>Implies Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.ImpliesTerm
	 * @generated
	 */
	public Adapter createImpliesTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.NaryTerm <em>Nary Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.NaryTerm
	 * @generated
	 */
	public Adapter createNaryTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.NotTerm <em>Not Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.NotTerm
	 * @generated
	 */
	public Adapter createNotTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.OrTerm <em>Or Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.OrTerm
	 * @generated
	 */
	public Adapter createOrTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.TrueTerm <em>True Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.TrueTerm
	 * @generated
	 */
	public Adapter createTrueTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.UnaryTerm <em>Unary Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.UnaryTerm
	 * @generated
	 */
	public Adapter createUnaryTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.terms.XorTerm <em>Xor Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.terms.XorTerm
	 * @generated
	 */
	public Adapter createXorTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpTerm <em>Imp Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpTerm
	 * @generated
	 */
	public Adapter createImpTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TermsAdapterFactory
