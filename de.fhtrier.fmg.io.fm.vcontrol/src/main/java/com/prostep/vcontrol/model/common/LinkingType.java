/**
 */
package com.prostep.vcontrol.model.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Linking Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.common.CommonPackage#getLinkingType()
 * @model extendedMetaData="name='LinkingType'"
 * @generated
 */
public final class LinkingType extends AbstractEnumerator {
	/**
	 * The '<em><b>TRACEABILITY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TRACEABILITY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRACEABILITY_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TRACEABILITY = 0;

	/**
	 * The '<em><b>IMPLIES</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>IMPLIES</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IMPLIES_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int IMPLIES = 1;

	/**
	 * The '<em><b>EQUIVALENT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EQUIVALENT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EQUIVALENT_LITERAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EQUIVALENT = 2;

	/**
	 * The '<em><b>TRACEABILITY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TRACEABILITY
	 * @generated
	 * @ordered
	 */
	public static final LinkingType TRACEABILITY_LITERAL = new LinkingType(TRACEABILITY, "TRACEABILITY", "TRACEABILITY");

	/**
	 * The '<em><b>IMPLIES</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMPLIES
	 * @generated
	 * @ordered
	 */
	public static final LinkingType IMPLIES_LITERAL = new LinkingType(IMPLIES, "IMPLIES", "IMPLIES");

	/**
	 * The '<em><b>EQUIVALENT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EQUIVALENT
	 * @generated
	 * @ordered
	 */
	public static final LinkingType EQUIVALENT_LITERAL = new LinkingType(EQUIVALENT, "EQUIVALENT", "EQUIVALENT");

	/**
	 * An array of all the '<em><b>Linking Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final LinkingType[] VALUES_ARRAY =
		new LinkingType[] {
			TRACEABILITY_LITERAL,
			IMPLIES_LITERAL,
			EQUIVALENT_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Linking Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Linking Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LinkingType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LinkingType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Linking Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LinkingType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LinkingType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Linking Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LinkingType get(int value) {
		switch (value) {
			case TRACEABILITY: return TRACEABILITY_LITERAL;
			case IMPLIES: return IMPLIES_LITERAL;
			case EQUIVALENT: return EQUIVALENT_LITERAL;
		}
		return null;
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private LinkingType(int value, String name, String literal) {
		super(value, name, literal);
	}

} //LinkingType
