/**
 */
package com.prostep.vcontrol.model.terms.impl;

import com.prostep.vcontrol.model.terms.AbstractMathTerm;
import com.prostep.vcontrol.model.terms.TermsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Math Term</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class AbstractMathTermImpl extends AtomicTermImpl implements AbstractMathTerm {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractMathTermImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return TermsPackage.Literals.ABSTRACT_MATH_TERM;
	}

} //AbstractMathTermImpl
