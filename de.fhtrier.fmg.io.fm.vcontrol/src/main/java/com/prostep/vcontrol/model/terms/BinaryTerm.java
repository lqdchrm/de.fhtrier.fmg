/**
 */
package com.prostep.vcontrol.model.terms;

import com.prostep.vcontrol.model.common.ImpTerm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.terms.BinaryTerm#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.BinaryTerm#getLeftOperand <em>Left Operand</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.BinaryTerm#getRightOperand <em>Right Operand</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getBinaryTerm()
 * @model abstract="true"
 *        extendedMetaData="name='BinaryTerm' kind='elementOnly'"
 * @generated
 */
public interface BinaryTerm extends ImpTerm {
	/**
	 * Returns the value of the '<em><b>Group1</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group1</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group1</em>' attribute list.
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getBinaryTerm_Group1()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:9'"
	 * @generated
	 */
	FeatureMap getGroup1();

	/**
	 * Returns the value of the '<em><b>Left Operand</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpTerm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Operand</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Operand</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getBinaryTerm_LeftOperand()
	 * @model type="com.prostep.vcontrol.model.common.ImpTerm" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='leftOperand' group='#group:9'"
	 * @generated
	 */
	EList getLeftOperand();

	/**
	 * Returns the value of the '<em><b>Right Operand</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpTerm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Operand</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Operand</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getBinaryTerm_RightOperand()
	 * @model type="com.prostep.vcontrol.model.common.ImpTerm" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='rightOperand' group='#group:9'"
	 * @generated
	 */
	EList getRightOperand();

} // BinaryTerm
