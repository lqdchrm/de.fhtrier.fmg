/**
 */
package com.prostep.vcontrol.model.terms.impl;

import com.prostep.vcontrol.model.common.impl.ImpTermImpl;

import com.prostep.vcontrol.model.terms.ErrorStatus;
import com.prostep.vcontrol.model.terms.ErrorStatusTerm;
import com.prostep.vcontrol.model.terms.TermsPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Error Status Term</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.ErrorStatusTermImpl#getErrorStatus <em>Error Status</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.impl.ErrorStatusTermImpl#getMessage <em>Message</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ErrorStatusTermImpl extends ImpTermImpl implements ErrorStatusTerm {
	/**
	 * The default value of the '{@link #getErrorStatus() <em>Error Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorStatus()
	 * @generated
	 * @ordered
	 */
	protected static final ErrorStatus ERROR_STATUS_EDEFAULT = ErrorStatus.OK_LITERAL;

	/**
	 * The cached value of the '{@link #getErrorStatus() <em>Error Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorStatus()
	 * @generated
	 * @ordered
	 */
	protected ErrorStatus errorStatus = ERROR_STATUS_EDEFAULT;

	/**
	 * This is true if the Error Status attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean errorStatusESet;

	/**
	 * The default value of the '{@link #getMessage() <em>Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected String message = MESSAGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ErrorStatusTermImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return TermsPackage.Literals.ERROR_STATUS_TERM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ErrorStatus getErrorStatus() {
		return errorStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setErrorStatus(ErrorStatus newErrorStatus) {
		ErrorStatus oldErrorStatus = errorStatus;
		errorStatus = newErrorStatus == null ? ERROR_STATUS_EDEFAULT : newErrorStatus;
		boolean oldErrorStatusESet = errorStatusESet;
		errorStatusESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TermsPackage.ERROR_STATUS_TERM__ERROR_STATUS, oldErrorStatus, errorStatus, !oldErrorStatusESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetErrorStatus() {
		ErrorStatus oldErrorStatus = errorStatus;
		boolean oldErrorStatusESet = errorStatusESet;
		errorStatus = ERROR_STATUS_EDEFAULT;
		errorStatusESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, TermsPackage.ERROR_STATUS_TERM__ERROR_STATUS, oldErrorStatus, ERROR_STATUS_EDEFAULT, oldErrorStatusESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetErrorStatus() {
		return errorStatusESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessage(String newMessage) {
		String oldMessage = message;
		message = newMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TermsPackage.ERROR_STATUS_TERM__MESSAGE, oldMessage, message));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TermsPackage.ERROR_STATUS_TERM__ERROR_STATUS:
				return getErrorStatus();
			case TermsPackage.ERROR_STATUS_TERM__MESSAGE:
				return getMessage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TermsPackage.ERROR_STATUS_TERM__ERROR_STATUS:
				setErrorStatus((ErrorStatus)newValue);
				return;
			case TermsPackage.ERROR_STATUS_TERM__MESSAGE:
				setMessage((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case TermsPackage.ERROR_STATUS_TERM__ERROR_STATUS:
				unsetErrorStatus();
				return;
			case TermsPackage.ERROR_STATUS_TERM__MESSAGE:
				setMessage(MESSAGE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TermsPackage.ERROR_STATUS_TERM__ERROR_STATUS:
				return isSetErrorStatus();
			case TermsPackage.ERROR_STATUS_TERM__MESSAGE:
				return MESSAGE_EDEFAULT == null ? message != null : !MESSAGE_EDEFAULT.equals(message);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (errorStatus: ");
		if (errorStatusESet) result.append(errorStatus); else result.append("<unset>");
		result.append(", message: ");
		result.append(message);
		result.append(')');
		return result.toString();
	}

} //ErrorStatusTermImpl
