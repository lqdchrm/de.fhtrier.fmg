/**
 */
package com.prostep.vcontrol.model.feature;

import com.prostep.vcontrol.model.common.CommonPackage;

import com.prostep.vcontrol.model.terms.TermsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.feature.FeatureFactory
 * @model kind="package"
 * @generated
 */
public interface FeaturePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "feature";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.prostep.com/vcontrol/model/feature.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "com.prostep.vcontrol.model.feature";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FeaturePackage eINSTANCE = com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl.init();

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.feature.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.feature.impl.DocumentRootImpl
	 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 0;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__FEATURE = 3;

	/**
	 * The feature id for the '<em><b>Feature Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__FEATURE_GROUP = 4;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__FEATURE_MODEL = 5;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__FEATURE_NODE = 6;

	/**
	 * The feature id for the '<em><b>Feature Occurrence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__FEATURE_OCCURRENCE = 7;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 8;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl
	 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureNode()
	 * @generated
	 */
	int FEATURE_NODE = 4;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__GROUP = CommonPackage.IMP_NODE__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__PROPERTIES = CommonPackage.IMP_NODE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__EXTENSION = CommonPackage.IMP_NODE__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__HREF = CommonPackage.IMP_NODE__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__ID = CommonPackage.IMP_NODE__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__IDREF = CommonPackage.IMP_NODE__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__LABEL = CommonPackage.IMP_NODE__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__TYPE = CommonPackage.IMP_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__UUID = CommonPackage.IMP_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__VERSION = CommonPackage.IMP_NODE__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__DESCRIPTION = CommonPackage.IMP_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__ID1 = CommonPackage.IMP_NODE__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__NAME = CommonPackage.IMP_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__GROUP1 = CommonPackage.IMP_NODE__GROUP1;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__IMP_CONFIGURED_ELEMENTS = CommonPackage.IMP_NODE__IMP_CONFIGURED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Right Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__RIGHT_ELEMENTS = CommonPackage.IMP_NODE__RIGHT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Left Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__LEFT_ELEMENTS = CommonPackage.IMP_NODE__LEFT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Extensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__EXTENSIONS = CommonPackage.IMP_NODE__EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__IMP_CONFIGURED_ELEMENTS1 = CommonPackage.IMP_NODE__IMP_CONFIGURED_ELEMENTS1;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__OPTIONAL = CommonPackage.IMP_NODE__OPTIONAL;

	/**
	 * The feature id for the '<em><b>Right Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__RIGHT_ELEMENTS1 = CommonPackage.IMP_NODE__RIGHT_ELEMENTS1;

	/**
	 * The feature id for the '<em><b>Group2</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__GROUP2 = CommonPackage.IMP_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__PARENT = CommonPackage.IMP_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__CHILDREN = CommonPackage.IMP_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__MODEL = CommonPackage.IMP_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Occurrences</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__OCCURRENCES = CommonPackage.IMP_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Model1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__MODEL1 = CommonPackage.IMP_NODE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Occurrences1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__OCCURRENCES1 = CommonPackage.IMP_NODE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Parent1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE__PARENT1 = CommonPackage.IMP_NODE_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NODE_FEATURE_COUNT = CommonPackage.IMP_NODE_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.feature.impl.FeatureImpl <em>Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.feature.impl.FeatureImpl
	 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeature()
	 * @generated
	 */
	int FEATURE = 1;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__GROUP = FEATURE_NODE__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__PROPERTIES = FEATURE_NODE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__EXTENSION = FEATURE_NODE__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__HREF = FEATURE_NODE__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__ID = FEATURE_NODE__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__IDREF = FEATURE_NODE__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__LABEL = FEATURE_NODE__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__TYPE = FEATURE_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__UUID = FEATURE_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__VERSION = FEATURE_NODE__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__DESCRIPTION = FEATURE_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__ID1 = FEATURE_NODE__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__NAME = FEATURE_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__GROUP1 = FEATURE_NODE__GROUP1;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__IMP_CONFIGURED_ELEMENTS = FEATURE_NODE__IMP_CONFIGURED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Right Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__RIGHT_ELEMENTS = FEATURE_NODE__RIGHT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Left Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__LEFT_ELEMENTS = FEATURE_NODE__LEFT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Extensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__EXTENSIONS = FEATURE_NODE__EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__IMP_CONFIGURED_ELEMENTS1 = FEATURE_NODE__IMP_CONFIGURED_ELEMENTS1;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__OPTIONAL = FEATURE_NODE__OPTIONAL;

	/**
	 * The feature id for the '<em><b>Right Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__RIGHT_ELEMENTS1 = FEATURE_NODE__RIGHT_ELEMENTS1;

	/**
	 * The feature id for the '<em><b>Group2</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__GROUP2 = FEATURE_NODE__GROUP2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__PARENT = FEATURE_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__CHILDREN = FEATURE_NODE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__MODEL = FEATURE_NODE__MODEL;

	/**
	 * The feature id for the '<em><b>Occurrences</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__OCCURRENCES = FEATURE_NODE__OCCURRENCES;

	/**
	 * The feature id for the '<em><b>Model1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__MODEL1 = FEATURE_NODE__MODEL1;

	/**
	 * The feature id for the '<em><b>Occurrences1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__OCCURRENCES1 = FEATURE_NODE__OCCURRENCES1;

	/**
	 * The feature id for the '<em><b>Parent1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__PARENT1 = FEATURE_NODE__PARENT1;

	/**
	 * The number of structural features of the '<em>Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_FEATURE_COUNT = FEATURE_NODE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.feature.impl.FeatureGroupImpl <em>Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.feature.impl.FeatureGroupImpl
	 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureGroup()
	 * @generated
	 */
	int FEATURE_GROUP = 2;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__GROUP = FEATURE_NODE__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__PROPERTIES = FEATURE_NODE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__EXTENSION = FEATURE_NODE__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__HREF = FEATURE_NODE__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__ID = FEATURE_NODE__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__IDREF = FEATURE_NODE__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__LABEL = FEATURE_NODE__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__TYPE = FEATURE_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__UUID = FEATURE_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__VERSION = FEATURE_NODE__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__DESCRIPTION = FEATURE_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__ID1 = FEATURE_NODE__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__NAME = FEATURE_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__GROUP1 = FEATURE_NODE__GROUP1;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__IMP_CONFIGURED_ELEMENTS = FEATURE_NODE__IMP_CONFIGURED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Right Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__RIGHT_ELEMENTS = FEATURE_NODE__RIGHT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Left Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__LEFT_ELEMENTS = FEATURE_NODE__LEFT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Extensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__EXTENSIONS = FEATURE_NODE__EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__IMP_CONFIGURED_ELEMENTS1 = FEATURE_NODE__IMP_CONFIGURED_ELEMENTS1;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__OPTIONAL = FEATURE_NODE__OPTIONAL;

	/**
	 * The feature id for the '<em><b>Right Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__RIGHT_ELEMENTS1 = FEATURE_NODE__RIGHT_ELEMENTS1;

	/**
	 * The feature id for the '<em><b>Group2</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__GROUP2 = FEATURE_NODE__GROUP2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__PARENT = FEATURE_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__CHILDREN = FEATURE_NODE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__MODEL = FEATURE_NODE__MODEL;

	/**
	 * The feature id for the '<em><b>Occurrences</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__OCCURRENCES = FEATURE_NODE__OCCURRENCES;

	/**
	 * The feature id for the '<em><b>Model1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__MODEL1 = FEATURE_NODE__MODEL1;

	/**
	 * The feature id for the '<em><b>Occurrences1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__OCCURRENCES1 = FEATURE_NODE__OCCURRENCES1;

	/**
	 * The feature id for the '<em><b>Parent1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__PARENT1 = FEATURE_NODE__PARENT1;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__MAX = FEATURE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP__MIN = FEATURE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_GROUP_FEATURE_COUNT = FEATURE_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.feature.impl.FeatureModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.feature.impl.FeatureModelImpl
	 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureModel()
	 * @generated
	 */
	int FEATURE_MODEL = 3;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__GROUP = CommonPackage.IMP_MODEL__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__PROPERTIES = CommonPackage.IMP_MODEL__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__EXTENSION = CommonPackage.IMP_MODEL__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__HREF = CommonPackage.IMP_MODEL__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__ID = CommonPackage.IMP_MODEL__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__IDREF = CommonPackage.IMP_MODEL__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__LABEL = CommonPackage.IMP_MODEL__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__TYPE = CommonPackage.IMP_MODEL__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__UUID = CommonPackage.IMP_MODEL__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__VERSION = CommonPackage.IMP_MODEL__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__DESCRIPTION = CommonPackage.IMP_MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__ID1 = CommonPackage.IMP_MODEL__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__NAME = CommonPackage.IMP_MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__GROUP1 = CommonPackage.IMP_MODEL__GROUP1;

	/**
	 * The feature id for the '<em><b>Project</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__PROJECT = CommonPackage.IMP_MODEL__PROJECT;

	/**
	 * The feature id for the '<em><b>Imp Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__IMP_CONFIGURATIONS = CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__CONSTRAINTS = CommonPackage.IMP_MODEL__CONSTRAINTS;

	/**
	 * The feature id for the '<em><b>Imp Configurations1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__IMP_CONFIGURATIONS1 = CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS1;

	/**
	 * The feature id for the '<em><b>Project1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__PROJECT1 = CommonPackage.IMP_MODEL__PROJECT1;

	/**
	 * The feature id for the '<em><b>Group2</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__GROUP2 = CommonPackage.IMP_MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Root Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL__ROOT_FEATURE = CommonPackage.IMP_MODEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_FEATURE_COUNT = CommonPackage.IMP_MODEL_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.feature.impl.FeatureOccurrenceImpl <em>Occurrence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.feature.impl.FeatureOccurrenceImpl
	 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureOccurrence()
	 * @generated
	 */
	int FEATURE_OCCURRENCE = 5;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__GROUP = TermsPackage.ATOMIC_TERM__GROUP;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__EXTENSION = TermsPackage.ATOMIC_TERM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__HREF = TermsPackage.ATOMIC_TERM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__ID = TermsPackage.ATOMIC_TERM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__IDREF = TermsPackage.ATOMIC_TERM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__LABEL = TermsPackage.ATOMIC_TERM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__TYPE = TermsPackage.ATOMIC_TERM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__UUID = TermsPackage.ATOMIC_TERM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__VERSION = TermsPackage.ATOMIC_TERM__VERSION;

	/**
	 * The feature id for the '<em><b>Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__ELEMENT = TermsPackage.ATOMIC_TERM__ELEMENT;

	/**
	 * The feature id for the '<em><b>Group2</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__GROUP2 = TermsPackage.ATOMIC_TERM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Feature Node</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__FEATURE_NODE = TermsPackage.ATOMIC_TERM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Feature Node1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE__FEATURE_NODE1 = TermsPackage.ATOMIC_TERM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Occurrence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OCCURRENCE_FEATURE_COUNT = TermsPackage.ATOMIC_TERM_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.feature.FeatureGroupType <em>Group Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.feature.FeatureGroupType
	 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureGroupType()
	 * @generated
	 */
	int FEATURE_GROUP_TYPE = 6;

	/**
	 * The meta object id for the '<em>Group Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.feature.FeatureGroupType
	 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureGroupTypeObject()
	 * @generated
	 */
	int FEATURE_GROUP_TYPE_OBJECT = 7;


	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.feature.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see com.prostep.vcontrol.model.feature.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see com.prostep.vcontrol.model.feature.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see com.prostep.vcontrol.model.feature.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see com.prostep.vcontrol.model.feature.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature</em>'.
	 * @see com.prostep.vcontrol.model.feature.DocumentRoot#getFeature()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Feature();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureGroup <em>Feature Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Group</em>'.
	 * @see com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureGroup()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_FeatureGroup();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Model</em>'.
	 * @see com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureModel()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_FeatureModel();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureNode <em>Feature Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Node</em>'.
	 * @see com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureNode()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_FeatureNode();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureOccurrence <em>Feature Occurrence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Occurrence</em>'.
	 * @see com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureOccurrence()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_FeatureOccurrence();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.feature.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature</em>'.
	 * @see com.prostep.vcontrol.model.feature.Feature
	 * @generated
	 */
	EClass getFeature();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.feature.FeatureGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureGroup
	 * @generated
	 */
	EClass getFeatureGroup();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.feature.FeatureGroup#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureGroup#getMax()
	 * @see #getFeatureGroup()
	 * @generated
	 */
	EAttribute getFeatureGroup_Max();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.feature.FeatureGroup#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureGroup#getMin()
	 * @see #getFeatureGroup()
	 * @generated
	 */
	EAttribute getFeatureGroup_Min();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.feature.FeatureModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureModel
	 * @generated
	 */
	EClass getFeatureModel();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.feature.FeatureModel#getGroup2 <em>Group2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group2</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureModel#getGroup2()
	 * @see #getFeatureModel()
	 * @generated
	 */
	EAttribute getFeatureModel_Group2();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.feature.FeatureModel#getRootFeature <em>Root Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Root Feature</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureModel#getRootFeature()
	 * @see #getFeatureModel()
	 * @generated
	 */
	EReference getFeatureModel_RootFeature();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.feature.FeatureNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureNode
	 * @generated
	 */
	EClass getFeatureNode();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.feature.FeatureNode#getGroup2 <em>Group2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group2</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureNode#getGroup2()
	 * @see #getFeatureNode()
	 * @generated
	 */
	EAttribute getFeatureNode_Group2();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.feature.FeatureNode#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parent</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureNode#getParent()
	 * @see #getFeatureNode()
	 * @generated
	 */
	EReference getFeatureNode_Parent();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.feature.FeatureNode#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureNode#getChildren()
	 * @see #getFeatureNode()
	 * @generated
	 */
	EReference getFeatureNode_Children();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.feature.FeatureNode#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Model</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureNode#getModel()
	 * @see #getFeatureNode()
	 * @generated
	 */
	EReference getFeatureNode_Model();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.feature.FeatureNode#getOccurrences <em>Occurrences</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Occurrences</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureNode#getOccurrences()
	 * @see #getFeatureNode()
	 * @generated
	 */
	EReference getFeatureNode_Occurrences();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.feature.FeatureNode#getModel1 <em>Model1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model1</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureNode#getModel1()
	 * @see #getFeatureNode()
	 * @generated
	 */
	EAttribute getFeatureNode_Model1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.feature.FeatureNode#getOccurrences1 <em>Occurrences1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Occurrences1</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureNode#getOccurrences1()
	 * @see #getFeatureNode()
	 * @generated
	 */
	EAttribute getFeatureNode_Occurrences1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.feature.FeatureNode#getParent1 <em>Parent1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Parent1</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureNode#getParent1()
	 * @see #getFeatureNode()
	 * @generated
	 */
	EAttribute getFeatureNode_Parent1();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.feature.FeatureOccurrence <em>Occurrence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Occurrence</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureOccurrence
	 * @generated
	 */
	EClass getFeatureOccurrence();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.feature.FeatureOccurrence#getGroup2 <em>Group2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group2</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureOccurrence#getGroup2()
	 * @see #getFeatureOccurrence()
	 * @generated
	 */
	EAttribute getFeatureOccurrence_Group2();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.feature.FeatureOccurrence#getFeatureNode <em>Feature Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Feature Node</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureOccurrence#getFeatureNode()
	 * @see #getFeatureOccurrence()
	 * @generated
	 */
	EReference getFeatureOccurrence_FeatureNode();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.feature.FeatureOccurrence#getFeatureNode1 <em>Feature Node1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Feature Node1</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureOccurrence#getFeatureNode1()
	 * @see #getFeatureOccurrence()
	 * @generated
	 */
	EAttribute getFeatureOccurrence_FeatureNode1();

	/**
	 * Returns the meta object for enum '{@link com.prostep.vcontrol.model.feature.FeatureGroupType <em>Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Group Type</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureGroupType
	 * @generated
	 */
	EEnum getFeatureGroupType();

	/**
	 * Returns the meta object for data type '{@link com.prostep.vcontrol.model.feature.FeatureGroupType <em>Group Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Group Type Object</em>'.
	 * @see com.prostep.vcontrol.model.feature.FeatureGroupType
	 * @model instanceClass="com.prostep.vcontrol.model.feature.FeatureGroupType"
	 *        extendedMetaData="name='FeatureGroupType:Object' baseType='FeatureGroupType'"
	 * @generated
	 */
	EDataType getFeatureGroupTypeObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FeatureFactory getFeatureFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.feature.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.feature.impl.DocumentRootImpl
		 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__FEATURE = eINSTANCE.getDocumentRoot_Feature();

		/**
		 * The meta object literal for the '<em><b>Feature Group</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__FEATURE_GROUP = eINSTANCE.getDocumentRoot_FeatureGroup();

		/**
		 * The meta object literal for the '<em><b>Feature Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__FEATURE_MODEL = eINSTANCE.getDocumentRoot_FeatureModel();

		/**
		 * The meta object literal for the '<em><b>Feature Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__FEATURE_NODE = eINSTANCE.getDocumentRoot_FeatureNode();

		/**
		 * The meta object literal for the '<em><b>Feature Occurrence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__FEATURE_OCCURRENCE = eINSTANCE.getDocumentRoot_FeatureOccurrence();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.feature.impl.FeatureImpl <em>Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.feature.impl.FeatureImpl
		 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeature()
		 * @generated
		 */
		EClass FEATURE = eINSTANCE.getFeature();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.feature.impl.FeatureGroupImpl <em>Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.feature.impl.FeatureGroupImpl
		 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureGroup()
		 * @generated
		 */
		EClass FEATURE_GROUP = eINSTANCE.getFeatureGroup();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_GROUP__MAX = eINSTANCE.getFeatureGroup_Max();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_GROUP__MIN = eINSTANCE.getFeatureGroup_Min();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.feature.impl.FeatureModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.feature.impl.FeatureModelImpl
		 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureModel()
		 * @generated
		 */
		EClass FEATURE_MODEL = eINSTANCE.getFeatureModel();

		/**
		 * The meta object literal for the '<em><b>Group2</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_MODEL__GROUP2 = eINSTANCE.getFeatureModel_Group2();

		/**
		 * The meta object literal for the '<em><b>Root Feature</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL__ROOT_FEATURE = eINSTANCE.getFeatureModel_RootFeature();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.feature.impl.FeatureNodeImpl
		 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureNode()
		 * @generated
		 */
		EClass FEATURE_NODE = eINSTANCE.getFeatureNode();

		/**
		 * The meta object literal for the '<em><b>Group2</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_NODE__GROUP2 = eINSTANCE.getFeatureNode_Group2();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_NODE__PARENT = eINSTANCE.getFeatureNode_Parent();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_NODE__CHILDREN = eINSTANCE.getFeatureNode_Children();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_NODE__MODEL = eINSTANCE.getFeatureNode_Model();

		/**
		 * The meta object literal for the '<em><b>Occurrences</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_NODE__OCCURRENCES = eINSTANCE.getFeatureNode_Occurrences();

		/**
		 * The meta object literal for the '<em><b>Model1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_NODE__MODEL1 = eINSTANCE.getFeatureNode_Model1();

		/**
		 * The meta object literal for the '<em><b>Occurrences1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_NODE__OCCURRENCES1 = eINSTANCE.getFeatureNode_Occurrences1();

		/**
		 * The meta object literal for the '<em><b>Parent1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_NODE__PARENT1 = eINSTANCE.getFeatureNode_Parent1();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.feature.impl.FeatureOccurrenceImpl <em>Occurrence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.feature.impl.FeatureOccurrenceImpl
		 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureOccurrence()
		 * @generated
		 */
		EClass FEATURE_OCCURRENCE = eINSTANCE.getFeatureOccurrence();

		/**
		 * The meta object literal for the '<em><b>Group2</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_OCCURRENCE__GROUP2 = eINSTANCE.getFeatureOccurrence_Group2();

		/**
		 * The meta object literal for the '<em><b>Feature Node</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_OCCURRENCE__FEATURE_NODE = eINSTANCE.getFeatureOccurrence_FeatureNode();

		/**
		 * The meta object literal for the '<em><b>Feature Node1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_OCCURRENCE__FEATURE_NODE1 = eINSTANCE.getFeatureOccurrence_FeatureNode1();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.feature.FeatureGroupType <em>Group Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.feature.FeatureGroupType
		 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureGroupType()
		 * @generated
		 */
		EEnum FEATURE_GROUP_TYPE = eINSTANCE.getFeatureGroupType();

		/**
		 * The meta object literal for the '<em>Group Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.feature.FeatureGroupType
		 * @see com.prostep.vcontrol.model.feature.impl.FeaturePackageImpl#getFeatureGroupTypeObject()
		 * @generated
		 */
		EDataType FEATURE_GROUP_TYPE_OBJECT = eINSTANCE.getFeatureGroupTypeObject();

	}

} //FeaturePackage
