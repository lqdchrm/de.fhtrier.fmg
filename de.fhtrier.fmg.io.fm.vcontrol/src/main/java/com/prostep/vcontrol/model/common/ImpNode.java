/**
 */
package com.prostep.vcontrol.model.common;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Imp Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpNode()
 * @model abstract="true"
 *        extendedMetaData="name='ImpNode' kind='elementOnly'"
 * @generated
 */
public interface ImpNode extends ImpConfigurableElement {
} // ImpNode
