/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.CommonPackage;
import com.prostep.vcontrol.model.common.ImpConnector;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Imp Connector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConnectorImpl#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConnectorImpl#getSourcePort <em>Source Port</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConnectorImpl#getTargetPort <em>Target Port</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConnectorImpl#getSourcePort1 <em>Source Port1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConnectorImpl#getTargetPort1 <em>Target Port1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ImpConnectorImpl extends ImpItemImpl implements ImpConnector {
	/**
	 * The cached value of the '{@link #getGroup1() <em>Group1</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup1()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group1;

	/**
	 * The default value of the '{@link #getSourcePort1() <em>Source Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourcePort1()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_PORT1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourcePort1() <em>Source Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourcePort1()
	 * @generated
	 * @ordered
	 */
	protected String sourcePort1 = SOURCE_PORT1_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetPort1() <em>Target Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetPort1()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_PORT1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTargetPort1() <em>Target Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetPort1()
	 * @generated
	 * @ordered
	 */
	protected String targetPort1 = TARGET_PORT1_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImpConnectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CommonPackage.Literals.IMP_CONNECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup1() {
		if (group1 == null) {
			group1 = new BasicFeatureMap(this, CommonPackage.IMP_CONNECTOR__GROUP1);
		}
		return group1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getSourcePort() {
		return getGroup1().list(CommonPackage.Literals.IMP_CONNECTOR__SOURCE_PORT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getTargetPort() {
		return getGroup1().list(CommonPackage.Literals.IMP_CONNECTOR__TARGET_PORT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourcePort1() {
		return sourcePort1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourcePort1(String newSourcePort1) {
		String oldSourcePort1 = sourcePort1;
		sourcePort1 = newSourcePort1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONNECTOR__SOURCE_PORT1, oldSourcePort1, sourcePort1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTargetPort1() {
		return targetPort1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetPort1(String newTargetPort1) {
		String oldTargetPort1 = targetPort1;
		targetPort1 = newTargetPort1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONNECTOR__TARGET_PORT1, oldTargetPort1, targetPort1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CommonPackage.IMP_CONNECTOR__GROUP1:
				return ((InternalEList)getGroup1()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONNECTOR__SOURCE_PORT:
				return ((InternalEList)getSourcePort()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONNECTOR__TARGET_PORT:
				return ((InternalEList)getTargetPort()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CommonPackage.IMP_CONNECTOR__GROUP1:
				if (coreType) return getGroup1();
				return ((FeatureMap.Internal)getGroup1()).getWrapper();
			case CommonPackage.IMP_CONNECTOR__SOURCE_PORT:
				return getSourcePort();
			case CommonPackage.IMP_CONNECTOR__TARGET_PORT:
				return getTargetPort();
			case CommonPackage.IMP_CONNECTOR__SOURCE_PORT1:
				return getSourcePort1();
			case CommonPackage.IMP_CONNECTOR__TARGET_PORT1:
				return getTargetPort1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CommonPackage.IMP_CONNECTOR__GROUP1:
				((FeatureMap.Internal)getGroup1()).set(newValue);
				return;
			case CommonPackage.IMP_CONNECTOR__SOURCE_PORT:
				getSourcePort().clear();
				getSourcePort().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONNECTOR__TARGET_PORT:
				getTargetPort().clear();
				getTargetPort().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONNECTOR__SOURCE_PORT1:
				setSourcePort1((String)newValue);
				return;
			case CommonPackage.IMP_CONNECTOR__TARGET_PORT1:
				setTargetPort1((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_CONNECTOR__GROUP1:
				getGroup1().clear();
				return;
			case CommonPackage.IMP_CONNECTOR__SOURCE_PORT:
				getSourcePort().clear();
				return;
			case CommonPackage.IMP_CONNECTOR__TARGET_PORT:
				getTargetPort().clear();
				return;
			case CommonPackage.IMP_CONNECTOR__SOURCE_PORT1:
				setSourcePort1(SOURCE_PORT1_EDEFAULT);
				return;
			case CommonPackage.IMP_CONNECTOR__TARGET_PORT1:
				setTargetPort1(TARGET_PORT1_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_CONNECTOR__GROUP1:
				return group1 != null && !group1.isEmpty();
			case CommonPackage.IMP_CONNECTOR__SOURCE_PORT:
				return !getSourcePort().isEmpty();
			case CommonPackage.IMP_CONNECTOR__TARGET_PORT:
				return !getTargetPort().isEmpty();
			case CommonPackage.IMP_CONNECTOR__SOURCE_PORT1:
				return SOURCE_PORT1_EDEFAULT == null ? sourcePort1 != null : !SOURCE_PORT1_EDEFAULT.equals(sourcePort1);
			case CommonPackage.IMP_CONNECTOR__TARGET_PORT1:
				return TARGET_PORT1_EDEFAULT == null ? targetPort1 != null : !TARGET_PORT1_EDEFAULT.equals(targetPort1);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group1: ");
		result.append(group1);
		result.append(", sourcePort1: ");
		result.append(sourcePort1);
		result.append(", targetPort1: ");
		result.append(targetPort1);
		result.append(')');
		return result.toString();
	}

} //ImpConnectorImpl
