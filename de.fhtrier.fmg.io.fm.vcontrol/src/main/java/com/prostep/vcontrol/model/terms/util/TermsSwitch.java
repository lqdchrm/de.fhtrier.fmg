/**
 */
package com.prostep.vcontrol.model.terms.util;

import com.prostep.vcontrol.model.common.ImpTerm;

import com.prostep.vcontrol.model.terms.*;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.terms.TermsPackage
 * @generated
 */
public class TermsSwitch {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TermsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TermsSwitch() {
		if (modelPackage == null) {
			modelPackage = TermsPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public Object doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch((EClass)eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TermsPackage.ABSTRACT_MATH_TERM: {
				AbstractMathTerm abstractMathTerm = (AbstractMathTerm)theEObject;
				Object result = caseAbstractMathTerm(abstractMathTerm);
				if (result == null) result = caseAtomicTerm(abstractMathTerm);
				if (result == null) result = caseImpTerm(abstractMathTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.AND_TERM: {
				AndTerm andTerm = (AndTerm)theEObject;
				Object result = caseAndTerm(andTerm);
				if (result == null) result = caseNaryTerm(andTerm);
				if (result == null) result = caseImpTerm(andTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.ATOMIC_TERM: {
				AtomicTerm atomicTerm = (AtomicTerm)theEObject;
				Object result = caseAtomicTerm(atomicTerm);
				if (result == null) result = caseImpTerm(atomicTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.BINARY_TERM: {
				BinaryTerm binaryTerm = (BinaryTerm)theEObject;
				Object result = caseBinaryTerm(binaryTerm);
				if (result == null) result = caseImpTerm(binaryTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.DOCUMENT_ROOT: {
				DocumentRoot documentRoot = (DocumentRoot)theEObject;
				Object result = caseDocumentRoot(documentRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.EQUIVALENT_TERM: {
				EquivalentTerm equivalentTerm = (EquivalentTerm)theEObject;
				Object result = caseEquivalentTerm(equivalentTerm);
				if (result == null) result = caseBinaryTerm(equivalentTerm);
				if (result == null) result = caseImpTerm(equivalentTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.ERROR_STATUS_TERM: {
				ErrorStatusTerm errorStatusTerm = (ErrorStatusTerm)theEObject;
				Object result = caseErrorStatusTerm(errorStatusTerm);
				if (result == null) result = caseImpTerm(errorStatusTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.EXCLUDES_TERM: {
				ExcludesTerm excludesTerm = (ExcludesTerm)theEObject;
				Object result = caseExcludesTerm(excludesTerm);
				if (result == null) result = caseBinaryTerm(excludesTerm);
				if (result == null) result = caseImpTerm(excludesTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.FALSE_TERM: {
				FalseTerm falseTerm = (FalseTerm)theEObject;
				Object result = caseFalseTerm(falseTerm);
				if (result == null) result = caseImpTerm(falseTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.IMPLIES_TERM: {
				ImpliesTerm impliesTerm = (ImpliesTerm)theEObject;
				Object result = caseImpliesTerm(impliesTerm);
				if (result == null) result = caseBinaryTerm(impliesTerm);
				if (result == null) result = caseImpTerm(impliesTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.NARY_TERM: {
				NaryTerm naryTerm = (NaryTerm)theEObject;
				Object result = caseNaryTerm(naryTerm);
				if (result == null) result = caseImpTerm(naryTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.NOT_TERM: {
				NotTerm notTerm = (NotTerm)theEObject;
				Object result = caseNotTerm(notTerm);
				if (result == null) result = caseUnaryTerm(notTerm);
				if (result == null) result = caseImpTerm(notTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.OR_TERM: {
				OrTerm orTerm = (OrTerm)theEObject;
				Object result = caseOrTerm(orTerm);
				if (result == null) result = caseNaryTerm(orTerm);
				if (result == null) result = caseImpTerm(orTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.TRUE_TERM: {
				TrueTerm trueTerm = (TrueTerm)theEObject;
				Object result = caseTrueTerm(trueTerm);
				if (result == null) result = caseImpTerm(trueTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.UNARY_TERM: {
				UnaryTerm unaryTerm = (UnaryTerm)theEObject;
				Object result = caseUnaryTerm(unaryTerm);
				if (result == null) result = caseImpTerm(unaryTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TermsPackage.XOR_TERM: {
				XorTerm xorTerm = (XorTerm)theEObject;
				Object result = caseXorTerm(xorTerm);
				if (result == null) result = caseNaryTerm(xorTerm);
				if (result == null) result = caseImpTerm(xorTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Math Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Math Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAbstractMathTerm(AbstractMathTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>And Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>And Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAndTerm(AndTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atomic Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atomic Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAtomicTerm(AtomicTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseBinaryTerm(BinaryTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDocumentRoot(DocumentRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equivalent Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equivalent Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseEquivalentTerm(EquivalentTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Error Status Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Error Status Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseErrorStatusTerm(ErrorStatusTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Excludes Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Excludes Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseExcludesTerm(ExcludesTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>False Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>False Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseFalseTerm(FalseTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Implies Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Implies Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpliesTerm(ImpliesTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Nary Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Nary Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseNaryTerm(NaryTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseNotTerm(NotTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseOrTerm(OrTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>True Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>True Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseTrueTerm(TrueTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseUnaryTerm(UnaryTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Xor Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Xor Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseXorTerm(XorTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpTerm(ImpTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public Object defaultCase(EObject object) {
		return null;
	}

} //TermsSwitch
