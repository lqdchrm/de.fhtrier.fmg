/**
 */
package com.prostep.vcontrol.model.common.util;

import com.prostep.vcontrol.model.common.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.common.CommonPackage
 * @generated
 */
public class CommonAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CommonPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommonAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = CommonPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommonSwitch modelSwitch =
		new CommonSwitch() {
			public Object caseConstraint(Constraint object) {
				return createConstraintAdapter();
			}
			public Object caseDescribedItem(DescribedItem object) {
				return createDescribedItemAdapter();
			}
			public Object caseIdentifiable(Identifiable object) {
				return createIdentifiableAdapter();
			}
			public Object caseImpConfigurableElement(ImpConfigurableElement object) {
				return createImpConfigurableElementAdapter();
			}
			public Object caseImpConfigurableElementExtension(ImpConfigurableElementExtension object) {
				return createImpConfigurableElementExtensionAdapter();
			}
			public Object caseImpConfigurableGroup(ImpConfigurableGroup object) {
				return createImpConfigurableGroupAdapter();
			}
			public Object caseImpConfiguration(ImpConfiguration object) {
				return createImpConfigurationAdapter();
			}
			public Object caseImpConfiguredElement(ImpConfiguredElement object) {
				return createImpConfiguredElementAdapter();
			}
			public Object caseImpConnector(ImpConnector object) {
				return createImpConnectorAdapter();
			}
			public Object caseImpExtensibleElement(ImpExtensibleElement object) {
				return createImpExtensibleElementAdapter();
			}
			public Object caseImpItem(ImpItem object) {
				return createImpItemAdapter();
			}
			public Object caseImpLinkingRelation(ImpLinkingRelation object) {
				return createImpLinkingRelationAdapter();
			}
			public Object caseImpModel(ImpModel object) {
				return createImpModelAdapter();
			}
			public Object caseImpNode(ImpNode object) {
				return createImpNodeAdapter();
			}
			public Object caseImpPortNode(ImpPortNode object) {
				return createImpPortNodeAdapter();
			}
			public Object caseImpProject(ImpProject object) {
				return createImpProjectAdapter();
			}
			public Object caseImpTerm(ImpTerm object) {
				return createImpTermAdapter();
			}
			public Object caseNamedItem(NamedItem object) {
				return createNamedItemAdapter();
			}
			public Object caseProperty(Property object) {
				return createPropertyAdapter();
			}
			public Object casePropertySet(PropertySet object) {
				return createPropertySetAdapter();
			}
			public Object caseDocumentRoot(DocumentRoot object) {
				return createDocumentRootAdapter();
			}
			public Object defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter)modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.Constraint
	 * @generated
	 */
	public Adapter createConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.DescribedItem <em>Described Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.DescribedItem
	 * @generated
	 */
	public Adapter createDescribedItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.Identifiable <em>Identifiable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.Identifiable
	 * @generated
	 */
	public Adapter createIdentifiableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement <em>Imp Configurable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElement
	 * @generated
	 */
	public Adapter createImpConfigurableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension <em>Imp Configurable Element Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElementExtension
	 * @generated
	 */
	public Adapter createImpConfigurableElementExtensionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpConfigurableGroup <em>Imp Configurable Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableGroup
	 * @generated
	 */
	public Adapter createImpConfigurableGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpConfiguration <em>Imp Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguration
	 * @generated
	 */
	public Adapter createImpConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement <em>Imp Configured Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement
	 * @generated
	 */
	public Adapter createImpConfiguredElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpConnector <em>Imp Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpConnector
	 * @generated
	 */
	public Adapter createImpConnectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpExtensibleElement <em>Imp Extensible Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpExtensibleElement
	 * @generated
	 */
	public Adapter createImpExtensibleElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpItem <em>Imp Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpItem
	 * @generated
	 */
	public Adapter createImpItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation <em>Imp Linking Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation
	 * @generated
	 */
	public Adapter createImpLinkingRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpModel <em>Imp Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpModel
	 * @generated
	 */
	public Adapter createImpModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpNode <em>Imp Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpNode
	 * @generated
	 */
	public Adapter createImpNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpPortNode <em>Imp Port Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpPortNode
	 * @generated
	 */
	public Adapter createImpPortNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpProject <em>Imp Project</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpProject
	 * @generated
	 */
	public Adapter createImpProjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.ImpTerm <em>Imp Term</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.ImpTerm
	 * @generated
	 */
	public Adapter createImpTermAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.NamedItem <em>Named Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.NamedItem
	 * @generated
	 */
	public Adapter createNamedItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.Property
	 * @generated
	 */
	public Adapter createPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.PropertySet <em>Property Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.PropertySet
	 * @generated
	 */
	public Adapter createPropertySetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.prostep.vcontrol.model.common.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot
	 * @generated
	 */
	public Adapter createDocumentRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //CommonAdapterFactory
