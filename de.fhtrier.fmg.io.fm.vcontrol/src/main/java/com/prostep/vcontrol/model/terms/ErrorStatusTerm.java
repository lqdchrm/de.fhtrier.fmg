/**
 */
package com.prostep.vcontrol.model.terms;

import com.prostep.vcontrol.model.common.ImpTerm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Error Status Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.terms.ErrorStatusTerm#getErrorStatus <em>Error Status</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.ErrorStatusTerm#getMessage <em>Message</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getErrorStatusTerm()
 * @model extendedMetaData="name='ErrorStatusTerm' kind='elementOnly'"
 * @generated
 */
public interface ErrorStatusTerm extends ImpTerm {
	/**
	 * Returns the value of the '<em><b>Error Status</b></em>' attribute.
	 * The literals are from the enumeration {@link com.prostep.vcontrol.model.terms.ErrorStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Error Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Error Status</em>' attribute.
	 * @see com.prostep.vcontrol.model.terms.ErrorStatus
	 * @see #isSetErrorStatus()
	 * @see #unsetErrorStatus()
	 * @see #setErrorStatus(ErrorStatus)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getErrorStatusTerm_ErrorStatus()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='errorStatus'"
	 * @generated
	 */
	ErrorStatus getErrorStatus();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.ErrorStatusTerm#getErrorStatus <em>Error Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Error Status</em>' attribute.
	 * @see com.prostep.vcontrol.model.terms.ErrorStatus
	 * @see #isSetErrorStatus()
	 * @see #unsetErrorStatus()
	 * @see #getErrorStatus()
	 * @generated
	 */
	void setErrorStatus(ErrorStatus value);

	/**
	 * Unsets the value of the '{@link com.prostep.vcontrol.model.terms.ErrorStatusTerm#getErrorStatus <em>Error Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetErrorStatus()
	 * @see #getErrorStatus()
	 * @see #setErrorStatus(ErrorStatus)
	 * @generated
	 */
	void unsetErrorStatus();

	/**
	 * Returns whether the value of the '{@link com.prostep.vcontrol.model.terms.ErrorStatusTerm#getErrorStatus <em>Error Status</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Error Status</em>' attribute is set.
	 * @see #unsetErrorStatus()
	 * @see #getErrorStatus()
	 * @see #setErrorStatus(ErrorStatus)
	 * @generated
	 */
	boolean isSetErrorStatus();

	/**
	 * Returns the value of the '<em><b>Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' attribute.
	 * @see #setMessage(String)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getErrorStatusTerm_Message()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='message'"
	 * @generated
	 */
	String getMessage();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.ErrorStatusTerm#getMessage <em>Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message</em>' attribute.
	 * @see #getMessage()
	 * @generated
	 */
	void setMessage(String value);

} // ErrorStatusTerm
