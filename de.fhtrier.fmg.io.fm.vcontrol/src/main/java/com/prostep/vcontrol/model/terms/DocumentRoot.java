/**
 */
package com.prostep.vcontrol.model.terms;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getMixed <em>Mixed</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getAbstractMathTerm <em>Abstract Math Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getAndTerm <em>And Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getAtomicTerm <em>Atomic Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getBinaryTerm <em>Binary Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getEquivalentTerm <em>Equivalent Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getErrorStatusTerm <em>Error Status Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getExcludesTerm <em>Excludes Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getFalseTerm <em>False Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getImpliesTerm <em>Implies Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getNaryTerm <em>Nary Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getNotTerm <em>Not Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getOrTerm <em>Or Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getTrueTerm <em>True Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getUnaryTerm <em>Unary Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.DocumentRoot#getXorTerm <em>Xor Term</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot()
 * @model extendedMetaData="name='' kind='mixed'"
 * @generated
 */
public interface DocumentRoot extends EObject {
	/**
	 * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mixed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mixed</em>' attribute list.
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_Mixed()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' name=':mixed'"
	 * @generated
	 */
	FeatureMap getMixed();

	/**
	 * Returns the value of the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XMLNS Prefix Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XMLNS Prefix Map</em>' map.
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_XMLNSPrefixMap()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry" keyType="java.lang.String" valueType="java.lang.String" transient="true"
	 *        extendedMetaData="kind='attribute' name='xmlns:prefix'"
	 * @generated
	 */
	EMap getXMLNSPrefixMap();

	/**
	 * Returns the value of the '<em><b>XSI Schema Location</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XSI Schema Location</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XSI Schema Location</em>' map.
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_XSISchemaLocation()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry" keyType="java.lang.String" valueType="java.lang.String" transient="true"
	 *        extendedMetaData="kind='attribute' name='xsi:schemaLocation'"
	 * @generated
	 */
	EMap getXSISchemaLocation();

	/**
	 * Returns the value of the '<em><b>Abstract Math Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Math Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Math Term</em>' containment reference.
	 * @see #setAbstractMathTerm(AbstractMathTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_AbstractMathTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AbstractMathTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	AbstractMathTerm getAbstractMathTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getAbstractMathTerm <em>Abstract Math Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Math Term</em>' containment reference.
	 * @see #getAbstractMathTerm()
	 * @generated
	 */
	void setAbstractMathTerm(AbstractMathTerm value);

	/**
	 * Returns the value of the '<em><b>And Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Term</em>' containment reference.
	 * @see #setAndTerm(AndTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_AndTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AndTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	AndTerm getAndTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getAndTerm <em>And Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>And Term</em>' containment reference.
	 * @see #getAndTerm()
	 * @generated
	 */
	void setAndTerm(AndTerm value);

	/**
	 * Returns the value of the '<em><b>Atomic Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Term</em>' containment reference.
	 * @see #setAtomicTerm(AtomicTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_AtomicTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AtomicTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicTerm getAtomicTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getAtomicTerm <em>Atomic Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Term</em>' containment reference.
	 * @see #getAtomicTerm()
	 * @generated
	 */
	void setAtomicTerm(AtomicTerm value);

	/**
	 * Returns the value of the '<em><b>Binary Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binary Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binary Term</em>' containment reference.
	 * @see #setBinaryTerm(BinaryTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_BinaryTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='BinaryTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	BinaryTerm getBinaryTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getBinaryTerm <em>Binary Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binary Term</em>' containment reference.
	 * @see #getBinaryTerm()
	 * @generated
	 */
	void setBinaryTerm(BinaryTerm value);

	/**
	 * Returns the value of the '<em><b>Equivalent Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equivalent Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equivalent Term</em>' containment reference.
	 * @see #setEquivalentTerm(EquivalentTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_EquivalentTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='EquivalentTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	EquivalentTerm getEquivalentTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getEquivalentTerm <em>Equivalent Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equivalent Term</em>' containment reference.
	 * @see #getEquivalentTerm()
	 * @generated
	 */
	void setEquivalentTerm(EquivalentTerm value);

	/**
	 * Returns the value of the '<em><b>Error Status Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Error Status Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Error Status Term</em>' containment reference.
	 * @see #setErrorStatusTerm(ErrorStatusTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_ErrorStatusTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ErrorStatusTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	ErrorStatusTerm getErrorStatusTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getErrorStatusTerm <em>Error Status Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Error Status Term</em>' containment reference.
	 * @see #getErrorStatusTerm()
	 * @generated
	 */
	void setErrorStatusTerm(ErrorStatusTerm value);

	/**
	 * Returns the value of the '<em><b>Excludes Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Excludes Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Excludes Term</em>' containment reference.
	 * @see #setExcludesTerm(ExcludesTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_ExcludesTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ExcludesTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	ExcludesTerm getExcludesTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getExcludesTerm <em>Excludes Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Excludes Term</em>' containment reference.
	 * @see #getExcludesTerm()
	 * @generated
	 */
	void setExcludesTerm(ExcludesTerm value);

	/**
	 * Returns the value of the '<em><b>False Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>False Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>False Term</em>' containment reference.
	 * @see #setFalseTerm(FalseTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_FalseTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='FalseTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	FalseTerm getFalseTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getFalseTerm <em>False Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>False Term</em>' containment reference.
	 * @see #getFalseTerm()
	 * @generated
	 */
	void setFalseTerm(FalseTerm value);

	/**
	 * Returns the value of the '<em><b>Implies Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implies Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implies Term</em>' containment reference.
	 * @see #setImpliesTerm(ImpliesTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_ImpliesTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ImpliesTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	ImpliesTerm getImpliesTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getImpliesTerm <em>Implies Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implies Term</em>' containment reference.
	 * @see #getImpliesTerm()
	 * @generated
	 */
	void setImpliesTerm(ImpliesTerm value);

	/**
	 * Returns the value of the '<em><b>Nary Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nary Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nary Term</em>' containment reference.
	 * @see #setNaryTerm(NaryTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_NaryTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='NaryTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	NaryTerm getNaryTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getNaryTerm <em>Nary Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nary Term</em>' containment reference.
	 * @see #getNaryTerm()
	 * @generated
	 */
	void setNaryTerm(NaryTerm value);

	/**
	 * Returns the value of the '<em><b>Not Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Term</em>' containment reference.
	 * @see #setNotTerm(NotTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_NotTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='NotTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	NotTerm getNotTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getNotTerm <em>Not Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not Term</em>' containment reference.
	 * @see #getNotTerm()
	 * @generated
	 */
	void setNotTerm(NotTerm value);

	/**
	 * Returns the value of the '<em><b>Or Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Term</em>' containment reference.
	 * @see #setOrTerm(OrTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_OrTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='OrTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	OrTerm getOrTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getOrTerm <em>Or Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Or Term</em>' containment reference.
	 * @see #getOrTerm()
	 * @generated
	 */
	void setOrTerm(OrTerm value);

	/**
	 * Returns the value of the '<em><b>True Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>True Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>True Term</em>' containment reference.
	 * @see #setTrueTerm(TrueTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_TrueTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='TrueTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	TrueTerm getTrueTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getTrueTerm <em>True Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>True Term</em>' containment reference.
	 * @see #getTrueTerm()
	 * @generated
	 */
	void setTrueTerm(TrueTerm value);

	/**
	 * Returns the value of the '<em><b>Unary Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unary Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unary Term</em>' containment reference.
	 * @see #setUnaryTerm(UnaryTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_UnaryTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='UnaryTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	UnaryTerm getUnaryTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getUnaryTerm <em>Unary Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unary Term</em>' containment reference.
	 * @see #getUnaryTerm()
	 * @generated
	 */
	void setUnaryTerm(UnaryTerm value);

	/**
	 * Returns the value of the '<em><b>Xor Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xor Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xor Term</em>' containment reference.
	 * @see #setXorTerm(XorTerm)
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getDocumentRoot_XorTerm()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='XorTerm' namespace='##targetNamespace'"
	 * @generated
	 */
	XorTerm getXorTerm();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.terms.DocumentRoot#getXorTerm <em>Xor Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xor Term</em>' containment reference.
	 * @see #getXorTerm()
	 * @generated
	 */
	void setXorTerm(XorTerm value);

} // DocumentRoot
