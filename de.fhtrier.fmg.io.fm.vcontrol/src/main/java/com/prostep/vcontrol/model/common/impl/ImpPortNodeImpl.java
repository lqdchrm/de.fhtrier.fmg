/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.CommonPackage;
import com.prostep.vcontrol.model.common.ImpPortNode;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Imp Port Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpPortNodeImpl#getGroup2 <em>Group2</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpPortNodeImpl#getConnectorsForSourcePort <em>Connectors For Source Port</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpPortNodeImpl#getConnectorsForTargetPort <em>Connectors For Target Port</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpPortNodeImpl#getConnectorsForSourcePort1 <em>Connectors For Source Port1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpPortNodeImpl#getConnectorsForTargetPort1 <em>Connectors For Target Port1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ImpPortNodeImpl extends ImpConfigurableElementImpl implements ImpPortNode {
	/**
	 * The cached value of the '{@link #getGroup2() <em>Group2</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup2()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group2;

	/**
	 * The default value of the '{@link #getConnectorsForSourcePort1() <em>Connectors For Source Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectorsForSourcePort1()
	 * @generated
	 * @ordered
	 */
	protected static final String CONNECTORS_FOR_SOURCE_PORT1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConnectorsForSourcePort1() <em>Connectors For Source Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectorsForSourcePort1()
	 * @generated
	 * @ordered
	 */
	protected String connectorsForSourcePort1 = CONNECTORS_FOR_SOURCE_PORT1_EDEFAULT;

	/**
	 * The default value of the '{@link #getConnectorsForTargetPort1() <em>Connectors For Target Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectorsForTargetPort1()
	 * @generated
	 * @ordered
	 */
	protected static final String CONNECTORS_FOR_TARGET_PORT1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConnectorsForTargetPort1() <em>Connectors For Target Port1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectorsForTargetPort1()
	 * @generated
	 * @ordered
	 */
	protected String connectorsForTargetPort1 = CONNECTORS_FOR_TARGET_PORT1_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImpPortNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CommonPackage.Literals.IMP_PORT_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup2() {
		if (group2 == null) {
			group2 = new BasicFeatureMap(this, CommonPackage.IMP_PORT_NODE__GROUP2);
		}
		return group2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getConnectorsForSourcePort() {
		return getGroup2().list(CommonPackage.Literals.IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getConnectorsForTargetPort() {
		return getGroup2().list(CommonPackage.Literals.IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConnectorsForSourcePort1() {
		return connectorsForSourcePort1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnectorsForSourcePort1(String newConnectorsForSourcePort1) {
		String oldConnectorsForSourcePort1 = connectorsForSourcePort1;
		connectorsForSourcePort1 = newConnectorsForSourcePort1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT1, oldConnectorsForSourcePort1, connectorsForSourcePort1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConnectorsForTargetPort1() {
		return connectorsForTargetPort1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnectorsForTargetPort1(String newConnectorsForTargetPort1) {
		String oldConnectorsForTargetPort1 = connectorsForTargetPort1;
		connectorsForTargetPort1 = newConnectorsForTargetPort1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT1, oldConnectorsForTargetPort1, connectorsForTargetPort1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CommonPackage.IMP_PORT_NODE__GROUP2:
				return ((InternalEList)getGroup2()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT:
				return ((InternalEList)getConnectorsForSourcePort()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT:
				return ((InternalEList)getConnectorsForTargetPort()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CommonPackage.IMP_PORT_NODE__GROUP2:
				if (coreType) return getGroup2();
				return ((FeatureMap.Internal)getGroup2()).getWrapper();
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT:
				return getConnectorsForSourcePort();
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT:
				return getConnectorsForTargetPort();
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT1:
				return getConnectorsForSourcePort1();
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT1:
				return getConnectorsForTargetPort1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CommonPackage.IMP_PORT_NODE__GROUP2:
				((FeatureMap.Internal)getGroup2()).set(newValue);
				return;
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT:
				getConnectorsForSourcePort().clear();
				getConnectorsForSourcePort().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT:
				getConnectorsForTargetPort().clear();
				getConnectorsForTargetPort().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT1:
				setConnectorsForSourcePort1((String)newValue);
				return;
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT1:
				setConnectorsForTargetPort1((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_PORT_NODE__GROUP2:
				getGroup2().clear();
				return;
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT:
				getConnectorsForSourcePort().clear();
				return;
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT:
				getConnectorsForTargetPort().clear();
				return;
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT1:
				setConnectorsForSourcePort1(CONNECTORS_FOR_SOURCE_PORT1_EDEFAULT);
				return;
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT1:
				setConnectorsForTargetPort1(CONNECTORS_FOR_TARGET_PORT1_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_PORT_NODE__GROUP2:
				return group2 != null && !group2.isEmpty();
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT:
				return !getConnectorsForSourcePort().isEmpty();
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT:
				return !getConnectorsForTargetPort().isEmpty();
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT1:
				return CONNECTORS_FOR_SOURCE_PORT1_EDEFAULT == null ? connectorsForSourcePort1 != null : !CONNECTORS_FOR_SOURCE_PORT1_EDEFAULT.equals(connectorsForSourcePort1);
			case CommonPackage.IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT1:
				return CONNECTORS_FOR_TARGET_PORT1_EDEFAULT == null ? connectorsForTargetPort1 != null : !CONNECTORS_FOR_TARGET_PORT1_EDEFAULT.equals(connectorsForTargetPort1);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group2: ");
		result.append(group2);
		result.append(", connectorsForSourcePort1: ");
		result.append(connectorsForSourcePort1);
		result.append(", connectorsForTargetPort1: ");
		result.append(connectorsForTargetPort1);
		result.append(')');
		return result.toString();
	}

} //ImpPortNodeImpl
