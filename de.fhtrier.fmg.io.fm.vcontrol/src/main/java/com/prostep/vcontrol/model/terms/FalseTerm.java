/**
 */
package com.prostep.vcontrol.model.terms;

import com.prostep.vcontrol.model.common.ImpTerm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>False Term</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getFalseTerm()
 * @model extendedMetaData="name='FalseTerm' kind='elementOnly'"
 * @generated
 */
public interface FalseTerm extends ImpTerm {
} // FalseTerm
