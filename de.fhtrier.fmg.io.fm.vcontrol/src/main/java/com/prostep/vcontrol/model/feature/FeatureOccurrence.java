/**
 */
package com.prostep.vcontrol.model.feature;

import com.prostep.vcontrol.model.terms.AtomicTerm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Occurrence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureOccurrence#getGroup2 <em>Group2</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureOccurrence#getFeatureNode <em>Feature Node</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.FeatureOccurrence#getFeatureNode1 <em>Feature Node1</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureOccurrence()
 * @model extendedMetaData="name='FeatureOccurrence' kind='elementOnly'"
 * @generated
 */
public interface FeatureOccurrence extends AtomicTerm {
	/**
	 * Returns the value of the '<em><b>Group2</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group2</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group2</em>' attribute list.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureOccurrence_Group2()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:12'"
	 * @generated
	 */
	FeatureMap getGroup2();

	/**
	 * Returns the value of the '<em><b>Feature Node</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.feature.FeatureNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Node</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Node</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureOccurrence_FeatureNode()
	 * @model type="com.prostep.vcontrol.model.feature.FeatureNode" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='featureNode' group='#group:12'"
	 * @generated
	 */
	EList getFeatureNode();

	/**
	 * Returns the value of the '<em><b>Feature Node1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Node1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Node1</em>' attribute.
	 * @see #setFeatureNode1(String)
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureOccurrence_FeatureNode1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='featureNode'"
	 * @generated
	 */
	String getFeatureNode1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.feature.FeatureOccurrence#getFeatureNode1 <em>Feature Node1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Node1</em>' attribute.
	 * @see #getFeatureNode1()
	 * @generated
	 */
	void setFeatureNode1(String value);

} // FeatureOccurrence
