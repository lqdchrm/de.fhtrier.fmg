/**
 */
package com.prostep.vcontrol.model.terms.impl;

import com.prostep.vcontrol.model.terms.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TermsFactoryImpl extends EFactoryImpl implements TermsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TermsFactory init() {
		try {
			TermsFactory theTermsFactory = (TermsFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.prostep.com/vcontrol/model/terms.ecore"); 
			if (theTermsFactory != null) {
				return theTermsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TermsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TermsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TermsPackage.AND_TERM: return createAndTerm();
			case TermsPackage.ATOMIC_TERM: return createAtomicTerm();
			case TermsPackage.DOCUMENT_ROOT: return createDocumentRoot();
			case TermsPackage.EQUIVALENT_TERM: return createEquivalentTerm();
			case TermsPackage.ERROR_STATUS_TERM: return createErrorStatusTerm();
			case TermsPackage.EXCLUDES_TERM: return createExcludesTerm();
			case TermsPackage.FALSE_TERM: return createFalseTerm();
			case TermsPackage.IMPLIES_TERM: return createImpliesTerm();
			case TermsPackage.NOT_TERM: return createNotTerm();
			case TermsPackage.OR_TERM: return createOrTerm();
			case TermsPackage.TRUE_TERM: return createTrueTerm();
			case TermsPackage.XOR_TERM: return createXorTerm();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case TermsPackage.ERROR_STATUS:
				return createErrorStatusFromString(eDataType, initialValue);
			case TermsPackage.ERROR_STATUS_OBJECT:
				return createErrorStatusObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case TermsPackage.ERROR_STATUS:
				return convertErrorStatusToString(eDataType, instanceValue);
			case TermsPackage.ERROR_STATUS_OBJECT:
				return convertErrorStatusObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AndTerm createAndTerm() {
		AndTermImpl andTerm = new AndTermImpl();
		return andTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicTerm createAtomicTerm() {
		AtomicTermImpl atomicTerm = new AtomicTermImpl();
		return atomicTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EquivalentTerm createEquivalentTerm() {
		EquivalentTermImpl equivalentTerm = new EquivalentTermImpl();
		return equivalentTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ErrorStatusTerm createErrorStatusTerm() {
		ErrorStatusTermImpl errorStatusTerm = new ErrorStatusTermImpl();
		return errorStatusTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExcludesTerm createExcludesTerm() {
		ExcludesTermImpl excludesTerm = new ExcludesTermImpl();
		return excludesTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FalseTerm createFalseTerm() {
		FalseTermImpl falseTerm = new FalseTermImpl();
		return falseTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpliesTerm createImpliesTerm() {
		ImpliesTermImpl impliesTerm = new ImpliesTermImpl();
		return impliesTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotTerm createNotTerm() {
		NotTermImpl notTerm = new NotTermImpl();
		return notTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrTerm createOrTerm() {
		OrTermImpl orTerm = new OrTermImpl();
		return orTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrueTerm createTrueTerm() {
		TrueTermImpl trueTerm = new TrueTermImpl();
		return trueTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XorTerm createXorTerm() {
		XorTermImpl xorTerm = new XorTermImpl();
		return xorTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ErrorStatus createErrorStatusFromString(EDataType eDataType, String initialValue) {
		ErrorStatus result = ErrorStatus.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertErrorStatusToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ErrorStatus createErrorStatusObjectFromString(EDataType eDataType, String initialValue) {
		return createErrorStatusFromString(TermsPackage.Literals.ERROR_STATUS, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertErrorStatusObjectToString(EDataType eDataType, Object instanceValue) {
		return convertErrorStatusToString(TermsPackage.Literals.ERROR_STATUS, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TermsPackage getTermsPackage() {
		return (TermsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static TermsPackage getPackage() {
		return TermsPackage.eINSTANCE;
	}

} //TermsFactoryImpl
