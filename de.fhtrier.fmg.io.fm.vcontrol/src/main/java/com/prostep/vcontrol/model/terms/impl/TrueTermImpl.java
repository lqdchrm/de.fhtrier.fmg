/**
 */
package com.prostep.vcontrol.model.terms.impl;

import com.prostep.vcontrol.model.common.impl.ImpTermImpl;

import com.prostep.vcontrol.model.terms.TermsPackage;
import com.prostep.vcontrol.model.terms.TrueTerm;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>True Term</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class TrueTermImpl extends ImpTermImpl implements TrueTerm {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TrueTermImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return TermsPackage.Literals.TRUE_TERM;
	}

} //TrueTermImpl
