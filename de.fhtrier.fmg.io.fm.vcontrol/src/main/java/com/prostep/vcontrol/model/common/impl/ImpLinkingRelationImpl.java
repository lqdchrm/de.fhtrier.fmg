/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.CommonPackage;
import com.prostep.vcontrol.model.common.ImpLinkingRelation;
import com.prostep.vcontrol.model.common.LinkingType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Imp Linking Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getRightElement <em>Right Element</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getLeftElement <em>Left Element</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getExtension <em>Extension</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getHref <em>Href</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getId <em>Id</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getIdref <em>Idref</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getLeftElement1 <em>Left Element1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getLinkingType <em>Linking Type</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getRightElement1 <em>Right Element1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getType <em>Type</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getUuid <em>Uuid</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl#getVersion <em>Version</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ImpLinkingRelationImpl extends EObjectImpl implements ImpLinkingRelation {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The default value of the '{@link #getHref() <em>Href</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHref()
	 * @generated
	 * @ordered
	 */
	protected static final String HREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHref() <em>Href</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHref()
	 * @generated
	 * @ordered
	 */
	protected String href = HREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getIdref() <em>Idref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdref()
	 * @generated
	 * @ordered
	 */
	protected static final String IDREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIdref() <em>Idref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdref()
	 * @generated
	 * @ordered
	 */
	protected String idref = IDREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getLeftElement1() <em>Left Element1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftElement1()
	 * @generated
	 * @ordered
	 */
	protected static final String LEFT_ELEMENT1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLeftElement1() <em>Left Element1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftElement1()
	 * @generated
	 * @ordered
	 */
	protected String leftElement1 = LEFT_ELEMENT1_EDEFAULT;

	/**
	 * The default value of the '{@link #getLinkingType() <em>Linking Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkingType()
	 * @generated
	 * @ordered
	 */
	protected static final LinkingType LINKING_TYPE_EDEFAULT = LinkingType.TRACEABILITY_LITERAL;

	/**
	 * The cached value of the '{@link #getLinkingType() <em>Linking Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkingType()
	 * @generated
	 * @ordered
	 */
	protected LinkingType linkingType = LINKING_TYPE_EDEFAULT;

	/**
	 * This is true if the Linking Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean linkingTypeESet;

	/**
	 * The default value of the '{@link #getRightElement1() <em>Right Element1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightElement1()
	 * @generated
	 * @ordered
	 */
	protected static final String RIGHT_ELEMENT1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRightElement1() <em>Right Element1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightElement1()
	 * @generated
	 * @ordered
	 */
	protected String rightElement1 = RIGHT_ELEMENT1_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final Object TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Object type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUuid()
	 * @generated
	 * @ordered
	 */
	protected static final String UUID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUuid()
	 * @generated
	 * @ordered
	 */
	protected String uuid = UUID_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = "2.0";

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * This is true if the Version attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean versionESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImpLinkingRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CommonPackage.Literals.IMP_LINKING_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, CommonPackage.IMP_LINKING_RELATION__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getRightElement() {
		return getGroup().list(CommonPackage.Literals.IMP_LINKING_RELATION__RIGHT_ELEMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getLeftElement() {
		return getGroup().list(CommonPackage.Literals.IMP_LINKING_RELATION__LEFT_ELEMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getExtension() {
		return getGroup().list(CommonPackage.Literals.IMP_LINKING_RELATION__EXTENSION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHref() {
		return href;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHref(String newHref) {
		String oldHref = href;
		href = newHref;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_LINKING_RELATION__HREF, oldHref, href));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_LINKING_RELATION__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIdref() {
		return idref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdref(String newIdref) {
		String oldIdref = idref;
		idref = newIdref;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_LINKING_RELATION__IDREF, oldIdref, idref));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_LINKING_RELATION__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLeftElement1() {
		return leftElement1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftElement1(String newLeftElement1) {
		String oldLeftElement1 = leftElement1;
		leftElement1 = newLeftElement1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_LINKING_RELATION__LEFT_ELEMENT1, oldLeftElement1, leftElement1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkingType getLinkingType() {
		return linkingType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkingType(LinkingType newLinkingType) {
		LinkingType oldLinkingType = linkingType;
		linkingType = newLinkingType == null ? LINKING_TYPE_EDEFAULT : newLinkingType;
		boolean oldLinkingTypeESet = linkingTypeESet;
		linkingTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_LINKING_RELATION__LINKING_TYPE, oldLinkingType, linkingType, !oldLinkingTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLinkingType() {
		LinkingType oldLinkingType = linkingType;
		boolean oldLinkingTypeESet = linkingTypeESet;
		linkingType = LINKING_TYPE_EDEFAULT;
		linkingTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.IMP_LINKING_RELATION__LINKING_TYPE, oldLinkingType, LINKING_TYPE_EDEFAULT, oldLinkingTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLinkingType() {
		return linkingTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRightElement1() {
		return rightElement1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightElement1(String newRightElement1) {
		String oldRightElement1 = rightElement1;
		rightElement1 = newRightElement1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_LINKING_RELATION__RIGHT_ELEMENT1, oldRightElement1, rightElement1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Object newType) {
		Object oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_LINKING_RELATION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUuid(String newUuid) {
		String oldUuid = uuid;
		uuid = newUuid;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_LINKING_RELATION__UUID, oldUuid, uuid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		boolean oldVersionESet = versionESet;
		versionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_LINKING_RELATION__VERSION, oldVersion, version, !oldVersionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetVersion() {
		String oldVersion = version;
		boolean oldVersionESet = versionESet;
		version = VERSION_EDEFAULT;
		versionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.IMP_LINKING_RELATION__VERSION, oldVersion, VERSION_EDEFAULT, oldVersionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetVersion() {
		return versionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CommonPackage.IMP_LINKING_RELATION__GROUP:
				return ((InternalEList)getGroup()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_LINKING_RELATION__RIGHT_ELEMENT:
				return ((InternalEList)getRightElement()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_LINKING_RELATION__LEFT_ELEMENT:
				return ((InternalEList)getLeftElement()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_LINKING_RELATION__EXTENSION:
				return ((InternalEList)getExtension()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CommonPackage.IMP_LINKING_RELATION__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case CommonPackage.IMP_LINKING_RELATION__RIGHT_ELEMENT:
				return getRightElement();
			case CommonPackage.IMP_LINKING_RELATION__LEFT_ELEMENT:
				return getLeftElement();
			case CommonPackage.IMP_LINKING_RELATION__EXTENSION:
				return getExtension();
			case CommonPackage.IMP_LINKING_RELATION__HREF:
				return getHref();
			case CommonPackage.IMP_LINKING_RELATION__ID:
				return getId();
			case CommonPackage.IMP_LINKING_RELATION__IDREF:
				return getIdref();
			case CommonPackage.IMP_LINKING_RELATION__LABEL:
				return getLabel();
			case CommonPackage.IMP_LINKING_RELATION__LEFT_ELEMENT1:
				return getLeftElement1();
			case CommonPackage.IMP_LINKING_RELATION__LINKING_TYPE:
				return getLinkingType();
			case CommonPackage.IMP_LINKING_RELATION__RIGHT_ELEMENT1:
				return getRightElement1();
			case CommonPackage.IMP_LINKING_RELATION__TYPE:
				return getType();
			case CommonPackage.IMP_LINKING_RELATION__UUID:
				return getUuid();
			case CommonPackage.IMP_LINKING_RELATION__VERSION:
				return getVersion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CommonPackage.IMP_LINKING_RELATION__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__RIGHT_ELEMENT:
				getRightElement().clear();
				getRightElement().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__LEFT_ELEMENT:
				getLeftElement().clear();
				getLeftElement().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__EXTENSION:
				getExtension().clear();
				getExtension().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__HREF:
				setHref((String)newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__ID:
				setId((String)newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__IDREF:
				setIdref((String)newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__LABEL:
				setLabel((String)newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__LEFT_ELEMENT1:
				setLeftElement1((String)newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__LINKING_TYPE:
				setLinkingType((LinkingType)newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__RIGHT_ELEMENT1:
				setRightElement1((String)newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__TYPE:
				setType(newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__UUID:
				setUuid((String)newValue);
				return;
			case CommonPackage.IMP_LINKING_RELATION__VERSION:
				setVersion((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_LINKING_RELATION__GROUP:
				getGroup().clear();
				return;
			case CommonPackage.IMP_LINKING_RELATION__RIGHT_ELEMENT:
				getRightElement().clear();
				return;
			case CommonPackage.IMP_LINKING_RELATION__LEFT_ELEMENT:
				getLeftElement().clear();
				return;
			case CommonPackage.IMP_LINKING_RELATION__EXTENSION:
				getExtension().clear();
				return;
			case CommonPackage.IMP_LINKING_RELATION__HREF:
				setHref(HREF_EDEFAULT);
				return;
			case CommonPackage.IMP_LINKING_RELATION__ID:
				setId(ID_EDEFAULT);
				return;
			case CommonPackage.IMP_LINKING_RELATION__IDREF:
				setIdref(IDREF_EDEFAULT);
				return;
			case CommonPackage.IMP_LINKING_RELATION__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case CommonPackage.IMP_LINKING_RELATION__LEFT_ELEMENT1:
				setLeftElement1(LEFT_ELEMENT1_EDEFAULT);
				return;
			case CommonPackage.IMP_LINKING_RELATION__LINKING_TYPE:
				unsetLinkingType();
				return;
			case CommonPackage.IMP_LINKING_RELATION__RIGHT_ELEMENT1:
				setRightElement1(RIGHT_ELEMENT1_EDEFAULT);
				return;
			case CommonPackage.IMP_LINKING_RELATION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case CommonPackage.IMP_LINKING_RELATION__UUID:
				setUuid(UUID_EDEFAULT);
				return;
			case CommonPackage.IMP_LINKING_RELATION__VERSION:
				unsetVersion();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_LINKING_RELATION__GROUP:
				return group != null && !group.isEmpty();
			case CommonPackage.IMP_LINKING_RELATION__RIGHT_ELEMENT:
				return !getRightElement().isEmpty();
			case CommonPackage.IMP_LINKING_RELATION__LEFT_ELEMENT:
				return !getLeftElement().isEmpty();
			case CommonPackage.IMP_LINKING_RELATION__EXTENSION:
				return !getExtension().isEmpty();
			case CommonPackage.IMP_LINKING_RELATION__HREF:
				return HREF_EDEFAULT == null ? href != null : !HREF_EDEFAULT.equals(href);
			case CommonPackage.IMP_LINKING_RELATION__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case CommonPackage.IMP_LINKING_RELATION__IDREF:
				return IDREF_EDEFAULT == null ? idref != null : !IDREF_EDEFAULT.equals(idref);
			case CommonPackage.IMP_LINKING_RELATION__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case CommonPackage.IMP_LINKING_RELATION__LEFT_ELEMENT1:
				return LEFT_ELEMENT1_EDEFAULT == null ? leftElement1 != null : !LEFT_ELEMENT1_EDEFAULT.equals(leftElement1);
			case CommonPackage.IMP_LINKING_RELATION__LINKING_TYPE:
				return isSetLinkingType();
			case CommonPackage.IMP_LINKING_RELATION__RIGHT_ELEMENT1:
				return RIGHT_ELEMENT1_EDEFAULT == null ? rightElement1 != null : !RIGHT_ELEMENT1_EDEFAULT.equals(rightElement1);
			case CommonPackage.IMP_LINKING_RELATION__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
			case CommonPackage.IMP_LINKING_RELATION__UUID:
				return UUID_EDEFAULT == null ? uuid != null : !UUID_EDEFAULT.equals(uuid);
			case CommonPackage.IMP_LINKING_RELATION__VERSION:
				return isSetVersion();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", href: ");
		result.append(href);
		result.append(", id: ");
		result.append(id);
		result.append(", idref: ");
		result.append(idref);
		result.append(", label: ");
		result.append(label);
		result.append(", leftElement1: ");
		result.append(leftElement1);
		result.append(", linkingType: ");
		if (linkingTypeESet) result.append(linkingType); else result.append("<unset>");
		result.append(", rightElement1: ");
		result.append(rightElement1);
		result.append(", type: ");
		result.append(type);
		result.append(", uuid: ");
		result.append(uuid);
		result.append(", version: ");
		if (versionESet) result.append(version); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //ImpLinkingRelationImpl
