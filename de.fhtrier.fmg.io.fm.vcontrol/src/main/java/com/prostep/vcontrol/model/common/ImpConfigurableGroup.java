/**
 */
package com.prostep.vcontrol.model.common;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Imp Configurable Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.ImpConfigurableGroup#getGroupType <em>Group Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableGroup()
 * @model abstract="true"
 *        extendedMetaData="name='ImpConfigurableGroup' kind='elementOnly'"
 * @generated
 */
public interface ImpConfigurableGroup extends ImpConfigurableElement {
	/**
	 * Returns the value of the '<em><b>Group Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.prostep.vcontrol.model.common.ImpConfigurableGroupType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Type</em>' attribute.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableGroupType
	 * @see #isSetGroupType()
	 * @see #unsetGroupType()
	 * @see #setGroupType(ImpConfigurableGroupType)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getImpConfigurableGroup_GroupType()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='groupType'"
	 * @generated
	 */
	ImpConfigurableGroupType getGroupType();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.ImpConfigurableGroup#getGroupType <em>Group Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Type</em>' attribute.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableGroupType
	 * @see #isSetGroupType()
	 * @see #unsetGroupType()
	 * @see #getGroupType()
	 * @generated
	 */
	void setGroupType(ImpConfigurableGroupType value);

	/**
	 * Unsets the value of the '{@link com.prostep.vcontrol.model.common.ImpConfigurableGroup#getGroupType <em>Group Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetGroupType()
	 * @see #getGroupType()
	 * @see #setGroupType(ImpConfigurableGroupType)
	 * @generated
	 */
	void unsetGroupType();

	/**
	 * Returns whether the value of the '{@link com.prostep.vcontrol.model.common.ImpConfigurableGroup#getGroupType <em>Group Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Group Type</em>' attribute is set.
	 * @see #unsetGroupType()
	 * @see #getGroupType()
	 * @see #setGroupType(ImpConfigurableGroupType)
	 * @generated
	 */
	boolean isSetGroupType();

} // ImpConfigurableGroup
