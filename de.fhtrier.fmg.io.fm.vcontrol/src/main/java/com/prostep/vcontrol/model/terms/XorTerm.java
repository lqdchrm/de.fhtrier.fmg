/**
 */
package com.prostep.vcontrol.model.terms;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Xor Term</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getXorTerm()
 * @model extendedMetaData="name='XorTerm' kind='elementOnly'"
 * @generated
 */
public interface XorTerm extends NaryTerm {
} // XorTerm
