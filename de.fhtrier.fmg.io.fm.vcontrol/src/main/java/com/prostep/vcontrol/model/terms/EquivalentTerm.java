/**
 */
package com.prostep.vcontrol.model.terms;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Equivalent Term</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getEquivalentTerm()
 * @model extendedMetaData="name='EquivalentTerm' kind='elementOnly'"
 * @generated
 */
public interface EquivalentTerm extends BinaryTerm {
} // EquivalentTerm
