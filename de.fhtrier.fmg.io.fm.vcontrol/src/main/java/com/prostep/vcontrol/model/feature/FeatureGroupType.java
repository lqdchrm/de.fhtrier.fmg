/**
 */
package com.prostep.vcontrol.model.feature;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Group Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeatureGroupType()
 * @model extendedMetaData="name='FeatureGroupType'"
 * @generated
 */
public final class FeatureGroupType extends AbstractEnumerator {
	/**
	 * The '<em><b>CARDINALITYGROUP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CARDINALITYGROUP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CARDINALITYGROUP_LITERAL
	 * @model literal="CARDINALITY_GROUP"
	 * @generated
	 * @ordered
	 */
	public static final int CARDINALITYGROUP = 0;

	/**
	 * The '<em><b>ORGROUP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ORGROUP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ORGROUP_LITERAL
	 * @model literal="OR_GROUP"
	 * @generated
	 * @ordered
	 */
	public static final int ORGROUP = 1;

	/**
	 * The '<em><b>XORGROUP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>XORGROUP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #XORGROUP_LITERAL
	 * @model literal="XOR_GROUP"
	 * @generated
	 * @ordered
	 */
	public static final int XORGROUP = 2;

	/**
	 * The '<em><b>CARDINALITYGROUP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CARDINALITYGROUP
	 * @generated
	 * @ordered
	 */
	public static final FeatureGroupType CARDINALITYGROUP_LITERAL = new FeatureGroupType(CARDINALITYGROUP, "CARDINALITYGROUP", "CARDINALITY_GROUP");

	/**
	 * The '<em><b>ORGROUP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ORGROUP
	 * @generated
	 * @ordered
	 */
	public static final FeatureGroupType ORGROUP_LITERAL = new FeatureGroupType(ORGROUP, "ORGROUP", "OR_GROUP");

	/**
	 * The '<em><b>XORGROUP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #XORGROUP
	 * @generated
	 * @ordered
	 */
	public static final FeatureGroupType XORGROUP_LITERAL = new FeatureGroupType(XORGROUP, "XORGROUP", "XOR_GROUP");

	/**
	 * An array of all the '<em><b>Group Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final FeatureGroupType[] VALUES_ARRAY =
		new FeatureGroupType[] {
			CARDINALITYGROUP_LITERAL,
			ORGROUP_LITERAL,
			XORGROUP_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Group Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Group Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FeatureGroupType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FeatureGroupType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Group Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FeatureGroupType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FeatureGroupType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Group Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FeatureGroupType get(int value) {
		switch (value) {
			case CARDINALITYGROUP: return CARDINALITYGROUP_LITERAL;
			case ORGROUP: return ORGROUP_LITERAL;
			case XORGROUP: return XORGROUP_LITERAL;
		}
		return null;
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FeatureGroupType(int value, String name, String literal) {
		super(value, name, literal);
	}

} //FeatureGroupType
