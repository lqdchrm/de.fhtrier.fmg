/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.CommonPackage;
import com.prostep.vcontrol.model.common.ImpModel;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Imp Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpModelImpl#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpModelImpl#getProject <em>Project</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpModelImpl#getImpConfigurations <em>Imp Configurations</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpModelImpl#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpModelImpl#getImpConfigurations1 <em>Imp Configurations1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpModelImpl#getProject1 <em>Project1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ImpModelImpl extends ImpItemImpl implements ImpModel {
	/**
	 * The cached value of the '{@link #getGroup1() <em>Group1</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup1()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group1;

	/**
	 * The default value of the '{@link #getImpConfigurations1() <em>Imp Configurations1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpConfigurations1()
	 * @generated
	 * @ordered
	 */
	protected static final String IMP_CONFIGURATIONS1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImpConfigurations1() <em>Imp Configurations1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpConfigurations1()
	 * @generated
	 * @ordered
	 */
	protected String impConfigurations1 = IMP_CONFIGURATIONS1_EDEFAULT;

	/**
	 * The default value of the '{@link #getProject1() <em>Project1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProject1()
	 * @generated
	 * @ordered
	 */
	protected static final String PROJECT1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProject1() <em>Project1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProject1()
	 * @generated
	 * @ordered
	 */
	protected String project1 = PROJECT1_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImpModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CommonPackage.Literals.IMP_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup1() {
		if (group1 == null) {
			group1 = new BasicFeatureMap(this, CommonPackage.IMP_MODEL__GROUP1);
		}
		return group1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getProject() {
		return getGroup1().list(CommonPackage.Literals.IMP_MODEL__PROJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getImpConfigurations() {
		return getGroup1().list(CommonPackage.Literals.IMP_MODEL__IMP_CONFIGURATIONS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getConstraints() {
		return getGroup1().list(CommonPackage.Literals.IMP_MODEL__CONSTRAINTS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImpConfigurations1() {
		return impConfigurations1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpConfigurations1(String newImpConfigurations1) {
		String oldImpConfigurations1 = impConfigurations1;
		impConfigurations1 = newImpConfigurations1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS1, oldImpConfigurations1, impConfigurations1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProject1() {
		return project1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProject1(String newProject1) {
		String oldProject1 = project1;
		project1 = newProject1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_MODEL__PROJECT1, oldProject1, project1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CommonPackage.IMP_MODEL__GROUP1:
				return ((InternalEList)getGroup1()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_MODEL__PROJECT:
				return ((InternalEList)getProject()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS:
				return ((InternalEList)getImpConfigurations()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_MODEL__CONSTRAINTS:
				return ((InternalEList)getConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CommonPackage.IMP_MODEL__GROUP1:
				if (coreType) return getGroup1();
				return ((FeatureMap.Internal)getGroup1()).getWrapper();
			case CommonPackage.IMP_MODEL__PROJECT:
				return getProject();
			case CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS:
				return getImpConfigurations();
			case CommonPackage.IMP_MODEL__CONSTRAINTS:
				return getConstraints();
			case CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS1:
				return getImpConfigurations1();
			case CommonPackage.IMP_MODEL__PROJECT1:
				return getProject1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CommonPackage.IMP_MODEL__GROUP1:
				((FeatureMap.Internal)getGroup1()).set(newValue);
				return;
			case CommonPackage.IMP_MODEL__PROJECT:
				getProject().clear();
				getProject().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS:
				getImpConfigurations().clear();
				getImpConfigurations().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_MODEL__CONSTRAINTS:
				getConstraints().clear();
				getConstraints().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS1:
				setImpConfigurations1((String)newValue);
				return;
			case CommonPackage.IMP_MODEL__PROJECT1:
				setProject1((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_MODEL__GROUP1:
				getGroup1().clear();
				return;
			case CommonPackage.IMP_MODEL__PROJECT:
				getProject().clear();
				return;
			case CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS:
				getImpConfigurations().clear();
				return;
			case CommonPackage.IMP_MODEL__CONSTRAINTS:
				getConstraints().clear();
				return;
			case CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS1:
				setImpConfigurations1(IMP_CONFIGURATIONS1_EDEFAULT);
				return;
			case CommonPackage.IMP_MODEL__PROJECT1:
				setProject1(PROJECT1_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_MODEL__GROUP1:
				return group1 != null && !group1.isEmpty();
			case CommonPackage.IMP_MODEL__PROJECT:
				return !getProject().isEmpty();
			case CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS:
				return !getImpConfigurations().isEmpty();
			case CommonPackage.IMP_MODEL__CONSTRAINTS:
				return !getConstraints().isEmpty();
			case CommonPackage.IMP_MODEL__IMP_CONFIGURATIONS1:
				return IMP_CONFIGURATIONS1_EDEFAULT == null ? impConfigurations1 != null : !IMP_CONFIGURATIONS1_EDEFAULT.equals(impConfigurations1);
			case CommonPackage.IMP_MODEL__PROJECT1:
				return PROJECT1_EDEFAULT == null ? project1 != null : !PROJECT1_EDEFAULT.equals(project1);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group1: ");
		result.append(group1);
		result.append(", impConfigurations1: ");
		result.append(impConfigurations1);
		result.append(", project1: ");
		result.append(project1);
		result.append(')');
		return result.toString();
	}

} //ImpModelImpl
