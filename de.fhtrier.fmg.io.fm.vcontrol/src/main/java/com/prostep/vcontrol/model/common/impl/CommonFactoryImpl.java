/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CommonFactoryImpl extends EFactoryImpl implements CommonFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CommonFactory init() {
		try {
			CommonFactory theCommonFactory = (CommonFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.prostep.com/vcontrol/model/common.ecore"); 
			if (theCommonFactory != null) {
				return theCommonFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CommonFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommonFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CommonPackage.CONSTRAINT: return createConstraint();
			case CommonPackage.IDENTIFIABLE: return createIdentifiable();
			case CommonPackage.IMP_CONFIGURATION: return createImpConfiguration();
			case CommonPackage.IMP_CONFIGURED_ELEMENT: return createImpConfiguredElement();
			case CommonPackage.IMP_LINKING_RELATION: return createImpLinkingRelation();
			case CommonPackage.IMP_PROJECT: return createImpProject();
			case CommonPackage.PROPERTY: return createProperty();
			case CommonPackage.DOCUMENT_ROOT: return createDocumentRoot();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case CommonPackage.IMP_CONFIGURABLE_GROUP_TYPE:
				return createImpConfigurableGroupTypeFromString(eDataType, initialValue);
			case CommonPackage.LINKING_TYPE:
				return createLinkingTypeFromString(eDataType, initialValue);
			case CommonPackage.PORT_DIRECTION:
				return createPortDirectionFromString(eDataType, initialValue);
			case CommonPackage.SELECTION_KIND:
				return createSelectionKindFromString(eDataType, initialValue);
			case CommonPackage.IMP_CONFIGURABLE_GROUP_TYPE_OBJECT:
				return createImpConfigurableGroupTypeObjectFromString(eDataType, initialValue);
			case CommonPackage.LINKING_TYPE_OBJECT:
				return createLinkingTypeObjectFromString(eDataType, initialValue);
			case CommonPackage.PORT_DIRECTION_OBJECT:
				return createPortDirectionObjectFromString(eDataType, initialValue);
			case CommonPackage.SELECTION_KIND_OBJECT:
				return createSelectionKindObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case CommonPackage.IMP_CONFIGURABLE_GROUP_TYPE:
				return convertImpConfigurableGroupTypeToString(eDataType, instanceValue);
			case CommonPackage.LINKING_TYPE:
				return convertLinkingTypeToString(eDataType, instanceValue);
			case CommonPackage.PORT_DIRECTION:
				return convertPortDirectionToString(eDataType, instanceValue);
			case CommonPackage.SELECTION_KIND:
				return convertSelectionKindToString(eDataType, instanceValue);
			case CommonPackage.IMP_CONFIGURABLE_GROUP_TYPE_OBJECT:
				return convertImpConfigurableGroupTypeObjectToString(eDataType, instanceValue);
			case CommonPackage.LINKING_TYPE_OBJECT:
				return convertLinkingTypeObjectToString(eDataType, instanceValue);
			case CommonPackage.PORT_DIRECTION_OBJECT:
				return convertPortDirectionObjectToString(eDataType, instanceValue);
			case CommonPackage.SELECTION_KIND_OBJECT:
				return convertSelectionKindObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint createConstraint() {
		ConstraintImpl constraint = new ConstraintImpl();
		return constraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Identifiable createIdentifiable() {
		IdentifiableImpl identifiable = new IdentifiableImpl();
		return identifiable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpConfiguration createImpConfiguration() {
		ImpConfigurationImpl impConfiguration = new ImpConfigurationImpl();
		return impConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpConfiguredElement createImpConfiguredElement() {
		ImpConfiguredElementImpl impConfiguredElement = new ImpConfiguredElementImpl();
		return impConfiguredElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpLinkingRelation createImpLinkingRelation() {
		ImpLinkingRelationImpl impLinkingRelation = new ImpLinkingRelationImpl();
		return impLinkingRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpProject createImpProject() {
		ImpProjectImpl impProject = new ImpProjectImpl();
		return impProject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property createProperty() {
		PropertyImpl property = new PropertyImpl();
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpConfigurableGroupType createImpConfigurableGroupTypeFromString(EDataType eDataType, String initialValue) {
		ImpConfigurableGroupType result = ImpConfigurableGroupType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertImpConfigurableGroupTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkingType createLinkingTypeFromString(EDataType eDataType, String initialValue) {
		LinkingType result = LinkingType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLinkingTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDirection createPortDirectionFromString(EDataType eDataType, String initialValue) {
		PortDirection result = PortDirection.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPortDirectionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectionKind createSelectionKindFromString(EDataType eDataType, String initialValue) {
		SelectionKind result = SelectionKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSelectionKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImpConfigurableGroupType createImpConfigurableGroupTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createImpConfigurableGroupTypeFromString(CommonPackage.Literals.IMP_CONFIGURABLE_GROUP_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertImpConfigurableGroupTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertImpConfigurableGroupTypeToString(CommonPackage.Literals.IMP_CONFIGURABLE_GROUP_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkingType createLinkingTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createLinkingTypeFromString(CommonPackage.Literals.LINKING_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLinkingTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertLinkingTypeToString(CommonPackage.Literals.LINKING_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDirection createPortDirectionObjectFromString(EDataType eDataType, String initialValue) {
		return createPortDirectionFromString(CommonPackage.Literals.PORT_DIRECTION, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPortDirectionObjectToString(EDataType eDataType, Object instanceValue) {
		return convertPortDirectionToString(CommonPackage.Literals.PORT_DIRECTION, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectionKind createSelectionKindObjectFromString(EDataType eDataType, String initialValue) {
		return createSelectionKindFromString(CommonPackage.Literals.SELECTION_KIND, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSelectionKindObjectToString(EDataType eDataType, Object instanceValue) {
		return convertSelectionKindToString(CommonPackage.Literals.SELECTION_KIND, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommonPackage getCommonPackage() {
		return (CommonPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static CommonPackage getPackage() {
		return CommonPackage.eINSTANCE;
	}

} //CommonFactoryImpl
