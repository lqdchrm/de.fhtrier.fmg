/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.CommonPackage;
import com.prostep.vcontrol.model.common.ImpConfiguredElement;
import com.prostep.vcontrol.model.common.SelectionKind;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Imp Configured Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getImpConfigurableElement <em>Imp Configurable Element</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getImpConfiguration <em>Imp Configuration</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getConfigurationForHook <em>Configuration For Hook</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getExtension <em>Extension</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getHref <em>Href</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getId <em>Id</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getIdref <em>Idref</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getImpConfigurableElement1 <em>Imp Configurable Element1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getImpConfiguration1 <em>Imp Configuration1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getSelectionInformation <em>Selection Information</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getType <em>Type</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getUuid <em>Uuid</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl#getVersion <em>Version</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ImpConfiguredElementImpl extends EObjectImpl implements ImpConfiguredElement {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The default value of the '{@link #getHref() <em>Href</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHref()
	 * @generated
	 * @ordered
	 */
	protected static final String HREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHref() <em>Href</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHref()
	 * @generated
	 * @ordered
	 */
	protected String href = HREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getIdref() <em>Idref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdref()
	 * @generated
	 * @ordered
	 */
	protected static final String IDREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIdref() <em>Idref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdref()
	 * @generated
	 * @ordered
	 */
	protected String idref = IDREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getImpConfigurableElement1() <em>Imp Configurable Element1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpConfigurableElement1()
	 * @generated
	 * @ordered
	 */
	protected static final String IMP_CONFIGURABLE_ELEMENT1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImpConfigurableElement1() <em>Imp Configurable Element1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpConfigurableElement1()
	 * @generated
	 * @ordered
	 */
	protected String impConfigurableElement1 = IMP_CONFIGURABLE_ELEMENT1_EDEFAULT;

	/**
	 * The default value of the '{@link #getImpConfiguration1() <em>Imp Configuration1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpConfiguration1()
	 * @generated
	 * @ordered
	 */
	protected static final String IMP_CONFIGURATION1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImpConfiguration1() <em>Imp Configuration1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpConfiguration1()
	 * @generated
	 * @ordered
	 */
	protected String impConfiguration1 = IMP_CONFIGURATION1_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getSelectionInformation() <em>Selection Information</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectionInformation()
	 * @generated
	 * @ordered
	 */
	protected static final SelectionKind SELECTION_INFORMATION_EDEFAULT = SelectionKind.NOTYETDECIDED_LITERAL;

	/**
	 * The cached value of the '{@link #getSelectionInformation() <em>Selection Information</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectionInformation()
	 * @generated
	 * @ordered
	 */
	protected SelectionKind selectionInformation = SELECTION_INFORMATION_EDEFAULT;

	/**
	 * This is true if the Selection Information attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean selectionInformationESet;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final Object TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Object type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUuid()
	 * @generated
	 * @ordered
	 */
	protected static final String UUID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUuid()
	 * @generated
	 * @ordered
	 */
	protected String uuid = UUID_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = "2.0";

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * This is true if the Version attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean versionESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImpConfiguredElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CommonPackage.Literals.IMP_CONFIGURED_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, CommonPackage.IMP_CONFIGURED_ELEMENT__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getImpConfigurableElement() {
		return getGroup().list(CommonPackage.Literals.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getImpConfiguration() {
		return getGroup().list(CommonPackage.Literals.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getConfigurationForHook() {
		return getGroup().list(CommonPackage.Literals.IMP_CONFIGURED_ELEMENT__CONFIGURATION_FOR_HOOK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getExtension() {
		return getGroup().list(CommonPackage.Literals.IMP_CONFIGURED_ELEMENT__EXTENSION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHref() {
		return href;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHref(String newHref) {
		String oldHref = href;
		href = newHref;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURED_ELEMENT__HREF, oldHref, href));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURED_ELEMENT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIdref() {
		return idref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdref(String newIdref) {
		String oldIdref = idref;
		idref = newIdref;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURED_ELEMENT__IDREF, oldIdref, idref));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImpConfigurableElement1() {
		return impConfigurableElement1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpConfigurableElement1(String newImpConfigurableElement1) {
		String oldImpConfigurableElement1 = impConfigurableElement1;
		impConfigurableElement1 = newImpConfigurableElement1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT1, oldImpConfigurableElement1, impConfigurableElement1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImpConfiguration1() {
		return impConfiguration1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpConfiguration1(String newImpConfiguration1) {
		String oldImpConfiguration1 = impConfiguration1;
		impConfiguration1 = newImpConfiguration1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION1, oldImpConfiguration1, impConfiguration1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURED_ELEMENT__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectionKind getSelectionInformation() {
		return selectionInformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectionInformation(SelectionKind newSelectionInformation) {
		SelectionKind oldSelectionInformation = selectionInformation;
		selectionInformation = newSelectionInformation == null ? SELECTION_INFORMATION_EDEFAULT : newSelectionInformation;
		boolean oldSelectionInformationESet = selectionInformationESet;
		selectionInformationESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURED_ELEMENT__SELECTION_INFORMATION, oldSelectionInformation, selectionInformation, !oldSelectionInformationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSelectionInformation() {
		SelectionKind oldSelectionInformation = selectionInformation;
		boolean oldSelectionInformationESet = selectionInformationESet;
		selectionInformation = SELECTION_INFORMATION_EDEFAULT;
		selectionInformationESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.IMP_CONFIGURED_ELEMENT__SELECTION_INFORMATION, oldSelectionInformation, SELECTION_INFORMATION_EDEFAULT, oldSelectionInformationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSelectionInformation() {
		return selectionInformationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Object newType) {
		Object oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURED_ELEMENT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUuid(String newUuid) {
		String oldUuid = uuid;
		uuid = newUuid;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURED_ELEMENT__UUID, oldUuid, uuid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		boolean oldVersionESet = versionESet;
		versionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURED_ELEMENT__VERSION, oldVersion, version, !oldVersionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetVersion() {
		String oldVersion = version;
		boolean oldVersionESet = versionESet;
		version = VERSION_EDEFAULT;
		versionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, CommonPackage.IMP_CONFIGURED_ELEMENT__VERSION, oldVersion, VERSION_EDEFAULT, oldVersionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetVersion() {
		return versionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURED_ELEMENT__GROUP:
				return ((InternalEList)getGroup()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT:
				return ((InternalEList)getImpConfigurableElement()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION:
				return ((InternalEList)getImpConfiguration()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__CONFIGURATION_FOR_HOOK:
				return ((InternalEList)getConfigurationForHook()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__EXTENSION:
				return ((InternalEList)getExtension()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURED_ELEMENT__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT:
				return getImpConfigurableElement();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION:
				return getImpConfiguration();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__CONFIGURATION_FOR_HOOK:
				return getConfigurationForHook();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__EXTENSION:
				return getExtension();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__HREF:
				return getHref();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__ID:
				return getId();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IDREF:
				return getIdref();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT1:
				return getImpConfigurableElement1();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION1:
				return getImpConfiguration1();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__LABEL:
				return getLabel();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__SELECTION_INFORMATION:
				return getSelectionInformation();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__TYPE:
				return getType();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__UUID:
				return getUuid();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__VERSION:
				return getVersion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURED_ELEMENT__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT:
				getImpConfigurableElement().clear();
				getImpConfigurableElement().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION:
				getImpConfiguration().clear();
				getImpConfiguration().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__CONFIGURATION_FOR_HOOK:
				getConfigurationForHook().clear();
				getConfigurationForHook().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__EXTENSION:
				getExtension().clear();
				getExtension().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__HREF:
				setHref((String)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__ID:
				setId((String)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IDREF:
				setIdref((String)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT1:
				setImpConfigurableElement1((String)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION1:
				setImpConfiguration1((String)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__LABEL:
				setLabel((String)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__SELECTION_INFORMATION:
				setSelectionInformation((SelectionKind)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__TYPE:
				setType(newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__UUID:
				setUuid((String)newValue);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__VERSION:
				setVersion((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURED_ELEMENT__GROUP:
				getGroup().clear();
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT:
				getImpConfigurableElement().clear();
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION:
				getImpConfiguration().clear();
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__CONFIGURATION_FOR_HOOK:
				getConfigurationForHook().clear();
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__EXTENSION:
				getExtension().clear();
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__HREF:
				setHref(HREF_EDEFAULT);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__ID:
				setId(ID_EDEFAULT);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IDREF:
				setIdref(IDREF_EDEFAULT);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT1:
				setImpConfigurableElement1(IMP_CONFIGURABLE_ELEMENT1_EDEFAULT);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION1:
				setImpConfiguration1(IMP_CONFIGURATION1_EDEFAULT);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__SELECTION_INFORMATION:
				unsetSelectionInformation();
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__UUID:
				setUuid(UUID_EDEFAULT);
				return;
			case CommonPackage.IMP_CONFIGURED_ELEMENT__VERSION:
				unsetVersion();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURED_ELEMENT__GROUP:
				return group != null && !group.isEmpty();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT:
				return !getImpConfigurableElement().isEmpty();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION:
				return !getImpConfiguration().isEmpty();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__CONFIGURATION_FOR_HOOK:
				return !getConfigurationForHook().isEmpty();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__EXTENSION:
				return !getExtension().isEmpty();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__HREF:
				return HREF_EDEFAULT == null ? href != null : !HREF_EDEFAULT.equals(href);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IDREF:
				return IDREF_EDEFAULT == null ? idref != null : !IDREF_EDEFAULT.equals(idref);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT1:
				return IMP_CONFIGURABLE_ELEMENT1_EDEFAULT == null ? impConfigurableElement1 != null : !IMP_CONFIGURABLE_ELEMENT1_EDEFAULT.equals(impConfigurableElement1);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION1:
				return IMP_CONFIGURATION1_EDEFAULT == null ? impConfiguration1 != null : !IMP_CONFIGURATION1_EDEFAULT.equals(impConfiguration1);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__SELECTION_INFORMATION:
				return isSetSelectionInformation();
			case CommonPackage.IMP_CONFIGURED_ELEMENT__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__UUID:
				return UUID_EDEFAULT == null ? uuid != null : !UUID_EDEFAULT.equals(uuid);
			case CommonPackage.IMP_CONFIGURED_ELEMENT__VERSION:
				return isSetVersion();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", href: ");
		result.append(href);
		result.append(", id: ");
		result.append(id);
		result.append(", idref: ");
		result.append(idref);
		result.append(", impConfigurableElement1: ");
		result.append(impConfigurableElement1);
		result.append(", impConfiguration1: ");
		result.append(impConfiguration1);
		result.append(", label: ");
		result.append(label);
		result.append(", selectionInformation: ");
		if (selectionInformationESet) result.append(selectionInformation); else result.append("<unset>");
		result.append(", type: ");
		result.append(type);
		result.append(", uuid: ");
		result.append(uuid);
		result.append(", version: ");
		if (versionESet) result.append(version); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //ImpConfiguredElementImpl
