/**
 */
package com.prostep.vcontrol.model.common.impl;

import com.prostep.vcontrol.model.common.CommonPackage;
import com.prostep.vcontrol.model.common.ImpConfiguration;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Imp Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurationImpl#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurationImpl#getImpConfiguredElements <em>Imp Configured Elements</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurationImpl#getModels <em>Models</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurationImpl#getProject <em>Project</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurationImpl#getModels1 <em>Models1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.impl.ImpConfigurationImpl#getProject1 <em>Project1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ImpConfigurationImpl extends ImpItemImpl implements ImpConfiguration {
	/**
	 * The cached value of the '{@link #getGroup1() <em>Group1</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup1()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group1;

	/**
	 * The default value of the '{@link #getModels1() <em>Models1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModels1()
	 * @generated
	 * @ordered
	 */
	protected static final String MODELS1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModels1() <em>Models1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModels1()
	 * @generated
	 * @ordered
	 */
	protected String models1 = MODELS1_EDEFAULT;

	/**
	 * The default value of the '{@link #getProject1() <em>Project1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProject1()
	 * @generated
	 * @ordered
	 */
	protected static final String PROJECT1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProject1() <em>Project1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProject1()
	 * @generated
	 * @ordered
	 */
	protected String project1 = PROJECT1_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImpConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return CommonPackage.Literals.IMP_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup1() {
		if (group1 == null) {
			group1 = new BasicFeatureMap(this, CommonPackage.IMP_CONFIGURATION__GROUP1);
		}
		return group1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getImpConfiguredElements() {
		return getGroup1().list(CommonPackage.Literals.IMP_CONFIGURATION__IMP_CONFIGURED_ELEMENTS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getModels() {
		return getGroup1().list(CommonPackage.Literals.IMP_CONFIGURATION__MODELS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getProject() {
		return getGroup1().list(CommonPackage.Literals.IMP_CONFIGURATION__PROJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModels1() {
		return models1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModels1(String newModels1) {
		String oldModels1 = models1;
		models1 = newModels1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURATION__MODELS1, oldModels1, models1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProject1() {
		return project1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProject1(String newProject1) {
		String oldProject1 = project1;
		project1 = newProject1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CommonPackage.IMP_CONFIGURATION__PROJECT1, oldProject1, project1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURATION__GROUP1:
				return ((InternalEList)getGroup1()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONFIGURATION__IMP_CONFIGURED_ELEMENTS:
				return ((InternalEList)getImpConfiguredElements()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONFIGURATION__MODELS:
				return ((InternalEList)getModels()).basicRemove(otherEnd, msgs);
			case CommonPackage.IMP_CONFIGURATION__PROJECT:
				return ((InternalEList)getProject()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURATION__GROUP1:
				if (coreType) return getGroup1();
				return ((FeatureMap.Internal)getGroup1()).getWrapper();
			case CommonPackage.IMP_CONFIGURATION__IMP_CONFIGURED_ELEMENTS:
				return getImpConfiguredElements();
			case CommonPackage.IMP_CONFIGURATION__MODELS:
				return getModels();
			case CommonPackage.IMP_CONFIGURATION__PROJECT:
				return getProject();
			case CommonPackage.IMP_CONFIGURATION__MODELS1:
				return getModels1();
			case CommonPackage.IMP_CONFIGURATION__PROJECT1:
				return getProject1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURATION__GROUP1:
				((FeatureMap.Internal)getGroup1()).set(newValue);
				return;
			case CommonPackage.IMP_CONFIGURATION__IMP_CONFIGURED_ELEMENTS:
				getImpConfiguredElements().clear();
				getImpConfiguredElements().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONFIGURATION__MODELS:
				getModels().clear();
				getModels().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONFIGURATION__PROJECT:
				getProject().clear();
				getProject().addAll((Collection)newValue);
				return;
			case CommonPackage.IMP_CONFIGURATION__MODELS1:
				setModels1((String)newValue);
				return;
			case CommonPackage.IMP_CONFIGURATION__PROJECT1:
				setProject1((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURATION__GROUP1:
				getGroup1().clear();
				return;
			case CommonPackage.IMP_CONFIGURATION__IMP_CONFIGURED_ELEMENTS:
				getImpConfiguredElements().clear();
				return;
			case CommonPackage.IMP_CONFIGURATION__MODELS:
				getModels().clear();
				return;
			case CommonPackage.IMP_CONFIGURATION__PROJECT:
				getProject().clear();
				return;
			case CommonPackage.IMP_CONFIGURATION__MODELS1:
				setModels1(MODELS1_EDEFAULT);
				return;
			case CommonPackage.IMP_CONFIGURATION__PROJECT1:
				setProject1(PROJECT1_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CommonPackage.IMP_CONFIGURATION__GROUP1:
				return group1 != null && !group1.isEmpty();
			case CommonPackage.IMP_CONFIGURATION__IMP_CONFIGURED_ELEMENTS:
				return !getImpConfiguredElements().isEmpty();
			case CommonPackage.IMP_CONFIGURATION__MODELS:
				return !getModels().isEmpty();
			case CommonPackage.IMP_CONFIGURATION__PROJECT:
				return !getProject().isEmpty();
			case CommonPackage.IMP_CONFIGURATION__MODELS1:
				return MODELS1_EDEFAULT == null ? models1 != null : !MODELS1_EDEFAULT.equals(models1);
			case CommonPackage.IMP_CONFIGURATION__PROJECT1:
				return PROJECT1_EDEFAULT == null ? project1 != null : !PROJECT1_EDEFAULT.equals(project1);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group1: ");
		result.append(group1);
		result.append(", models1: ");
		result.append(models1);
		result.append(", project1: ");
		result.append(project1);
		result.append(')');
		return result.toString();
	}

} //ImpConfigurationImpl
