/**
 */
package com.prostep.vcontrol.model.terms;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Math Term</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getAbstractMathTerm()
 * @model abstract="true"
 *        extendedMetaData="name='AbstractMathTerm' kind='elementOnly'"
 * @generated
 */
public interface AbstractMathTerm extends AtomicTerm {
} // AbstractMathTerm
