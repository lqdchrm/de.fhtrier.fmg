/**
 */
package com.prostep.vcontrol.model.terms;

import com.prostep.vcontrol.model.common.ImpTerm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.terms.UnaryTerm#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.terms.UnaryTerm#getOperand <em>Operand</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getUnaryTerm()
 * @model abstract="true"
 *        extendedMetaData="name='UnaryTerm' kind='elementOnly'"
 * @generated
 */
public interface UnaryTerm extends ImpTerm {
	/**
	 * Returns the value of the '<em><b>Group1</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group1</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group1</em>' attribute list.
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getUnaryTerm_Group1()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:9'"
	 * @generated
	 */
	FeatureMap getGroup1();

	/**
	 * Returns the value of the '<em><b>Operand</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpTerm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operand</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operand</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.terms.TermsPackage#getUnaryTerm_Operand()
	 * @model type="com.prostep.vcontrol.model.common.ImpTerm" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='operand' group='#group:9'"
	 * @generated
	 */
	EList getOperand();

} // UnaryTerm
