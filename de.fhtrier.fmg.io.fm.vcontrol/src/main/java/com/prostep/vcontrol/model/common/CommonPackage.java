/**
 */
package com.prostep.vcontrol.model.common;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.common.CommonFactory
 * @model kind="package"
 * @generated
 */
public interface CommonPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "common";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.prostep.com/vcontrol/model/common.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "com.prostep.vcontrol.model.common";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CommonPackage eINSTANCE = com.prostep.vcontrol.model.common.impl.CommonPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.PropertySetImpl <em>Property Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.PropertySetImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getPropertySet()
	 * @generated
	 */
	int PROPERTY_SET = 19;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET__PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET__EXTENSION = 2;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET__HREF = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET__ID = 4;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET__IDREF = 5;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET__LABEL = 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET__TYPE = 7;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET__UUID = 8;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET__VERSION = 9;

	/**
	 * The number of structural features of the '<em>Property Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_SET_FEATURE_COUNT = 10;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpItemImpl <em>Imp Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpItemImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpItem()
	 * @generated
	 */
	int IMP_ITEM = 10;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__GROUP = PROPERTY_SET__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__PROPERTIES = PROPERTY_SET__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__EXTENSION = PROPERTY_SET__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__HREF = PROPERTY_SET__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__ID = PROPERTY_SET__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__IDREF = PROPERTY_SET__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__LABEL = PROPERTY_SET__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__TYPE = PROPERTY_SET__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__UUID = PROPERTY_SET__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__VERSION = PROPERTY_SET__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__DESCRIPTION = PROPERTY_SET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__ID1 = PROPERTY_SET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM__NAME = PROPERTY_SET_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Imp Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_ITEM_FEATURE_COUNT = PROPERTY_SET_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ConstraintImpl <em>Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ConstraintImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getConstraint()
	 * @generated
	 */
	int CONSTRAINT = 0;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__GROUP = IMP_ITEM__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__PROPERTIES = IMP_ITEM__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__EXTENSION = IMP_ITEM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__HREF = IMP_ITEM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__ID = IMP_ITEM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__IDREF = IMP_ITEM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__LABEL = IMP_ITEM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__TYPE = IMP_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__UUID = IMP_ITEM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__VERSION = IMP_ITEM__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__DESCRIPTION = IMP_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__ID1 = IMP_ITEM__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__NAME = IMP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__GROUP1 = IMP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Root Term</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__ROOT_TERM = IMP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Model</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__MODEL = IMP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Ignore</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__IGNORE = IMP_ITEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Model1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__MODEL1 = IMP_ITEM_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_FEATURE_COUNT = IMP_ITEM_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.DescribedItemImpl <em>Described Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.DescribedItemImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getDescribedItem()
	 * @generated
	 */
	int DESCRIBED_ITEM = 1;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ITEM__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ITEM__EXTENSION = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ITEM__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ITEM__HREF = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ITEM__ID = 4;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ITEM__IDREF = 5;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ITEM__LABEL = 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ITEM__TYPE = 7;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ITEM__UUID = 8;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ITEM__VERSION = 9;

	/**
	 * The number of structural features of the '<em>Described Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ITEM_FEATURE_COUNT = 10;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.IdentifiableImpl <em>Identifiable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.IdentifiableImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getIdentifiable()
	 * @generated
	 */
	int IDENTIFIABLE = 2;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__EXTENSION = 1;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__HREF = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__ID = 3;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__ID1 = 4;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__IDREF = 5;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__LABEL = 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__TYPE = 7;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__UUID = 8;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__VERSION = 9;

	/**
	 * The number of structural features of the '<em>Identifiable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE_FEATURE_COUNT = 10;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl <em>Imp Configurable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfigurableElement()
	 * @generated
	 */
	int IMP_CONFIGURABLE_ELEMENT = 3;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__GROUP = IMP_ITEM__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__PROPERTIES = IMP_ITEM__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__EXTENSION = IMP_ITEM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__HREF = IMP_ITEM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__ID = IMP_ITEM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__IDREF = IMP_ITEM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__LABEL = IMP_ITEM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__TYPE = IMP_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__UUID = IMP_ITEM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__VERSION = IMP_ITEM__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__DESCRIPTION = IMP_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__ID1 = IMP_ITEM__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__NAME = IMP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__GROUP1 = IMP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS = IMP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS = IMP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Left Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS = IMP_ITEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Extensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__EXTENSIONS = IMP_ITEM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS1 = IMP_ITEM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__OPTIONAL = IMP_ITEM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Right Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS1 = IMP_ITEM_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Imp Configurable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT_FEATURE_COUNT = IMP_ITEM_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementExtensionImpl <em>Imp Configurable Element Extension</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpConfigurableElementExtensionImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfigurableElementExtension()
	 * @generated
	 */
	int IMP_CONFIGURABLE_ELEMENT_EXTENSION = 4;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT_EXTENSION__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT_EXTENSION__EXTENSION = 1;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT_EXTENSION__HREF = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT_EXTENSION__ID = 3;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT_EXTENSION__IDREF = 4;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT_EXTENSION__LABEL = 5;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT_EXTENSION__TYPE = 6;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT_EXTENSION__UUID = 7;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT_EXTENSION__VERSION = 8;

	/**
	 * The number of structural features of the '<em>Imp Configurable Element Extension</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_ELEMENT_EXTENSION_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableGroupImpl <em>Imp Configurable Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpConfigurableGroupImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfigurableGroup()
	 * @generated
	 */
	int IMP_CONFIGURABLE_GROUP = 5;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__GROUP = IMP_CONFIGURABLE_ELEMENT__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__PROPERTIES = IMP_CONFIGURABLE_ELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__EXTENSION = IMP_CONFIGURABLE_ELEMENT__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__HREF = IMP_CONFIGURABLE_ELEMENT__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__ID = IMP_CONFIGURABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__IDREF = IMP_CONFIGURABLE_ELEMENT__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__LABEL = IMP_CONFIGURABLE_ELEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__TYPE = IMP_CONFIGURABLE_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__UUID = IMP_CONFIGURABLE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__VERSION = IMP_CONFIGURABLE_ELEMENT__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__DESCRIPTION = IMP_CONFIGURABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__ID1 = IMP_CONFIGURABLE_ELEMENT__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__NAME = IMP_CONFIGURABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__GROUP1 = IMP_CONFIGURABLE_ELEMENT__GROUP1;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__IMP_CONFIGURED_ELEMENTS = IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Right Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__RIGHT_ELEMENTS = IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Left Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__LEFT_ELEMENTS = IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Extensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__EXTENSIONS = IMP_CONFIGURABLE_ELEMENT__EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__IMP_CONFIGURED_ELEMENTS1 = IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS1;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__OPTIONAL = IMP_CONFIGURABLE_ELEMENT__OPTIONAL;

	/**
	 * The feature id for the '<em><b>Right Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__RIGHT_ELEMENTS1 = IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS1;

	/**
	 * The feature id for the '<em><b>Group Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP__GROUP_TYPE = IMP_CONFIGURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Imp Configurable Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURABLE_GROUP_FEATURE_COUNT = IMP_CONFIGURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpConfigurationImpl <em>Imp Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpConfigurationImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfiguration()
	 * @generated
	 */
	int IMP_CONFIGURATION = 6;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__GROUP = IMP_ITEM__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__PROPERTIES = IMP_ITEM__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__EXTENSION = IMP_ITEM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__HREF = IMP_ITEM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__ID = IMP_ITEM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__IDREF = IMP_ITEM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__LABEL = IMP_ITEM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__TYPE = IMP_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__UUID = IMP_ITEM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__VERSION = IMP_ITEM__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__DESCRIPTION = IMP_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__ID1 = IMP_ITEM__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__NAME = IMP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__GROUP1 = IMP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__IMP_CONFIGURED_ELEMENTS = IMP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__MODELS = IMP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Project</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__PROJECT = IMP_ITEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Models1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__MODELS1 = IMP_ITEM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Project1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION__PROJECT1 = IMP_ITEM_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Imp Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURATION_FEATURE_COUNT = IMP_ITEM_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl <em>Imp Configured Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfiguredElement()
	 * @generated
	 */
	int IMP_CONFIGURED_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Imp Configurable Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Imp Configuration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION = 2;

	/**
	 * The feature id for the '<em><b>Configuration For Hook</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__CONFIGURATION_FOR_HOOK = 3;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__EXTENSION = 4;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__HREF = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__ID = 6;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__IDREF = 7;

	/**
	 * The feature id for the '<em><b>Imp Configurable Element1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT1 = 8;

	/**
	 * The feature id for the '<em><b>Imp Configuration1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION1 = 9;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__LABEL = 10;

	/**
	 * The feature id for the '<em><b>Selection Information</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__SELECTION_INFORMATION = 11;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__TYPE = 12;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__UUID = 13;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT__VERSION = 14;

	/**
	 * The number of structural features of the '<em>Imp Configured Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONFIGURED_ELEMENT_FEATURE_COUNT = 15;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpConnectorImpl <em>Imp Connector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpConnectorImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConnector()
	 * @generated
	 */
	int IMP_CONNECTOR = 8;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__GROUP = IMP_ITEM__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__PROPERTIES = IMP_ITEM__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__EXTENSION = IMP_ITEM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__HREF = IMP_ITEM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__ID = IMP_ITEM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__IDREF = IMP_ITEM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__LABEL = IMP_ITEM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__TYPE = IMP_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__UUID = IMP_ITEM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__VERSION = IMP_ITEM__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__DESCRIPTION = IMP_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__ID1 = IMP_ITEM__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__NAME = IMP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__GROUP1 = IMP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Port</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__SOURCE_PORT = IMP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target Port</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__TARGET_PORT = IMP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Source Port1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__SOURCE_PORT1 = IMP_ITEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Target Port1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR__TARGET_PORT1 = IMP_ITEM_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Imp Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_CONNECTOR_FEATURE_COUNT = IMP_ITEM_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpExtensibleElementImpl <em>Imp Extensible Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpExtensibleElementImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpExtensibleElement()
	 * @generated
	 */
	int IMP_EXTENSIBLE_ELEMENT = 9;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_EXTENSIBLE_ELEMENT__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_EXTENSIBLE_ELEMENT__EXTENSION = 1;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_EXTENSIBLE_ELEMENT__HREF = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_EXTENSIBLE_ELEMENT__ID = 3;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_EXTENSIBLE_ELEMENT__IDREF = 4;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_EXTENSIBLE_ELEMENT__LABEL = 5;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_EXTENSIBLE_ELEMENT__TYPE = 6;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_EXTENSIBLE_ELEMENT__UUID = 7;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_EXTENSIBLE_ELEMENT__VERSION = 8;

	/**
	 * The number of structural features of the '<em>Imp Extensible Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_EXTENSIBLE_ELEMENT_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl <em>Imp Linking Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpLinkingRelation()
	 * @generated
	 */
	int IMP_LINKING_RELATION = 11;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Right Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__RIGHT_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Left Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__LEFT_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__EXTENSION = 3;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__HREF = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__ID = 5;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__IDREF = 6;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__LABEL = 7;

	/**
	 * The feature id for the '<em><b>Left Element1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__LEFT_ELEMENT1 = 8;

	/**
	 * The feature id for the '<em><b>Linking Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__LINKING_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Right Element1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__RIGHT_ELEMENT1 = 10;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__TYPE = 11;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__UUID = 12;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION__VERSION = 13;

	/**
	 * The number of structural features of the '<em>Imp Linking Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_LINKING_RELATION_FEATURE_COUNT = 14;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpModelImpl <em>Imp Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpModelImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpModel()
	 * @generated
	 */
	int IMP_MODEL = 12;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__GROUP = IMP_ITEM__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__PROPERTIES = IMP_ITEM__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__EXTENSION = IMP_ITEM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__HREF = IMP_ITEM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__ID = IMP_ITEM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__IDREF = IMP_ITEM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__LABEL = IMP_ITEM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__TYPE = IMP_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__UUID = IMP_ITEM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__VERSION = IMP_ITEM__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__DESCRIPTION = IMP_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__ID1 = IMP_ITEM__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__NAME = IMP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__GROUP1 = IMP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Project</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__PROJECT = IMP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Imp Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__IMP_CONFIGURATIONS = IMP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__CONSTRAINTS = IMP_ITEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Imp Configurations1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__IMP_CONFIGURATIONS1 = IMP_ITEM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Project1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL__PROJECT1 = IMP_ITEM_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Imp Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_MODEL_FEATURE_COUNT = IMP_ITEM_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpNodeImpl <em>Imp Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpNodeImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpNode()
	 * @generated
	 */
	int IMP_NODE = 13;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__GROUP = IMP_CONFIGURABLE_ELEMENT__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__PROPERTIES = IMP_CONFIGURABLE_ELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__EXTENSION = IMP_CONFIGURABLE_ELEMENT__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__HREF = IMP_CONFIGURABLE_ELEMENT__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__ID = IMP_CONFIGURABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__IDREF = IMP_CONFIGURABLE_ELEMENT__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__LABEL = IMP_CONFIGURABLE_ELEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__TYPE = IMP_CONFIGURABLE_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__UUID = IMP_CONFIGURABLE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__VERSION = IMP_CONFIGURABLE_ELEMENT__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__DESCRIPTION = IMP_CONFIGURABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__ID1 = IMP_CONFIGURABLE_ELEMENT__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__NAME = IMP_CONFIGURABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__GROUP1 = IMP_CONFIGURABLE_ELEMENT__GROUP1;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__IMP_CONFIGURED_ELEMENTS = IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Right Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__RIGHT_ELEMENTS = IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Left Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__LEFT_ELEMENTS = IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Extensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__EXTENSIONS = IMP_CONFIGURABLE_ELEMENT__EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__IMP_CONFIGURED_ELEMENTS1 = IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS1;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__OPTIONAL = IMP_CONFIGURABLE_ELEMENT__OPTIONAL;

	/**
	 * The feature id for the '<em><b>Right Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE__RIGHT_ELEMENTS1 = IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS1;

	/**
	 * The number of structural features of the '<em>Imp Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_NODE_FEATURE_COUNT = IMP_CONFIGURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpPortNodeImpl <em>Imp Port Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpPortNodeImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpPortNode()
	 * @generated
	 */
	int IMP_PORT_NODE = 14;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__GROUP = IMP_CONFIGURABLE_ELEMENT__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__PROPERTIES = IMP_CONFIGURABLE_ELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__EXTENSION = IMP_CONFIGURABLE_ELEMENT__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__HREF = IMP_CONFIGURABLE_ELEMENT__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__ID = IMP_CONFIGURABLE_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__IDREF = IMP_CONFIGURABLE_ELEMENT__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__LABEL = IMP_CONFIGURABLE_ELEMENT__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__TYPE = IMP_CONFIGURABLE_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__UUID = IMP_CONFIGURABLE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__VERSION = IMP_CONFIGURABLE_ELEMENT__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__DESCRIPTION = IMP_CONFIGURABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__ID1 = IMP_CONFIGURABLE_ELEMENT__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__NAME = IMP_CONFIGURABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__GROUP1 = IMP_CONFIGURABLE_ELEMENT__GROUP1;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__IMP_CONFIGURED_ELEMENTS = IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Right Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__RIGHT_ELEMENTS = IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Left Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__LEFT_ELEMENTS = IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Extensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__EXTENSIONS = IMP_CONFIGURABLE_ELEMENT__EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Imp Configured Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__IMP_CONFIGURED_ELEMENTS1 = IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS1;

	/**
	 * The feature id for the '<em><b>Optional</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__OPTIONAL = IMP_CONFIGURABLE_ELEMENT__OPTIONAL;

	/**
	 * The feature id for the '<em><b>Right Elements1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__RIGHT_ELEMENTS1 = IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS1;

	/**
	 * The feature id for the '<em><b>Group2</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__GROUP2 = IMP_CONFIGURABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Connectors For Source Port</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT = IMP_CONFIGURABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Connectors For Target Port</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT = IMP_CONFIGURABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Connectors For Source Port1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT1 = IMP_CONFIGURABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Connectors For Target Port1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT1 = IMP_CONFIGURABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Imp Port Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PORT_NODE_FEATURE_COUNT = IMP_CONFIGURABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpProjectImpl <em>Imp Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpProjectImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpProject()
	 * @generated
	 */
	int IMP_PROJECT = 15;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__GROUP = IMP_ITEM__GROUP;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__PROPERTIES = IMP_ITEM__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__EXTENSION = IMP_ITEM__EXTENSION;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__HREF = IMP_ITEM__HREF;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__ID = IMP_ITEM__ID;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__IDREF = IMP_ITEM__IDREF;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__LABEL = IMP_ITEM__LABEL;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__TYPE = IMP_ITEM__TYPE;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__UUID = IMP_ITEM__UUID;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__VERSION = IMP_ITEM__VERSION;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__DESCRIPTION = IMP_ITEM__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__ID1 = IMP_ITEM__ID1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__NAME = IMP_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Group1</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__GROUP1 = IMP_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__MODELS = IMP_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Imp Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__IMP_CONFIGURATIONS = IMP_ITEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Imp Extensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT__IMP_EXTENSIONS = IMP_ITEM_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Imp Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_PROJECT_FEATURE_COUNT = IMP_ITEM_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.ImpTermImpl <em>Imp Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.ImpTermImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpTerm()
	 * @generated
	 */
	int IMP_TERM = 16;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_TERM__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_TERM__EXTENSION = 1;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_TERM__HREF = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_TERM__ID = 3;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_TERM__IDREF = 4;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_TERM__LABEL = 5;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_TERM__TYPE = 6;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_TERM__UUID = 7;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_TERM__VERSION = 8;

	/**
	 * The number of structural features of the '<em>Imp Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMP_TERM_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.NamedItemImpl <em>Named Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.NamedItemImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getNamedItem()
	 * @generated
	 */
	int NAMED_ITEM = 17;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ITEM__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ITEM__EXTENSION = 1;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ITEM__HREF = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ITEM__ID = 3;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ITEM__IDREF = 4;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ITEM__LABEL = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ITEM__NAME = 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ITEM__TYPE = 7;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ITEM__UUID = 8;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ITEM__VERSION = 9;

	/**
	 * The number of structural features of the '<em>Named Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ITEM_FEATURE_COUNT = 10;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.PropertyImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 18;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__EXTENSION = 1;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__HREF = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__ID = 3;

	/**
	 * The feature id for the '<em><b>Idref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__IDREF = 4;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__KEY = 5;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__LABEL = 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__TYPE = 7;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__UUID = 8;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__VALUE = 9;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__VERSION = 10;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = 11;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.impl.DocumentRootImpl
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 20;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__CONSTRAINT = 3;

	/**
	 * The feature id for the '<em><b>Described Item</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__DESCRIBED_ITEM = 4;

	/**
	 * The feature id for the '<em><b>Identifiable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IDENTIFIABLE = 5;

	/**
	 * The feature id for the '<em><b>Imp Configurable Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT = 6;

	/**
	 * The feature id for the '<em><b>Imp Configurable Element Extension</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT_EXTENSION = 7;

	/**
	 * The feature id for the '<em><b>Imp Configurable Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_CONFIGURABLE_GROUP = 8;

	/**
	 * The feature id for the '<em><b>Imp Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_CONFIGURATION = 9;

	/**
	 * The feature id for the '<em><b>Imp Configured Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_CONFIGURED_ELEMENT = 10;

	/**
	 * The feature id for the '<em><b>Imp Connector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_CONNECTOR = 11;

	/**
	 * The feature id for the '<em><b>Imp Extensible Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_EXTENSIBLE_ELEMENT = 12;

	/**
	 * The feature id for the '<em><b>Imp Item</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_ITEM = 13;

	/**
	 * The feature id for the '<em><b>Imp Linking Relation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_LINKING_RELATION = 14;

	/**
	 * The feature id for the '<em><b>Imp Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_MODEL = 15;

	/**
	 * The feature id for the '<em><b>Imp Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_NODE = 16;

	/**
	 * The feature id for the '<em><b>Imp Port Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_PORT_NODE = 17;

	/**
	 * The feature id for the '<em><b>Imp Project</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_PROJECT = 18;

	/**
	 * The feature id for the '<em><b>Imp Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__IMP_TERM = 19;

	/**
	 * The feature id for the '<em><b>Named Item</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__NAMED_ITEM = 20;

	/**
	 * The feature id for the '<em><b>Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__PROPERTY = 21;

	/**
	 * The feature id for the '<em><b>Property Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__PROPERTY_SET = 22;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 23;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.ImpConfigurableGroupType <em>Imp Configurable Group Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableGroupType
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfigurableGroupType()
	 * @generated
	 */
	int IMP_CONFIGURABLE_GROUP_TYPE = 21;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.LinkingType <em>Linking Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.LinkingType
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getLinkingType()
	 * @generated
	 */
	int LINKING_TYPE = 22;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.PortDirection <em>Port Direction</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.PortDirection
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getPortDirection()
	 * @generated
	 */
	int PORT_DIRECTION = 23;

	/**
	 * The meta object id for the '{@link com.prostep.vcontrol.model.common.SelectionKind <em>Selection Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.SelectionKind
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getSelectionKind()
	 * @generated
	 */
	int SELECTION_KIND = 24;

	/**
	 * The meta object id for the '<em>Imp Configurable Group Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableGroupType
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfigurableGroupTypeObject()
	 * @generated
	 */
	int IMP_CONFIGURABLE_GROUP_TYPE_OBJECT = 25;

	/**
	 * The meta object id for the '<em>Linking Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.LinkingType
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getLinkingTypeObject()
	 * @generated
	 */
	int LINKING_TYPE_OBJECT = 26;

	/**
	 * The meta object id for the '<em>Port Direction Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.PortDirection
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getPortDirectionObject()
	 * @generated
	 */
	int PORT_DIRECTION_OBJECT = 27;

	/**
	 * The meta object id for the '<em>Selection Kind Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.prostep.vcontrol.model.common.SelectionKind
	 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getSelectionKindObject()
	 * @generated
	 */
	int SELECTION_KIND_OBJECT = 28;


	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraint</em>'.
	 * @see com.prostep.vcontrol.model.common.Constraint
	 * @generated
	 */
	EClass getConstraint();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.Constraint#getGroup1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group1</em>'.
	 * @see com.prostep.vcontrol.model.common.Constraint#getGroup1()
	 * @see #getConstraint()
	 * @generated
	 */
	EAttribute getConstraint_Group1();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.Constraint#getRootTerm <em>Root Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Root Term</em>'.
	 * @see com.prostep.vcontrol.model.common.Constraint#getRootTerm()
	 * @see #getConstraint()
	 * @generated
	 */
	EReference getConstraint_RootTerm();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.Constraint#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Model</em>'.
	 * @see com.prostep.vcontrol.model.common.Constraint#getModel()
	 * @see #getConstraint()
	 * @generated
	 */
	EReference getConstraint_Model();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Constraint#isIgnore <em>Ignore</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ignore</em>'.
	 * @see com.prostep.vcontrol.model.common.Constraint#isIgnore()
	 * @see #getConstraint()
	 * @generated
	 */
	EAttribute getConstraint_Ignore();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Constraint#getModel1 <em>Model1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model1</em>'.
	 * @see com.prostep.vcontrol.model.common.Constraint#getModel1()
	 * @see #getConstraint()
	 * @generated
	 */
	EAttribute getConstraint_Model1();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.DescribedItem <em>Described Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Described Item</em>'.
	 * @see com.prostep.vcontrol.model.common.DescribedItem
	 * @generated
	 */
	EClass getDescribedItem();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.DescribedItem#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.prostep.vcontrol.model.common.DescribedItem#getGroup()
	 * @see #getDescribedItem()
	 * @generated
	 */
	EAttribute getDescribedItem_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.DescribedItem#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.DescribedItem#getExtension()
	 * @see #getDescribedItem()
	 * @generated
	 */
	EReference getDescribedItem_Extension();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.DescribedItem#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see com.prostep.vcontrol.model.common.DescribedItem#getDescription()
	 * @see #getDescribedItem()
	 * @generated
	 */
	EAttribute getDescribedItem_Description();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.DescribedItem#getHref <em>Href</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Href</em>'.
	 * @see com.prostep.vcontrol.model.common.DescribedItem#getHref()
	 * @see #getDescribedItem()
	 * @generated
	 */
	EAttribute getDescribedItem_Href();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.DescribedItem#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.prostep.vcontrol.model.common.DescribedItem#getId()
	 * @see #getDescribedItem()
	 * @generated
	 */
	EAttribute getDescribedItem_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.DescribedItem#getIdref <em>Idref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Idref</em>'.
	 * @see com.prostep.vcontrol.model.common.DescribedItem#getIdref()
	 * @see #getDescribedItem()
	 * @generated
	 */
	EAttribute getDescribedItem_Idref();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.DescribedItem#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.prostep.vcontrol.model.common.DescribedItem#getLabel()
	 * @see #getDescribedItem()
	 * @generated
	 */
	EAttribute getDescribedItem_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.DescribedItem#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see com.prostep.vcontrol.model.common.DescribedItem#getType()
	 * @see #getDescribedItem()
	 * @generated
	 */
	EAttribute getDescribedItem_Type();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.DescribedItem#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see com.prostep.vcontrol.model.common.DescribedItem#getUuid()
	 * @see #getDescribedItem()
	 * @generated
	 */
	EAttribute getDescribedItem_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.DescribedItem#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.prostep.vcontrol.model.common.DescribedItem#getVersion()
	 * @see #getDescribedItem()
	 * @generated
	 */
	EAttribute getDescribedItem_Version();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.Identifiable <em>Identifiable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Identifiable</em>'.
	 * @see com.prostep.vcontrol.model.common.Identifiable
	 * @generated
	 */
	EClass getIdentifiable();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.Identifiable#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.prostep.vcontrol.model.common.Identifiable#getGroup()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EAttribute getIdentifiable_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.Identifiable#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.Identifiable#getExtension()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EReference getIdentifiable_Extension();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Identifiable#getHref <em>Href</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Href</em>'.
	 * @see com.prostep.vcontrol.model.common.Identifiable#getHref()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EAttribute getIdentifiable_Href();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Identifiable#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.prostep.vcontrol.model.common.Identifiable#getId()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EAttribute getIdentifiable_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Identifiable#getId1 <em>Id1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id1</em>'.
	 * @see com.prostep.vcontrol.model.common.Identifiable#getId1()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EAttribute getIdentifiable_Id1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Identifiable#getIdref <em>Idref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Idref</em>'.
	 * @see com.prostep.vcontrol.model.common.Identifiable#getIdref()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EAttribute getIdentifiable_Idref();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Identifiable#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.prostep.vcontrol.model.common.Identifiable#getLabel()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EAttribute getIdentifiable_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Identifiable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see com.prostep.vcontrol.model.common.Identifiable#getType()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EAttribute getIdentifiable_Type();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Identifiable#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see com.prostep.vcontrol.model.common.Identifiable#getUuid()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EAttribute getIdentifiable_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Identifiable#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.prostep.vcontrol.model.common.Identifiable#getVersion()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EAttribute getIdentifiable_Version();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement <em>Imp Configurable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Configurable Element</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElement
	 * @generated
	 */
	EClass getImpConfigurableElement();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getGroup1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElement#getGroup1()
	 * @see #getImpConfigurableElement()
	 * @generated
	 */
	EAttribute getImpConfigurableElement_Group1();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getImpConfiguredElements <em>Imp Configured Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imp Configured Elements</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElement#getImpConfiguredElements()
	 * @see #getImpConfigurableElement()
	 * @generated
	 */
	EReference getImpConfigurableElement_ImpConfiguredElements();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getRightElements <em>Right Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Right Elements</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElement#getRightElements()
	 * @see #getImpConfigurableElement()
	 * @generated
	 */
	EReference getImpConfigurableElement_RightElements();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getLeftElements <em>Left Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Left Elements</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElement#getLeftElements()
	 * @see #getImpConfigurableElement()
	 * @generated
	 */
	EReference getImpConfigurableElement_LeftElements();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getExtensions <em>Extensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extensions</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElement#getExtensions()
	 * @see #getImpConfigurableElement()
	 * @generated
	 */
	EReference getImpConfigurableElement_Extensions();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getImpConfiguredElements1 <em>Imp Configured Elements1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Imp Configured Elements1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElement#getImpConfiguredElements1()
	 * @see #getImpConfigurableElement()
	 * @generated
	 */
	EAttribute getImpConfigurableElement_ImpConfiguredElements1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#isOptional <em>Optional</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Optional</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElement#isOptional()
	 * @see #getImpConfigurableElement()
	 * @generated
	 */
	EAttribute getImpConfigurableElement_Optional();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfigurableElement#getRightElements1 <em>Right Elements1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Right Elements1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElement#getRightElements1()
	 * @see #getImpConfigurableElement()
	 * @generated
	 */
	EAttribute getImpConfigurableElement_RightElements1();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension <em>Imp Configurable Element Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Configurable Element Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElementExtension
	 * @generated
	 */
	EClass getImpConfigurableElementExtension();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getGroup()
	 * @see #getImpConfigurableElementExtension()
	 * @generated
	 */
	EAttribute getImpConfigurableElementExtension_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getExtension()
	 * @see #getImpConfigurableElementExtension()
	 * @generated
	 */
	EReference getImpConfigurableElementExtension_Extension();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getHref <em>Href</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Href</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getHref()
	 * @see #getImpConfigurableElementExtension()
	 * @generated
	 */
	EAttribute getImpConfigurableElementExtension_Href();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getId()
	 * @see #getImpConfigurableElementExtension()
	 * @generated
	 */
	EAttribute getImpConfigurableElementExtension_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getIdref <em>Idref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Idref</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getIdref()
	 * @see #getImpConfigurableElementExtension()
	 * @generated
	 */
	EAttribute getImpConfigurableElementExtension_Idref();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getLabel()
	 * @see #getImpConfigurableElementExtension()
	 * @generated
	 */
	EAttribute getImpConfigurableElementExtension_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getType()
	 * @see #getImpConfigurableElementExtension()
	 * @generated
	 */
	EAttribute getImpConfigurableElementExtension_Type();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getUuid()
	 * @see #getImpConfigurableElementExtension()
	 * @generated
	 */
	EAttribute getImpConfigurableElementExtension_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableElementExtension#getVersion()
	 * @see #getImpConfigurableElementExtension()
	 * @generated
	 */
	EAttribute getImpConfigurableElementExtension_Version();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpConfigurableGroup <em>Imp Configurable Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Configurable Group</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableGroup
	 * @generated
	 */
	EClass getImpConfigurableGroup();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfigurableGroup#getGroupType <em>Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Group Type</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableGroup#getGroupType()
	 * @see #getImpConfigurableGroup()
	 * @generated
	 */
	EAttribute getImpConfigurableGroup_GroupType();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpConfiguration <em>Imp Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Configuration</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguration
	 * @generated
	 */
	EClass getImpConfiguration();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.ImpConfiguration#getGroup1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguration#getGroup1()
	 * @see #getImpConfiguration()
	 * @generated
	 */
	EAttribute getImpConfiguration_Group1();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfiguration#getImpConfiguredElements <em>Imp Configured Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imp Configured Elements</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguration#getImpConfiguredElements()
	 * @see #getImpConfiguration()
	 * @generated
	 */
	EReference getImpConfiguration_ImpConfiguredElements();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfiguration#getModels <em>Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Models</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguration#getModels()
	 * @see #getImpConfiguration()
	 * @generated
	 */
	EReference getImpConfiguration_Models();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfiguration#getProject <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Project</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguration#getProject()
	 * @see #getImpConfiguration()
	 * @generated
	 */
	EReference getImpConfiguration_Project();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguration#getModels1 <em>Models1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Models1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguration#getModels1()
	 * @see #getImpConfiguration()
	 * @generated
	 */
	EAttribute getImpConfiguration_Models1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguration#getProject1 <em>Project1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Project1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguration#getProject1()
	 * @see #getImpConfiguration()
	 * @generated
	 */
	EAttribute getImpConfiguration_Project1();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement <em>Imp Configured Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Configured Element</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement
	 * @generated
	 */
	EClass getImpConfiguredElement();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getGroup()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EAttribute getImpConfiguredElement_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getImpConfigurableElement <em>Imp Configurable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imp Configurable Element</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getImpConfigurableElement()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EReference getImpConfiguredElement_ImpConfigurableElement();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getImpConfiguration <em>Imp Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imp Configuration</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getImpConfiguration()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EReference getImpConfiguredElement_ImpConfiguration();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getConfigurationForHook <em>Configuration For Hook</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Configuration For Hook</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getConfigurationForHook()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EReference getImpConfiguredElement_ConfigurationForHook();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getExtension()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EReference getImpConfiguredElement_Extension();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getHref <em>Href</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Href</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getHref()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EAttribute getImpConfiguredElement_Href();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getId()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EAttribute getImpConfiguredElement_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getIdref <em>Idref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Idref</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getIdref()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EAttribute getImpConfiguredElement_Idref();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getImpConfigurableElement1 <em>Imp Configurable Element1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Imp Configurable Element1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getImpConfigurableElement1()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EAttribute getImpConfiguredElement_ImpConfigurableElement1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getImpConfiguration1 <em>Imp Configuration1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Imp Configuration1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getImpConfiguration1()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EAttribute getImpConfiguredElement_ImpConfiguration1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getLabel()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EAttribute getImpConfiguredElement_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getSelectionInformation <em>Selection Information</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selection Information</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getSelectionInformation()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EAttribute getImpConfiguredElement_SelectionInformation();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getType()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EAttribute getImpConfiguredElement_Type();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getUuid()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EAttribute getImpConfiguredElement_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConfiguredElement#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfiguredElement#getVersion()
	 * @see #getImpConfiguredElement()
	 * @generated
	 */
	EAttribute getImpConfiguredElement_Version();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpConnector <em>Imp Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Connector</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConnector
	 * @generated
	 */
	EClass getImpConnector();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.ImpConnector#getGroup1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConnector#getGroup1()
	 * @see #getImpConnector()
	 * @generated
	 */
	EAttribute getImpConnector_Group1();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConnector#getSourcePort <em>Source Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Source Port</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConnector#getSourcePort()
	 * @see #getImpConnector()
	 * @generated
	 */
	EReference getImpConnector_SourcePort();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpConnector#getTargetPort <em>Target Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Target Port</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConnector#getTargetPort()
	 * @see #getImpConnector()
	 * @generated
	 */
	EReference getImpConnector_TargetPort();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConnector#getSourcePort1 <em>Source Port1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Port1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConnector#getSourcePort1()
	 * @see #getImpConnector()
	 * @generated
	 */
	EAttribute getImpConnector_SourcePort1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpConnector#getTargetPort1 <em>Target Port1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Port1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConnector#getTargetPort1()
	 * @see #getImpConnector()
	 * @generated
	 */
	EAttribute getImpConnector_TargetPort1();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpExtensibleElement <em>Imp Extensible Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Extensible Element</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpExtensibleElement
	 * @generated
	 */
	EClass getImpExtensibleElement();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.ImpExtensibleElement#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpExtensibleElement#getGroup()
	 * @see #getImpExtensibleElement()
	 * @generated
	 */
	EAttribute getImpExtensibleElement_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpExtensibleElement#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpExtensibleElement#getExtension()
	 * @see #getImpExtensibleElement()
	 * @generated
	 */
	EReference getImpExtensibleElement_Extension();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpExtensibleElement#getHref <em>Href</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Href</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpExtensibleElement#getHref()
	 * @see #getImpExtensibleElement()
	 * @generated
	 */
	EAttribute getImpExtensibleElement_Href();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpExtensibleElement#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpExtensibleElement#getId()
	 * @see #getImpExtensibleElement()
	 * @generated
	 */
	EAttribute getImpExtensibleElement_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpExtensibleElement#getIdref <em>Idref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Idref</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpExtensibleElement#getIdref()
	 * @see #getImpExtensibleElement()
	 * @generated
	 */
	EAttribute getImpExtensibleElement_Idref();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpExtensibleElement#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpExtensibleElement#getLabel()
	 * @see #getImpExtensibleElement()
	 * @generated
	 */
	EAttribute getImpExtensibleElement_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpExtensibleElement#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpExtensibleElement#getType()
	 * @see #getImpExtensibleElement()
	 * @generated
	 */
	EAttribute getImpExtensibleElement_Type();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpExtensibleElement#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpExtensibleElement#getUuid()
	 * @see #getImpExtensibleElement()
	 * @generated
	 */
	EAttribute getImpExtensibleElement_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpExtensibleElement#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpExtensibleElement#getVersion()
	 * @see #getImpExtensibleElement()
	 * @generated
	 */
	EAttribute getImpExtensibleElement_Version();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpItem <em>Imp Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Item</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpItem
	 * @generated
	 */
	EClass getImpItem();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpItem#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpItem#getDescription()
	 * @see #getImpItem()
	 * @generated
	 */
	EAttribute getImpItem_Description();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpItem#getId1 <em>Id1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpItem#getId1()
	 * @see #getImpItem()
	 * @generated
	 */
	EAttribute getImpItem_Id1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpItem#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpItem#getName()
	 * @see #getImpItem()
	 * @generated
	 */
	EAttribute getImpItem_Name();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation <em>Imp Linking Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Linking Relation</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation
	 * @generated
	 */
	EClass getImpLinkingRelation();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getGroup()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EAttribute getImpLinkingRelation_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getRightElement <em>Right Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Right Element</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getRightElement()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EReference getImpLinkingRelation_RightElement();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLeftElement <em>Left Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Left Element</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getLeftElement()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EReference getImpLinkingRelation_LeftElement();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getExtension()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EReference getImpLinkingRelation_Extension();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getHref <em>Href</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Href</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getHref()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EAttribute getImpLinkingRelation_Href();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getId()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EAttribute getImpLinkingRelation_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getIdref <em>Idref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Idref</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getIdref()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EAttribute getImpLinkingRelation_Idref();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getLabel()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EAttribute getImpLinkingRelation_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLeftElement1 <em>Left Element1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Left Element1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getLeftElement1()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EAttribute getImpLinkingRelation_LeftElement1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getLinkingType <em>Linking Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Linking Type</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getLinkingType()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EAttribute getImpLinkingRelation_LinkingType();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getRightElement1 <em>Right Element1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Right Element1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getRightElement1()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EAttribute getImpLinkingRelation_RightElement1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getType()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EAttribute getImpLinkingRelation_Type();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getUuid()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EAttribute getImpLinkingRelation_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpLinkingRelation#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpLinkingRelation#getVersion()
	 * @see #getImpLinkingRelation()
	 * @generated
	 */
	EAttribute getImpLinkingRelation_Version();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpModel <em>Imp Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Model</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpModel
	 * @generated
	 */
	EClass getImpModel();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.ImpModel#getGroup1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpModel#getGroup1()
	 * @see #getImpModel()
	 * @generated
	 */
	EAttribute getImpModel_Group1();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpModel#getProject <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Project</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpModel#getProject()
	 * @see #getImpModel()
	 * @generated
	 */
	EReference getImpModel_Project();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpModel#getImpConfigurations <em>Imp Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imp Configurations</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpModel#getImpConfigurations()
	 * @see #getImpModel()
	 * @generated
	 */
	EReference getImpModel_ImpConfigurations();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpModel#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpModel#getConstraints()
	 * @see #getImpModel()
	 * @generated
	 */
	EReference getImpModel_Constraints();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpModel#getImpConfigurations1 <em>Imp Configurations1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Imp Configurations1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpModel#getImpConfigurations1()
	 * @see #getImpModel()
	 * @generated
	 */
	EAttribute getImpModel_ImpConfigurations1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpModel#getProject1 <em>Project1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Project1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpModel#getProject1()
	 * @see #getImpModel()
	 * @generated
	 */
	EAttribute getImpModel_Project1();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpNode <em>Imp Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Node</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpNode
	 * @generated
	 */
	EClass getImpNode();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpPortNode <em>Imp Port Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Port Node</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpPortNode
	 * @generated
	 */
	EClass getImpPortNode();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.ImpPortNode#getGroup2 <em>Group2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group2</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpPortNode#getGroup2()
	 * @see #getImpPortNode()
	 * @generated
	 */
	EAttribute getImpPortNode_Group2();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForSourcePort <em>Connectors For Source Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connectors For Source Port</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForSourcePort()
	 * @see #getImpPortNode()
	 * @generated
	 */
	EReference getImpPortNode_ConnectorsForSourcePort();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForTargetPort <em>Connectors For Target Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connectors For Target Port</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForTargetPort()
	 * @see #getImpPortNode()
	 * @generated
	 */
	EReference getImpPortNode_ConnectorsForTargetPort();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForSourcePort1 <em>Connectors For Source Port1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Connectors For Source Port1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForSourcePort1()
	 * @see #getImpPortNode()
	 * @generated
	 */
	EAttribute getImpPortNode_ConnectorsForSourcePort1();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForTargetPort1 <em>Connectors For Target Port1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Connectors For Target Port1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpPortNode#getConnectorsForTargetPort1()
	 * @see #getImpPortNode()
	 * @generated
	 */
	EAttribute getImpPortNode_ConnectorsForTargetPort1();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpProject <em>Imp Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Project</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpProject
	 * @generated
	 */
	EClass getImpProject();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.ImpProject#getGroup1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group1</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpProject#getGroup1()
	 * @see #getImpProject()
	 * @generated
	 */
	EAttribute getImpProject_Group1();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpProject#getModels <em>Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Models</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpProject#getModels()
	 * @see #getImpProject()
	 * @generated
	 */
	EReference getImpProject_Models();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpProject#getImpConfigurations <em>Imp Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imp Configurations</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpProject#getImpConfigurations()
	 * @see #getImpProject()
	 * @generated
	 */
	EReference getImpProject_ImpConfigurations();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpProject#getImpExtensions <em>Imp Extensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imp Extensions</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpProject#getImpExtensions()
	 * @see #getImpProject()
	 * @generated
	 */
	EReference getImpProject_ImpExtensions();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.ImpTerm <em>Imp Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imp Term</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpTerm
	 * @generated
	 */
	EClass getImpTerm();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.ImpTerm#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpTerm#getGroup()
	 * @see #getImpTerm()
	 * @generated
	 */
	EAttribute getImpTerm_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.ImpTerm#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpTerm#getExtension()
	 * @see #getImpTerm()
	 * @generated
	 */
	EReference getImpTerm_Extension();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpTerm#getHref <em>Href</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Href</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpTerm#getHref()
	 * @see #getImpTerm()
	 * @generated
	 */
	EAttribute getImpTerm_Href();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpTerm#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpTerm#getId()
	 * @see #getImpTerm()
	 * @generated
	 */
	EAttribute getImpTerm_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpTerm#getIdref <em>Idref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Idref</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpTerm#getIdref()
	 * @see #getImpTerm()
	 * @generated
	 */
	EAttribute getImpTerm_Idref();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpTerm#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpTerm#getLabel()
	 * @see #getImpTerm()
	 * @generated
	 */
	EAttribute getImpTerm_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpTerm#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpTerm#getType()
	 * @see #getImpTerm()
	 * @generated
	 */
	EAttribute getImpTerm_Type();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpTerm#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpTerm#getUuid()
	 * @see #getImpTerm()
	 * @generated
	 */
	EAttribute getImpTerm_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.ImpTerm#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpTerm#getVersion()
	 * @see #getImpTerm()
	 * @generated
	 */
	EAttribute getImpTerm_Version();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.NamedItem <em>Named Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Item</em>'.
	 * @see com.prostep.vcontrol.model.common.NamedItem
	 * @generated
	 */
	EClass getNamedItem();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.NamedItem#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.prostep.vcontrol.model.common.NamedItem#getGroup()
	 * @see #getNamedItem()
	 * @generated
	 */
	EAttribute getNamedItem_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.NamedItem#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.NamedItem#getExtension()
	 * @see #getNamedItem()
	 * @generated
	 */
	EReference getNamedItem_Extension();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.NamedItem#getHref <em>Href</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Href</em>'.
	 * @see com.prostep.vcontrol.model.common.NamedItem#getHref()
	 * @see #getNamedItem()
	 * @generated
	 */
	EAttribute getNamedItem_Href();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.NamedItem#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.prostep.vcontrol.model.common.NamedItem#getId()
	 * @see #getNamedItem()
	 * @generated
	 */
	EAttribute getNamedItem_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.NamedItem#getIdref <em>Idref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Idref</em>'.
	 * @see com.prostep.vcontrol.model.common.NamedItem#getIdref()
	 * @see #getNamedItem()
	 * @generated
	 */
	EAttribute getNamedItem_Idref();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.NamedItem#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.prostep.vcontrol.model.common.NamedItem#getLabel()
	 * @see #getNamedItem()
	 * @generated
	 */
	EAttribute getNamedItem_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.NamedItem#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.prostep.vcontrol.model.common.NamedItem#getName()
	 * @see #getNamedItem()
	 * @generated
	 */
	EAttribute getNamedItem_Name();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.NamedItem#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see com.prostep.vcontrol.model.common.NamedItem#getType()
	 * @see #getNamedItem()
	 * @generated
	 */
	EAttribute getNamedItem_Type();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.NamedItem#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see com.prostep.vcontrol.model.common.NamedItem#getUuid()
	 * @see #getNamedItem()
	 * @generated
	 */
	EAttribute getNamedItem_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.NamedItem#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.prostep.vcontrol.model.common.NamedItem#getVersion()
	 * @see #getNamedItem()
	 * @generated
	 */
	EAttribute getNamedItem_Version();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see com.prostep.vcontrol.model.common.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.Property#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.prostep.vcontrol.model.common.Property#getGroup()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.Property#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.Property#getExtension()
	 * @see #getProperty()
	 * @generated
	 */
	EReference getProperty_Extension();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Property#getHref <em>Href</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Href</em>'.
	 * @see com.prostep.vcontrol.model.common.Property#getHref()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Href();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Property#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.prostep.vcontrol.model.common.Property#getId()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Property#getIdref <em>Idref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Idref</em>'.
	 * @see com.prostep.vcontrol.model.common.Property#getIdref()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Idref();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Property#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see com.prostep.vcontrol.model.common.Property#getKey()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Key();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Property#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.prostep.vcontrol.model.common.Property#getLabel()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Property#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see com.prostep.vcontrol.model.common.Property#getType()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Type();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Property#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see com.prostep.vcontrol.model.common.Property#getUuid()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Property#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.prostep.vcontrol.model.common.Property#getValue()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Value();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.Property#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.prostep.vcontrol.model.common.Property#getVersion()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Version();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.PropertySet <em>Property Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Set</em>'.
	 * @see com.prostep.vcontrol.model.common.PropertySet
	 * @generated
	 */
	EClass getPropertySet();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.PropertySet#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see com.prostep.vcontrol.model.common.PropertySet#getGroup()
	 * @see #getPropertySet()
	 * @generated
	 */
	EAttribute getPropertySet_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.PropertySet#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see com.prostep.vcontrol.model.common.PropertySet#getProperties()
	 * @see #getPropertySet()
	 * @generated
	 */
	EReference getPropertySet_Properties();

	/**
	 * Returns the meta object for the containment reference list '{@link com.prostep.vcontrol.model.common.PropertySet#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.PropertySet#getExtension()
	 * @see #getPropertySet()
	 * @generated
	 */
	EReference getPropertySet_Extension();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.PropertySet#getHref <em>Href</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Href</em>'.
	 * @see com.prostep.vcontrol.model.common.PropertySet#getHref()
	 * @see #getPropertySet()
	 * @generated
	 */
	EAttribute getPropertySet_Href();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.PropertySet#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.prostep.vcontrol.model.common.PropertySet#getId()
	 * @see #getPropertySet()
	 * @generated
	 */
	EAttribute getPropertySet_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.PropertySet#getIdref <em>Idref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Idref</em>'.
	 * @see com.prostep.vcontrol.model.common.PropertySet#getIdref()
	 * @see #getPropertySet()
	 * @generated
	 */
	EAttribute getPropertySet_Idref();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.PropertySet#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.prostep.vcontrol.model.common.PropertySet#getLabel()
	 * @see #getPropertySet()
	 * @generated
	 */
	EAttribute getPropertySet_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.PropertySet#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see com.prostep.vcontrol.model.common.PropertySet#getType()
	 * @see #getPropertySet()
	 * @generated
	 */
	EAttribute getPropertySet_Type();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.PropertySet#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see com.prostep.vcontrol.model.common.PropertySet#getUuid()
	 * @see #getPropertySet()
	 * @generated
	 */
	EAttribute getPropertySet_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link com.prostep.vcontrol.model.common.PropertySet#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see com.prostep.vcontrol.model.common.PropertySet#getVersion()
	 * @see #getPropertySet()
	 * @generated
	 */
	EAttribute getPropertySet_Version();

	/**
	 * Returns the meta object for class '{@link com.prostep.vcontrol.model.common.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link com.prostep.vcontrol.model.common.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link com.prostep.vcontrol.model.common.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link com.prostep.vcontrol.model.common.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Constraint</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getConstraint()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Constraint();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getDescribedItem <em>Described Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Described Item</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getDescribedItem()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_DescribedItem();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getIdentifiable <em>Identifiable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Identifiable</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getIdentifiable()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Identifiable();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableElement <em>Imp Configurable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Configurable Element</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableElement()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpConfigurableElement();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableElementExtension <em>Imp Configurable Element Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Configurable Element Extension</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableElementExtension()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpConfigurableElementExtension();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableGroup <em>Imp Configurable Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Configurable Group</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpConfigurableGroup()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpConfigurableGroup();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfiguration <em>Imp Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Configuration</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpConfiguration()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpConfiguration();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConfiguredElement <em>Imp Configured Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Configured Element</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpConfiguredElement()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpConfiguredElement();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpConnector <em>Imp Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Connector</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpConnector()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpConnector();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpExtensibleElement <em>Imp Extensible Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Extensible Element</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpExtensibleElement()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpExtensibleElement();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpItem <em>Imp Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Item</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpItem()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpItem();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpLinkingRelation <em>Imp Linking Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Linking Relation</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpLinkingRelation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpLinkingRelation();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpModel <em>Imp Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Model</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpModel()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpModel();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpNode <em>Imp Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Node</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpNode()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpNode();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpPortNode <em>Imp Port Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Port Node</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpPortNode()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpPortNode();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpProject <em>Imp Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Project</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpProject()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpProject();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getImpTerm <em>Imp Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp Term</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getImpTerm()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ImpTerm();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getNamedItem <em>Named Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Named Item</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getNamedItem()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_NamedItem();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getProperty()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Property();

	/**
	 * Returns the meta object for the containment reference '{@link com.prostep.vcontrol.model.common.DocumentRoot#getPropertySet <em>Property Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property Set</em>'.
	 * @see com.prostep.vcontrol.model.common.DocumentRoot#getPropertySet()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_PropertySet();

	/**
	 * Returns the meta object for enum '{@link com.prostep.vcontrol.model.common.ImpConfigurableGroupType <em>Imp Configurable Group Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Imp Configurable Group Type</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableGroupType
	 * @generated
	 */
	EEnum getImpConfigurableGroupType();

	/**
	 * Returns the meta object for enum '{@link com.prostep.vcontrol.model.common.LinkingType <em>Linking Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Linking Type</em>'.
	 * @see com.prostep.vcontrol.model.common.LinkingType
	 * @generated
	 */
	EEnum getLinkingType();

	/**
	 * Returns the meta object for enum '{@link com.prostep.vcontrol.model.common.PortDirection <em>Port Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Port Direction</em>'.
	 * @see com.prostep.vcontrol.model.common.PortDirection
	 * @generated
	 */
	EEnum getPortDirection();

	/**
	 * Returns the meta object for enum '{@link com.prostep.vcontrol.model.common.SelectionKind <em>Selection Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Selection Kind</em>'.
	 * @see com.prostep.vcontrol.model.common.SelectionKind
	 * @generated
	 */
	EEnum getSelectionKind();

	/**
	 * Returns the meta object for data type '{@link com.prostep.vcontrol.model.common.ImpConfigurableGroupType <em>Imp Configurable Group Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Imp Configurable Group Type Object</em>'.
	 * @see com.prostep.vcontrol.model.common.ImpConfigurableGroupType
	 * @model instanceClass="com.prostep.vcontrol.model.common.ImpConfigurableGroupType"
	 *        extendedMetaData="name='ImpConfigurableGroupType:Object' baseType='ImpConfigurableGroupType'"
	 * @generated
	 */
	EDataType getImpConfigurableGroupTypeObject();

	/**
	 * Returns the meta object for data type '{@link com.prostep.vcontrol.model.common.LinkingType <em>Linking Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Linking Type Object</em>'.
	 * @see com.prostep.vcontrol.model.common.LinkingType
	 * @model instanceClass="com.prostep.vcontrol.model.common.LinkingType"
	 *        extendedMetaData="name='LinkingType:Object' baseType='LinkingType'"
	 * @generated
	 */
	EDataType getLinkingTypeObject();

	/**
	 * Returns the meta object for data type '{@link com.prostep.vcontrol.model.common.PortDirection <em>Port Direction Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Port Direction Object</em>'.
	 * @see com.prostep.vcontrol.model.common.PortDirection
	 * @model instanceClass="com.prostep.vcontrol.model.common.PortDirection"
	 *        extendedMetaData="name='PortDirection:Object' baseType='PortDirection'"
	 * @generated
	 */
	EDataType getPortDirectionObject();

	/**
	 * Returns the meta object for data type '{@link com.prostep.vcontrol.model.common.SelectionKind <em>Selection Kind Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Selection Kind Object</em>'.
	 * @see com.prostep.vcontrol.model.common.SelectionKind
	 * @model instanceClass="com.prostep.vcontrol.model.common.SelectionKind"
	 *        extendedMetaData="name='SelectionKind:Object' baseType='SelectionKind'"
	 * @generated
	 */
	EDataType getSelectionKindObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CommonFactory getCommonFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ConstraintImpl <em>Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ConstraintImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getConstraint()
		 * @generated
		 */
		EClass CONSTRAINT = eINSTANCE.getConstraint();

		/**
		 * The meta object literal for the '<em><b>Group1</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTRAINT__GROUP1 = eINSTANCE.getConstraint_Group1();

		/**
		 * The meta object literal for the '<em><b>Root Term</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT__ROOT_TERM = eINSTANCE.getConstraint_RootTerm();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT__MODEL = eINSTANCE.getConstraint_Model();

		/**
		 * The meta object literal for the '<em><b>Ignore</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTRAINT__IGNORE = eINSTANCE.getConstraint_Ignore();

		/**
		 * The meta object literal for the '<em><b>Model1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTRAINT__MODEL1 = eINSTANCE.getConstraint_Model1();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.DescribedItemImpl <em>Described Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.DescribedItemImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getDescribedItem()
		 * @generated
		 */
		EClass DESCRIBED_ITEM = eINSTANCE.getDescribedItem();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIBED_ITEM__GROUP = eINSTANCE.getDescribedItem_Group();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESCRIBED_ITEM__EXTENSION = eINSTANCE.getDescribedItem_Extension();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIBED_ITEM__DESCRIPTION = eINSTANCE.getDescribedItem_Description();

		/**
		 * The meta object literal for the '<em><b>Href</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIBED_ITEM__HREF = eINSTANCE.getDescribedItem_Href();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIBED_ITEM__ID = eINSTANCE.getDescribedItem_Id();

		/**
		 * The meta object literal for the '<em><b>Idref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIBED_ITEM__IDREF = eINSTANCE.getDescribedItem_Idref();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIBED_ITEM__LABEL = eINSTANCE.getDescribedItem_Label();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIBED_ITEM__TYPE = eINSTANCE.getDescribedItem_Type();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIBED_ITEM__UUID = eINSTANCE.getDescribedItem_Uuid();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIBED_ITEM__VERSION = eINSTANCE.getDescribedItem_Version();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.IdentifiableImpl <em>Identifiable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.IdentifiableImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getIdentifiable()
		 * @generated
		 */
		EClass IDENTIFIABLE = eINSTANCE.getIdentifiable();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIABLE__GROUP = eINSTANCE.getIdentifiable_Group();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IDENTIFIABLE__EXTENSION = eINSTANCE.getIdentifiable_Extension();

		/**
		 * The meta object literal for the '<em><b>Href</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIABLE__HREF = eINSTANCE.getIdentifiable_Href();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIABLE__ID = eINSTANCE.getIdentifiable_Id();

		/**
		 * The meta object literal for the '<em><b>Id1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIABLE__ID1 = eINSTANCE.getIdentifiable_Id1();

		/**
		 * The meta object literal for the '<em><b>Idref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIABLE__IDREF = eINSTANCE.getIdentifiable_Idref();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIABLE__LABEL = eINSTANCE.getIdentifiable_Label();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIABLE__TYPE = eINSTANCE.getIdentifiable_Type();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIABLE__UUID = eINSTANCE.getIdentifiable_Uuid();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IDENTIFIABLE__VERSION = eINSTANCE.getIdentifiable_Version();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl <em>Imp Configurable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpConfigurableElementImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfigurableElement()
		 * @generated
		 */
		EClass IMP_CONFIGURABLE_ELEMENT = eINSTANCE.getImpConfigurableElement();

		/**
		 * The meta object literal for the '<em><b>Group1</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT__GROUP1 = eINSTANCE.getImpConfigurableElement_Group1();

		/**
		 * The meta object literal for the '<em><b>Imp Configured Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS = eINSTANCE.getImpConfigurableElement_ImpConfiguredElements();

		/**
		 * The meta object literal for the '<em><b>Right Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS = eINSTANCE.getImpConfigurableElement_RightElements();

		/**
		 * The meta object literal for the '<em><b>Left Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURABLE_ELEMENT__LEFT_ELEMENTS = eINSTANCE.getImpConfigurableElement_LeftElements();

		/**
		 * The meta object literal for the '<em><b>Extensions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURABLE_ELEMENT__EXTENSIONS = eINSTANCE.getImpConfigurableElement_Extensions();

		/**
		 * The meta object literal for the '<em><b>Imp Configured Elements1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT__IMP_CONFIGURED_ELEMENTS1 = eINSTANCE.getImpConfigurableElement_ImpConfiguredElements1();

		/**
		 * The meta object literal for the '<em><b>Optional</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT__OPTIONAL = eINSTANCE.getImpConfigurableElement_Optional();

		/**
		 * The meta object literal for the '<em><b>Right Elements1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT__RIGHT_ELEMENTS1 = eINSTANCE.getImpConfigurableElement_RightElements1();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableElementExtensionImpl <em>Imp Configurable Element Extension</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpConfigurableElementExtensionImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfigurableElementExtension()
		 * @generated
		 */
		EClass IMP_CONFIGURABLE_ELEMENT_EXTENSION = eINSTANCE.getImpConfigurableElementExtension();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT_EXTENSION__GROUP = eINSTANCE.getImpConfigurableElementExtension_Group();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURABLE_ELEMENT_EXTENSION__EXTENSION = eINSTANCE.getImpConfigurableElementExtension_Extension();

		/**
		 * The meta object literal for the '<em><b>Href</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT_EXTENSION__HREF = eINSTANCE.getImpConfigurableElementExtension_Href();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT_EXTENSION__ID = eINSTANCE.getImpConfigurableElementExtension_Id();

		/**
		 * The meta object literal for the '<em><b>Idref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT_EXTENSION__IDREF = eINSTANCE.getImpConfigurableElementExtension_Idref();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT_EXTENSION__LABEL = eINSTANCE.getImpConfigurableElementExtension_Label();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT_EXTENSION__TYPE = eINSTANCE.getImpConfigurableElementExtension_Type();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT_EXTENSION__UUID = eINSTANCE.getImpConfigurableElementExtension_Uuid();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_ELEMENT_EXTENSION__VERSION = eINSTANCE.getImpConfigurableElementExtension_Version();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpConfigurableGroupImpl <em>Imp Configurable Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpConfigurableGroupImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfigurableGroup()
		 * @generated
		 */
		EClass IMP_CONFIGURABLE_GROUP = eINSTANCE.getImpConfigurableGroup();

		/**
		 * The meta object literal for the '<em><b>Group Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURABLE_GROUP__GROUP_TYPE = eINSTANCE.getImpConfigurableGroup_GroupType();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpConfigurationImpl <em>Imp Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpConfigurationImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfiguration()
		 * @generated
		 */
		EClass IMP_CONFIGURATION = eINSTANCE.getImpConfiguration();

		/**
		 * The meta object literal for the '<em><b>Group1</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURATION__GROUP1 = eINSTANCE.getImpConfiguration_Group1();

		/**
		 * The meta object literal for the '<em><b>Imp Configured Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURATION__IMP_CONFIGURED_ELEMENTS = eINSTANCE.getImpConfiguration_ImpConfiguredElements();

		/**
		 * The meta object literal for the '<em><b>Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURATION__MODELS = eINSTANCE.getImpConfiguration_Models();

		/**
		 * The meta object literal for the '<em><b>Project</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURATION__PROJECT = eINSTANCE.getImpConfiguration_Project();

		/**
		 * The meta object literal for the '<em><b>Models1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURATION__MODELS1 = eINSTANCE.getImpConfiguration_Models1();

		/**
		 * The meta object literal for the '<em><b>Project1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURATION__PROJECT1 = eINSTANCE.getImpConfiguration_Project1();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl <em>Imp Configured Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpConfiguredElementImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfiguredElement()
		 * @generated
		 */
		EClass IMP_CONFIGURED_ELEMENT = eINSTANCE.getImpConfiguredElement();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURED_ELEMENT__GROUP = eINSTANCE.getImpConfiguredElement_Group();

		/**
		 * The meta object literal for the '<em><b>Imp Configurable Element</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT = eINSTANCE.getImpConfiguredElement_ImpConfigurableElement();

		/**
		 * The meta object literal for the '<em><b>Imp Configuration</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION = eINSTANCE.getImpConfiguredElement_ImpConfiguration();

		/**
		 * The meta object literal for the '<em><b>Configuration For Hook</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURED_ELEMENT__CONFIGURATION_FOR_HOOK = eINSTANCE.getImpConfiguredElement_ConfigurationForHook();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONFIGURED_ELEMENT__EXTENSION = eINSTANCE.getImpConfiguredElement_Extension();

		/**
		 * The meta object literal for the '<em><b>Href</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURED_ELEMENT__HREF = eINSTANCE.getImpConfiguredElement_Href();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURED_ELEMENT__ID = eINSTANCE.getImpConfiguredElement_Id();

		/**
		 * The meta object literal for the '<em><b>Idref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURED_ELEMENT__IDREF = eINSTANCE.getImpConfiguredElement_Idref();

		/**
		 * The meta object literal for the '<em><b>Imp Configurable Element1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURED_ELEMENT__IMP_CONFIGURABLE_ELEMENT1 = eINSTANCE.getImpConfiguredElement_ImpConfigurableElement1();

		/**
		 * The meta object literal for the '<em><b>Imp Configuration1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURED_ELEMENT__IMP_CONFIGURATION1 = eINSTANCE.getImpConfiguredElement_ImpConfiguration1();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURED_ELEMENT__LABEL = eINSTANCE.getImpConfiguredElement_Label();

		/**
		 * The meta object literal for the '<em><b>Selection Information</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURED_ELEMENT__SELECTION_INFORMATION = eINSTANCE.getImpConfiguredElement_SelectionInformation();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURED_ELEMENT__TYPE = eINSTANCE.getImpConfiguredElement_Type();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURED_ELEMENT__UUID = eINSTANCE.getImpConfiguredElement_Uuid();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONFIGURED_ELEMENT__VERSION = eINSTANCE.getImpConfiguredElement_Version();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpConnectorImpl <em>Imp Connector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpConnectorImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConnector()
		 * @generated
		 */
		EClass IMP_CONNECTOR = eINSTANCE.getImpConnector();

		/**
		 * The meta object literal for the '<em><b>Group1</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONNECTOR__GROUP1 = eINSTANCE.getImpConnector_Group1();

		/**
		 * The meta object literal for the '<em><b>Source Port</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONNECTOR__SOURCE_PORT = eINSTANCE.getImpConnector_SourcePort();

		/**
		 * The meta object literal for the '<em><b>Target Port</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_CONNECTOR__TARGET_PORT = eINSTANCE.getImpConnector_TargetPort();

		/**
		 * The meta object literal for the '<em><b>Source Port1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONNECTOR__SOURCE_PORT1 = eINSTANCE.getImpConnector_SourcePort1();

		/**
		 * The meta object literal for the '<em><b>Target Port1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_CONNECTOR__TARGET_PORT1 = eINSTANCE.getImpConnector_TargetPort1();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpExtensibleElementImpl <em>Imp Extensible Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpExtensibleElementImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpExtensibleElement()
		 * @generated
		 */
		EClass IMP_EXTENSIBLE_ELEMENT = eINSTANCE.getImpExtensibleElement();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_EXTENSIBLE_ELEMENT__GROUP = eINSTANCE.getImpExtensibleElement_Group();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_EXTENSIBLE_ELEMENT__EXTENSION = eINSTANCE.getImpExtensibleElement_Extension();

		/**
		 * The meta object literal for the '<em><b>Href</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_EXTENSIBLE_ELEMENT__HREF = eINSTANCE.getImpExtensibleElement_Href();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_EXTENSIBLE_ELEMENT__ID = eINSTANCE.getImpExtensibleElement_Id();

		/**
		 * The meta object literal for the '<em><b>Idref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_EXTENSIBLE_ELEMENT__IDREF = eINSTANCE.getImpExtensibleElement_Idref();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_EXTENSIBLE_ELEMENT__LABEL = eINSTANCE.getImpExtensibleElement_Label();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_EXTENSIBLE_ELEMENT__TYPE = eINSTANCE.getImpExtensibleElement_Type();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_EXTENSIBLE_ELEMENT__UUID = eINSTANCE.getImpExtensibleElement_Uuid();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_EXTENSIBLE_ELEMENT__VERSION = eINSTANCE.getImpExtensibleElement_Version();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpItemImpl <em>Imp Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpItemImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpItem()
		 * @generated
		 */
		EClass IMP_ITEM = eINSTANCE.getImpItem();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_ITEM__DESCRIPTION = eINSTANCE.getImpItem_Description();

		/**
		 * The meta object literal for the '<em><b>Id1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_ITEM__ID1 = eINSTANCE.getImpItem_Id1();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_ITEM__NAME = eINSTANCE.getImpItem_Name();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl <em>Imp Linking Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpLinkingRelationImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpLinkingRelation()
		 * @generated
		 */
		EClass IMP_LINKING_RELATION = eINSTANCE.getImpLinkingRelation();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_LINKING_RELATION__GROUP = eINSTANCE.getImpLinkingRelation_Group();

		/**
		 * The meta object literal for the '<em><b>Right Element</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_LINKING_RELATION__RIGHT_ELEMENT = eINSTANCE.getImpLinkingRelation_RightElement();

		/**
		 * The meta object literal for the '<em><b>Left Element</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_LINKING_RELATION__LEFT_ELEMENT = eINSTANCE.getImpLinkingRelation_LeftElement();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_LINKING_RELATION__EXTENSION = eINSTANCE.getImpLinkingRelation_Extension();

		/**
		 * The meta object literal for the '<em><b>Href</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_LINKING_RELATION__HREF = eINSTANCE.getImpLinkingRelation_Href();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_LINKING_RELATION__ID = eINSTANCE.getImpLinkingRelation_Id();

		/**
		 * The meta object literal for the '<em><b>Idref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_LINKING_RELATION__IDREF = eINSTANCE.getImpLinkingRelation_Idref();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_LINKING_RELATION__LABEL = eINSTANCE.getImpLinkingRelation_Label();

		/**
		 * The meta object literal for the '<em><b>Left Element1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_LINKING_RELATION__LEFT_ELEMENT1 = eINSTANCE.getImpLinkingRelation_LeftElement1();

		/**
		 * The meta object literal for the '<em><b>Linking Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_LINKING_RELATION__LINKING_TYPE = eINSTANCE.getImpLinkingRelation_LinkingType();

		/**
		 * The meta object literal for the '<em><b>Right Element1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_LINKING_RELATION__RIGHT_ELEMENT1 = eINSTANCE.getImpLinkingRelation_RightElement1();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_LINKING_RELATION__TYPE = eINSTANCE.getImpLinkingRelation_Type();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_LINKING_RELATION__UUID = eINSTANCE.getImpLinkingRelation_Uuid();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_LINKING_RELATION__VERSION = eINSTANCE.getImpLinkingRelation_Version();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpModelImpl <em>Imp Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpModelImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpModel()
		 * @generated
		 */
		EClass IMP_MODEL = eINSTANCE.getImpModel();

		/**
		 * The meta object literal for the '<em><b>Group1</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_MODEL__GROUP1 = eINSTANCE.getImpModel_Group1();

		/**
		 * The meta object literal for the '<em><b>Project</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_MODEL__PROJECT = eINSTANCE.getImpModel_Project();

		/**
		 * The meta object literal for the '<em><b>Imp Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_MODEL__IMP_CONFIGURATIONS = eINSTANCE.getImpModel_ImpConfigurations();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_MODEL__CONSTRAINTS = eINSTANCE.getImpModel_Constraints();

		/**
		 * The meta object literal for the '<em><b>Imp Configurations1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_MODEL__IMP_CONFIGURATIONS1 = eINSTANCE.getImpModel_ImpConfigurations1();

		/**
		 * The meta object literal for the '<em><b>Project1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_MODEL__PROJECT1 = eINSTANCE.getImpModel_Project1();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpNodeImpl <em>Imp Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpNodeImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpNode()
		 * @generated
		 */
		EClass IMP_NODE = eINSTANCE.getImpNode();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpPortNodeImpl <em>Imp Port Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpPortNodeImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpPortNode()
		 * @generated
		 */
		EClass IMP_PORT_NODE = eINSTANCE.getImpPortNode();

		/**
		 * The meta object literal for the '<em><b>Group2</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_PORT_NODE__GROUP2 = eINSTANCE.getImpPortNode_Group2();

		/**
		 * The meta object literal for the '<em><b>Connectors For Source Port</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT = eINSTANCE.getImpPortNode_ConnectorsForSourcePort();

		/**
		 * The meta object literal for the '<em><b>Connectors For Target Port</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT = eINSTANCE.getImpPortNode_ConnectorsForTargetPort();

		/**
		 * The meta object literal for the '<em><b>Connectors For Source Port1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_PORT_NODE__CONNECTORS_FOR_SOURCE_PORT1 = eINSTANCE.getImpPortNode_ConnectorsForSourcePort1();

		/**
		 * The meta object literal for the '<em><b>Connectors For Target Port1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_PORT_NODE__CONNECTORS_FOR_TARGET_PORT1 = eINSTANCE.getImpPortNode_ConnectorsForTargetPort1();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpProjectImpl <em>Imp Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpProjectImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpProject()
		 * @generated
		 */
		EClass IMP_PROJECT = eINSTANCE.getImpProject();

		/**
		 * The meta object literal for the '<em><b>Group1</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_PROJECT__GROUP1 = eINSTANCE.getImpProject_Group1();

		/**
		 * The meta object literal for the '<em><b>Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_PROJECT__MODELS = eINSTANCE.getImpProject_Models();

		/**
		 * The meta object literal for the '<em><b>Imp Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_PROJECT__IMP_CONFIGURATIONS = eINSTANCE.getImpProject_ImpConfigurations();

		/**
		 * The meta object literal for the '<em><b>Imp Extensions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_PROJECT__IMP_EXTENSIONS = eINSTANCE.getImpProject_ImpExtensions();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.ImpTermImpl <em>Imp Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.ImpTermImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpTerm()
		 * @generated
		 */
		EClass IMP_TERM = eINSTANCE.getImpTerm();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_TERM__GROUP = eINSTANCE.getImpTerm_Group();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMP_TERM__EXTENSION = eINSTANCE.getImpTerm_Extension();

		/**
		 * The meta object literal for the '<em><b>Href</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_TERM__HREF = eINSTANCE.getImpTerm_Href();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_TERM__ID = eINSTANCE.getImpTerm_Id();

		/**
		 * The meta object literal for the '<em><b>Idref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_TERM__IDREF = eINSTANCE.getImpTerm_Idref();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_TERM__LABEL = eINSTANCE.getImpTerm_Label();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_TERM__TYPE = eINSTANCE.getImpTerm_Type();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_TERM__UUID = eINSTANCE.getImpTerm_Uuid();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMP_TERM__VERSION = eINSTANCE.getImpTerm_Version();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.NamedItemImpl <em>Named Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.NamedItemImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getNamedItem()
		 * @generated
		 */
		EClass NAMED_ITEM = eINSTANCE.getNamedItem();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ITEM__GROUP = eINSTANCE.getNamedItem_Group();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMED_ITEM__EXTENSION = eINSTANCE.getNamedItem_Extension();

		/**
		 * The meta object literal for the '<em><b>Href</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ITEM__HREF = eINSTANCE.getNamedItem_Href();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ITEM__ID = eINSTANCE.getNamedItem_Id();

		/**
		 * The meta object literal for the '<em><b>Idref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ITEM__IDREF = eINSTANCE.getNamedItem_Idref();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ITEM__LABEL = eINSTANCE.getNamedItem_Label();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ITEM__NAME = eINSTANCE.getNamedItem_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ITEM__TYPE = eINSTANCE.getNamedItem_Type();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ITEM__UUID = eINSTANCE.getNamedItem_Uuid();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ITEM__VERSION = eINSTANCE.getNamedItem_Version();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.PropertyImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__GROUP = eINSTANCE.getProperty_Group();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY__EXTENSION = eINSTANCE.getProperty_Extension();

		/**
		 * The meta object literal for the '<em><b>Href</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__HREF = eINSTANCE.getProperty_Href();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__ID = eINSTANCE.getProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Idref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__IDREF = eINSTANCE.getProperty_Idref();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__KEY = eINSTANCE.getProperty_Key();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__LABEL = eINSTANCE.getProperty_Label();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__TYPE = eINSTANCE.getProperty_Type();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__UUID = eINSTANCE.getProperty_Uuid();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__VALUE = eINSTANCE.getProperty_Value();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__VERSION = eINSTANCE.getProperty_Version();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.PropertySetImpl <em>Property Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.PropertySetImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getPropertySet()
		 * @generated
		 */
		EClass PROPERTY_SET = eINSTANCE.getPropertySet();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_SET__GROUP = eINSTANCE.getPropertySet_Group();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_SET__PROPERTIES = eINSTANCE.getPropertySet_Properties();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_SET__EXTENSION = eINSTANCE.getPropertySet_Extension();

		/**
		 * The meta object literal for the '<em><b>Href</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_SET__HREF = eINSTANCE.getPropertySet_Href();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_SET__ID = eINSTANCE.getPropertySet_Id();

		/**
		 * The meta object literal for the '<em><b>Idref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_SET__IDREF = eINSTANCE.getPropertySet_Idref();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_SET__LABEL = eINSTANCE.getPropertySet_Label();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_SET__TYPE = eINSTANCE.getPropertySet_Type();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_SET__UUID = eINSTANCE.getPropertySet_Uuid();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY_SET__VERSION = eINSTANCE.getPropertySet_Version();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.impl.DocumentRootImpl
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Constraint</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__CONSTRAINT = eINSTANCE.getDocumentRoot_Constraint();

		/**
		 * The meta object literal for the '<em><b>Described Item</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__DESCRIBED_ITEM = eINSTANCE.getDocumentRoot_DescribedItem();

		/**
		 * The meta object literal for the '<em><b>Identifiable</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IDENTIFIABLE = eINSTANCE.getDocumentRoot_Identifiable();

		/**
		 * The meta object literal for the '<em><b>Imp Configurable Element</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT = eINSTANCE.getDocumentRoot_ImpConfigurableElement();

		/**
		 * The meta object literal for the '<em><b>Imp Configurable Element Extension</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_CONFIGURABLE_ELEMENT_EXTENSION = eINSTANCE.getDocumentRoot_ImpConfigurableElementExtension();

		/**
		 * The meta object literal for the '<em><b>Imp Configurable Group</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_CONFIGURABLE_GROUP = eINSTANCE.getDocumentRoot_ImpConfigurableGroup();

		/**
		 * The meta object literal for the '<em><b>Imp Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_CONFIGURATION = eINSTANCE.getDocumentRoot_ImpConfiguration();

		/**
		 * The meta object literal for the '<em><b>Imp Configured Element</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_CONFIGURED_ELEMENT = eINSTANCE.getDocumentRoot_ImpConfiguredElement();

		/**
		 * The meta object literal for the '<em><b>Imp Connector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_CONNECTOR = eINSTANCE.getDocumentRoot_ImpConnector();

		/**
		 * The meta object literal for the '<em><b>Imp Extensible Element</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_EXTENSIBLE_ELEMENT = eINSTANCE.getDocumentRoot_ImpExtensibleElement();

		/**
		 * The meta object literal for the '<em><b>Imp Item</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_ITEM = eINSTANCE.getDocumentRoot_ImpItem();

		/**
		 * The meta object literal for the '<em><b>Imp Linking Relation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_LINKING_RELATION = eINSTANCE.getDocumentRoot_ImpLinkingRelation();

		/**
		 * The meta object literal for the '<em><b>Imp Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_MODEL = eINSTANCE.getDocumentRoot_ImpModel();

		/**
		 * The meta object literal for the '<em><b>Imp Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_NODE = eINSTANCE.getDocumentRoot_ImpNode();

		/**
		 * The meta object literal for the '<em><b>Imp Port Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_PORT_NODE = eINSTANCE.getDocumentRoot_ImpPortNode();

		/**
		 * The meta object literal for the '<em><b>Imp Project</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_PROJECT = eINSTANCE.getDocumentRoot_ImpProject();

		/**
		 * The meta object literal for the '<em><b>Imp Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__IMP_TERM = eINSTANCE.getDocumentRoot_ImpTerm();

		/**
		 * The meta object literal for the '<em><b>Named Item</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__NAMED_ITEM = eINSTANCE.getDocumentRoot_NamedItem();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__PROPERTY = eINSTANCE.getDocumentRoot_Property();

		/**
		 * The meta object literal for the '<em><b>Property Set</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__PROPERTY_SET = eINSTANCE.getDocumentRoot_PropertySet();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.ImpConfigurableGroupType <em>Imp Configurable Group Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.ImpConfigurableGroupType
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfigurableGroupType()
		 * @generated
		 */
		EEnum IMP_CONFIGURABLE_GROUP_TYPE = eINSTANCE.getImpConfigurableGroupType();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.LinkingType <em>Linking Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.LinkingType
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getLinkingType()
		 * @generated
		 */
		EEnum LINKING_TYPE = eINSTANCE.getLinkingType();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.PortDirection <em>Port Direction</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.PortDirection
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getPortDirection()
		 * @generated
		 */
		EEnum PORT_DIRECTION = eINSTANCE.getPortDirection();

		/**
		 * The meta object literal for the '{@link com.prostep.vcontrol.model.common.SelectionKind <em>Selection Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.SelectionKind
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getSelectionKind()
		 * @generated
		 */
		EEnum SELECTION_KIND = eINSTANCE.getSelectionKind();

		/**
		 * The meta object literal for the '<em>Imp Configurable Group Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.ImpConfigurableGroupType
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getImpConfigurableGroupTypeObject()
		 * @generated
		 */
		EDataType IMP_CONFIGURABLE_GROUP_TYPE_OBJECT = eINSTANCE.getImpConfigurableGroupTypeObject();

		/**
		 * The meta object literal for the '<em>Linking Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.LinkingType
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getLinkingTypeObject()
		 * @generated
		 */
		EDataType LINKING_TYPE_OBJECT = eINSTANCE.getLinkingTypeObject();

		/**
		 * The meta object literal for the '<em>Port Direction Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.PortDirection
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getPortDirectionObject()
		 * @generated
		 */
		EDataType PORT_DIRECTION_OBJECT = eINSTANCE.getPortDirectionObject();

		/**
		 * The meta object literal for the '<em>Selection Kind Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.prostep.vcontrol.model.common.SelectionKind
		 * @see com.prostep.vcontrol.model.common.impl.CommonPackageImpl#getSelectionKindObject()
		 * @generated
		 */
		EDataType SELECTION_KIND_OBJECT = eINSTANCE.getSelectionKindObject();

	}

} //CommonPackage
