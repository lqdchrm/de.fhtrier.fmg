/**
 */
package com.prostep.vcontrol.model.feature;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getFeature()
 * @model extendedMetaData="name='Feature' kind='elementOnly'"
 * @generated
 */
public interface Feature extends FeatureNode {
} // Feature
