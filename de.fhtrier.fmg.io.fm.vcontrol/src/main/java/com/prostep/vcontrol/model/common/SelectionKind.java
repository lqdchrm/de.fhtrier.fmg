/**
 */
package com.prostep.vcontrol.model.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.AbstractEnumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Selection Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.common.CommonPackage#getSelectionKind()
 * @model extendedMetaData="name='SelectionKind'"
 * @generated
 */
public final class SelectionKind extends AbstractEnumerator {
	/**
	 * The '<em><b>NOTYETDECIDED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NOTYETDECIDED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOTYETDECIDED_LITERAL
	 * @model literal="NOT_YET_DECIDED"
	 * @generated
	 * @ordered
	 */
	public static final int NOTYETDECIDED = 0;

	/**
	 * The '<em><b>SELECTEDEXPLICIT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SELECTEDEXPLICIT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SELECTEDEXPLICIT_LITERAL
	 * @model literal="SELECTED_EXPLICIT"
	 * @generated
	 * @ordered
	 */
	public static final int SELECTEDEXPLICIT = 1;

	/**
	 * The '<em><b>SELECTEDDERIVED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SELECTEDDERIVED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SELECTEDDERIVED_LITERAL
	 * @model literal="SELECTED_DERIVED"
	 * @generated
	 * @ordered
	 */
	public static final int SELECTEDDERIVED = 2;

	/**
	 * The '<em><b>DESELECTEDEXPLICIT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DESELECTEDEXPLICIT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DESELECTEDEXPLICIT_LITERAL
	 * @model literal="DESELECTED_EXPLICIT"
	 * @generated
	 * @ordered
	 */
	public static final int DESELECTEDEXPLICIT = 3;

	/**
	 * The '<em><b>DESELECTEDDERIVED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DESELECTEDDERIVED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DESELECTEDDERIVED_LITERAL
	 * @model literal="DESELECTED_DERIVED"
	 * @generated
	 * @ordered
	 */
	public static final int DESELECTEDDERIVED = 4;

	/**
	 * The '<em><b>MANDATORYELEMENT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MANDATORYELEMENT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MANDATORYELEMENT_LITERAL
	 * @model literal="MANDATORY_ELEMENT"
	 * @generated
	 * @ordered
	 */
	public static final int MANDATORYELEMENT = 5;

	/**
	 * The '<em><b>NOTYETDECIDED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOTYETDECIDED
	 * @generated
	 * @ordered
	 */
	public static final SelectionKind NOTYETDECIDED_LITERAL = new SelectionKind(NOTYETDECIDED, "NOTYETDECIDED", "NOT_YET_DECIDED");

	/**
	 * The '<em><b>SELECTEDEXPLICIT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SELECTEDEXPLICIT
	 * @generated
	 * @ordered
	 */
	public static final SelectionKind SELECTEDEXPLICIT_LITERAL = new SelectionKind(SELECTEDEXPLICIT, "SELECTEDEXPLICIT", "SELECTED_EXPLICIT");

	/**
	 * The '<em><b>SELECTEDDERIVED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SELECTEDDERIVED
	 * @generated
	 * @ordered
	 */
	public static final SelectionKind SELECTEDDERIVED_LITERAL = new SelectionKind(SELECTEDDERIVED, "SELECTEDDERIVED", "SELECTED_DERIVED");

	/**
	 * The '<em><b>DESELECTEDEXPLICIT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DESELECTEDEXPLICIT
	 * @generated
	 * @ordered
	 */
	public static final SelectionKind DESELECTEDEXPLICIT_LITERAL = new SelectionKind(DESELECTEDEXPLICIT, "DESELECTEDEXPLICIT", "DESELECTED_EXPLICIT");

	/**
	 * The '<em><b>DESELECTEDDERIVED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DESELECTEDDERIVED
	 * @generated
	 * @ordered
	 */
	public static final SelectionKind DESELECTEDDERIVED_LITERAL = new SelectionKind(DESELECTEDDERIVED, "DESELECTEDDERIVED", "DESELECTED_DERIVED");

	/**
	 * The '<em><b>MANDATORYELEMENT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MANDATORYELEMENT
	 * @generated
	 * @ordered
	 */
	public static final SelectionKind MANDATORYELEMENT_LITERAL = new SelectionKind(MANDATORYELEMENT, "MANDATORYELEMENT", "MANDATORY_ELEMENT");

	/**
	 * An array of all the '<em><b>Selection Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final SelectionKind[] VALUES_ARRAY =
		new SelectionKind[] {
			NOTYETDECIDED_LITERAL,
			SELECTEDEXPLICIT_LITERAL,
			SELECTEDDERIVED_LITERAL,
			DESELECTEDEXPLICIT_LITERAL,
			DESELECTEDDERIVED_LITERAL,
			MANDATORYELEMENT_LITERAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Selection Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Selection Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SelectionKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SelectionKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Selection Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SelectionKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SelectionKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Selection Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SelectionKind get(int value) {
		switch (value) {
			case NOTYETDECIDED: return NOTYETDECIDED_LITERAL;
			case SELECTEDEXPLICIT: return SELECTEDEXPLICIT_LITERAL;
			case SELECTEDDERIVED: return SELECTEDDERIVED_LITERAL;
			case DESELECTEDEXPLICIT: return DESELECTEDEXPLICIT_LITERAL;
			case DESELECTEDDERIVED: return DESELECTEDDERIVED_LITERAL;
			case MANDATORYELEMENT: return MANDATORYELEMENT_LITERAL;
		}
		return null;
	}

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private SelectionKind(int value, String name, String literal) {
		super(value, name, literal);
	}

} //SelectionKind
