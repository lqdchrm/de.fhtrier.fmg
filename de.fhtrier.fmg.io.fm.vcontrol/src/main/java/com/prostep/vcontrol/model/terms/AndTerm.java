/**
 */
package com.prostep.vcontrol.model.terms;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>And Term</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.prostep.vcontrol.model.terms.TermsPackage#getAndTerm()
 * @model extendedMetaData="name='AndTerm' kind='elementOnly'"
 * @generated
 */
public interface AndTerm extends NaryTerm {
} // AndTerm
