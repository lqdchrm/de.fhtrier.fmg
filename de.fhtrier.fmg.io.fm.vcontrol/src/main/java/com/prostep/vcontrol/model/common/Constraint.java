/**
 */
package com.prostep.vcontrol.model.common;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.common.Constraint#getGroup1 <em>Group1</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.Constraint#getRootTerm <em>Root Term</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.Constraint#getModel <em>Model</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.Constraint#isIgnore <em>Ignore</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.common.Constraint#getModel1 <em>Model1</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.common.CommonPackage#getConstraint()
 * @model extendedMetaData="name='Constraint' kind='elementOnly'"
 * @generated
 */
public interface Constraint extends ImpItem {
	/**
	 * Returns the value of the '<em><b>Group1</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group1</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group1</em>' attribute list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getConstraint_Group1()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:13'"
	 * @generated
	 */
	FeatureMap getGroup1();

	/**
	 * Returns the value of the '<em><b>Root Term</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpTerm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Term</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Term</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getConstraint_RootTerm()
	 * @model type="com.prostep.vcontrol.model.common.ImpTerm" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='rootTerm' group='#group:13'"
	 * @generated
	 */
	EList getRootTerm();

	/**
	 * Returns the value of the '<em><b>Model</b></em>' containment reference list.
	 * The list contents are of type {@link com.prostep.vcontrol.model.common.ImpModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model</em>' containment reference list.
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getConstraint_Model()
	 * @model type="com.prostep.vcontrol.model.common.ImpModel" containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model' group='#group:13'"
	 * @generated
	 */
	EList getModel();

	/**
	 * Returns the value of the '<em><b>Ignore</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ignore</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ignore</em>' attribute.
	 * @see #isSetIgnore()
	 * @see #unsetIgnore()
	 * @see #setIgnore(boolean)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getConstraint_Ignore()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean" required="true"
	 *        extendedMetaData="kind='attribute' name='ignore'"
	 * @generated
	 */
	boolean isIgnore();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.Constraint#isIgnore <em>Ignore</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ignore</em>' attribute.
	 * @see #isSetIgnore()
	 * @see #unsetIgnore()
	 * @see #isIgnore()
	 * @generated
	 */
	void setIgnore(boolean value);

	/**
	 * Unsets the value of the '{@link com.prostep.vcontrol.model.common.Constraint#isIgnore <em>Ignore</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIgnore()
	 * @see #isIgnore()
	 * @see #setIgnore(boolean)
	 * @generated
	 */
	void unsetIgnore();

	/**
	 * Returns whether the value of the '{@link com.prostep.vcontrol.model.common.Constraint#isIgnore <em>Ignore</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Ignore</em>' attribute is set.
	 * @see #unsetIgnore()
	 * @see #isIgnore()
	 * @see #setIgnore(boolean)
	 * @generated
	 */
	boolean isSetIgnore();

	/**
	 * Returns the value of the '<em><b>Model1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model1</em>' attribute.
	 * @see #setModel1(String)
	 * @see com.prostep.vcontrol.model.common.CommonPackage#getConstraint_Model1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='model'"
	 * @generated
	 */
	String getModel1();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.common.Constraint#getModel1 <em>Model1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model1</em>' attribute.
	 * @see #getModel1()
	 * @generated
	 */
	void setModel1(String value);

} // Constraint
