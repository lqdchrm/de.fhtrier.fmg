/**
 */
package com.prostep.vcontrol.model.terms.impl;

import com.prostep.vcontrol.model.terms.NotTerm;
import com.prostep.vcontrol.model.terms.TermsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Not Term</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class NotTermImpl extends UnaryTermImpl implements NotTerm {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NotTermImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return TermsPackage.Literals.NOT_TERM;
	}

} //NotTermImpl
