/**
 */
package com.prostep.vcontrol.model.feature;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.prostep.vcontrol.model.feature.DocumentRoot#getMixed <em>Mixed</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeature <em>Feature</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureGroup <em>Feature Group</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureModel <em>Feature Model</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureNode <em>Feature Node</em>}</li>
 *   <li>{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureOccurrence <em>Feature Occurrence</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getDocumentRoot()
 * @model extendedMetaData="name='' kind='mixed'"
 * @generated
 */
public interface DocumentRoot extends EObject {
	/**
	 * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mixed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mixed</em>' attribute list.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getDocumentRoot_Mixed()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' name=':mixed'"
	 * @generated
	 */
	FeatureMap getMixed();

	/**
	 * Returns the value of the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XMLNS Prefix Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XMLNS Prefix Map</em>' map.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getDocumentRoot_XMLNSPrefixMap()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry" keyType="java.lang.String" valueType="java.lang.String" transient="true"
	 *        extendedMetaData="kind='attribute' name='xmlns:prefix'"
	 * @generated
	 */
	EMap getXMLNSPrefixMap();

	/**
	 * Returns the value of the '<em><b>XSI Schema Location</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XSI Schema Location</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XSI Schema Location</em>' map.
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getDocumentRoot_XSISchemaLocation()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry" keyType="java.lang.String" valueType="java.lang.String" transient="true"
	 *        extendedMetaData="kind='attribute' name='xsi:schemaLocation'"
	 * @generated
	 */
	EMap getXSISchemaLocation();

	/**
	 * Returns the value of the '<em><b>Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' containment reference.
	 * @see #setFeature(Feature)
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getDocumentRoot_Feature()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Feature' namespace='##targetNamespace'"
	 * @generated
	 */
	Feature getFeature();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeature <em>Feature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' containment reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(Feature value);

	/**
	 * Returns the value of the '<em><b>Feature Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Group</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Group</em>' containment reference.
	 * @see #setFeatureGroup(FeatureGroup)
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getDocumentRoot_FeatureGroup()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='FeatureGroup' namespace='##targetNamespace'"
	 * @generated
	 */
	FeatureGroup getFeatureGroup();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureGroup <em>Feature Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Group</em>' containment reference.
	 * @see #getFeatureGroup()
	 * @generated
	 */
	void setFeatureGroup(FeatureGroup value);

	/**
	 * Returns the value of the '<em><b>Feature Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Model</em>' containment reference.
	 * @see #setFeatureModel(FeatureModel)
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getDocumentRoot_FeatureModel()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='FeatureModel' namespace='##targetNamespace'"
	 * @generated
	 */
	FeatureModel getFeatureModel();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureModel <em>Feature Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Model</em>' containment reference.
	 * @see #getFeatureModel()
	 * @generated
	 */
	void setFeatureModel(FeatureModel value);

	/**
	 * Returns the value of the '<em><b>Feature Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Node</em>' containment reference.
	 * @see #setFeatureNode(FeatureNode)
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getDocumentRoot_FeatureNode()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='FeatureNode' namespace='##targetNamespace'"
	 * @generated
	 */
	FeatureNode getFeatureNode();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureNode <em>Feature Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Node</em>' containment reference.
	 * @see #getFeatureNode()
	 * @generated
	 */
	void setFeatureNode(FeatureNode value);

	/**
	 * Returns the value of the '<em><b>Feature Occurrence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Occurrence</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Occurrence</em>' containment reference.
	 * @see #setFeatureOccurrence(FeatureOccurrence)
	 * @see com.prostep.vcontrol.model.feature.FeaturePackage#getDocumentRoot_FeatureOccurrence()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='FeatureOccurrence' namespace='##targetNamespace'"
	 * @generated
	 */
	FeatureOccurrence getFeatureOccurrence();

	/**
	 * Sets the value of the '{@link com.prostep.vcontrol.model.feature.DocumentRoot#getFeatureOccurrence <em>Feature Occurrence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Occurrence</em>' containment reference.
	 * @see #getFeatureOccurrence()
	 * @generated
	 */
	void setFeatureOccurrence(FeatureOccurrence value);

} // DocumentRoot
