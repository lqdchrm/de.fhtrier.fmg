/**
 */
package com.prostep.vcontrol.model.common.util;

import com.prostep.vcontrol.model.common.*;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.prostep.vcontrol.model.common.CommonPackage
 * @generated
 */
public class CommonSwitch {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CommonPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommonSwitch() {
		if (modelPackage == null) {
			modelPackage = CommonPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public Object doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch((EClass)eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case CommonPackage.CONSTRAINT: {
				Constraint constraint = (Constraint)theEObject;
				Object result = caseConstraint(constraint);
				if (result == null) result = caseImpItem(constraint);
				if (result == null) result = casePropertySet(constraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.DESCRIBED_ITEM: {
				DescribedItem describedItem = (DescribedItem)theEObject;
				Object result = caseDescribedItem(describedItem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IDENTIFIABLE: {
				Identifiable identifiable = (Identifiable)theEObject;
				Object result = caseIdentifiable(identifiable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT: {
				ImpConfigurableElement impConfigurableElement = (ImpConfigurableElement)theEObject;
				Object result = caseImpConfigurableElement(impConfigurableElement);
				if (result == null) result = caseImpItem(impConfigurableElement);
				if (result == null) result = casePropertySet(impConfigurableElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_CONFIGURABLE_ELEMENT_EXTENSION: {
				ImpConfigurableElementExtension impConfigurableElementExtension = (ImpConfigurableElementExtension)theEObject;
				Object result = caseImpConfigurableElementExtension(impConfigurableElementExtension);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_CONFIGURABLE_GROUP: {
				ImpConfigurableGroup impConfigurableGroup = (ImpConfigurableGroup)theEObject;
				Object result = caseImpConfigurableGroup(impConfigurableGroup);
				if (result == null) result = caseImpConfigurableElement(impConfigurableGroup);
				if (result == null) result = caseImpItem(impConfigurableGroup);
				if (result == null) result = casePropertySet(impConfigurableGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_CONFIGURATION: {
				ImpConfiguration impConfiguration = (ImpConfiguration)theEObject;
				Object result = caseImpConfiguration(impConfiguration);
				if (result == null) result = caseImpItem(impConfiguration);
				if (result == null) result = casePropertySet(impConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_CONFIGURED_ELEMENT: {
				ImpConfiguredElement impConfiguredElement = (ImpConfiguredElement)theEObject;
				Object result = caseImpConfiguredElement(impConfiguredElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_CONNECTOR: {
				ImpConnector impConnector = (ImpConnector)theEObject;
				Object result = caseImpConnector(impConnector);
				if (result == null) result = caseImpItem(impConnector);
				if (result == null) result = casePropertySet(impConnector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_EXTENSIBLE_ELEMENT: {
				ImpExtensibleElement impExtensibleElement = (ImpExtensibleElement)theEObject;
				Object result = caseImpExtensibleElement(impExtensibleElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_ITEM: {
				ImpItem impItem = (ImpItem)theEObject;
				Object result = caseImpItem(impItem);
				if (result == null) result = casePropertySet(impItem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_LINKING_RELATION: {
				ImpLinkingRelation impLinkingRelation = (ImpLinkingRelation)theEObject;
				Object result = caseImpLinkingRelation(impLinkingRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_MODEL: {
				ImpModel impModel = (ImpModel)theEObject;
				Object result = caseImpModel(impModel);
				if (result == null) result = caseImpItem(impModel);
				if (result == null) result = casePropertySet(impModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_NODE: {
				ImpNode impNode = (ImpNode)theEObject;
				Object result = caseImpNode(impNode);
				if (result == null) result = caseImpConfigurableElement(impNode);
				if (result == null) result = caseImpItem(impNode);
				if (result == null) result = casePropertySet(impNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_PORT_NODE: {
				ImpPortNode impPortNode = (ImpPortNode)theEObject;
				Object result = caseImpPortNode(impPortNode);
				if (result == null) result = caseImpConfigurableElement(impPortNode);
				if (result == null) result = caseImpItem(impPortNode);
				if (result == null) result = casePropertySet(impPortNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_PROJECT: {
				ImpProject impProject = (ImpProject)theEObject;
				Object result = caseImpProject(impProject);
				if (result == null) result = caseImpItem(impProject);
				if (result == null) result = casePropertySet(impProject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.IMP_TERM: {
				ImpTerm impTerm = (ImpTerm)theEObject;
				Object result = caseImpTerm(impTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.NAMED_ITEM: {
				NamedItem namedItem = (NamedItem)theEObject;
				Object result = caseNamedItem(namedItem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.PROPERTY: {
				Property property = (Property)theEObject;
				Object result = caseProperty(property);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.PROPERTY_SET: {
				PropertySet propertySet = (PropertySet)theEObject;
				Object result = casePropertySet(propertySet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CommonPackage.DOCUMENT_ROOT: {
				DocumentRoot documentRoot = (DocumentRoot)theEObject;
				Object result = caseDocumentRoot(documentRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseConstraint(Constraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Described Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Described Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDescribedItem(DescribedItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identifiable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identifiable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseIdentifiable(Identifiable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Configurable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Configurable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpConfigurableElement(ImpConfigurableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Configurable Element Extension</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Configurable Element Extension</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpConfigurableElementExtension(ImpConfigurableElementExtension object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Configurable Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Configurable Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpConfigurableGroup(ImpConfigurableGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpConfiguration(ImpConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Configured Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Configured Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpConfiguredElement(ImpConfiguredElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Connector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Connector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpConnector(ImpConnector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Extensible Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Extensible Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpExtensibleElement(ImpExtensibleElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpItem(ImpItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Linking Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Linking Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpLinkingRelation(ImpLinkingRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpModel(ImpModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpNode(ImpNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Port Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Port Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpPortNode(ImpPortNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Project</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Project</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpProject(ImpProject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imp Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imp Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseImpTerm(ImpTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseNamedItem(NamedItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseProperty(Property object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object casePropertySet(PropertySet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseDocumentRoot(DocumentRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public Object defaultCase(EObject object) {
		return null;
	}

} //CommonSwitch
