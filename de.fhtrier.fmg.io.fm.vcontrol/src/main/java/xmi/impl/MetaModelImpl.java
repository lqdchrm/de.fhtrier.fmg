/**
 */
package xmi.impl;

import org.eclipse.emf.ecore.EClass;

import xmi.MetaModel;
import xmi.XmiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Meta Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class MetaModelImpl extends PackageReferenceImpl implements MetaModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetaModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return XmiPackage.Literals.META_MODEL;
	}

} //MetaModelImpl
