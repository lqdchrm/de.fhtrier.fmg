/**
 */
package xmi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see xmi.XmiPackage#getMetaModel()
 * @model extendedMetaData="name='MetaModel' kind='elementOnly'"
 * @generated
 */
public interface MetaModel extends PackageReference {
} // MetaModel
