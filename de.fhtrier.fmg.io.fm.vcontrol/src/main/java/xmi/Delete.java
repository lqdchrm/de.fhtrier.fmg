/**
 */
package xmi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Delete</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see xmi.XmiPackage#getDelete()
 * @model extendedMetaData="name='Delete' kind='elementOnly'"
 * @generated
 */
public interface Delete extends Difference {
} // Delete
