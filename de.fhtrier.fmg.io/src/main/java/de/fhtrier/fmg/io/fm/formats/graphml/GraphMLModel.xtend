package de.fhtrier.fmg.io.fm.formats.graphml

import de.fhtrier.fmg.core.fm.IFeatureModel
import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.RelationType

class GraphMLModel {
	
	String graphML
	String tree
	
	def static from(IFeatureModel model, String name) {
		new GraphMLModel(model, name)
	}
		
	new(IFeatureModel model, String name) {
		graphML = buildGraphMLRep(model, name).toString
		tree = buildTreeRep(model, name).toString
	}	

	def private buildGraphMLRep(IFeatureModel model, String name) '''
		«buildHeader(name)»

		«buildNodes(model)»
		
		«buildEdges(model)»
		
		«buildCTCs(model)»
		
		«buildFooter»
	'''

	def private buildHeader(String name) '''
		<?xml version="1.0" encoding="UTF-8"?>
		<!-- «name» -->
		<!-- A feature model generated via FMG@FH-Trier -->
		<graphml xmlns="http://graphml.graphdrawing.org/xmlns">
		<graph edgedefault="undirected">

		<!-- data schema -->
		<key id="name" for="node" attr.name="name" attr.type="string"/>
		<key id="nodetype" for="node" attr.name="nodetype" attr.type="string"/>
		<key id="optional" for="node" attr.name="optional" attr.type="boolean"/>
		<key id="grouptype" for="node" attr.name="grouptype" attr.type="string"/>

		<key id="edgetype" for="edge" attr.name="edgetype" attr.type="string"/>
		<key id="constrainttype" for="edge" attr.name="constrainttype" attr.type="string"/>
	'''
	
	def private buildFooter() '''
		</graph>
		</graphml>
	'''
	
	/**
	 * returns as string whether the feature is a single feature or a feature group.
	 * 
	 * a feature group is a feature, which has only outgoing relations of type 'or' or
	 * 'xor' 
	 */
	def private nodetype(IFeature f) {
		var result = 'featureGroup'
		if (f.children.head == null || !f.children.forall([r | (r.type == RelationType::Or || r.type==RelationType::Xor)]))
			result = 'feature'
		return result
	}
	
	/**
	 * returns as string whether the feature is optional, i.e. its incoming relation type is of:
	 * 
	 * mandatory -> false
	 * optional -> true
	 * xor -> false
	 * or -> false
	 * 
	 * the root feature is always marked as mandatory
	 */
	def private optional(IFeature f) {
		if (f.parent !=null && f.parent.type != RelationType::Mandatory)
			return true
		return false
	} 
	
	def private grouptype(IFeature f) {
		switch(f.children.head.type) {
			case RelationType::Or: return 'or'
			case RelationType::Xor: return 'alternative'			
		}
		throw new IllegalArgumentException("Feature has Relationtype " + f.children.head)
	}
	
	def private buildNodes(IFeatureModel model) '''
		<!-- nodes -->
		«FOR f : model.features»
			<node id="«f.name»">
				<data key="name">«f.name»</data>
				<data key="nodetype">«f.nodetype»</data>
				<data key="optional">«f.optional»</data>
				«IF f.nodetype.equals('featureGroup')»
					<data key="grouptype">«f.grouptype»</data>
				«ENDIF»
			</node>
		«ENDFOR»
	'''

	def private buildEdges(IFeatureModel model) '''
		<!-- edges -->
		«FOR f : model.features»
			«IF f.children.size > 0»
				<!-- «f.name» to -->
			«ENDIF»
			«FOR r : f.children»
				«FOR subF : r.destinations»
					<edge source="«f.name»" target="«subF.name»">
						<data key="edgetype">tree</data>
					</edge>
				«ENDFOR»
			«ENDFOR»
		«ENDFOR»
	'''
	
	def private buildCTCs(IFeatureModel model) '''
		<!-- Cross Tree Constraints -->
		«FOR ctc : model.crossTreeConstraints»
			<edge source="«ctc.origin.name»" target="«ctc.destination.name»">
				<data key="edgetype">constraint</data>
				<data key="constrainttype">«ctc.type.toString.toLowerCase»</data>
			</edge>
		«ENDFOR»
	'''

	def private buildTreeRep(IFeatureModel model, String name) '''
	<tree>
		<declarations>
			<attributeDecl name="name" type="String"/>
			<attributeDecl name="nodetype" type="String"/>
			<attributeDecl name="optional" type="String"/>
			<attributeDecl name="grouptype" type="String"/>		
		</declarations>
		«model.root.buildNode»
	</tree>
	'''

	def private CharSequence buildNode(IFeature f) '''
	«IF f.children.size >0»
		<branch>
	«ELSE»
		<leaf>
	«ENDIF»
		<attribute name="name" value="«f.name»"/>
		<attribute name="nodetype" value="«f.nodetype»"/>
		<attribute name="optional" value="«f.optional»"/>
		«IF f.nodetype.equals('featureGroup')»
			<attribute name="grouptype" value="«f.grouptype»"/>
		«ENDIF»
		«FOR r : f.children»
			«FOR subF : r.destinations»
				«subF.buildNode»
			«ENDFOR»
		«ENDFOR»
	«IF f.children.size >0»
		</branch>
	«ELSE»
		</leaf>
	«ENDIF»
	'''

	def GraphMLtoString() {
		return graphML
	}
	
	def TreetoString() {
		return tree
	}
}