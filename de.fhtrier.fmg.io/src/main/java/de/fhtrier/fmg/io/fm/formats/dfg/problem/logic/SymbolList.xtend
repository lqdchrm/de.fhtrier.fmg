package de.fhtrier.fmg.io.fm.formats.dfg.problem.logic

class SymbolList {

	public Predicates predicates
	
	new() { predicates = new Predicates() }
		
	override toString() { '''
		list_of_symbols.
		«predicates»
		end_of_list.
	'''.toString
	}
}