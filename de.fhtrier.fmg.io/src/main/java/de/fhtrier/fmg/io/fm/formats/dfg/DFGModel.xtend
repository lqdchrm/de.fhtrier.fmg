package de.fhtrier.fmg.io.fm.formats.dfg

import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.IFeatureModel
import de.fhtrier.fmg.core.fm.IRelation
import de.fhtrier.fmg.core.fm.RelationType
import de.fhtrier.fmg.core.fm.ICrossTreeConstraint
import de.fhtrier.fmg.core.fm.CrossTreeConstraintType
import de.fhtrier.fmg.io.fm.formats.dfg.problem.logic.Formula
import de.fhtrier.fmg.io.fm.formats.dfg.problem.logic.Predicate
import de.fhtrier.fmg.logic.impl.nodes.And
import de.fhtrier.fmg.logic.impl.nodes.Equal
import de.fhtrier.fmg.logic.impl.nodes.Implies
import de.fhtrier.fmg.logic.impl.nodes.Literal
import de.fhtrier.fmg.logic.impl.nodes.Not
import de.fhtrier.fmg.logic.impl.nodes.Or
import de.fhtrier.fmg.logic.INode


class DFGModel {
	Problem problem
	
	new(String name) {
		this.problem = new Problem(name)
	}
		
	def static from(IFeatureModel model, String name) {
		
		val dfg = new DFGModel(name)
			
		val clauses = model.features.filter[f | f.getChildren.size > 0].map[g | dfg.createLogic(g)]
		val ctcs = model.crossTreeConstraints.map[g | dfg.createLogic(g)]

		// add predicates				
		model.features.forEach[dfg.problem.symbols.predicates.items.add(new Predicate(it))]
		
		// add formulas
		// root
		dfg.problem.formulas.items.add(new Formula(new Equal(newArrayList(dfg.transform(model.root), new Literal("true"))).toString))		
		
		// relations
		clauses.forEach[dfg.problem.formulas.items.add(new Formula(it.toString))]
			
		// ctcs
		ctcs.forEach[dfg.problem.formulas.items.add(new Formula(it.toString))]	
			
		return dfg
	}
		
	def create result : new Literal(f.name) transform(IFeature f) {
	}
		
	override toString() { problem.toString }
		
	def INode createLogic(ICrossTreeConstraint ctc) {
		switch(ctc.type) {
			case CrossTreeConstraintType::Requires:
				new Implies(transform(ctc.origin), transform(ctc.destination))
			case CrossTreeConstraintType::Excludes:
				new Not(new And(newArrayList(transform(ctc.origin), transform(ctc.destination))))
			default:
				null
		}
	}	
	
	def INode createLogic(IFeature f) {
		if (f.getChildren.size > 1) {
			new And(f.getChildren.map[createNode(it)])
		} else {
			createNode(f.getChildren.head)
		}
	}
	
	def INode createNode(IRelation r) {
		switch(r.type) {
			case RelationType::Mandatory:
				new Equal(newArrayList(transform(r.getOrigin), transform(r.destinations.head)))
			case RelationType::Optional:
				new Implies(transform(r.destinations.head), transform(r.getOrigin))
			case RelationType::Or:
				new Equal(newArrayList(transform(r.getOrigin), new Or(r.destinations.map[transform(it)])))
			case RelationType::Xor:
				new And(
					r.destinations.map[y |
						new Equal(newArrayList(
							transform(y),
							new And(
								r.destinations.filter[it != y].map[new Not(transform(it))] +
								newArrayList(transform(r.getOrigin) as INode)
							)
						))	
					]
				)
			default:
				null
		}
	}
}