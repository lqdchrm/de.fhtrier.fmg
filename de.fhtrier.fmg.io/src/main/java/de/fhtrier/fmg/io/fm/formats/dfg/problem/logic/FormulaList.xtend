package de.fhtrier.fmg.io.fm.formats.dfg.problem.logic

import java.util.List
import de.fhtrier.fmg.io.fm.formats.dfg.problem.logic.Formula

class FormulaList {
	
	String originType
	public List<Formula> items
	
	new() { 
		items = newArrayList
		setAxioms
	}
		
	override toString() { '''
		«IF items.size > 0»
			list_of_formulae(«originType»).
			«FOR item : items»
				«item»
			«ENDFOR»
			end_of_list.
		«ENDIF»
	'''.toString
	}
	
	def setAxioms() { originType = "axioms" }
	def setConjectures() { originType = "conjectures"}
}