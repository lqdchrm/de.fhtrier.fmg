package de.fhtrier.fmg.io.fm.formats.dfg.problem.logic

class Formula {
	
	String term
	
	new(String term) { this.term = term }
	
	override toString() { "formula(" + term + ")." }
}