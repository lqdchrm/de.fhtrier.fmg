package de.fhtrier.fmg.io.parameters.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import de.fhtrier.fmg.core.gen.IParameterSet;
import de.fhtrier.fmg.io.parameters.IParameterSetSerializer;

public class ParameterSetSerializer implements IParameterSetSerializer {

	@Override
	public void loadFromFile(IParameterSet params, Reader file)
			throws IOException {
		BufferedReader reader = new BufferedReader(file);
		String line;
		while ((line = reader.readLine()) != null) {
			String[] args = line.split("\t");
			String key = args[0];
			String type = args[1];
			String val = args.length == 3 ? args[2] : "";

			switch (type) {
			case "int":
				params.setValue(key, Integer.parseInt(val));
				break;
			case "long":
				params.setValue(key, Long.parseLong(val));
				break;
			case "float":
				params.setValue(key, Float.parseFloat(val));
				break;
			case "java.lang.String":
			case "string":
				params.setValue(key, val);
				break;

			}
		}
		reader.close();
	}

	@Override
	public void saveToFile(IParameterSet params, Writer file)
			throws IOException {
		BufferedWriter writer = new BufferedWriter(file);

		for (String param : params.getParams()) {
			writer.write(param + "\t");
			String type = params.getType(param);
			writer.write(type + "\t");
			switch (type) {
			case "int":
				writer.write(String.valueOf(params.getInt(param)));
				break;
			case "long":
				writer.write(String.valueOf(params.getLong(param)));
				break;
			case "float":
				writer.write(String.valueOf(params.getFloat(param)));
				break;
			case "java.lang.String":
			case "string":
				writer.write(params.getString(param));
				break;
			}
			writer.newLine();
		}
		writer.close();
	}
}
