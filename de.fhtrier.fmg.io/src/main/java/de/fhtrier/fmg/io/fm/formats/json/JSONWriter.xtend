package de.fhtrier.fmg.io.fm.formats.json

import de.fhtrier.fmg.io.fm.IFMWriter
import de.fhtrier.fmg.core.fm.IFeatureModel
import java.io.IOException
import java.io.Writer

import static de.fhtrier.fmg.io.fm.formats.json.JSONModel.*

class JSONWriter implements IFMWriter {
	
	Writer writer
	
	new(Writer writer) {
		this.writer = writer
	}

	override write(IFeatureModel model, String name) throws IOException {
		val _model = JSONModel::from(model, name)
		writer.write(_model.toString())
	}

	override close() throws IOException {
		writer.close()
	}
	
	override flush() throws IOException {
		writer.flush()
	}	
}