package de.fhtrier.fmg.io.fm.formats.dfg.problem

class Description {
	public String name = ""
	public String author = ""
	public String version = ""
	public String logic = ""
	private String status = "unknown"
	public String description = ""
	public String date = ""
		
	override toString() { '''
		list_of_descriptions.
		name( {* «name» *} ).
		author( {* «author» *} ).
		«IF version.length>0»
			version( {* «version» *} ).
		«ENDIF»
		«IF logic.length>0»
			logic( {* «logic» *} ).
		«ENDIF»
		status(«status»).
		description( {* «description» *} ).
		end_of_list.
	'''.toString 
	}
	
	def setSatisfiable() { status = "satisfiable" }
	def setUnsatisfiable() { status = "unsatisfiable" }
	def setStatusUnknown() { status = "unknown" }
}