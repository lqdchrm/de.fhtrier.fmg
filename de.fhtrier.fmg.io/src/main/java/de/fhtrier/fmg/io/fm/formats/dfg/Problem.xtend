package de.fhtrier.fmg.io.fm.formats.dfg

import de.fhtrier.fmg.io.fm.formats.dfg.problem.Description
import de.fhtrier.fmg.io.fm.formats.dfg.problem.LogicalPart

class Problem {
	
	String name
	
	Description description
	def getDescription() { description }
	
	LogicalPart logic
	
	def getSymbols() { logic.symbols }
	def getClauses() { logic.clauses }
	def getFormulas() { logic.formulas }
	
	new(String name) {
		this.name = name
		description = new Description()
		description.name = name
		logic = new LogicalPart()
	}
	
	override toString() { '''
		begin_problem( «name»).
		
		«description»
		
		«logic»
		
		end_problem.
	'''.toString
	}
}