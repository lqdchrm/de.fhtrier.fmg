package de.fhtrier.fmg.io.fm.formats.dfg.problem.logic

import de.fhtrier.fmg.core.fm.IFeature

class Predicate {
	
	IFeature feature
	
	new(IFeature feature) { this.feature = feature }
		
	override toString() { '''(«feature.getName», 0)'''.toString }
}