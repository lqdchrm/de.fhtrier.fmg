package de.fhtrier.fmg.io.fm.impl;

import de.fhtrier.fmg.io.fm.IFMWriter;
import de.fhtrier.fmg.io.fm.formats.dfg.DFGWriter;
import de.fhtrier.fmg.io.fm.formats.graphml.GraphMLWriter;
import de.fhtrier.fmg.io.fm.formats.graphml.TreeWriter;
import de.fhtrier.fmg.io.fm.formats.json.JSONWriter;
import de.fhtrier.fmg.io.fm.formats.sxfm.SXFMWriter;
import de.fhtrier.fmg.io.fm.formats.vcontrol.VControlXMIWriter;
import de.fhtrier.fmg.utils.factory.impl.Factory;

public class FMWriterFactory extends Factory<IFMWriter, FMWriterDescriptor> {

	private static FMWriterFactory	instance	= null;

	public static FMWriterFactory getInstance() {

		if (FMWriterFactory.instance == null) {
			FMWriterFactory.instance = new FMWriterFactory();
		}

		return FMWriterFactory.instance;
	}

	public FMWriterFactory() {
		registerType(new FMWriterDescriptor("DFG", DFGWriter.class, "dfg", ""));
		registerType(new FMWriterDescriptor("VControl",
				VControlXMIWriter.class,
				"xmi", ""));
		registerType(new FMWriterDescriptor("SXFM", SXFMWriter.class, "xml", ""));
		registerType(new FMWriterDescriptor("JSON", JSONWriter.class, "json", ""));
		registerType(new FMWriterDescriptor("GraphML", GraphMLWriter.class, "xml", ""));
		registerType(new FMWriterDescriptor("XML-Tree", TreeWriter.class, "xml", ""));
	}
}
