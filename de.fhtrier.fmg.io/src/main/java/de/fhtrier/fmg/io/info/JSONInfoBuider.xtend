package de.fhtrier.fmg.io.info

import de.fhtrier.fmg.impl.runtime.FrameworkFactory
import de.fhtrier.fmg.io.fm.impl.FMWriterFactory

class JSONInfoBuider {
  
  def static info() '''
  {
    "generators" : [ 
        «FOR genName : FrameworkFactory::current.generators SEPARATOR ','»
          {
            "name" : "«genName»",
            "params" : [
              «var gen = FrameworkFactory::current.createGenerator(genName)»
              «var set = gen.createParameterSet("Default", 0)»
              «FOR param : set.params SEPARATOR ','»
                {
                  "name" : "«param»",
                  "type" : "«set.getType(param)»",
                  "default" : "«set.getAsString(param)»",
                  "description" : "«set.getDescription(param)»"
                }
              «ENDFOR»
            ]
          }
        «ENDFOR»
    ], 
    "formats" : [ 
        «FOR format : FMWriterFactory::instance.registeredTypes SEPARATOR ','»
        {
        	"name" : "«format»",
        	"extension": "«FMWriterFactory::instance.getDescriptor(format).fileExtension»"
        }
        «ENDFOR»
    ]
  }
  '''
}