package de.fhtrier.fmg.io.fm.formats.dfg.problem.logic

import java.util.List

class Predicates {
	
	public List<Predicate> items
	
	new() { items = newArrayList }
		
	override toString() { '''
		«IF items.size > 0»
			predicates[«items.join(', ')»].
		«ENDIF»
	'''.toString
	}
}