package de.fhtrier.fmg.io.fm;

import java.io.IOException;

import de.fhtrier.fmg.core.fm.IFeatureModel;


public interface IFMWriter {
	void close() throws IOException;

	void flush() throws IOException;

	void write(IFeatureModel model, String name) throws IOException;
}
