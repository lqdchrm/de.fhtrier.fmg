package de.fhtrier.fmg.io.fm.impl;

import java.io.Writer;

import de.fhtrier.fmg.io.fm.IFMWriter;
import de.fhtrier.fmg.io.fm.IFMWriterDescriptor;
import de.fhtrier.fmg.utils.factory.impl.BasicTypeDescriptor;

public class FMWriterDescriptor extends BasicTypeDescriptor<IFMWriter>
		implements IFMWriterDescriptor {

	private String	fileExtension;
	private String	description;

	public FMWriterDescriptor(String name,
			Class<? extends IFMWriter> returnType,
			String fileExtension, String description) {
		super(name, returnType, Writer.class);

		this.fileExtension = fileExtension;
		this.description = description;
	}

	@Override
	public String getFileExtension() {
		return this.fileExtension;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}
}
