package de.fhtrier.fmg.io.fm.formats.dfg.problem

import de.fhtrier.fmg.io.fm.formats.dfg.problem.logic.SymbolList
import de.fhtrier.fmg.io.fm.formats.dfg.problem.logic.FormulaList
import de.fhtrier.fmg.io.fm.formats.dfg.problem.logic.ClauseList

class LogicalPart {
	
	public SymbolList symbols
	public FormulaList formulas
	public ClauseList clauses
	
	new() {
		symbols = new SymbolList()
		formulas = new FormulaList()
		clauses = new ClauseList()
	}
		
	override toString() { '''
		«symbols»
		
		«formulas»
		
		«clauses»
	'''.toString
	}
}