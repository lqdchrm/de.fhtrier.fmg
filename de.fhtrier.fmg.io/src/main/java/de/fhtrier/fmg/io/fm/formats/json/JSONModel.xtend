package de.fhtrier.fmg.io.fm.formats.json

import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.IFeatureModel
import de.fhtrier.fmg.core.fm.IRelation
import de.fhtrier.fmg.core.fm.RelationType
import de.fhtrier.fmg.core.fm.ICrossTreeConstraint
import de.fhtrier.fmg.core.fm.CrossTreeConstraintType

class JSONModel {
	
	String asString
	
	def static from(IFeatureModel model, String name) {
		new JSONModel(model, name)
	}
	
	new(IFeatureModel model, String name) {
		asString = buildStringRep(model, name).toString
	}	
	
	def buildStringRep(IFeatureModel model, String name) '''
	{
	  "name" : "«name»",
	  "tree" :
	  «model.root.toString(0)»,
	  "ctcs" : [
	«FOR c : model.crossTreeConstraints SEPARATOR ', '»
		«c.toString(2)»
	«ENDFOR»
	  ]
	}'''
	
	def toString(ICrossTreeConstraint c, int indent) '''
		«indent.createIndent»{
		«(indent+1).createIndent»"type" : "«c.type.toString2»",
		«(indent+1).createIndent»"origin" : "«c.origin.name»",
		«(indent+1).createIndent»"destination" : "«c.destination.name»"
		«(indent).createIndent»}
	'''
	
	def CharSequence toString(IFeature f, int indent) '''
		«IF f.children.size > 0»
			«indent.createIndent»{
			«(indent+1).createIndent»"name" : "«f.name»",
			«(indent+1).createIndent»"relations" : [
			«FOR r : f.children SEPARATOR ', ' AFTER (indent+1).createIndent+']\n'»
				«r.toString(indent+2)»
			«ENDFOR»
			«(indent).createIndent»}
		«ELSE»
			«indent.createIndent»{"name" : "«f.name»"}
		«ENDIF»
	'''
	
	def toString(IRelation r, int indent) '''
		«indent.createIndent»{
		«(indent+1).createIndent»"type" : "«r.type.toString2»",
		«IF r.destinations.size > 0»«(indent+1).createIndent»"destinations" : [«ENDIF»
		«FOR c : r.destinations SEPARATOR ', ' AFTER (indent+1).createIndent+']\n'»
			«c.toString(indent+2)»
		«ENDFOR»
		«(indent).createIndent»}
	'''
	
	def toString2(RelationType rtype) {
		switch(rtype) {
			case RelationType::Mandatory : "mandatory" 
			case RelationType::Optional :  "optional"
			case RelationType::Or :        "or"
			case RelationType::Xor :       "xor"
		}
	}
	
	def toString2(CrossTreeConstraintType ctype) {
		switch(ctype) {
			case CrossTreeConstraintType::Excludes : "excludes"
			case CrossTreeConstraintType::Requires : "requires"
		}
	}
							
	def private create strIndent : new StringBuffer(indent) createIndent(int indent) {
		if (indent>0)
			for (int i : 1..indent)
				strIndent.append("  ")			
	}
	
	override toString() {
		return asString
	}
}
