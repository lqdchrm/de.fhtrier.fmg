package de.fhtrier.fmg.io.fm.formats.vcontrol

import com.prostep.vcontrol.model.common.CommonFactory
import com.prostep.vcontrol.model.common.Constraint
import com.prostep.vcontrol.model.feature.FeatureFactory
import com.prostep.vcontrol.model.feature.FeatureNode
import com.prostep.vcontrol.model.terms.TermsFactory
import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.IRelation
import de.fhtrier.fmg.core.fm.RelationType
import de.fhtrier.fmg.core.fm.CrossTreeConstraintType
import de.fhtrier.fmg.core.fm.ICrossTreeConstraint
import org.eclipse.emf.ecore.util.EcoreUtil

class VControlModelCache {
	
	def private create id : new String(EcoreUtil::generateUUID) createID(FeatureNode node) {}
	
	def private create id : new String(EcoreUtil::generateUUID) createID(Constraint constraint) {}
	
	def create node : FeatureFactory::eINSTANCE.createFeature() createNode(IFeature feature) {
		node.name = feature.name

		node.optional = (feature.parent != null &&
						 feature.parent.type != RelationType::Mandatory)

		node.id = createID(node)
	}
	
	def create node : FeatureFactory::eINSTANCE.createFeatureGroup() createNode(IRelation rel) {
		node.name = rel.destinations.map[it.name].join("")
		
		node.optional = true
		
		node.id = createID(node)
		
		switch(rel.type) {
			case RelationType::Xor: {
				node.min = 1
				node.max = 1
			}
			case RelationType::Or: {
				node.min = 1
				node.max = rel.destinations.size
			}
		}
	}
	
	def create constraint : CommonFactory::eINSTANCE.createConstraint createConstraint(ICrossTreeConstraint ctc) {
		val termsFactory = TermsFactory::eINSTANCE

		constraint.id = createID(constraint)

		switch(ctc.type) {	
			case CrossTreeConstraintType::Requires: {
				val implies = termsFactory.createImpliesTerm
				val left = termsFactory.createAtomicTerm
				val right = termsFactory.createAtomicTerm
				left.element = createNode(ctc.origin).id
				right.element = createNode(ctc.destination).id
				implies.leftOperand.add(left)
				implies.rightOperand.add(right) 
				constraint.rootTerm.add(implies)
			}
			
			case CrossTreeConstraintType::Excludes: {
				val excludes = termsFactory.createExcludesTerm
				val left = termsFactory.createAtomicTerm
				val right = termsFactory.createAtomicTerm
				left.element = createNode(ctc.origin).id
				right.element = createNode(ctc.destination).id
				excludes.leftOperand.add(left)
				excludes.rightOperand.add(right) 
				constraint.rootTerm.add(excludes)
			}
		}
	}
	
}