package de.fhtrier.fmg.io.fm.formats.dfg.problem.logic

import java.util.List

class ClauseList {
	
	String originType
	String clauseType
	
	List<Clause> items
	
	new() {
		items = newArrayList
		setAsConjectures
		setTypeCNF
	}
	
	def used() { items.size > 0 }

	def add(Clause item) { return items += item }
	def remove(Clause item) { return items.remove(item) }	
	
	override toString() { '''
		«IF used»
			list_of_clauses(«originType»,«clauseType»).
			«FOR clause : items»
				«clause»
			«ENDFOR»
			end_of_list.
		«ENDIF»
	'''.toString 
	}

	def setAsAxioms() { originType = "axioms" }
	def setAsConjectures() { originType = "conjectures" }
	
	def setTypeCNF() { clauseType = "cnf" }
	def setTypeDNF() { clauseType = "dnf" }
}