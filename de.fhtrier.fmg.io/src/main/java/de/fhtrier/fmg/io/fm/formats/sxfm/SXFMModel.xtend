package de.fhtrier.fmg.io.fm.formats.sxfm

import de.fhtrier.fmg.core.fm.CrossTreeConstraintType
import de.fhtrier.fmg.core.fm.ICrossTreeConstraint
import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.IFeatureModel
import de.fhtrier.fmg.core.fm.IRelation
import de.fhtrier.fmg.core.fm.RelationType

class SXFMModel {
	
	String asString
	
	def static from(IFeatureModel model, String name) {
		new SXFMModel(model, name)
	}
	
	new(IFeatureModel model, String name) {
		asString = buildStringRep(model, name).toString
	}	
	
	def buildStringRep(IFeatureModel model, String name) '''
	<feature_model name="«name»">
	<feature_tree>
	«model.root.toString(":r", 0)»
	</feature_tree>
	<constraints>
	«model.crossTreeConstraints.map[toString2].join("\n")»
	</constraints>
	</feature_model>
	'''
		
	def CharSequence toString(IFeature f, String prefix, int indent) '''
		«indent.createIndent»«prefix» f«f.name» («createID(f)»)
		«FOR r : f.children»
			«r.toString(indent+1)»
		«ENDFOR»
	'''
	
	def toString(IRelation r, int indent) {
		switch(r.type) {
			case RelationType::Mandatory : r.destinations.head.toString(":m", indent) 
			case RelationType::Optional :  r.destinations.head.toString(":o", indent)
			case RelationType::Or :        indent.createIndent + ":g (" + r.createID + ") [1,*]\n" + r.destinations.map[toString(":", indent + 1)].join
			case RelationType::Xor :       indent.createIndent + ":g (" + r.createID + ") [1,1]\n" + r.destinations.map[toString(":", indent + 1)].join
		}
	}
	
	def toString2(ICrossTreeConstraint ctc) {
		switch(ctc.type) {
			case CrossTreeConstraintType::Requires : ctc.createName + ": ~" + ctc.origin.createID + " or " + ctc.destination.createID   
			case CrossTreeConstraintType::Excludes : ctc.createName + ": ~" + ctc.origin.createID + " or ~" + ctc.destination.createID   
		}
	}
		
	static int ids = 0
	def private create id : new StringBuffer() createID(IFeature f) {
		id.append("id_f_")
		id.append(ids)
		ids = ids + 1
	}
	
	def private create id : new StringBuffer() createID(IRelation r) {
		id.append("id_r_")
		id.append(ids)
		ids = ids + 1
	}
	
	def private create name : new StringBuffer() createName(ICrossTreeConstraint ctc) {
		name.append("c")
		name.append(ids)
		ids = ids + 1
	}
		
	def private create strIndent : new StringBuffer(indent) createIndent(int indent) {
		if (indent>0)
			for (int i : 1..indent)
				strIndent.append("\t")			
	}
	
	override toString() {
		return asString
	}
}
