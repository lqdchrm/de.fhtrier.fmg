package de.fhtrier.fmg.io.fm.formats.vcontrol

import com.prostep.vcontrol.model.common.CommonFactory
import com.prostep.vcontrol.model.common.CommonPackage
import com.prostep.vcontrol.model.common.ImpProject
import com.prostep.vcontrol.model.feature.FeatureFactory
import com.prostep.vcontrol.model.feature.FeatureNode
import com.prostep.vcontrol.model.feature.FeaturePackage
import com.prostep.vcontrol.model.feature.FeaturePackage$Literals
import com.prostep.vcontrol.model.feature.util.FeatureResourceFactoryImpl
import com.prostep.vcontrol.model.terms.TermsPackage
import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.IFeatureModel
import de.fhtrier.fmg.core.fm.IRelation
import de.fhtrier.fmg.core.fm.RelationType
import java.io.ByteArrayOutputStream
import java.io.IOException
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.XMLResource

class VControlModel {
	
	boolean valid
	ImpProject project
	String asString
	VControlModelCache cache
	
	def static from(IFeatureModel model, String name) {
		new VControlModel(model, name)
	}
	
	new(IFeatureModel model, String name) {
		valid = false
		cache = new VControlModelCache
		
		// initialize ecore packages
		CommonPackage::eINSTANCE.eClass
		FeaturePackage::eINSTANCE.eClass
		TermsPackage::eINSTANCE.eClass

		// get factories
		val commonFactory = CommonFactory::eINSTANCE
		val featureFactory = FeatureFactory::eINSTANCE

		// the project as document root
		project = commonFactory.createImpProject
		project.id = EcoreUtil::generateUUID
		project.name = name + " exported from Feature Model Generator FH Trier"

		// create model
		val _model = featureFactory.createFeatureModel
		_model.name = name
		_model.id = EcoreUtil::generateUUID
		
		// create features
		val root = model.root
		val FeatureNode _root = createFeatureTree(root)

		// constraints
		_model.constraints.addAll(model.crossTreeConstraints.map[cache.createConstraint(it)].toList)

		// add root node to model
		val type = FeaturePackage$Literals::FEATURE_MODEL__ROOT_FEATURE
		_model.group2.add(type, _root)

		// add model to project
		project.models.add(_model);

		// build string
		valid = buildStringRep
	}
	
	def private buildStringRep() {
		val resource = new FeatureResourceFactoryImpl().createResource(URI::createFileURI("temp.xmi")) as XMLResource
		resource.getContents().add(project)

		val saveOptions = resource.defaultSaveOptions
		saveOptions.put(XMLResource::OPTION_FORMATTED, Boolean::TRUE)
		saveOptions.put(XMLResource::OPTION_DOM_USE_NAMESPACES_IN_SCOPE, Boolean::TRUE)
		saveOptions.put(XMLResource::OPTION_DECLARE_XML, Boolean::TRUE)

		val stream = new ByteArrayOutputStream()
		try {
			resource.save(stream, saveOptions)
		} catch (IOException e) {
			return false
		}

		asString = stream.toString()
		return true
	}	
		
	def private FeatureNode createFeatureGroup(IRelation rel) {
		val FeatureNode group = cache.createNode(rel)
		
		group.children.addAll(
			rel.destinations.map[createFeatureTree(it)].toList
		)
				
		return group
	}
		
	
	def	private FeatureNode createFeatureTree(IFeature feature) {
		var FeatureNode node = cache.createNode(feature)

		val parent = node
		
		feature.children.forEach[rel | 
			switch(rel.type) {
				case RelationType::Mandatory: parent.children.addAll(rel.destinations.map[createFeatureTree(it)].filter[it != null].toList) 
				case RelationType::Optional: parent.children.addAll(rel.destinations.map[createFeatureTree(it)].filter[it != null].toList)
				case RelationType::Xor: if (rel.destinations.size > 0) parent.children.addAll(createFeatureGroup(rel))
				case RelationType::Or: if (rel.destinations.size > 0) parent.children.addAll(createFeatureGroup(rel))
			}
		]

		return node
	}
		
	override toString() {
		return asString
	}
}