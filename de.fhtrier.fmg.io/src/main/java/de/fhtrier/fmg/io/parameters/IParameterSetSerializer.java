package de.fhtrier.fmg.io.parameters;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import de.fhtrier.fmg.core.gen.IParameterSet;

public interface IParameterSetSerializer {

	void loadFromFile(IParameterSet params, Reader reader) throws IOException;

	void saveToFile(IParameterSet params, Writer writer) throws IOException;

}
