package de.fhtrier.fmg.io.fm.formats.dfg.problem.logic

public class Clause {
	
	String text
	def setText(String text) { this.text = text }
		
	override toString() { '''clause(«text»).'''.toString }
}