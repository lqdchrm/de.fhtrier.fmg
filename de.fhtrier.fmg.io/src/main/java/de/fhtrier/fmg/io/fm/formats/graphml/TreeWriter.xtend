package de.fhtrier.fmg.io.fm.formats.graphml

import de.fhtrier.fmg.io.fm.IFMWriter
import java.io.Writer
import de.fhtrier.fmg.core.fm.IFeatureModel
import java.io.IOException

class TreeWriter implements IFMWriter {
	
	Writer writer
	
	new(Writer writer) {
		this.writer = writer
	}

	override write(IFeatureModel model, String name) throws IOException {
		val _model = GraphMLModel::from(model, name)
		writer.write(_model.TreetoString)
	}

	override close() throws IOException {
		writer.close()
	}
	
	override flush() throws IOException {
		writer.flush()
	}}