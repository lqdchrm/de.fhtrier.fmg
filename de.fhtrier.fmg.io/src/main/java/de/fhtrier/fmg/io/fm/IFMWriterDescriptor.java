package de.fhtrier.fmg.io.fm;

import de.fhtrier.fmg.utils.factory.ITypeDescriptor;

public interface IFMWriterDescriptor extends ITypeDescriptor<IFMWriter> {

	String getFileExtension();

	String getDescription();

	void setDescription(String description);
}
