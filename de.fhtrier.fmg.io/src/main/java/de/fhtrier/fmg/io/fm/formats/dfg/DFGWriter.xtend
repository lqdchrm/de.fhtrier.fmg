package de.fhtrier.fmg.io.fm.formats.dfg

import de.fhtrier.fmg.core.fm.IFeatureModel
import de.fhtrier.fmg.io.fm.IFMWriter
import java.io.IOException
import java.io.Writer

class DFGWriter implements IFMWriter {
	Writer writer
	
	new(Writer writer) {
		this.writer = writer
	}
	
	override write(IFeatureModel model, String name) throws IOException {
		val _model = DFGModel::from(model, name)
		writer.write(_model.toString)
	}

	override close() throws IOException {
		writer.close()
	}
	
	override flush() throws IOException {
		writer.flush()
	}
	
}