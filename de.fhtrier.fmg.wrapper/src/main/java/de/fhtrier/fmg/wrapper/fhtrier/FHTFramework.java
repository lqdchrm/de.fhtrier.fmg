package de.fhtrier.fmg.wrapper.fhtrier;

import de.fhtrier.fmg.core.gen.IGenerator;
import de.fhtrier.fmg.core.gen.IGeneratorDescriptor;
import de.fhtrier.fmg.core.runtime.IFramework;
import de.fhtrier.fmg.impl.gen.GeneratorDescriptor;
import de.fhtrier.fmg.impl.gen.GeneratorFactory;
import de.fhtrier.fmg.impl.gen.generators.CustomGenerator;
import de.fhtrier.fmg.impl.gen.generators.DefaultGenerator;
import de.fhtrier.fmg.impl.gen.generators.MergeGenerator;
import de.fhtrier.fmg.impl.gen.generators.SimpleGenerator;
import de.fhtrier.fmg.impl.gen.generators.UnSatGenerator;

public class FHTFramework implements IFramework {

	private GeneratorFactory	factory;

	public FHTFramework() {
		factory = new GeneratorFactory();

		factory.registerType(new GeneratorDescriptor("Simple",
				SimpleGenerator.class));
		
		factory.registerType(new GeneratorDescriptor("Default",
				DefaultGenerator.class));

		factory.registerType(new GeneratorDescriptor("Custom",
				CustomGenerator.class));

		factory.registerType(new GeneratorDescriptor("Merge",
				MergeGenerator.class));

		factory.registerType(new GeneratorDescriptor("Unsat",
				UnSatGenerator.class));
	}

	@Override
	public IGenerator createGenerator(String name) {
		return factory.createInstanceOfType(name, name);
	}

	@Override
	public Iterable<? extends String> getGenerators() {
		return factory.getRegisteredTypes();
	}

	@Override
	public void registerGenerator(IGeneratorDescriptor desc) {
		factory.registerType(desc);
	}
}
