package de.fhtrier.fmg.gui.swing;

import java.awt.Color;
import java.beans.ExceptionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import de.fhtrier.fmg.core.gen.IParameterSet;
import de.fhtrier.fmg.core.gen.IParameterSetChangedListener;
import de.fhtrier.fmg.core.runtime.IExceptionThrower;
import de.fhtrier.fmg.core.runtime.IFeatureModelMgr;
import de.fhtrier.fmg.core.runtime.IFeatureModelMgrListener;
import de.fhtrier.fmg.core.runtime.IManagedFeatureModel;

public class ParameterSetPanel extends JPanel implements
		IParameterSetChangedListener,
		CaretListener, IExceptionThrower, IFeatureModelMgrListener {

	private static final long		serialVersionUID	= 4102273953154348360L;

	private IFeatureModelMgr		mgr;

	// the data
	private IParameterSet			parameterSet;

	// tracks, if values are currently changing to
	// prevent cyclic updates
	private boolean					changing;

	// every parameter-set should have these
	// JTextField tbName;
	// JTextField tbSeed;

	// here we store the dynamic fields
	HashMap<String, JTextField>		fields;

	// Exception listeners
	private List<ExceptionListener>	listeners;

	public ParameterSetPanel() {
		this.changing = false;
		this.parameterSet = null;
		this.listeners = new ArrayList<ExceptionListener>();
		this.fields = new HashMap<>();

		initialize();
	}

	// constructor
	public ParameterSetPanel(IFeatureModelMgr mgr) {

		this.mgr = mgr;
		this.mgr.addFeatureModelMgrListener(this);

		this.changing = false;
		this.parameterSet = null;
		this.listeners = new ArrayList<ExceptionListener>();
		this.fields = new HashMap<>();

		initialize();
	}

	@Override
	public void addListener(ExceptionListener l) {
		if (!listeners.contains(l)) {
			listeners.add(l);
		}
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		updateData();
	}

	@Override
	public void exceptionThrown(Exception e) {
		for (ExceptionListener l : listeners) {
			l.exceptionThrown(e);
		}
	}

	@Override
	public void onChanged(final IParameterSet sender) {
		if (!changing) {
			changing = true;

			// put params
			for (String param : sender.getParams()) {
				switch (sender.getType(param)) {
				case "int":
					intToTextBox(sender.getInt(param), fields.get(param));
					break;
				case "long":
					longToTextBox(sender.getLong(param), fields.get(param));
					break;
				case "float":
					floatToTextBox(sender.getFloat(param), fields.get(param));
					break;
				case "java.lang.String":
				case "string":
					stringToTextBox(sender.getString(param), fields.get(param));
					break;
				}
			}
			getParent().doLayout();
			changing = false;
		}
	}

	@Override
	public void removeListener(ExceptionListener l) {
		if (listeners.contains(l)) {
			listeners.remove(l);
		}
	}

	// sets the parameter set to manage
	public void setParameters(IParameterSet parameters) {
		releaseParameters();

		this.parameterSet = parameters;

		bindParameters();

		this.repaint();
		this.doLayout();
	}

	// internally bind parameter set and listeners
	private void bindParameters() {
		if (this.parameterSet != null) {
			recreateFields();
			this.parameterSet.addListener(this);
			onChanged(this.parameterSet);
		}
	}

	// helper function to create a label
	private void createLabel(String text) {
		JLabel lbl = new JLabel(text);
		FormLayout layout = (FormLayout) getLayout();
		layout.appendRow(FormFactory.RELATED_GAP_ROWSPEC);
		layout.appendRow(FormFactory.DEFAULT_ROWSPEC);
		int row = layout.getRowCount();
		add(lbl, "2, " + row + ", right, default");
	}

	// helper function to create a textbox
	private JTextField createTextBox(String toolTip) {
		JTextField tb = new JTextField();
		tb.addCaretListener(this);
		tb.setToolTipText(toolTip);
		FormLayout layout = (FormLayout) getLayout();
		int row = layout.getRowCount();
		add(tb, "4, " + row + ", fill, default");
		return tb;
	}

	private void floatToTextBox(Float value, JTextField tb) {
		if (value == -1) {
			tb.setText("");
		} else {
			tb.setText(Float.toString(value));
		}
	}

	private void initialize() {
		setBorder(new TitledBorder(null, "Parameters", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.UNRELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), },
				new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNewLabel = new JLabel("New label");
		add(lblNewLabel, "2, 2, right, center");

		JTextField textField = new JTextField();
		add(textField, "4, 2, fill, top");

		this.setDoubleBuffered(true);
	}

	private void intToTextBox(Integer value, JTextField tb) {
		if (value == -1) {
			tb.setText("");
		} else {
			tb.setText(Integer.toString(value));
		}
	}

	private void longToTextBox(Long value, JTextField tb) {
		if (value == -1) {
			tb.setText("");
		} else {
			tb.setText(Long.toString(value));
		}
	}

	// recreates all fields
	private void recreateFields() {
		removeAll();
		fields.clear();
		FormLayout layout = (FormLayout) getLayout();
		while (layout.getRowCount() > 0) {
			layout.removeRow(1);
		}

		for (String param : this.parameterSet.getParams()) {
			createLabel(param + ":");
			JTextField t = createTextBox(this.parameterSet
					.getDescription(param));
			fields.put(param, t);
		}
	}

	// internally release parameter set
	private void releaseParameters() {
		if (this.parameterSet != null) {
			this.parameterSet.removeListener(this);
		}

		this.parameterSet = null;
	}

	private void setTextBoxInvalid(JTextField tb) {
		tb.setBackground(Color.RED);
	}

	private void setTextBoxValid(JTextField tb) {
		Color bgCol = UIManager.getColor("TextField.background");
		tb.setBackground(bgCol);
	}

	private void setTextBoxWarning(JTextField tb) {
		tb.setBackground(Color.YELLOW);
	}

	private void stringToTextBox(String value, JTextField tb) {
		tb.setText(value);
	}

	private Float textBoxToFloat(JTextField tb) {
		return textBoxToFloat(tb, false, 0.0f, 0.0f);
	}

	private Float textBoxToFloat(JTextField tb, boolean checkBounds, float min,
			float max) {
		try {
			setTextBoxValid(tb);
			float result = Float.parseFloat(tb.getText());
			if (checkBounds && (result < min || result > max)) {
				setTextBoxWarning(tb);
				exceptionThrown(new IllegalArgumentException(
						"Value should be in [" + min + ".." + max
								+ "], usage at own risk !!"));
			}
			return result;
		} catch (Exception e) {
			if (tb.getText().length() != 0) {
				setTextBoxInvalid(tb);
			}
			return -1.0f;
		}
	}

	@SuppressWarnings("unused")
	private Float textBoxToFloat(JTextField tb, float min, float max) {
		return textBoxToFloat(tb, true, min, max);
	}

	private Integer textBoxToInt(JTextField tb) {
		return textBoxToInt(tb, false, 0, 0);
	}

	private Integer textBoxToInt(JTextField tb, boolean checkBounds, int min,
			int max) {
		try {
			setTextBoxValid(tb);
			int result = Integer.parseInt(tb.getText());
			if (checkBounds && (result < min || result > max)) {
				setTextBoxWarning(tb);
				exceptionThrown(new IllegalArgumentException(
						"Value should be in [" + min + ".." + max
								+ "], usage at own risk !!"));
			}
			return result;
		} catch (Exception e) {
			if (tb.getText().length() != 0) {
				setTextBoxInvalid(tb);
			}
			return -1;
		}
	}

	@SuppressWarnings("unused")
	private Integer textBoxToInt(JTextField tb, int min, int max) {
		return textBoxToInt(tb, true, min, max);
	}

	private Long textBoxToLong(JTextField tb) {
		try {
			setTextBoxValid(tb);
			return Long.parseLong(tb.getText());
		} catch (Exception e) {
			if (tb.getText().length() != 0) {
				setTextBoxInvalid(tb);
			}
			return -1L;
		}
	}

	private String textBoxToString(JTextField tb) {
		return tb.getText();
	}

	// retrieves values from fields and puts 'em into the parameter set
	private void updateData() {
		if (!changing) {
			changing = true;
			if (this.parameterSet != null) {
				try {
					for (String param : this.parameterSet.getParams()) {
						switch (this.parameterSet.getType(param)) {
						case "int":
							this.parameterSet.setValue(param,
									textBoxToInt(fields.get(param)));
							break;
						case "long":
							this.parameterSet.setValue(param,
									textBoxToLong(fields.get(param)));
							break;
						case "float":
							this.parameterSet.setValue(param,
									textBoxToFloat(fields.get(param)));
							break;
						case "java.lang.String":
						case "string":
							this.parameterSet.setValue(param,
									textBoxToString(fields.get(param)));
							break;
						}
					}
				} catch (Exception e) {

				} finally {
					changing = false;
				}
			}
		}
	}

	@Override
	public void onSelectionChanged(IManagedFeatureModel m) {
		if (m != null) {
			setParameters(m.getParameters());
		} else {
			setParameters(null);
		}
	}
}
