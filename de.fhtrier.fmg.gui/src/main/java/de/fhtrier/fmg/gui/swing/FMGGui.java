package de.fhtrier.fmg.gui.swing;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.TitledBorder;

import de.fhtrier.fmg.core.gen.IGenerator;
import de.fhtrier.fmg.core.gen.IParameterSet;
import de.fhtrier.fmg.core.runtime.IManagedFeatureModel;
import de.fhtrier.fmg.gui.common.IFMGGui;
import de.fhtrier.fmg.gui.common.impl.FMGGuiBase;
import de.fhtrier.fmg.impl.runtime.FrameworkFactory;
import de.fhtrier.fmg.impl.runtime.ManagedFeatureModel;
import de.fhtrier.fmg.io.fm.impl.FMWriterFactory;

public class FMGGui extends FMGGuiBase implements IFMGGui {

	// GUI Stuff
	private JFrame				frame;
	private ParameterSetPanel	grpParams;
	private JTabbedPane			tabFolder;
	private LogPanel			log;
	private ModelList			modelList;

	/**
	 * @wbp.parser.constructor
	 */
	public FMGGui() {
		super();
	}

	@Override
	public void createFeatureModel() {
		super.createFeatureModel();

		// statistics
		log.setText(getStats().toString());
	}

	@Override
	public void exceptionThrown(Exception e) {
		final String text = e.getMessage();
		e.printStackTrace();
		if (text != null) {
			log.setText(text);
		}
	}

	@Override
	public void run() {
		this.stats = null;

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		initialize();
		this.IO = new FMGGuiIO(frame);

		frame.pack();
		frame.setVisible(true);
	}

	@Override
	public void run(IGenerator gen, IParameterSet params) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		initialize();
		this.IO = new FMGGuiIO(frame);

		frame.pack();
		frame.setVisible(true);

		IManagedFeatureModel model = new ManagedFeatureModel(0);
		model.setGenerator(gen);
		model.setParameters(params);

		this.getMgr().addElement(model);
		this.stats = null;
	}

	@Override
	public void saveAs(String format) {
		IManagedFeatureModel m = this.modelList.getFeatureModelMgr()
				.getSelected();
		if (m == null || m.getModel() == null) {
			exceptionThrown(new RuntimeException(
					"Please create a model first (or select one)"));
		}

		try {
			IO.saveAs(format, m.getModel(), m.getParameters());
			log.setText(format + "-File successfully saved.");
		} catch (IOException e) {
			exceptionThrown(e);
		}
	}

	@Override
	public void showAs(String format) {
		IManagedFeatureModel m = this.modelList.getFeatureModelMgr()
				.getSelected();
		if (m == null || m.getModel() == null) {
			exceptionThrown(new RuntimeException(
					"Please create a model first (or select one)"));
		}

		try {
			log.setText(IO.showAs(format, m.getModel(), m.getParameters()));
		} catch (IOException e) {
			exceptionThrown(e);
		}
	}

	private void setDoubleBuffered(Component c) {

		if (c instanceof JComponent) {
			((JComponent) c).setDoubleBuffered(true);
		}

		if (c instanceof Container) {
			for (Component child : ((Container) c).getComponents()) {
				setDoubleBuffered(child);
			}
		}
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 900, 576);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		JSplitPane spMain = new JSplitPane();
		spMain.setOneTouchExpandable(true);
		spMain.setContinuousLayout(true);
		spMain.setOrientation(JSplitPane.VERTICAL_SPLIT);
		spMain.setDividerLocation(250);
		spMain.setPreferredSize(new Dimension(1000, 600));
		frame.getContentPane().add(spMain);

		JSplitPane spTop = new JSplitPane();
		spTop.setContinuousLayout(true);
		spTop.setDividerLocation(350);
		spMain.setLeftComponent(spTop);

		JSplitPane spModels = new JSplitPane();
		spModels.setResizeWeight(1.0);
		spModels.setContinuousLayout(true);
		spTop.setLeftComponent(spModels);

		JPanel grpModels = new JPanel();
		spModels.setLeftComponent(grpModels);
		grpModels.setLayout(new CardLayout(0, 0));

		ButtonGroup btnGrpGenerators = new ButtonGroup();
		modelList = new ModelList(getMgr(), btnGrpGenerators);
		grpModels.add(modelList, "name_9010905394634");
		modelList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		modelList.setValueIsAdjusting(true);

		JPanel grpActions = new JPanel();
		spModels.setRightComponent(grpActions);
		grpActions.setBorder(new TitledBorder(null, "Actions",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagLayout gbl_grpActions = new GridBagLayout();
		gbl_grpActions.columnWidths = new int[] { 140 };
		gbl_grpActions.rowHeights = new int[] { 28, 28, 28, 28, 28, 12, 12, 12 };
		gbl_grpActions.columnWeights = new double[] { 1.0 };
		gbl_grpActions.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
				1.0, 1.0 };
		grpActions.setLayout(gbl_grpActions);

		JButton btnLoadParameters = new JButton("Load Parameters...");
		btnLoadParameters.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				loadParameters();
			}
		});
		GridBagConstraints gbc_btnLoadParameters = new GridBagConstraints();
		gbc_btnLoadParameters.fill = GridBagConstraints.BOTH;
		gbc_btnLoadParameters.insets = new Insets(0, 0, 5, 0);
		gbc_btnLoadParameters.gridx = 0;
		gbc_btnLoadParameters.gridy = 0;
		grpActions.add(btnLoadParameters, gbc_btnLoadParameters);

		JButton btnSaveParameters = new JButton("Save Parameters...");
		btnSaveParameters.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveParameters();
			}
		});
		GridBagConstraints gbc_btnSaveParameters = new GridBagConstraints();
		gbc_btnSaveParameters.insets = new Insets(0, 0, 5, 0);
		gbc_btnSaveParameters.fill = GridBagConstraints.BOTH;
		gbc_btnSaveParameters.gridx = 0;
		gbc_btnSaveParameters.gridy = 1;
		grpActions.add(btnSaveParameters, gbc_btnSaveParameters);

		JButton btnCreateFm = new JButton("Generate");
		btnCreateFm.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				createFeatureModel();
			}
		});
		GridBagConstraints gbc_btnCreateFm = new GridBagConstraints();
		gbc_btnCreateFm.fill = GridBagConstraints.BOTH;
		gbc_btnCreateFm.insets = new Insets(0, 0, 5, 0);
		gbc_btnCreateFm.gridx = 0;
		gbc_btnCreateFm.gridy = 2;
		grpActions.add(btnCreateFm, gbc_btnCreateFm);

		JButton btnCreateRandomFm = new JButton("Generate Random");
		btnCreateRandomFm.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				createRandomFeatureModel();
			}
		});
		GridBagConstraints gbc_btnCreateRandomFm = new GridBagConstraints();
		gbc_btnCreateRandomFm.fill = GridBagConstraints.BOTH;
		gbc_btnCreateRandomFm.insets = new Insets(0, 0, 5, 0);
		gbc_btnCreateRandomFm.gridx = 0;
		gbc_btnCreateRandomFm.gridy = 3;
		grpActions.add(btnCreateRandomFm, gbc_btnCreateRandomFm);

		JSplitPane spParamsLog = new JSplitPane();
		spParamsLog.setContinuousLayout(true);
		spParamsLog.setDividerLocation(300);
		spTop.setRightComponent(spParamsLog);

		grpParams = new ParameterSetPanel(getMgr());
		spParamsLog.setLeftComponent(grpParams);

		log = new LogPanel();
		spParamsLog.setRightComponent(log);

		tabFolder = new JTabbedPane(SwingConstants.TOP);
		spMain.setRightComponent(tabFolder);

		GraphContainer _graphContainer = new GraphContainer(getMgr());
		this.graphContainer = _graphContainer;
		tabFolder.removeAll();
		tabFolder.addTab("Graph", null, _graphContainer, null);

		setDoubleBuffered(frame);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,
				InputEvent.CTRL_MASK));
		mnFile.add(mntmExit);

		JMenu mnFeatureModels = new JMenu("Feature Models");
		menuBar.add(mnFeatureModels);

		JMenuItem mntmAddFeatureModel = new JMenuItem("Add Feature Model");
		mntmAddFeatureModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modelList.createNew();
			}
		});
		mnFeatureModels.add(mntmAddFeatureModel);

		JMenu mnGenerator = new JMenu("Generator");
		menuBar.add(mnGenerator);

		for (String s : FrameworkFactory.getCurrent().getGenerators()) {
			final String genName = s;
			JRadioButtonMenuItem rdbtnmntmNewRadioItem = new JRadioButtonMenuItem(
					genName);
			rdbtnmntmNewRadioItem.setName(genName);
			btnGrpGenerators.add(rdbtnmntmNewRadioItem);
			rdbtnmntmNewRadioItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					modelList.setGen(genName);
				}
			});
			mnGenerator.add(rdbtnmntmNewRadioItem);
		}

		JMenu mnShow = new JMenu("Show");
		menuBar.add(mnShow);

		JMenu mnExport = new JMenu("Export");
		menuBar.add(mnExport);

		for (String format : FMWriterFactory.getInstance().getRegisteredTypes()) {
			final String _format = format;
			JMenuItem mntmShow = new JMenuItem(format);
			mntmShow.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					showAs(_format);
				}
			});
			mnShow.add(mntmShow);
			JMenuItem mntmExport = new JMenuItem(format);
			mntmExport.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					saveAs(_format);
				}
			});
			mnExport.add(mntmExport);
		}
	}
}
