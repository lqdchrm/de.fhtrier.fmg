package de.fhtrier.fmg.gui.common.impl;

import java.beans.ExceptionListener;
import java.io.IOException;

import de.fhtrier.fmg.core.runtime.IFeatureModelMgr;
import de.fhtrier.fmg.core.runtime.IManagedFeatureModel;
import de.fhtrier.fmg.core.stats.IStatistics;
import de.fhtrier.fmg.gui.common.IFMGGui;
import de.fhtrier.fmg.gui.common.IFMGGuiIO;
import de.fhtrier.fmg.gui.common.IGraphContainer;
import de.fhtrier.fmg.impl.runtime.FeatureModelMgr;

public abstract class FMGGuiBase implements IFMGGui, ExceptionListener {

	// IO Stuff
	protected IFMGGuiIO			IO;

	// the model manager
	private IFeatureModelMgr	mgr;

	// statistics
	protected IStatistics		stats;

	// Gui
	protected IGraphContainer	graphContainer;

	public FMGGuiBase() {
		this.IO = null;
		this.mgr = FeatureModelMgr.getInstance();
		this.stats = null;
		this.graphContainer = null;
	}

	@Override
	public IFeatureModelMgr getMgr() {
		return mgr;
	}

	private IManagedFeatureModel getManagedFeatureModel() {
		IManagedFeatureModel m = mgr.getSelected();
		if (m == null)
			throw new RuntimeException(
					"Please select a feature model or create a new one");
		return m;
	}

	@Override
	public void createFeatureModel() {
		try {
			IManagedFeatureModel m = getManagedFeatureModel();
			m.regenerate();
			stats = m.getGenerator().getStats();
			graphContainer.setGraph(m.getGraph());
			graphContainer.reLayout();
		} catch (Exception e) {
			exceptionThrown(e);
		}
	}

	@Override
	public void createRandomFeatureModel() {
		IManagedFeatureModel m = mgr.getSelected();
		if (m != null) {
			m.getParameters().setSeed(-1L);
			createFeatureModel();
		}
	}

	@Override
	public void loadParameters() {
		try {
			IManagedFeatureModel m = mgr.getSelected();
			if (m != null) {
				IO.loadParameters(m.getParameters());
			}
		} catch (IOException e) {
			exceptionThrown(e);
		}
	}

	@Override
	public void saveParameters() {
		try {
			IManagedFeatureModel m = mgr.getSelected();
			if (m != null) {
				IO.saveParameters(m.getParameters());
			}
		} catch (IOException e) {
			exceptionThrown(e);
		}
	}

	@Override
	public IStatistics getStats() {
		return stats;
	}
}
