package de.fhtrier.fmg.gui.common;

import java.io.IOException;

import de.fhtrier.fmg.core.fm.IFeatureModel;
import de.fhtrier.fmg.core.gen.IParameterSet;

public interface IFMGGuiIO {

	void loadParameters(IParameterSet parameterSet) throws IOException;

	void saveAs(String format, IFeatureModel model,
			IParameterSet parameterSet) throws IOException;

	String showAs(String format, IFeatureModel model,
			IParameterSet parameterSet) throws IOException;

	void saveParameters(IParameterSet parameterSet) throws IOException;
}
