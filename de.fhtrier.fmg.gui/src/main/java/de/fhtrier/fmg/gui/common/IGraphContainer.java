package de.fhtrier.fmg.gui.common;

import com.mxgraph.view.mxGraph;

public interface IGraphContainer {

	void setGraph(mxGraph graph);

	void reLayout();
}
