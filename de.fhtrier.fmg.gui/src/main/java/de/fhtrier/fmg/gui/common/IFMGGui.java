package de.fhtrier.fmg.gui.common;

import de.fhtrier.fmg.core.runtime.IFMGProcessor;
import de.fhtrier.fmg.core.runtime.IFeatureModelMgr;

public interface IFMGGui extends IFMGProcessor {

	IFeatureModelMgr getMgr();

	void createFeatureModel();

	void createRandomFeatureModel();

	void loadParameters();

	void saveAs(String format);

	void saveParameters();

	void showAs(String format);
}
