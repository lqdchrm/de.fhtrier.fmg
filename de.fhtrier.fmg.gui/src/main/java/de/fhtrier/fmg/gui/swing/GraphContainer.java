package de.fhtrier.fmg.gui.swing;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

import de.fhtrier.fmg.core.runtime.IFeatureModelMgr;
import de.fhtrier.fmg.core.runtime.IFeatureModelMgrListener;
import de.fhtrier.fmg.core.runtime.IManagedFeatureModel;
import de.fhtrier.fmg.gui.common.IGraphContainer;

public class GraphContainer extends mxGraphComponent implements
		IGraphContainer, IFeatureModelMgrListener {

	/**
	 * 
	 */
	private static final long			serialVersionUID	= -486333570593040178L;

	private IFeatureModelMgr			mgr;
	private GraphContainerMouseHandler	mouseHandler;

	public GraphContainer(IFeatureModelMgr mgr) {
		super(new mxGraph());

		this.mgr = mgr;
		this.mgr.addFeatureModelMgrListener(this);

		this.mouseHandler = new GraphContainerMouseHandler(this);
		this.getGraphControl().addMouseListener(mouseHandler);
		this.getGraphControl().addMouseWheelListener(mouseHandler);
		this.getGraphControl().addMouseMotionListener(mouseHandler);

		setDoubleBuffered(true);
		setDragEnabled(false);
		setEnabled(false);
		setPanning(true);
		setCenterZoom(true);
		setAntiAlias(true);
	}

	@Override
	public void reLayout() {
		invalidate();
		repaint();
		getParent().doLayout();
	}

	@Override
	public void onSelectionChanged(IManagedFeatureModel m) {
		if (m != null && m.getGraph() != null) {
			setGraph(m.getGraph());
			reLayout();
		} else {
			setGraph(new mxGraph());
			reLayout();
		}
	}
	
	@Override
	public void setGraph(mxGraph arg0) {
		super.setGraph(arg0);
		if (mouseHandler != null)
			mouseHandler.updateZoom();
	}
}
