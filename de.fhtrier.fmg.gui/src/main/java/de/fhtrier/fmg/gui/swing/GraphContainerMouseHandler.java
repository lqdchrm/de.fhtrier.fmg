package de.fhtrier.fmg.gui.swing;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class GraphContainerMouseHandler implements MouseListener,
		MouseWheelListener, MouseMotionListener {

	private GraphContainer	container;
	private double			zoomScale;
	
	private Point startDrag;
	private Rectangle startRect;
	

	public GraphContainerMouseHandler(GraphContainer container) {
		if (container == null)
			throw new IllegalArgumentException(new NullPointerException());

		this.container = container;
		this.zoomScale = 1.0;
	}

	public void updateZoom() {
		container.zoomTo(zoomScale, true);
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent arg0) {
		if (arg0.getPreciseWheelRotation() > 0) {
			zoomScale /= 1.2;
		} else {
			zoomScale *= 1.2;
		}
		updateZoom();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON2) {
			zoomScale = 1.0;
			updateZoom();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		startDrag = new Point(e.getX(), e.getY());
		startRect = container.getGraphControl().getVisibleRect();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {

		Point currentDrag = arg0.getPoint();
		Point delta = new Point(startDrag.x - currentDrag.x, startDrag.y - currentDrag.y);
				
		startRect.translate(delta.x, delta.y);
		
		container.getGraphControl().scrollRectToVisible(startRect);
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
