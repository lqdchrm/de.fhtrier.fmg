package de.fhtrier.fmg.gui.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.ListModel;
import javax.swing.border.TitledBorder;

import de.fhtrier.fmg.core.runtime.IFeatureModelMgr;
import de.fhtrier.fmg.core.runtime.IManagedFeatureModel;
import de.fhtrier.fmg.core.runtime.IModelList;
import de.fhtrier.fmg.impl.runtime.FrameworkFactory;
import de.fhtrier.fmg.impl.runtime.ManagedFeatureModel;

public class ModelList extends JList<IManagedFeatureModel> implements
		IModelList {

	private static final long	serialVersionUID	= -3850472781944439875L;

	private IFeatureModelMgr	mgr;
	private JPopupMenu			menu;
	private ButtonGroup 		btnGrpGenerators;
	
	public ModelList(IFeatureModelMgr mgr, ButtonGroup btnGrpGenerators) {
		super();
		initialize();
		this.mgr = mgr;
		this.btnGrpGenerators = btnGrpGenerators;
		this.setModel(this.mgr);
		this.addListSelectionListener(this.mgr);
	}

	public IFeatureModelMgr getFeatureModelMgr() {
		return mgr;
	}

	public void createNew() {
		this.mgr.addElement(new ManagedFeatureModel(this.mgr.getSize()));
	}

	public void setGen(String generator) {
		IManagedFeatureModel selectedModel = getModelMgr().getSelected();
		if (selectedModel != null) {
			selectedModel.setGenerator(generator);
			for (Enumeration<AbstractButton> e = btnGrpGenerators.getElements(); e.hasMoreElements();) {
				AbstractButton btn = e.nextElement();
				if (btn.getName().equals(generator)) {
					btnGrpGenerators.setSelected(btn.getModel(), true);
				}
			}
		}
	}
	
	private void initialize() {
		setBorder(new TitledBorder(null, "Feature Models",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		menu = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Create New...");
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				createNew();
			}
		});
		menu.add(menuItem);
		this.setComponentPopupMenu(menu);

		for (String s : FrameworkFactory.getCurrent().getGenerators()) {
			final String genName = s;
			JMenuItem mntmSetDefaultGenerator = new JMenuItem(
				"Set Generator: " + genName);
			mntmSetDefaultGenerator.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					setGen(genName);
				}
			});
			menu.add(mntmSetDefaultGenerator);
		}
	}

	@Override
	public IFeatureModelMgr getModelMgr() {
		ListModel<IManagedFeatureModel> model = getModel();
		if (model instanceof IFeatureModelMgr)
			return (IFeatureModelMgr) model;

		return null;
	}
}
