package de.fhtrier.fmg.gui.swing;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

public class LogPanel extends JPanel {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 4500363190139718972L;

	private JTextArea			tbLog;

	public LogPanel() {
		super();
		initialize();
	}

	public void setText(String text) {
		tbLog.setText(text);
	}

	private void initialize() {
		setBorder(new TitledBorder(null, "Log", TitledBorder.LEFT,
				TitledBorder.TOP, null, null));
		setLayout(new MigLayout("", "[grow,fill]", "[grow,fill]"));

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 0,alignx left,aligny top");

		tbLog = new JTextArea();
		scrollPane.setViewportView(tbLog);
	}
}
