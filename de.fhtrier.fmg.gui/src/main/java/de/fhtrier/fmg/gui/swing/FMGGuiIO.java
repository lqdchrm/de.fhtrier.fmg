package de.fhtrier.fmg.gui.swing;

import java.awt.Dialog.ModalityType;
import java.awt.FileDialog;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.StringWriter;

import javax.swing.JFrame;

import de.fhtrier.fmg.core.fm.IFeatureModel;
import de.fhtrier.fmg.core.gen.IParameterSet;
import de.fhtrier.fmg.gui.common.IFMGGuiIO;
import de.fhtrier.fmg.io.fm.IFMWriter;
import de.fhtrier.fmg.io.fm.impl.FMWriterDescriptor;
import de.fhtrier.fmg.io.fm.impl.FMWriterFactory;
import de.fhtrier.fmg.io.parameters.impl.ParameterSetSerializer;

public class FMGGuiIO implements IFMGGuiIO {

	private JFrame	frame;

	public FMGGuiIO(JFrame frame) {
		this.frame = frame;
	}

	@Override
	public void loadParameters(IParameterSet parameterSet) throws IOException {
		File file = chooseFile(parameterSet.getName() + ".fmg",
				new String[] { "*.fmg" },
				new String[] { "FMG Parameters (*.fmg)" },
				true);
		if (file != null) {
			ParameterSetSerializer ser = new ParameterSetSerializer();
			ser.loadFromFile(parameterSet, new FileReader(file));
		}
	}

	@Override
	public String showAs(String format, IFeatureModel model,
			IParameterSet parameterSet) throws IOException {
		if (FMWriterFactory.getInstance().containsType(format)) {
			StringWriter strWriter = new StringWriter();
			BufferedWriter writer = new BufferedWriter(strWriter);
			IFMWriter w = FMWriterFactory.getInstance().createInstanceOfType(format, writer);
			w.write(model, parameterSet.getName());
			w.close();
			return strWriter.toString();
		} else
			throw new IllegalArgumentException("Unknown format");
	}

	@Override
	public void saveAs(String format, IFeatureModel model,
			IParameterSet parameterSet) throws IOException {

		if (FMWriterFactory.getInstance().containsType(format)) {
			FMWriterDescriptor desc = FMWriterFactory.getInstance()
					.getDescriptor(
							format);

			File file = chooseFile(
					parameterSet.getName() + "." + desc.getFileExtension(),
					new String[] { "*." + desc.getFileExtension() },
					new String[] { desc.getDescription() + " (*."
							+ desc.getFileExtension() + ")" },
					false);

			if (file != null) {
				BufferedWriter writer = new BufferedWriter(new FileWriter(file));
				IFMWriter w = FMWriterFactory.getInstance().createInstanceOfType(format, writer);
				w.write(model, parameterSet.getName());
				w.close();
			}
		} else
			throw new IllegalArgumentException("Unknown format");
	}

	@Override
	public void saveParameters(IParameterSet parameterSet) throws IOException {
		File file = chooseFile(parameterSet.getName() + ".fmg",
				new String[] { "*.fmg" },
				new String[] { "FMG Parameters (*.fmg)" },
				false);
		if (file != null) {
			ParameterSetSerializer ser = new ParameterSetSerializer();
			ser.saveToFile(parameterSet, new FileWriter(file));
		}
	}

	private File chooseFile(String initialFileName, String[] extensions,
			String[] names, boolean load) {
		FileDialog fileDialog = new FileDialog(frame);
		fileDialog.setMode(load ? FileDialog.LOAD : FileDialog.SAVE);

		fileDialog.setTitle(load ? "Load..." : "Save...");
		fileDialog.setFile(initialFileName);
		fileDialog.setFilenameFilter(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return true;
			}
		});

		fileDialog.setModalityType(ModalityType.APPLICATION_MODAL);
		fileDialog.setVisible(true);

		if (fileDialog.getFile() != null) {
			String firstFile = fileDialog.getDirectory() + '/'
					+ fileDialog.getFile();

			File file = new File(firstFile);
			return file;
		}
		return null;

	}
}
