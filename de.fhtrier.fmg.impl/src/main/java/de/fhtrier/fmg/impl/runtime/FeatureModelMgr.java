package de.fhtrier.fmg.impl.runtime;

import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.event.ListSelectionEvent;

import de.fhtrier.fmg.core.runtime.IFeatureModelMgr;
import de.fhtrier.fmg.core.runtime.IFeatureModelMgrListener;
import de.fhtrier.fmg.core.runtime.IManagedFeatureModel;
import de.fhtrier.fmg.core.runtime.IManagedFeatureModelChangedListener;
import de.fhtrier.fmg.core.runtime.IModelList;

public class FeatureModelMgr extends DefaultListModel<IManagedFeatureModel>
		implements IFeatureModelMgr, IManagedFeatureModelChangedListener {

	/**
	 * 
	 */
	private static final long		serialVersionUID	= 6158066236715980085L;
	IManagedFeatureModel			selected;
	List<IFeatureModelMgrListener>	listeners;

	private static IFeatureModelMgr	instance			= null;

	public static IFeatureModelMgr getInstance() {
		if (FeatureModelMgr.instance == null) {
			FeatureModelMgr.instance = new FeatureModelMgr();
		}

		return FeatureModelMgr.instance;
	}

	private FeatureModelMgr() {
		this.selected = null;
		this.listeners = new ArrayList<>();
	}

	@Override
	public IManagedFeatureModel getSelected() {
		return selected;
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource() instanceof IModelList) {
			IModelList ml = (IModelList) e.getSource();
			if (ml.getModelMgr() == this) {
				if (selected != ml.getSelectedValue()) {
					selected = ml.getSelectedValue();
					fireSelectedChanged();
				}
			}
		}
	}

	@Override
	public void addElement(IManagedFeatureModel element) {
		super.addElement(element);
		element.add(this);
	}

	@Override
	public boolean removeElement(Object obj) {
		if (obj instanceof IManagedFeatureModel) {
			((IManagedFeatureModel) obj).remove(this);
		}
		return super.removeElement(obj);
	}

	@Override
	public void onChanged(IManagedFeatureModel m) {
		int idx = indexOf(m);
		if (idx > -1) {
			fireContentsChanged(this, idx, idx);
			fireSelectedChanged();
		}
	}

	@Override
	public void addFeatureModelMgrListener(IFeatureModelMgrListener l) {
		if (!listeners.contains(l)) {
			listeners.add(l);
		}
	}

	@Override
	public void removeFeatureModelMgrListener(IFeatureModelMgrListener l) {
		if (listeners.contains(l)) {
			listeners.remove(l);
		}
	}

	private void fireSelectedChanged() {
		for (IFeatureModelMgrListener l : listeners) {
			l.onSelectionChanged(selected);
		}
	}

	@Override
	public IManagedFeatureModel getById(int id) {
		for (int i = 0; i < this.getSize(); ++i) {
			IManagedFeatureModel m = this.get(i);
			if (m.getId() == id)
				return m;
		}
		return null;
	}
}
