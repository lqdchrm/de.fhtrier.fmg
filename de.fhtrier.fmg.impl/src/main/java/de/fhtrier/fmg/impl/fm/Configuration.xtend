package de.fhtrier.fmg.impl.fm

import java.util.HashSet
import de.fhtrier.fmg.core.fm.IConfiguration
import de.fhtrier.fmg.core.fm.IFeatureModel

class Configuration implements IConfiguration {
	
	IFeatureModel model
	HashSet<Integer> features

	new(IFeatureModel model) {
		if (model == null)
			throw new IllegalArgumentException()
		
		this.model = model
		this.features = newHashSet
	}

	override getModel() {
		return model
	}

	override getFeatures() {
		features.map[model.getFeature(it)]
	}
		
	override hasFeature(int id) {
		features.contains(id)
	}

	override addFeature(int id) {
		features.add(id)
	}
	
	override removeFeature(int id) {
		features.remove(id)
	}
	
}