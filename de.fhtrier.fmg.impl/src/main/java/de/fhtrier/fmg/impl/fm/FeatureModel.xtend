package de.fhtrier.fmg.impl.fm

import de.fhtrier.fmg.core.fm.ICrossTreeConstraint
import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.IFeatureModel
import java.util.HashMap
import java.util.List

class FeatureModel implements IFeatureModel {
	
	int id
	String name
	String featurePrefix
	
	IFeature root
	HashMap<Integer, IFeature> features
	List<ICrossTreeConstraint> ctcs
	
	new(int id, String name, String featurePrefix) {
		this.id = id
		this.name = name
		this.featurePrefix = featurePrefix
		
		this.root = null
		this.features = newHashMap
		this.ctcs = newArrayList
	}
	
	override getId() {
		id
	}
	
	override getName() {
		name
	}
		
	override getFeaturePrefix() {
		featurePrefix
	}
		
	override getCrossTreeConstraints() {
		ctcs
	}
	
	override getFeature(int id) {
		features.get(id)
	}
	
	override getFeatures() {
		features.values
	}
		
	override getRoot() {
		root
	}
	
	def setRoot(IFeature f) {
		root = f
	}
	
	def addFeature(IFeature f) {
		features.put(f.id, f)
	}
	
	def addCrossTreeConstraint(ICrossTreeConstraint ctc) {
		ctcs.add(ctc)
	}	
}