package de.fhtrier.fmg.impl.gen.generators

import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.RelationType
import de.fhtrier.fmg.impl.gen.ParameterSet

class CustomGenerator extends DefaultGenerator {
	
	private val NUM_LEVELS_MANDATORY = "Mandatory Levels";
	
	new(String name) {
		super(name)
	}

	override addParameters(ParameterSet pms) {
		super.addParameters(pms);

		pms.addParam(NUM_LEVELS_MANDATORY, 3, "number of top-levels with only mandatory features")
	}	
	
	override chooseRelationType(IFeature parent) {
		
		if (parent.collectParents.size < params.getInt(NUM_LEVELS_MANDATORY))
			RelationType::Mandatory
		else
			super.chooseRelationType(parent)
	}
}