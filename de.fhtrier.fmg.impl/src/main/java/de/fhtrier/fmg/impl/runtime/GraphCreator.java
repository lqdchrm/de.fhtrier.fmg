package de.fhtrier.fmg.impl.runtime;

import com.mxgraph.view.mxGraph;

import de.fhtrier.fmg.core.fm.ICrossTreeConstraint;
import de.fhtrier.fmg.core.fm.IFeature;
import de.fhtrier.fmg.core.fm.IFeatureModel;
import de.fhtrier.fmg.core.fm.IRelation;

public class GraphCreator implements Runnable {

	public static final int	CREATENONE				= 0;
	public static final int	CREATEVERTICESANDEDGES	= 1;
	public static final int	CREATECTCS				= 2;

	private IFeatureModel	model;
	private mxGraph			graph;
	private int				mode;

	public GraphCreator(IFeatureModel model, mxGraph graph) {
		this.model = model;
		this.graph = graph;
		this.mode = GraphCreator.CREATEVERTICESANDEDGES | GraphCreator.CREATECTCS;
	}

	public void run(int mode) {
		this.mode = mode;
		run();
	}

	@Override
	public void run() {
		configGraph();

		graph.getModel().beginUpdate();
		try {
			if ((this.mode & GraphCreator.CREATEVERTICESANDEDGES) == GraphCreator.CREATEVERTICESANDEDGES) {
				createVertices();
				createEdges();
			}
			if ((this.mode & GraphCreator.CREATECTCS) == GraphCreator.CREATECTCS) {
				createCTCs();
			}
		} catch (InterruptedException e) {
		} finally {
			graph.getModel().endUpdate();
		}
	}

	private void configGraph() {
		graph.setEnabled(false);
		graph.setDisconnectOnMove(false);
		graph.setCellsEditable(false);
		graph.setCellsDisconnectable(false);
		graph.setCellsDeletable(false);
		graph.setCellsCloneable(false);
		graph.setCellsResizable(false);
		graph.setResetEdgesOnMove(true);
		graph.setResetEdgesOnConnect(true);
		graph.setCellsSelectable(true);
		graph.setCellsBendable(false);
		graph.setExtendParentsOnAdd(true);
		graph.setDropEnabled(false);
	}

	private void createCTCs() throws InterruptedException {
		Object parent = graph.getDefaultParent();

		for (ICrossTreeConstraint ctc : model.getCrossTreeConstraints()) {
			if (ctc != null) {
				IFeature fSrc = ctc.getOrigin();
				Object vSrc = fSrc.getUserData();
				if (vSrc != null) {
					IFeature fDest = ctc.getDestination();
					if (fDest != null) {
						Object vDest = fDest.getUserData();
						String style = "strokeColor=#";
						switch (ctc.getType()) {
						case Excludes:
							style += "FF0000";
							break;
						case Requires:
							style += "00CC00";
							break;
						default:
							break;
						}
						graph.insertEdge(parent, null, ctc, vSrc, vDest, style);
					}
				}
			}
		}
	}

	private void createEdges() throws InterruptedException {
		Object parent = graph.getDefaultParent();

		if (model != null) {
			for (IFeature feature : model.getFeatures()) {
				if (feature != null) {
					for (IRelation r : feature.getChildren()) {
						if (r != null) {
							IFeature fSrc = r.getOrigin();
							Object vSrc = fSrc.getUserData();
							if (vSrc != null) {
								int numDests = r.getNumDestinations();

								Object group = null;
								if (numDests > 0) {
									group = graph
											.insertVertex(
													parent,
													null,
													r.getType(),
													0,
													0,
													22,
													22,
													"fillColor=#CCCCCC;shape="
															+ com.mxgraph.util.mxConstants.SHAPE_ELLIPSE);
									graph.insertEdge(parent, null, "", vSrc,
											group);
								}

								for (IFeature fDest : r.getDestinations()) {
									if (fDest != null) {
										Object vDest = fDest.getUserData();
										String style = "dashed=";
										switch (r.getType()) {
										case Mandatory:
											style += "0";
											break;
										default:
											style += "1";
											break;
										}

										if (numDests <= 0) {
											graph.insertEdge(parent, null, r,
													vSrc, vDest, style);
										} else {
											graph.insertEdge(parent, null, r,
													group, vDest, style);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void createVertices() throws InterruptedException {
		Object parent = graph.getDefaultParent();

		if (model != null) {
			for (IFeature feature : model.getFeatures()) {
				if (feature != null) {
					Object v = graph.insertVertex(parent, null, feature, 0, 0,
							26, 26);
					feature.setUserData(v);
				}
			}
		}
	}
}
