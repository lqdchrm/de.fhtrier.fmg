 package de.fhtrier.fmg.impl.gen.generators

import de.fhtrier.fmg.core.fm.CrossTreeConstraintType
import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.RelationType
import de.fhtrier.fmg.impl.gen.GeneratorBase
import de.fhtrier.fmg.impl.gen.ParameterSet
import java.util.List
import java.lang.Math

class DefaultGenerator extends GeneratorBase {
	
	// params
	protected val MAX_BRANCHING_FACTOR = "Max Branching Factor";
	protected val MAX_SET_CHILDREN = "Max Children per Set";
	protected val NUM_CTCS = "Number of CTCs";
	protected val PROB_REQ = "Prob. Requires/Excludes";
	
	new(String name) {
		super(name)
	}
	
	override addParameters(ParameterSet pms) {
		super.addParameters(pms);

		pms.addParam(MAX_BRANCHING_FACTOR, 3, "statistical mean of outgoing relations per feature")
		pms.addParam(MAX_SET_CHILDREN, 3, "statistical mean of generated children per or-/xor-group")
		pms.addParam(NUM_CTCS, 0, "number of cross-tree-constraints to generate")
		pms.addParam(PROB_REQ, 0.5f, "ratio of *Requires* to *Excludes* constraints (from 0.0 to 1.0)");
	}	

	override createFeatureTree() {
		// add root feature
		model.root = cache.feature(0)

		// create children

		// track which features should get children
		var List<IFeature> worklist = newArrayList(cache.feature(0))
					
		while (worklist.size > 0 && model.features.size < params.getInt(NUM_FEATURES))
		{
			// track which new features were created
			val List<IFeature> newFeatures = newArrayList
			
			// generate children for every feature in worklist
			for(f : worklist) {
				
				if (model.features.size < params.getInt(NUM_FEATURES)) {
					val featuresCreated = createChildrenForFeature(f) 
					newFeatures.addAll(featuresCreated)
				}
			}
			
			// if new features have been created, iterate
			if (newFeatures.size > 0)
				worklist = newFeatures
		}
	}
	
	def protected createChildrenForFeature(IFeature parent) {
		
		// track children created
		val List<IFeature> childrenCreated = newArrayList
		
		// get number of relations, that should be created
		val numRelations = featureRand.nextInt(params.getInt(MAX_BRANCHING_FACTOR)+1)

		// create relations
		for (i : 0..numRelations) {

			// determine relation type	
			var relType = chooseRelationType(parent)
			
			// create relation with features
			val featuresCreated = addRelationToFeature(parent, relType)
			
			// add created features
			childrenCreated.addAll(featuresCreated)
		}
	
		// return list of newly created children
		return childrenCreated
	}
	
	def protected chooseRelationType(IFeature parent) {
		// equally distributed probability for each relation type
		RelationType::values.get(featureRand.nextInt(RelationType::values.size))
	}
	
	def protected addRelationToFeature(IFeature f, RelationType type) {

		// track childrenCreated
		val List<IFeature> childrenCreated = newArrayList

		// determine number of children for relation
		val numChildren = chooseNumChildren(type)
		
		// if we have children
		if (numChildren >= 1) {
			// iterate
			for(j : 1..numChildren) {
					
				// create child and add it
				val child = cache.feature(model.features.size)
				childrenCreated.add(child)
			}
		}

		// link children to parent
		if (childrenCreated.size > 0) {
			cache.relation(f, type, childrenCreated)
		}

		return childrenCreated
	}
	
	def private chooseNumChildren(RelationType type) {
		
		var numChildren = 0
		
		switch(type) {
			case RelationType::Mandatory: {
				numChildren = featureRand.nextInt(2)
			}
			case RelationType::Optional: {
				numChildren = featureRand.nextInt(2)
			}
			case RelationType::Or: {
				numChildren = featureRand.nextInt(params.getInt(MAX_SET_CHILDREN)+1)
				if (numChildren == 1)
					//relType = RelationType::Optional
					numChildren = 0
			}
			case RelationType::Xor: {
				numChildren = featureRand.nextInt(params.getInt(MAX_SET_CHILDREN)+1)
				if (numChildren == 1)
					// relType = RelationType::Mandatory
					numChildren = 0
			}
		}
		
		// check if we have still features to produce
		val childrenLeft = params.getInt(NUM_FEATURES)-model.features.size
		numChildren = Math::min(numChildren, childrenLeft)
		
		return numChildren
	}
		
	override createCTCs() {
		while (model.crossTreeConstraints.size < params.getInt(NUM_CTCS)) {
			var f1 = model.getFeature(ctcRand.nextInt(model.features.size)+1)
			var f2 = model.getFeature(ctcRand.nextInt(model.features.size)+1)
			while (!areNCPFeatures(f1, f2)) {
				f1 = model.getFeature(ctcRand.nextInt(model.features.size)+1)
				f2 = model.getFeature(ctcRand.nextInt(model.features.size)+1)
			}		
			val type = getCTCType
			cache.ctc(f1, type, f2)
		}
	}				
	
	def private getCTCType() {
		if (ctcRand.nextFloat() < params.getFloat(PROB_REQ)) {
			CrossTreeConstraintType::Requires
		} else {
			CrossTreeConstraintType::Excludes
		}
	}
		
	/**
	 * areNoCommomPathFeatures - checks if features are not on same path
	 * 
	 * @return true, if features are on different paths
	 */
	def protected boolean areNCPFeatures(IFeature f1, IFeature f2) {
		if (f1 != null && f2 != null && f1 != f2) {
			return (!collectParents(f1).contains(f2) && !collectParents(f2).contains(f1))
		}
		return false
	}
}