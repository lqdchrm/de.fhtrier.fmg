package de.fhtrier.fmg.impl.fm

import de.fhtrier.fmg.core.fm.ICrossTreeConstraint
import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.CrossTreeConstraintType

class CrossTreeConstraint implements ICrossTreeConstraint {
	
	int 	 id
	IFeature origin
	IFeature dest
	CrossTreeConstraintType type

	new(int id, IFeature origin, CrossTreeConstraintType type, IFeature dest) {
		this.id = id
		this.origin = origin
		this.dest = dest
		this.type = type
	}

	override getId() {
		return id
	}
	
	override getDestination() {
		return dest
	}
	
	override getOrigin() {
		return origin
	}
	
	override getType() {
		return type
	}
	
	override toString() {
		return type.toString
	}
}