package de.fhtrier.fmg.impl.fm

import java.util.List
import de.fhtrier.fmg.core.fm.IRelation
import de.fhtrier.fmg.core.fm.RelationType
import de.fhtrier.fmg.core.fm.IFeature

class Relation implements IRelation {
	
	List<IFeature> dests
	IFeature origin
	RelationType type

	new(IFeature origin, RelationType type, Iterable<? extends IFeature> dests) {
		this.origin = origin
		this.type = type
		this.dests = newArrayList()
		dests.forEach[this.dests.add(it)]
	}

	override getDestinations() {
		return this.dests
	}
		
	override getOrigin() {
		return this.origin
	}
	
	override getType() {
		return this.type
	}
	
	override toString() {
		""
	}

	override getNumDestinations() {
		return this.dests.size
	}
	
}