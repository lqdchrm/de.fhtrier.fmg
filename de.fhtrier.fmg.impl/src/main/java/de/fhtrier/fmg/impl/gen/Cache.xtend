package de.fhtrier.fmg.impl.gen

import de.fhtrier.fmg.core.ops.IFMOperations
import de.fhtrier.fmg.core.fm.CrossTreeConstraintType
import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.RelationType
import de.fhtrier.fmg.impl.fm.Feature
import de.fhtrier.fmg.impl.fm.FeatureModel
import de.fhtrier.fmg.impl.fm.Relation
import de.fhtrier.fmg.impl.fm.CrossTreeConstraint

class Cache implements IFMOperations {

	private FeatureModel model
	
	new(FeatureModel model) {
		this.model = model
	}
	
	override create result : new Feature(id, this.model.featurePrefix + id) feature(int id) {
		if (id == 0) {
			result.name = this.model.featurePrefix + "root"
		}
		
		model.addFeature(result)
	}
	
	override create result : new Relation(origin, type, dests) relation(IFeature origin, RelationType type, Iterable<? extends IFeature> dests) {
		(origin as Feature)?.addChild(result)
		dests.forEach[(it as Feature)?.setParent(result)]
	}
	
	def create result : new CrossTreeConstraint(model.crossTreeConstraints.size, origin, type, dest) ctc(IFeature origin, CrossTreeConstraintType type, IFeature dest) {
		model.addCrossTreeConstraint(result)
	}   
}