package de.fhtrier.fmg.impl.stats

import de.fhtrier.fmg.core.stats.IStatistics

class Statistics implements IStatistics {
	
	public int Depth
	public int NumNodes
	public int NumRequires
	public int NumExcludes
	public long startTime
	public long stopTime
	

	override toString() '''
		Depth: «Depth»
		NumNodes: «NumNodes»
		NumRequires: «NumRequires»
		NumExcludes: «NumExcludes»
		Duration: «(stopTime-startTime)» ms
	'''
	override getDepth() {
		Depth
	}
	
	override getNumNodes() {
		NumNodes
	}

	override getNumExcludes() {
		NumExcludes
	}
	
	override getNumRequires() {
		NumRequires
	}
	
	override getStartTime() {
		startTime
	}
	
	override getEndTime() {
		stopTime
	}
	
}