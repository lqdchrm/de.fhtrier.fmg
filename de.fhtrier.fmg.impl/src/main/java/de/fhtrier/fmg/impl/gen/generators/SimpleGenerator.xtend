 package de.fhtrier.fmg.impl.gen.generators

import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.RelationType
import java.util.List
import de.fhtrier.fmg.impl.gen.ParameterSet

/**
 * A simple feature generator, that is similar to the default generator,
 * except that it clearly separates features and feature groups.
 * 
 * In this generator a feature group is a feature with a *single* child relation
 * of type OR or XOR.
 * 
 * Non feature groups are features with multiple child relations, which each can
 * be of type Optional or Mandatory 
 */
class SimpleGenerator extends DefaultGenerator {
		
	new(String name) {
		super(name)
	}
	
	override addParameters(ParameterSet pms) {
		super.addParameters(pms)
		
		pms.setValue(MAX_BRANCHING_FACTOR, 7)
		pms.setValue(MAX_SET_CHILDREN, 5)
		pms.setValue(NUM_CTCS, 10)
	}
	
	override createChildrenForFeature(IFeature parent) {
		
		// track children created
		val List<IFeature> childrenCreated = newArrayList
		
		// get number of relations, that should be created
		val createFeatureGroup = featureRand.nextBoolean
		var numRelations = 0
		if (!createFeatureGroup)
			numRelations = featureRand.nextInt(params.getInt(MAX_BRANCHING_FACTOR))

		// create relations
		for (i : 0..numRelations) {

			// determine relation type	
			var relType = chooseRelationType(numRelations, parent)
			
			// create relation with features
			val featuresCreated = addRelationToFeature(parent, relType)
			
			// add created features
			childrenCreated.addAll(featuresCreated)
		}
	
		// return list of newly created children
		return childrenCreated
	}
	
	def protected chooseRelationType(int numRelations, IFeature parent) {

		if (numRelations > 0)
		{
			val isOptional = featureRand.nextBoolean
			if (isOptional)
				RelationType::Optional
			else
				RelationType::Mandatory	
		} else
		{
			// equally distributed probability for each relation type
			RelationType::values.get(featureRand.nextInt(RelationType::values.size))
		}
	}
}