package de.fhtrier.fmg.impl.fm

import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.IRelation
import java.util.List

class Feature implements IFeature {
	
	int id
	String name
	IRelation parent
	List<IRelation> children
	Object userData
	
	new(int id, String name) {
		this.id = id
		this.name = name
		this.parent = null 
		this.children = newArrayList
	}

	override getId() {
		return id
	}

	override getName() {
		return name
	}
	
	override getParent() {
		return parent
	}
	
	override getChildren() {
		return children
	}
		
	override getUserData() {
		return userData
	}
	
	override setUserData(Object data) {
		this.userData = data
	}
	
	override toString() {
		name
	}
	
	def setName(String name) {
		this.name = name
	}
	
	def setParent(IRelation r) {
		this.parent = r
	}
	
	def addChild(IRelation r) {
		children.add(r)
	}
}