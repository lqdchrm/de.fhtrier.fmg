package de.fhtrier.fmg.impl.gen

import de.fhtrier.fmg.core.gen.IParameterSet
import java.util.List
import java.util.HashMap
import de.fhtrier.fmg.core.gen.IParameter
import de.fhtrier.fmg.core.gen.IParameterSetChangedListener

class ParameterSet implements IParameterSet {
	
	List<IParameterSetChangedListener> listeners
	HashMap<String, IParameter<Integer>> ints
	HashMap<String, IParameter<Long>> longs
	HashMap<String, IParameter<Float>> floats
	HashMap<String, IParameter<String>> strings
	
	List<String> params

	def addParam(String key, int value) {
		val param = new Parameter(key, typeof(int), value) 
		ints.put(key, param)
		params.add(param.name)
	}

	def addParam(String key, int value, String desc) {
		val param = new Parameter(key, typeof(int), value, desc) 
		ints.put(key, param)
		params.add(param.name)
	}

	def addParam(String key, long value) {
		val param = new Parameter(key, typeof(long), value)
		longs.put(key, param)
		params.add(param.name)
	}

	def addParam(String key, long value, String desc) {
		val param = new Parameter(key, typeof(long), value, desc)
		longs.put(key, param)
		params.add(param.name)
	}

	def addParam(String key, float value) {
		val param = new Parameter(key, typeof(float), value)
		floats.put(key, param)
		params.add(param.name)
	}
	
	def addParam(String key, float value, String desc) {
		val param = new Parameter(key, typeof(float), value, desc)
		floats.put(key, param)
		params.add(param.name)
	}

	def addParam(String key, String value) {
		val param = new Parameter(key, typeof(String), value)
		strings.put(key, param)
		params.add(param.name)
	}

	def addParam(String key, String value, String desc) {
		val param = new Parameter(key, typeof(String), value, desc)
		strings.put(key, param)
		params.add(param.name)
	}

	new(String name, long seed) {
		listeners = newArrayList
		this.ints = newHashMap
		this.longs = newHashMap
		this.floats = newHashMap
		this.strings = newHashMap
		this.params = newArrayList

    	addParam("Name", name, "The feature model's name");
    	addParam("Seed", seed, "A seed to deterministically reproduce this exact model");
	}

	override getName() {
		return getString("Name");
	}

	override setName(String name) {
		setValue("Name", name);
	}
	
	override getSeed() {
		return getLong("Seed");
	}

	override setSeed(long seed) {
    	setValue("Seed", seed);
	}

	override addListener(IParameterSetChangedListener l) {
		if (!listeners.contains(l)) {
			listeners.add(l)
			l.onChanged(this)
		}
	}
		
	override removeListener(IParameterSetChangedListener l) {
		if (listeners.contains(l))
			listeners.remove(l)
	}
	
	override getListeners() {
		listeners
	}
	
	override notifyChanged() {
		listeners.forEach[it.onChanged(this)]
	}

	override getFloat(String key) {
		val param = floats.get(key)
		if (param != null) {
			return param.value
		} else {
			throw new IllegalArgumentException("Unknown parameter " + key)
		}
	}
	
	override getInt(String key) {
		ints.get(key).value
	}
	
	override getLong(String key) {
		longs.get(key).value
	}
	
	override getString(String key) {
		strings.get(key).value
	}
	

	override setValue(String key, int value) {
		val param = ints.get(key)
		
		if (param != null) {
			param.value = value
		} else {
			val param2 = longs.get(key)
			if (param2 != null) {
				param2.value = new Long(value)
			} else {
				val param3 = floats.get(key)
				if (param3 != null) {
					param3.value = new Float(value)
				} else {
					throw new IllegalArgumentException("Unknown parameter " + key)
				}
			}	
		}
		
		notifyChanged
	}

	override setValue(String key, long value) {
		longs.get(key).value = value
		notifyChanged
	}

	override setValue(String key, float value) {
		floats.get(key).value = value
		notifyChanged
	}
	
	override setValue(String key, String value) {
		strings.get(key).value = value
		notifyChanged
	}

	override getParams() {
		return params
	}

	override getType(String key) {
		getParam(key)?.type.name.split("[.]").last.toLowerCase
	}

	def getParam(String key) {
		var IParameter<?> param = ints.get(key)
		if (param == null)
			param = longs.get(key)
		if (param == null)
			param = floats.get(key)
		if (param == null)
			param = strings.get(key)
		
		param
	}

  override getAsString(String key) {
    getParam(key).value.toString
  }

	override getDescription(String key) {
		val param = getParam(key)
		
		if (param != null)
			param.description	
	}
}
