package de.fhtrier.fmg.impl.gen.generators

import de.fhtrier.fmg.core.gen.IGenerator
import de.fhtrier.fmg.core.gen.IGeneratorListener
import de.fhtrier.fmg.core.gen.IParameterSet
import de.fhtrier.fmg.core.fm.CrossTreeConstraintType
import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.RelationType
import de.fhtrier.fmg.core.runtime.IManagedFeatureModel
import de.fhtrier.fmg.impl.stats.Statistics
import de.fhtrier.fmg.impl.fm.FeatureModel
import de.fhtrier.fmg.impl.runtime.FeatureModelMgr
import de.fhtrier.fmg.impl.gen.Cache
import de.fhtrier.fmg.impl.gen.ParameterSet
import de.fhtrier.fmg.impl.gen.GeneratorBase
import java.util.Random
import java.util.List

class MergeGenerator implements IGenerator {

	// internals
	List<IGeneratorListener> listeners

	// params
	private val FM_A = "Feature Model A"
	private val FM_B = "Feature Model B"
	private val NUM_CTCS = "Number of CTCs";
	private val PROB_REQ = "Prob. Requires/Excludes";
	
	
	// context
	String name
	Cache cache
	FeatureModel destModel
	IManagedFeatureModel modelA
	IManagedFeatureModel modelB
	IParameterSet params 
	
	Random rand
	
	new(String name) {
		this.name = name
		listeners = newArrayList
		cache = null
	}

	override addListener(IGeneratorListener l) {
		if (!listeners.contains(l))
			listeners.add(l)
	}
	
	override createParameterSet(String name, int seed) {
		var pms = new ParameterSet(name, seed)
		pms.addParam(FM_A, -1)
		pms.addParam(FM_B, -1)
		pms.addParam(NUM_CTCS, 5)
		pms.addParam(PROB_REQ, 0.5f)
		
		return pms
	}
	
	override generate(IParameterSet params) {
		this.params = params
		
		// create new feature model
		this.destModel = new FeatureModel(0, this.params.name, "")
		this.cache = new Cache(this.destModel)
		
		// init random generator
		this.params.seed = GeneratorBase::calcSeed(this.params.seed)
		this.rand = new Random(this.params.seed);
		
		// get models
		modelA = FeatureModelMgr::getInstance().getById(params.getInt(FM_A));
		modelB = FeatureModelMgr::getInstance().getById(params.getInt(FM_B));
		
		if (modelA==null || modelB==null)
			return null
		
		// calc offsets
		val offsetModelA = 1
		val offsetModelB = offsetModelA + modelA.model.features.size
		
		// create root
		destModel.root = cache.feature(0)
		
		// copy sub model nodes
		modelA.model.features.forEach[f |
			cache.feature(f.id+offsetModelA)
			f.children.forEach[r |
				val int src = r.origin.id
				val type = r.type
				val dests = r.destinations
				cache.relation(cache.feature(src + offsetModelA),
					type,
					dests.map[d | cache.feature(d.id + offsetModelA)]
				)				
			]
		]
		modelB.model.features.forEach[f |
			cache.feature(f.id+offsetModelB)
			f.children.forEach[r |
				val int src = r.origin.id
				val type = r.type
				val dests = r.destinations
				cache.relation(cache.feature(src + offsetModelB),
					type,
					dests.map[d | cache.feature(d.id + offsetModelB)]
				)				
			]
		]
		
		// copy ctcs
		modelA.model.crossTreeConstraints.forEach[ctc |
			cache.ctc(cache.feature(ctc.origin.id+offsetModelA),
				ctc.type,
				cache.feature(ctc.destination.id+offsetModelA)
			)
		]

		modelB.model.crossTreeConstraints.forEach[ctc |
			cache.ctc(cache.feature(ctc.origin.id+offsetModelB),
				ctc.type,
				cache.feature(ctc.destination.id+offsetModelB)
			)
		]
		
		// link submodels
		val List<IFeature> _modelARoot = newArrayList
		val List<IFeature> _modelBRoot = newArrayList
		_modelARoot.add(cache.feature(modelA.model.root.id+offsetModelA))
		_modelBRoot.add(cache.feature(modelB.model.root.id+offsetModelB))
		
		cache.relation(destModel.root, RelationType::Mandatory, _modelARoot)
		cache.relation(destModel.root, RelationType::Mandatory, _modelBRoot)

		var numCTCsRequired = params.getInt(NUM_CTCS) + modelA.model.crossTreeConstraints.size + modelB.model.crossTreeConstraints.size
				
		while (destModel.crossTreeConstraints.size < numCTCsRequired) {
			var f1 = destModel.getFeature(rand.nextInt(offsetModelB))
			var f2 = destModel.getFeature(offsetModelB + rand.nextInt(modelB.model.features.size-1))
			while (!checkCTCFeatures(f1, f2)) {
				f1 = destModel.getFeature(rand.nextInt(destModel.features.size)+1)
				f2 = destModel.getFeature(rand.nextInt(destModel.features.size)+1)
			}		
			val type = getCTCType
			cache.ctc(f1, type, f2)
		}
		return destModel
	}				
	
	def private getCTCType() {
		if (rand.nextFloat() < params.getFloat(PROB_REQ)) {
			CrossTreeConstraintType::Requires
		} else {
			CrossTreeConstraintType::Excludes
		}
	}
		
	def private boolean checkCTCFeatures(IFeature f1, IFeature f2) {
		if (f1 != null && f2 != null && f1 != f2) {
			return (!collectParents(f1).contains(f2) && !collectParents(f2).contains(f1))
		}
		return false
	}

	def private collectParents(IFeature f) {
		val List<IFeature> parents = newArrayList
		if (f.parent != null) {
			var IFeature cur = f.parent.origin
			while (cur != null) {
				parents.add(cur)
				if (cur.parent != null)
					cur = cur.parent.origin
				else
					cur = null				
			}
		}
		return parents
	}
	
	override getName() {
		return name
	}
	
	override getStats() {
		return new Statistics
	}
	
	override removeListener(IGeneratorListener l) {
		if (listeners.contains(l))
			listeners.remove(l)
	}
}