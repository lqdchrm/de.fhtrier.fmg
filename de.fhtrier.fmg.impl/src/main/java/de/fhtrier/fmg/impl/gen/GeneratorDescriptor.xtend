package de.fhtrier.fmg.impl.gen

import de.fhtrier.fmg.core.gen.IGeneratorDescriptor
import de.fhtrier.fmg.core.gen.IGenerator
import de.fhtrier.fmg.utils.factory.impl.BasicTypeDescriptor

class GeneratorDescriptor extends BasicTypeDescriptor<IGenerator>
		implements IGeneratorDescriptor {
	
	new(String name, Class<? extends IGenerator> returnType) {
		super(name, returnType, getStringType)
	}
	
	def static getStringType() {
		Class::forName("java.lang.String")
	}
}