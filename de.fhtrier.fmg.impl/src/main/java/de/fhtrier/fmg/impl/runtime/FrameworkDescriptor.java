package de.fhtrier.fmg.impl.runtime;

import java.lang.reflect.InvocationTargetException;

import de.fhtrier.fmg.core.runtime.IFramework;
import de.fhtrier.fmg.core.runtime.IFrameworkDescriptor;
import de.fhtrier.fmg.utils.factory.impl.BasicTypeDescriptor;

public class FrameworkDescriptor extends BasicTypeDescriptor<IFramework>
		implements IFrameworkDescriptor {

	private IFramework	instance	= null;

	public FrameworkDescriptor(String name,
			Class<? extends IFramework> returnType) {
		super(name, returnType);
	}

	@Override
	public IFramework createNew(Object... args) {
		if (instance == null) {
			try {
				instance = super.createNew(args);
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				// TODO proper exception handling
				e.printStackTrace();
			}
		}

		return instance;
	}

	@Override
	public IFramework getSingleton() {
		return createNew();
	}
}
