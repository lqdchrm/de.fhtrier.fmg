package de.fhtrier.fmg.impl.runtime;

import de.fhtrier.fmg.core.runtime.IFramework;
import de.fhtrier.fmg.core.runtime.IFrameworkDescriptor;
import de.fhtrier.fmg.utils.factory.impl.Factory;

public class FrameworkFactory extends
		Factory<IFramework, IFrameworkDescriptor> {

	private static FrameworkFactory	instance	= null;
	private static IFramework		current		= null;

	public static FrameworkFactory getInstance() {
		if (FrameworkFactory.instance == null) {
			FrameworkFactory.instance = new FrameworkFactory();
		}

		return FrameworkFactory.instance;
	}

	public static IFramework getCurrent() {
		return FrameworkFactory.current;
	}

	public static void setCurrent(String name) {
		FrameworkFactory.current = getInstance().createInstanceOfType(name);
	}
}
