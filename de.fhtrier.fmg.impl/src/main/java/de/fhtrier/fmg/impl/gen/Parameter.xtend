package de.fhtrier.fmg.impl.gen

import de.fhtrier.fmg.core.gen.IParameter

class Parameter<T> implements IParameter<T> {

	String name
	T value
	String desc
	Class<T> type

	new(String name, Class<T> type, T value) {
		this.name = name
		this.type = type
		this.value = value
		this.desc = ""
	}
	
	new(String name, Class<T> type, T value, String desc) {
		this.name = name
		this.value = value
		this.type = type
		this.desc = desc
	}

	override getName() {
		return name
	}
	
	override T getValue() {
		return value
	}
	
	override void setValue(T value) {
		this.value = value;
	}

	override getDescription() {
		return desc
	}
	
	override Class<T> getType() {
		return type
	}
}