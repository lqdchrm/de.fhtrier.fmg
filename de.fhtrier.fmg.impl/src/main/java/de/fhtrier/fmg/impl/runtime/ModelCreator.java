package de.fhtrier.fmg.impl.runtime;

import java.beans.ExceptionListener;
import java.util.ArrayList;
import java.util.List;

import de.fhtrier.fmg.core.fm.IFeature;
import de.fhtrier.fmg.core.fm.IFeatureModel;
import de.fhtrier.fmg.core.gen.IGenerator;
import de.fhtrier.fmg.core.gen.IGeneratorListener;
import de.fhtrier.fmg.core.gen.IParameterSet;
import de.fhtrier.fmg.core.runtime.IExceptionThrower;
import de.fhtrier.fmg.core.stats.IStatistics;

public class ModelCreator implements Runnable, IGeneratorListener,
		IExceptionThrower {

	private IGenerator				generator;
	private IParameterSet			params;
	private IFeatureModel			model;
	private IStatistics				stats;

	private List<ExceptionListener>	listeners;

	public ModelCreator(IGenerator generator, IParameterSet params) {
		this.generator = generator;
		this.params = params;
		this.model = null;
		this.stats = null;

		this.listeners = new ArrayList<ExceptionListener>();
	}

	@Override
	public void addListener(ExceptionListener l) {
		if (!listeners.contains(l)) {
			listeners.add(l);
		}
	}

	@Override
	public void exceptionThrown(Exception e) {
		for (ExceptionListener l : listeners) {
			l.exceptionThrown(e);
		}
	}

	@Override
	public void featureAdded(IGenerator sender, IFeature feature) {
		// TODO Auto-generated method stub
	}

	public IFeatureModel getModel() {
		return model;
	}

	public IStatistics getStats() {
		return stats;
	}

	@Override
	public void removeListener(ExceptionListener l) {
		if (listeners.contains(l)) {
			listeners.remove(l);
		}
	}

	@Override
	public void run() {
		try {
			generator.addListener(this);
			model = generator.generate(params);
			stats = generator.getStats();
		} catch (Exception e) {
			exceptionThrown(e);
		} finally {
			generator.removeListener(this);
		}
	}

	@Override
	public void updateAlternative(IFeature parent, List<IFeature> children) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMandatory(IFeature parent, IFeature child) {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateOptional(IFeature parent, IFeature child) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateOr(IFeature parent, List<IFeature> children) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateRoot(IFeature root) {
		// TODO Auto-generated method stub

	}
}
