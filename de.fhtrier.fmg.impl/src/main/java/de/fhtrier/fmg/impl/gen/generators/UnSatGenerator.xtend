package de.fhtrier.fmg.impl.gen.generators

import de.fhtrier.fmg.core.fm.CrossTreeConstraintType
import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.RelationType

class UnSatGenerator extends DefaultGenerator {
	
	new(String name) {
		super(name)
	}

	override createCTCs() {
		super.createCTCs
		
		// find two fully mandatory features and create excludes relation
		var f1 = findFullyMandatoryFeature(model.features.size)
		
		if (f1 != null) {
			
			var f2 = findFullyMandatoryFeature(f1.id)
			
			while (f2 != null && !this.areNCPFeatures(f1, f2)) {
				f2 = findFullyMandatoryFeature(f2.id)
			}
			
			if (f2 != null) {
				var type = CrossTreeConstraintType::Excludes
				cache.ctc(f1, type, f2)
			}
		}
	}
	
	def IFeature findFullyMandatoryFeature(int startIndex) {
		for (i: startIndex-1..0) {
			var f = model.getFeature(i)
			
			if (f == null || f.isFullyMandatory()) {
				return f
			}
		}
		return null
	}
	
	def private boolean isFullyMandatory(IFeature f) {
		f.parent == null || (f.parent.type == RelationType::Mandatory && f.parent.origin.isFullyMandatory())	
	}
}