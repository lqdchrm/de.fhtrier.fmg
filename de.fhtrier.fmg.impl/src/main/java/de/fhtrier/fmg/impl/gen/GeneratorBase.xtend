package de.fhtrier.fmg.impl.gen

import de.fhtrier.fmg.core.gen.IGenerator
import de.fhtrier.fmg.core.gen.IGeneratorListener
import de.fhtrier.fmg.core.gen.IParameterSet
import de.fhtrier.fmg.core.fm.CrossTreeConstraintType
import de.fhtrier.fmg.core.fm.IFeature
import de.fhtrier.fmg.core.fm.IFeatureModel
import de.fhtrier.fmg.core.stats.IStatistics
import de.fhtrier.fmg.impl.stats.Statistics
import de.fhtrier.fmg.impl.fm.FeatureModel
import java.util.List
import java.util.Random

abstract class GeneratorBase implements IGenerator {
	
	// internals
	List<IGeneratorListener> listeners
	
	// params
	protected val NUM_FEATURES = "Number of Features"
	protected val FEATURE_PREFIX = "Feature Prefix"
	
	// context
	String name
	protected Cache cache
	protected FeatureModel model
	protected IParameterSet params
	protected Statistics stats
	
	protected Random featureRand
	protected Random ctcRand

	new(String name) {
		this.name = name
		listeners = newArrayList
		cache = null
		stats = new Statistics
	}

	override addListener(IGeneratorListener l) {
		if (!listeners.contains(l))
			listeners.add(l)
	}

	override removeListener(IGeneratorListener l) {
		if (listeners.contains(l))
			listeners.remove(l)
	}

	override IStatistics getStats() {
		
		stats.NumNodes = model.features.size
		stats.Depth = model.features.fold(0, [result, f | Math::max(result, f.collectParents.size)])
		stats.NumRequires = model.crossTreeConstraints.filter([t | t.type == CrossTreeConstraintType::Requires]).size
		stats.NumExcludes = model.crossTreeConstraints.filter([t | t.type == CrossTreeConstraintType::Excludes]).size

		return stats
	}
	
	override createParameterSet(String name, int seed) {
		val pms = new ParameterSet(name, seed)
		
		addParameters(pms)
		
		return pms
	}	
		
	def void addParameters(ParameterSet pms) {
		pms.addParam(FEATURE_PREFIX, "", "common prefix for feature-names")
		pms.addParam(NUM_FEATURES, 50, "total number of features to generate")
	}		
		
	override IFeatureModel generate(IParameterSet params) {
		this.params = params
		
		// create feature model + cache
		this.model = new FeatureModel(0, this.params.name, this.params.getString(FEATURE_PREFIX))
		this.cache = new Cache(this.model)
		
		// init random generator with seed
		this.params.seed = calcSeed(this.params.seed)
		initRandom(this.params.seed)
		
		this.stats.startTime = System::currentTimeMillis
		
		// create feature tree
		createFeatureTree
		
		// create ctcs
		createCTCs
		
		this.stats.stopTime = System::currentTimeMillis
		
		return model
	}

	def protected collectParents(IFeature f) {
		val List<IFeature> parents = newArrayList
		if (f.parent != null) {
			var IFeature cur = f.parent.origin
			while (cur != null) {
				parents.add(cur)
				if (cur.parent != null)
					cur = cur.parent.origin
				else
					cur = null				
			}
		}
		return parents
	}		
	
	def private initRandom(long seed) {
			this.featureRand = new Random(params.seed)
			this.ctcRand = new Random(params.seed)	
	}

	def public static long calcSeed(long inSeed) {
		if (inSeed.equals(new Long(-1))) {
			val rnd = new Random
			return rnd.nextLong
		}
		else		
			return inSeed		
	}
	
	def protected void createFeatureTree() {
		
	}
	
	def protected void createCTCs() {
	}

	override getName() {
		name
	}
}