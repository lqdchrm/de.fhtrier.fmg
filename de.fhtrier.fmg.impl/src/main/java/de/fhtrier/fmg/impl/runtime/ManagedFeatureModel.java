package de.fhtrier.fmg.impl.runtime;

import java.util.ArrayList;
import java.util.List;

import com.mxgraph.layout.mxCompactTreeLayout;
import com.mxgraph.view.mxGraph;

import de.fhtrier.fmg.core.fm.IFeatureModel;
import de.fhtrier.fmg.core.gen.IGenerator;
import de.fhtrier.fmg.core.gen.IParameterSet;
import de.fhtrier.fmg.core.runtime.IManagedFeatureModel;
import de.fhtrier.fmg.core.runtime.IManagedFeatureModelChangedListener;

public class ManagedFeatureModel implements IManagedFeatureModel {

	int											id;
	IFeatureModel								model;
	IGenerator									generator;
	IParameterSet								parameters;
	mxGraph										graph;

	List<IManagedFeatureModelChangedListener>	listeners;

	public ManagedFeatureModel(int id) {
		this.id = id;
		this.model = null;
		this.generator = null;
		this.parameters = null;
		this.graph = null;
		this.listeners = new ArrayList<>();
	}

	@Override
	public int getId() {
		return id;
	}

	private void fireChanged() {
		for (IManagedFeatureModelChangedListener l : listeners) {
			l.onChanged(this);
		}
	}

	@Override
	public IFeatureModel getModel() {
		return model;
	}

	@Override
	public IGenerator getGenerator() {
		return generator;
	}

	@Override
	public void setGenerator(String name) {
		this.model = null;
		this.generator = FrameworkFactory.getCurrent().createGenerator(name);
		this.parameters = null;
		this.graph = null;
		fireChanged();
	}

	@Override
	public void setGenerator(IGenerator generator) {
		this.model = null;
		this.generator = generator;
		this.parameters = null;
		this.graph = null;
		fireChanged();
	}

	@Override
	public IParameterSet getParameters() {
		checkParameters();
		return parameters;
	}

	@Override
	public void setParameters(IParameterSet parameters) {
		this.model = null;
		this.parameters = parameters;
		this.graph = null;
		fireChanged();
	}

	@Override
	public mxGraph getGraph() {
		return graph;
	}

	private void checkParameters() {
		if (generator == null) {
			setGenerator("Default");
		}

		if (generator != null) {
			if (parameters == null) {
				parameters = generator.createParameterSet("Default", -1);
			}
		}
	}

	@Override
	public void regenerate() {
		if (generator != null) {

			checkParameters();

			// create model
			ModelCreator modelCreator = new ModelCreator(generator, parameters);
			modelCreator.run();

			model = modelCreator.getModel();

			// create graph
			graph = new mxGraph();
			GraphCreator graphCreator = new GraphCreator(model, graph);
			graphCreator.run(GraphCreator.CREATEVERTICESANDEDGES);

			// layout graph
			graph.getModel().beginUpdate();

			mxCompactTreeLayout layout = new mxCompactTreeLayout(graph, false);
			layout.setLevelDistance(20);
			layout.setNodeDistance(5);
			layout.execute(graph.getDefaultParent());

			graph.getModel().endUpdate();

			graphCreator.run(GraphCreator.CREATECTCS);

			fireChanged();
		}
	}

	@Override
	public String toString() {

		String _id = String.valueOf(this.id);
		String _name = "Unnamed";
		if (model != null) {
			_name = model.getName();
		}

		return _id + " - " + _name;
	}

	@Override
	public void add(IManagedFeatureModelChangedListener l) {
		if (!listeners.contains(l)) {
			listeners.add(l);
		}
	}

	@Override
	public void remove(IManagedFeatureModelChangedListener l) {
		if (listeners.contains(l)) {
			listeners.remove(l);
		}
	}
}
