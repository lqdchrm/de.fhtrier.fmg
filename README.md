# Feature Model Generator

## How to use

If built correctly, the subfolder `de.fhtrier.fmg/target` contains a Windows x64 executable, called **FMG.exe**.
This executable can be used to start a GUI or parameterized via commandline to be used in batch scripts.

### Use-Cases via GUI

When this executable is double-clicked or run via commandline without extra parameters, you are presented with
a simple GUI for generating feature-models:

![Gui after startup](./de.fhtrier.fmg.docs/img/gui-startup.png)

When you have added a feature-model, you can specify a specific generator and configure the generator's parameters.
![Added feature-model](./de.fhtrier.fmg.docs/img/gui-fm-added.png)

If you then press `Generate` or `Generate Random`, you will see a graphical representation of the generated feature-model in the lower pane. `Generate` will always produce the same feature-model for the same `Seed`-value in the parameters-pane. `Generate Random` will set
a random `Seed`-value, thus creating a new feature-model each time.
![Generated random feature-model](./de.fhtrier.fmg.docs/img/gui-fm-generated.png)

The generated feature-model can be output in various formats. You can inspect it by selecting `Show` from the top menu, which will generate the output in the `Log`-window
![Show output format in Log window](./de.fhtrier.fmg.docs/img/gui-fm-showformat.png)

If you want to save the generated feature-model in a specific format, you can select `Export` from the top menu, which will show a file-save-dialog:
![Show output format in Log window](./de.fhtrier.fmg.docs/img/gui-fm-exportformat.png)

### Use-Cases via Commandline

To use the `FMG.exe` as commandline-tool, you need to specify the following parameters via commandline:

* input file with parameters
* generator type to use
* output format

, e.g. to use the Default generator with the parameter-set from file Default.fmg and output as VControl-format you call:

~~~ {.bash}
$> FMG.exe -i Default.fmg -g Default -f VControl

<?xml version="1.0" encoding="ASCII"?>
<com.prostep.vcontrol.model.common:ImpProject xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:com.prostep.vcontrol.model.common="http://www.prostep.com/vcontrol/model/common.ecore" xmlns:com.prostep.vcontrol.model.feature="http://www.prostep.com/vcontrol/model/feature.ecore" xmlns:xmi="http://www.omg.org/XMI" xmi:id="_1akl8A92EeixWbhgLG2dYw" name="Default exported from Feature Model Generator FH Trier">
  <models xsi:type="com.prostep.vcontrol.model.feature:FeatureModel" xmi:id="_1alNAA92EeixWbhgLG2dYw" name="Default">
...
</models>
</com.prostep.vcontrol.model.common:ImpProject>
~~~

### Use-Cases for integration with other tools

If you plan to integrate `FMG.exe` into your own toolchains, you can query the framework for the available generators and available output formats. Passing the parameter `-j` or `--json` to FMG.exe will print a JSON-description of all generators and all output formats to *stdout*.

~~~ {.bash}
$> FMG.exe -j
~~~

Will produce:

~~~ {.json}
{
  "name" : "Default",
  "params" : [
    {
      "name" : "Name",
      "type" : "string",
      "default" : "Default",
      "description" : "The feature model's name"
    },
    {
      "name" : "Seed",
      "type" : "long",
      "default" : "0",
      "description" : "A seed to deterministically reproduce this exact model"
    },
    {
      "name" : "Feature Prefix",
      "type" : "string",
      "default" : "",
      "description" : "common prefix for feature-names"
    },
    {
      "name" : "Number of Features",
      "type" : "int",
      "default" : "50",
      "description" : "total number of features to generate"
    },
    {
      "name" : "Max Branching Factor",
      "type" : "int",
      "default" : "3",
      "description" : "statistical mean of outgoing relations per feature"
    },
    {
      "name" : "Max Children per Set",
      "type" : "int",
      "default" : "3",
      "description" : "statistical mean of generated children per or-/xor-group"
    },
    {
      "name" : "Number of CTCs",
      "type" : "int",
      "default" : "0",
      "description" : "number of cross-tree-constraints to generate"
    },
    {
      "name" : "Prob. Requires/Excludes",
      "type" : "float",
      "default" : "0.5",
      "description" : "ratio of *Requires* to *Excludes* constraints (from 0.0 to 1.0)"
    }
  ]
},
{
  "name" : "Simple",
  "params" : [
    {
      "name" : "Name",
      "type" : "string",
      "default" : "Default",
      "description" : "The feature model's name"
    },
...
~~~

## Configuring the Generators

This solution comes with some generators out-of-the-box.

### Default Generator

This generator demonstrates some default parameterization for feature-models:

Name                    |  Type  | Default-Value | Description
-----------------------:|:-------|:--------------|:--------------------------------------------------------------
Name                    | string | Default       | The feature model's name
Seed                    | long   | 0             | A seed to deterministically reproduce this exact model
Feature Prefix          | string |               | common prefix for feature-names
Number of Features      | int    | 50            | total number of features to generate
Max Branching Factor    | int    | 3             | statistical mean of outgoing relations per feature
Max Children per Set    | int    | 3             | statistical mean of generated children per or-/xor-group
Number of CTCs          | int    | 0             | number of cross-tree-constraints to generate
Prob. Requires/Excludes | float  | 0.5           |ratio of *Requires* to *Excludes* constraints (from 0.0 to 1.0)

### Other generators

The documentation for other generators can be queries via commandline:

~~~{.bash}
$> FMG.exe -j
~~~

## How to Build

### Prerequisites

* Install JDK >=1.8
* Install Maven >= 3.5
* Ensure JAVA_HOME environment variable points to JDK
* Ensure Maven is accessible in PATH:

~~~{.bash}
$> mvn --version
Apache Maven 3.5.2 (138edd61fd100ec658bfa2d307c43b76940a5d7d; 2017-10-18T09:58:13+02:00)
Maven home: C:\Program Files\Maven\3.5.2\bin\..
Java version: 1.8.0_161, vendor: Oracle Corporation
Java home: C:\Program Files\Java\jdk1.8.0_161\jre
Default locale: de_DE, platform encoding: Cp1252
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
~~~

### Build complete suite via batch file

Run the included batch file `build.bat` from console:

~~~ {.bash}
$> build.bat
~~~

This will build the following artifacts in `de.fhtrier.fmg/target`:

* the executable and main jar file
* the javadoc-documentation in `apidocs`

### Build only executable via maven

Change the working directory to `de.fhtrier.fmg.build` and run maven. This produces a main-jar and a
windows executable in the `target`-Folder of the main project.

~~~ {.bash}
$> cd de.fhtrier.fmg.build
$> mvn verify -Dmaven.repo.local=./.m2

...

$> cd ../de.fhtrier.de/target
$> FMG.exe --help
usage: fmg
 -f,--format <arg>      ( VControl GraphML DFG JSON SXFM XML-Tree )
 -g,--generator <arg>   ( Merge Custom Unsat Default Simple )
 -h,--help              prints this message
 -i,--input <arg>       *.fmg-file with parameter set
 -j,--json              print info as json
 -p,--print-params      print parameter set to stderr
 -x,--gui               launch gui
~~~

### Build via Eclipse (preview)

* Install `Eclipse for Java and DSL Developers` 64-Bit Version:
  * [Download-Link](http://www.eclipse.org/downloads/packages/eclipse-ide-java-and-dsl-developers/oxygen2)
* Startup Eclipse and select a workspace folder
* Import Projects:
  * File/Import -> Maven/Existing Maven Projects -> *Add all de.fhtrier.fmg projects*
  * Confirm any dialogs that pop-up for additional installations
* Wait for Eclipse to compile and update all projects
  * You are probably asked to install additional Eclipse-tooling (m2e)
  * A restart of eclipse might be necessary

## Development Hints

### Xtend
This solution uses the statically-typed language `Xtend`. You can read about here:
[Link to XTend](https://www.eclipse.org/xtend/documentation/index.html)

### Overview of the Package-Structure

![Structure](./de.fhtrier.fmg.docs/img/structure.png)