package de.fhtrier.fmg;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import de.fhtrier.fmg.core.fm.IFeatureModel;
import de.fhtrier.fmg.core.gen.IGenerator;
import de.fhtrier.fmg.core.gen.IParameterSet;
import de.fhtrier.fmg.core.runtime.IFMGProcessor;
import de.fhtrier.fmg.impl.runtime.FrameworkFactory;
import de.fhtrier.fmg.io.fm.IFMWriter;
import de.fhtrier.fmg.io.fm.impl.FMWriterFactory;
import de.fhtrier.fmg.io.info.JSONInfoBuider;
import de.fhtrier.fmg.io.parameters.IParameterSetSerializer;
import de.fhtrier.fmg.io.parameters.impl.ParameterSetSerializer;

public class CmdLineParser {

	public void parseArgs(String[] args) throws ParseException,
			IOException {

		// create options object...
		Options options = new Options();
		createOptions(options);

		// ... and parse its params
		CommandLineParser parser = new PosixParser();
		CommandLine cmdLine = parser.parse(options, args);

		// if we have no arguments show gui
		if (args.length == 0 || cmdLine.hasOption("gui")) {
			runGui();
		} else if (cmdLine.hasOption("help")) {
			showHelp(options);
		} else if (cmdLine.hasOption("json")) {
			printInfoAsJSON(options);
		} else {
			try {
				// run generator
				IGenerator generator = getGenerator(cmdLine);
				InputStream inputFile = getInputFile(cmdLine);
				String outputFormat = getOutputFormat(cmdLine);

				IParameterSet params = writeModel(generator, inputFile,
						outputFormat);

				writeParameterSet(params, cmdLine);

			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
				showHelp(options);
			}
		}
	}

	private void createOptions(Options options) {

		// create list of formats
		StringBuffer formats = new StringBuffer();
		formats.append("( ");
		for (String format : FMWriterFactory.getInstance().getRegisteredTypes()) {
			formats.append(format + " ");
		}
		formats.append(")");

		// create list of generators
		StringBuffer generators = new StringBuffer();
		generators.append("( ");
		for (String generator : FrameworkFactory.getCurrent().getGenerators()) {
			generators.append(generator + " ");
		}
		generators.append(")");

		// register options
		options.addOption("h", "help", false, "prints this message");
		options.addOption("g", "generator", true, generators.toString());
		options.addOption("i", "input", true, "*.fmg-file with parameter set");
		options.addOption("f", "format", true, formats.toString());
		options.addOption("p", "print-params", false,
				"print parameter set to stderr");
		options.addOption("x", "gui", false, "launch gui");
		options.addOption("j", "json", false, "print info as json");
	}

	private void showHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("fmg", options);
	}

	private void printInfoAsJSON(Options options) {
		System.out.println(JSONInfoBuider.info());
	}

	private IGenerator getGenerator(CommandLine cmdLine) {
		String generatorOption = cmdLine.getOptionValue("g", "Default");
		IGenerator generator = FrameworkFactory.getCurrent().createGenerator(
				generatorOption);

		if (generator == null)
			throw new IllegalArgumentException("Unknown generator "
					+ generatorOption);

		return generator;
	}

	private InputStream getInputFile(CommandLine cmdLine) throws FileNotFoundException {
		String inputOption = cmdLine.getOptionValue("i");
		if (inputOption == null)
			return System.in;

		File inputFile = new File(inputOption);
		if (!inputFile.exists())
			throw new IllegalArgumentException("File " + inputOption
					+ " does not exist");

		FileInputStream inputStream = new FileInputStream(inputFile);

		return inputStream;
	}

	private String getOutputFormat(CommandLine cmdLine) {
		String outputFormat = cmdLine.getOptionValue("f", "Default");
		if (!FMWriterFactory.getInstance().containsType(outputFormat))
			throw new IllegalArgumentException("Format " + outputFormat
					+ " is unknown");

		return outputFormat;
	}

	private void runGui() {
		IFMGProcessor gui = new de.fhtrier.fmg.gui.swing.FMGGui();
		gui.run();
	}

	private IParameterSet writeModel(IGenerator generator,
			InputStream inputFile,
			String outputFormat) throws IOException {

		// generator parameter set and load params from file
		IParameterSet params = generator.createParameterSet("undefined", -1);
		IParameterSetSerializer ser = new ParameterSetSerializer();
		ser.loadFromFile(params, new InputStreamReader(inputFile));

		// generator feature model
		IFeatureModel model = generator.generate(params);

		// print model to stdout
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
				System.out));
		IFMWriter w = FMWriterFactory.getInstance()
				.createInstanceOfType(outputFormat, writer);

		w.write(model, params.getName());

		w.close();

		return params;
	}

	private void writeParameterSet(IParameterSet params,
			CommandLine cmdLine) throws IOException {
		if (cmdLine.hasOption("p")) {
			IParameterSetSerializer ser = new ParameterSetSerializer();
			ser.saveToFile(params, new OutputStreamWriter(System.err));
		}
	}
}
