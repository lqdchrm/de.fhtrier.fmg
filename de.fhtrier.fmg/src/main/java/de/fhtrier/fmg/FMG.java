package de.fhtrier.fmg;

import de.fhtrier.fmg.core.runtime.IFrameworkDescriptor;
import de.fhtrier.fmg.impl.runtime.FrameworkDescriptor;
import de.fhtrier.fmg.impl.runtime.FrameworkFactory;
import de.fhtrier.fmg.wrapper.fhtrier.FHTFramework;

public class FMG {

	/**
	 * Launch the application.
	 * 
	 * @param args
	 *            commandline args
	 */
	public static void main(String[] args) {
		try {
			// register and select framework to use
			IFrameworkDescriptor descriptor = new FrameworkDescriptor("FHT",
					FHTFramework.class);
			FrameworkFactory.getInstance().registerType(descriptor);
			FrameworkFactory.setCurrent("FHT");

			// parse commandline arguments
			CmdLineParser parser = new CmdLineParser();
			parser.parseArgs(args);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
